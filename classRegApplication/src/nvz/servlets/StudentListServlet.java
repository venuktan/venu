package nvz.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nvz.services.CourseService;

public class StudentListServlet extends HttpServlet{
	public void doGet(HttpServletRequest request,  HttpServletResponse response) throws ServletException, IOException {		
		
		String courseName;
		PrintWriter out = response.getWriter();		
		
		courseName= request.getParameter("CourseName");
		ArrayList<String> studentList = CourseService.getCourseStudents(courseName);
		int size = studentList.size();
		response.setContentType("text/html");
		
		//out.println("hi..................."+size);
		
		out.println("<html><head><title>List of Students</title>");
		out.println("<body><h1>List of Students:</h1>"); //Shown in the Web Pages
		
		out.println("<h2>Course Name -->> " + courseName + "</h2>");
		
		
		for(int i =0; i < size; i++){
			out.println("<h2>Student: "+ (i+1) +" -->> " + studentList.get(i).toString() + "</h2>");
		}		
		
        out.println("</body></html>");		
	}//doGet Ends

}
