<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Greay Goose Bus Reservation System</title>
<spring:url value="/resources/css/bootstrap.css" var="bootstrap" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type="text/css" />
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.container-narrow {
	margin: 0 auto;
	max-width: 700px;
}

.container-narrow>hr {
	margin: 30px 0;
}

/* Main marketing message and sign up button */
.jumbotron {
	margin: 60px 0;
	text-align: center;
}

.jumbotron h1 {
	font-size: 72px;
	line-height: 1;
}

.jumbotron .btn {
	font-size: 21px;
	padding: 14px 24px;
}

/* Supporting marketing content */
.marketing {
	margin: 60px 0;
}

.marketing p+h4 {
	margin-top: 28px;
}
</style>
</head>
<body>


	<div class="container-narrow">
		<div class="masthead">
			<ul class="nav nav-pills pull-right">
				<li class="active"><a href="/">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
			<h3 class="muted">
				<a href="/"> Bus Reservation System</a>
			</h3>
		</div>
		<hr>
		<hr>
		<div class="jumbotron">
			<h1>Greay Goose Bus Reservation System</h1>

			<p class="lead">
				Online Bus Reservation System<br /> Do you want to reserver a seat
				for your travel?
			</p>
			<a class="btn btn-large btn-success"
				href='<c:url value="reserve/init"/>'>Reserve your seat</a> <br />
			<br />

			<p>
				<small
					style="font-size: 14px; margin-left: 10px; position: relative; top: 2px">
					Its easy! no sign up required. </small>
			</p>
		</div>

		<hr>
		<hr>

		<div class="footer">
			<p align="center">Copyright 2012 | Venu K Tangirala</p>
		</div>

	</div>
</body>
</html>