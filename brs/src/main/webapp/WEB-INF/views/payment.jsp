<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Greay Goose Bus Reservation System</title>
<spring:url value="/resources/css/bootstrap.css" var="bootstrap" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type="text/css" />
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
<script src="<c:url value='/resources/js/jquery-1.8.2.js'/>"></script>
<script src="<c:url value='/resources/js/jquery-ui.js'/>"></script>

<script>
	jQuery(document).ready(function() {

		jQuery("#expirationDate").datepicker({
			changeMonth : true,
			changeYear : true
		});
	});
</script>
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.container-narrow {
	margin: 0 auto;
	max-width: 700px;
}

.container-narrow>hr {
	margin: 30px 0;
}
</style>
</head>
<body>

	<div class="container-narrow">
		<div class="masthead">
			<ul class="nav nav-pills pull-right">
				<li class="active"><a href="/">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
			<h3 class="muted">
				<a href="/"> Bus Reservation System</a>
			</h3>
		</div>
		<hr />


		<form:form method="post" modelAttribute="paymentInfo"
			cssClass="form-horizontal">

			<div class="control-group">
				<form:label path="cardName" cssClass="control-label"> Enter your name as it appears on the card </form:label>
				<div class="controls">
					<form:input path="cardName" />
				</div>
			</div>

			<div class="control-group">
				<form:label path="expirationDate" cssClass="control-label"> Expiration Date </form:label>
				<div class="controls">
					<form:input path="expirationDate" readonly="true"/>
				</div>
			</div>

			<div class="control-group">
				<form:label path="cvv" cssClass="control-label"> CVV on the back of card </form:label>
				<div class="controls">
					<form:input path="cvv" />
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<input type="submit" class="btn btn-success" value="Pay" />
				</div>
			</div>
		</form:form>


		<hr />

		<div class="footer">
			<p align="center">Copyright 2012 | Venu K Tangirala</p>
		</div>
	</div>
</body>
</html>