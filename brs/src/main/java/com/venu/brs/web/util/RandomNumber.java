package com.venu.brs.web.util;

import java.util.Random;

public class RandomNumber {
	public static String gen8DigitRandomNumber() {
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder builder = new StringBuilder();
		builder.append("");
		try {
			Random rand = new Random();
			int i = rand.nextInt(10);
			int j = rand.nextInt(100);
			int k = rand.nextInt(100);
			int l = rand.nextInt(10);
			int chr1 = rand.nextInt(25);
			int chr2 = rand.nextInt(25);
			int chr3 = rand.nextInt(25);
			int chr4 = rand.nextInt(25);

			builder.append(chars.charAt(chr1));
			builder.append(i);
			builder.append(chars.charAt(chr2));
			builder.append(j);
			builder.append(chars.charAt(chr3));
			builder.append(k);
			builder.append(chars.charAt(chr4));
			builder.append(l);
		} catch (Exception e) {
		}
		return builder.toString();
	}
}
