package com.venu.brs.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

import com.venu.brs.domain.Route;

public class ApplicationConversionServiceFactoryBean extends
		FormattingConversionServiceFactoryBean {
	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
		registry.addConverter(routeConverter());
	}

	public static Converter<String, Route> routeConverter() {
		return new Converter<String, Route>() {

			@Override
			public Route convert(String string) {
				Route route = new Route();
				String from = string.substring(0, string.indexOf("-"));
				String to = string.substring(string.indexOf("-") + 1,
						string.length());
				route.setFrom(from);
				route.setTo(to);

				return route;
			}
		};
	}
}
