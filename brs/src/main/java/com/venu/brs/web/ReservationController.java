package com.venu.brs.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.venu.brs.domain.PaymentInfo;
import com.venu.brs.domain.Traveler;
import com.venu.brs.service.RouteService;
import com.venu.brs.service.SeatService;
import com.venu.brs.web.util.RandomNumber;

@Controller
@RequestMapping("/reserve/")
public class ReservationController {
	@Autowired
	private RouteService routeService;
	@Autowired
	private SeatService seatService;

	@RequestMapping(value = "init", method = RequestMethod.GET)
	public String init(Model uiModel) {

		uiModel.addAttribute("routes", routeService.getRoutes());
		uiModel.addAttribute("seats", seatService.getAvaialableSeatList());
		uiModel.addAttribute("traveler", new Traveler());

		return "init";
	}

	@RequestMapping(value = "init", method = RequestMethod.POST)
	public String confimSeat(Traveler traveler, HttpSession session) {
		session.setAttribute("traveler", traveler);
		seatService.removeSeat(traveler.getSeatNumer());
		return "redirect:/reserve/payment";
	}

	@RequestMapping(value = "payment", method = RequestMethod.GET)
	public String payment(Model uiModel) {
		uiModel.addAttribute("paymentInfo", new PaymentInfo());
		return "payment";
	}

	@RequestMapping(value = "payment", method = RequestMethod.POST)
	public String paymentConfirmed(Model uiModel, HttpSession session) {
		Traveler traveler = (Traveler) session.getAttribute("traveler");
		session.removeAttribute("traveler");
		uiModel.addAttribute("traveler", traveler);
		uiModel.addAttribute("confirmationNumber",
				RandomNumber.gen8DigitRandomNumber());
		return "paymentConfirmed";
	}
}
