package com.venu.brs.domain;

import java.util.Date;


public class Traveler {
	
	private int id;
	
	private Route route;
	private String seatNumer;
	private Date dateOfTravel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public String getSeatNumer() {
		return seatNumer;
	}

	public void setSeatNumer(String seatNumer) {
		this.seatNumer = seatNumer;
	}

	public Date getDateOfTravel() {
		return dateOfTravel;
	}

	public void setDateOfTravel(Date dateOfTravel) {
		this.dateOfTravel = dateOfTravel;
	}

}
