package com.venu.brs.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class SeatServiceImpl implements SeatService {

	private static List<String> seats = new ArrayList<String>();

	static {
		seats.add("A1");
		seats.add("A2");
		seats.add("A3");
		seats.add("A4");

		seats.add("B1");
		seats.add("B2");
		seats.add("B3");
		seats.add("B4");

		seats.add("C1");
		seats.add("C2");
		seats.add("C3");
		seats.add("C4");

		seats.add("D1");
		seats.add("D2");
		seats.add("D3");
		seats.add("A4");

		seats.add("E1");
		seats.add("E2");
		seats.add("E3");
		seats.add("E4");

		seats.add("F1");
		seats.add("F2");
		seats.add("F3");
		seats.add("F4");
	}

	@Override
	public List<String> getAvaialableSeatList() {
		return seats;
	}

	@Override
	public void removeSeat(String seat) {
		seats.remove(seat);
	}
}
