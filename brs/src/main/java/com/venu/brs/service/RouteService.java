package com.venu.brs.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.venu.brs.domain.Route;

@Component
public interface RouteService {
	public List<Route> getRoutes();
}
