package com.venu.brs.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface SeatService {
	public List<String> getAvaialableSeatList();
	public void removeSeat(String seat);
}
