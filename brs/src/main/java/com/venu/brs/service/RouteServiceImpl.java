package com.venu.brs.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.venu.brs.domain.Route;

@Service
public class RouteServiceImpl implements RouteService {

	private List<Route> routes;

	public RouteServiceImpl() {
		routes = new ArrayList<Route>();
		makeDummyData(routes);
	}

	@Override
	public List<Route> getRoutes() {
		return routes;
	}

	private void makeDummyData(List<Route> routes) {
		Route route = new Route();
		route.setId(1);
		route.setFrom("SF");
		route.setTo("LA");
		routes.add(route);

		route = new Route();
		route.setId(2);
		route.setFrom("SJ");
		route.setTo("LA");
		routes.add(route);

		route = new Route();
		route.setId(3);
		route.setFrom("SJ");
		route.setTo("Sacramento");
		routes.add(route);

	}
	
	public static void main(String[] args) {
		RouteServiceImpl impl = new RouteServiceImpl();
		System.out.println(impl.getRoutes());
	}

}
