package com.dataStructures.binarySearchTree;

import java.util.ArrayList;

public class FunctionsBST extends BinarySearchTree{
  
  public static void nodeToLeafPaths(Node node, int[] al,int index){
    if (node ==null){return;}
    al[index]=node.getData();
    index++;
    if(node.getLeft()==null && node.getRight()==null){ 
      BinarySearchTree.printArray(al, index);
    }
    if(node.getLeft()!=null){
      nodeToLeafPaths(node.getLeft(),al,index);
    }
    if(node.getRight()!=null){
      nodeToLeafPaths(node.getRight(), al,index);
    }    
  }
  
  public void hasPathSum(Node node, int sum, int[] al,int index){
    if (node ==null){return;}
    al[index]=node.getData();
    sum-=node.getData();
    index++;
    if(node.getLeft()==null && node.getRight()==null && sum==0){ 
      System.out.println("sum exists for root to leaf \n");
      BinarySearchTree.printArray(al, index);
    }
    if(node.getLeft()!=null){
      hasPathSum(node.getLeft(),sum,al,index);
    }
    if(node.getRight()!=null){
      hasPathSum(node.getRight(),sum,al,index);
    }    
  }
  
  public static Node min(Node node) {
    for(;node.getLeft()!=null;)
      node=node.getLeft();
    return node;
  }
  
  public static Node max(Node node) {
    for(;node.getRight()!=null;)
      node=node.getRight();
    return(node); 
  }
  
  public static BinarySearchTree.Node TrueSuccessor(Node node) {
    if (node.getRight()!=null){
      node=node.getRight();
      return (min(node));
    }
    else 
      return node;
  }
  
  
  public static void main ( String[] args){
    BinarySearchTree.root = new Node(100);
    int i=0;
    while (i<args.length){BinarySearchTree.insert(BinarySearchTree.root,Integer.parseInt(args[i++]));}
    
    BinarySearchTree.inorderPrintTree(BinarySearchTree.root);
    System.out.println();
    BinarySearchTree.postorderPrintTree(BinarySearchTree.root);

    System.out.printf("\n max depth = %d",BinarySearchTree.getMaxDepth(BinarySearchTree.root));
    System.out.printf("\n size of BT =%d\n",BinarySearchTree.getSize(BinarySearchTree.root));
    
    System.out.printf("max depth traversal:\n");
    BinarySearchTree.printAL(maxDepthTrav(BinarySearchTree.root));
    
    System.out.printf("all node to leaf traversals\n");
    nodeToLeafPaths(root,new int[100],0);
    
    System.out.printf("\n min: %d \t and max:%d",min(root).getData(),max(root).getData());
    
    System.out.println("\n true Sussessor of "+root.getData()+" is "+TrueSuccessor(root).getData());
    
    FunctionsBST fbt= new FunctionsBST();
    fbt.hasPathSum(root, 666,new int[100] , 0);
  }
  
}
