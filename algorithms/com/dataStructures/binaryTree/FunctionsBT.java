package com.dataStructures.binaryTree;

import java.util.ArrayList;

public class FunctionsBT extends BinaryTree{
  
  public static void nodeToLeafPaths(Node node, int[] al,int index){
    if (node ==null){return;}
    //al.add(index,node); // add current node to al
    al[index]=node.getData();
    index++;
    if(node.getLeft()==null && node.getRight()==null){ 
      //BinaryTree.printAL(al,index);// print the traversal 
      BinaryTree.printArray(al, index);
    }
    if(node.getLeft()!=null){
      nodeToLeafPaths(node.getLeft(),al,index);
    }
    if(node.getRight()!=null){
      nodeToLeafPaths(node.getRight(), al,index);
    }    
  }
  
  public void hasPathSum(Node node, int sum, int[] al,int index){
    if (node ==null){return;}
    al[index]=node.getData();
    sum-=node.getData();
    index++;
    if(node.getLeft()==null && node.getRight()==null && sum==0){ 
      System.out.println("sum exists for root to leaf \n");
      BinaryTree.printArray(al, index);
    }
    if(node.getLeft()!=null){
      hasPathSum(node.getLeft(),sum,al,index);
    }
    if(node.getRight()!=null){
      hasPathSum(node.getRight(),sum,al,index);
    }    
  }
  
  public static Node min(Node node) {
    for(;node.getLeft()!=null;)
      node=node.getLeft();
    return node;
  }
  
  public static Node max(Node node) {
    for(;node.getRight()!=null;)
      node=node.getRight();
    return(node); 
  }
  
  public static BinaryTree.Node TrueSuccessor(Node node) {
    if (node.getRight()!=null){
      node=node.getRight();
      return (min(node));
    }
    else 
      return node;
  }
  
  
  public static void main ( String[] args){
    BinaryTree.root = new Node(100);
    int i=0;
    while (i<args.length){BinaryTree.insert(BinaryTree.root,Integer.parseInt(args[i++]));}
    
    BinaryTree.inorderPrintTree(BinaryTree.root);
    System.out.println();
    BinaryTree.postorderPrintTree(BinaryTree.root);

    System.out.printf("\n max depth = %d",BinaryTree.getMaxDepth(BinaryTree.root));
    System.out.printf("\n size of BT =%d\n",BinaryTree.getSize(BinaryTree.root));
    
    System.out.printf("max depth traversal:\n");
    BinaryTree.printAL(maxDepthTrav(BinaryTree.root));
    
    System.out.printf("all node to leaf traversals\n");
    nodeToLeafPaths(root,new int[100],0);
    
    System.out.printf("\n min: %d \t and max:%d",min(root).getData(),max(root).getData());
    
    System.out.println("\n true Sussessor of "+root.getData()+" is "+TrueSuccessor(root).getData());
    
    FunctionsBT fbt= new FunctionsBT();
    fbt.hasPathSum(root, 666,new int[100] , 0);
  }//end of main
  
}// Functions class end
