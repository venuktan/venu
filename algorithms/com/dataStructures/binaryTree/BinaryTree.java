//Venu K Tangirala
//to run this pass an array on int: eg java BinaryTree 1 23 43 235
package com.dataStructures.binaryTree;
import java.util.*;

class BinaryTree {
  public static Node root;
  ArrayList<Node> al = new ArrayList<Node>();
  public static Node insert(Node node, int  data){
    if(node == null){//for root node and for 
      node = new Node(data);
    }else{
      if(data<=node.getData()){
        node.setLeft( insert(node.getLeft(),data)); 
      }else{ node.setRight( insert(node.getRight(),data)); }
    }

    return node;
  }//end of insert

  static class Node {
    private int data;
    private Node left=null, right=null;
  
    Node(int data){this.data=data; }
  
    public void setData(int data){this.data=data;}
    public void setLeft(Node left){ this.left=left;}
    public void setRight(Node right) { this.right=right;}

    public int getData(){ return data;}
    public Node getLeft() {return left;}
    public Node getRight(){return right;}
  }//end of Node class

  public static void inorderPrintTree(Node node){//inorder printing 
    if (node == null){ return;}
    inorderPrintTree(node.getLeft());
    System.out.print(node.getData()+" ");
    inorderPrintTree(node.getRight());
  }
  
  public static void postorderPrintTree(Node node){
    if (node == null){ return;}
    postorderPrintTree(node.getLeft());
    postorderPrintTree(node.getRight());
    System.out.print(node.getData()+" ");
  }
  
  public static int getMaxDepth(Node node){
    int LDepth=0, RDepth=0;
    if(node.getLeft()!=null){
      LDepth=getMaxDepth(node.getLeft());
    }
    else if (node.getRight()!=null){
      RDepth=getMaxDepth(node.getRight());
    }
    return (Math.max(LDepth,RDepth)+1);
  }

  public static int getSize(Node node){
    if (node == null) {return 0;}
    
    int lDepth = getSize(node.getLeft());
    int rDepth = getSize(node.getRight());

    return (lDepth + 1 +rDepth);
    
  }
  
  public static ArrayList<Node> maxDepthTrav(Node node){
    ArrayList<Node> lNodes = new ArrayList<Node>();
    ArrayList<Node> rNodes = new ArrayList<Node>();

    if (node.getLeft() !=null) { lNodes=maxDepthTrav(node.getLeft()); }
    if (node.getRight()!=null) { rNodes=maxDepthTrav(node.getRight()); }
    if (lNodes.size() >= rNodes.size()){
      lNodes.add(node);
      return lNodes;
    }
    else {
      rNodes.add(node);
      return rNodes;  
    }
  }//end of fn maxDepthTrav
  
  public static void printAL(ArrayList<Node> al){
    for(int i=al.size()-1; i>=0;i--){
      System.out.printf(" %d",al.get(i).getData());
    }
    System.out.println();
  }
  public static void printArray(int[] ints, int len) { 
    int i; 
    for (i=0; i<len; i++) { 
     System.out.print(ints[i] + " "); 
    } 
    System.out.println(); 
    return;
  } 
}

