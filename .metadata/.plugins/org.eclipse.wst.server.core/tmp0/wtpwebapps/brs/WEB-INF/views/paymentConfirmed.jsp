<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<title>Greay Goose Bus Reservation System</title>
<spring:url value="/resources/css/bootstrap.css" var="bootstrap" />
<link rel="stylesheet"
	href="<c:url value='/resources/css/bootstrap.css'/>" type="text/css" />
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.container-narrow {
	margin: 0 auto;
	max-width: 700px;
}

.container-narrow>hr {
	margin: 30px 0;
}
</style>
</head>
<body>


	<div class="container-narrow">
		<div class="masthead">
			<ul class="nav nav-pills pull-right">
				<li class="active"><a href="/">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
			<h3 class="muted">
				<a href="/"> Bus Reservation System</a>
			</h3>
		</div>
		<hr>
		<hr>
		<h3>Confirmation</h3>
		<p>Your trip has been confirmed and our confirmation number is :
			<strong>${ confirmationNumber}</strong></p>
		<p>
			Your seat number is <strong> ${traveler.seatNumer } </strong>and
			you're traveling from <strong>${ traveler.route.from}</strong> to <strong>${traveler.route.to
				}</strong> on <strong>${traveler.dateOfTravel }</strong>
		<hr>
		<hr>

		<div class="footer">
			<p align="center">Copyright 2012 | Venu K Tangirala</p>
		</div>

	</div>
</body>
</html>