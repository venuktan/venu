package com.venu;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet(description = "An XML servelet", urlPatterns = { "/XMLservlet" })
public class XMLservelet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String userName= request.getParameter("userName");		
		HttpSession session = request.getSession();
		ServletContext context = request.getServletContext();
		if (userName!=null && userName != ""){
			session.setAttribute("savedUserName", userName);
			context.setAttribute("savedUserName", userName);
		}
		out.println("Request parameter has username as"+userName );
		out.println("Session parameter has username as "+(String ) session.getAttribute("savedUserName") );
		out.println("Context parameter has username as "+(String ) context.getAttribute("savedUserName") );

	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String userName= request.getParameter("userName");
		String fullName= request.getParameter("fullName");
		out.println("Hello! from POST   \t"+userName +" full name:"+ fullName);
		String prof= request.getParameter("prof");
		out.println("You are a "+ prof);
		//String location= request.getParameter("location");
		String [] location = request.getParameterValues("location");
		out.println("Your location is: "+ location.length+ "places");
		for (int i =0;i<location.length;i++){
			out.println(location[i]);
		}
		
	}
}
