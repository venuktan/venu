package nvz.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nvz.services.CourseService;

public class StudentListServlet extends HttpServlet{
	public void doGet(HttpServletRequest request,  HttpServletResponse response) throws ServletException, IOException {		
		
		String className;
		PrintWriter out = response.getWriter();		
		
		className= request.getParameter("ClassName");
		ArrayList<String> studentList = CourseService.getCourseStudents(className);
		int size = studentList.size();
		response.setContentType("text/html");
				
		out.println("<html><head><title>Students List</title>");
		out.println("<body><h1>StudentsList :</h1>"); 
		
		out.println("<h2>Class Name:" + className + "</h2>");
		
		
		for(int i =0; i < size; i++){
			out.println("<h2>Student: "+ (i+1) +" " + studentList.get(i).toString() + "</h2>");
		}		
		
        out.println("</body></html>");		
	}//doGet Ends

}
