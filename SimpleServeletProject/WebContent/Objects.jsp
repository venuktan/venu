<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Objects</title>
</head>
<body>
<%
String userName = request.getParameter("userName");

if (userName !=null){
session.setAttribute("sessionUserName", userName)	;
application.setAttribute("applicationUserName", userName);
pageContext.setAttribute("pageContextUserName", userName);
pageContext.setAttribute("applicationUserName", userName, PageContext.APPLICATION_SCOPE);
}
%>

The user name is request obj is :<%=userName%>
<br>
The session User Nameis request obj is :<%=session.getAttribute("sessionUserName")%>
<br>
The application User Name is request obj is :<%=application.getAttribute("applicationUserName")%>
<br>
The page Context User Name is request obj is :<%=pageContext.getAttribute("pageContextUserName")%>

</body>
</html>