package com.matrix;
/* Exception for signaling errors in graph construction */

public class BadGraphException extends Exception
{
    public BadGraphException() {
	super("Bad graph construction step");
    }
}