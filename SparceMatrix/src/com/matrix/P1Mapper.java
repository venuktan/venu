package com.matrix;
import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;
/*
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.MapRunner;
import java.util.StringTokenizer;
*/
 /* Phase 1: As read in edges, note whether labeled as from
  matrix A or matrix B.
  Key for A edge is toNode.
  Key for B edge is fromNode.
 */

public class P1Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    public void map(WritableComparable key, Writable values,
		    OutputCollector output, Reporter reporter) throws IOException {
	try {
	    GraphEdge e = new GraphEdge(values.toString());
	    IntWritable k;
	    if (e.tag.equals("A"))
		k = new IntWritable(e.toNode);
	    else
		k = new IntWritable(e.fromNode);
	    output.collect(k, new Text(e.toString()));
	} catch (BadGraphException e) {}
    }

}
/*
public class P1Mapper extends
Mapper<LongWritable, Text, Text, IntWritable> {
private Text word = new Text();
private final static IntWritable one = new IntWritable(1);

protected void map(LongWritable key, Text value, Context context)
    throws IOException, InterruptedException {
String line = value.toString();
StringTokenizer tokenizer = new StringTokenizer(line);
while (tokenizer.hasMoreTokens()) {
    word.set(tokenizer.nextToken());
    context.write(word, one);
}
}
}
*/

