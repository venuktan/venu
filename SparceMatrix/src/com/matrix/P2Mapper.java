package com.matrix;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
//import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

/* Phase 2: Combine edges between each node pair */
/* Map: Generate key/value for each each,
   where key is node pair, value is edge

*/
public class P2Mapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    public void map(WritableComparable key, Writable values,
		    OutputCollector output, Reporter reporter) throws IOException {
	String es = values.toString();
	try {
	    GraphEdge e = new GraphEdge(es);
	    String ks = e.fromNode + " " + e.toNode;
	    output.collect(new Text(ks), new Text(e.toString()));
	} catch (BadGraphException e) {}
    
    }

}
