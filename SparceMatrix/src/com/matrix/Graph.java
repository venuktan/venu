package com.matrix;
import java.lang.Integer;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;


/*
  Representation of a directed graph as a set of edges.
  These are kept in sorted order as a Tree Set.
*/

class Graph {
    /* Baseline graph is a set of edges */
    TreeSet<GraphEdge> edges;

    /* Create empty graph */
    public Graph() {
	edges = new TreeSet();
    }
    
    /* Read graph from file */
    public Graph(File inputFile)
	throws IOException
    {
	edges = new TreeSet();
	BufferedReader reader = new BufferedReader(new FileReader(inputFile));
	String s;
	while ((s = reader.readLine()) != null) {
	    try {
		GraphEdge e = new GraphEdge(s);
		edges.add(e);
	    } catch (BadGraphException e) {}
	}
    }

    /* Add edge to graph */
    public boolean addEdge(GraphEdge e) {
	return edges.add(e);
    }

    /* Enumerate edges in graph */
    public Iterator<GraphEdge> iterator() {
	return edges.iterator();
    }

    /* Set all edge tags */
    public void assignTags(String newTag) {
	Iterator<GraphEdge> es = edges.iterator();
	while (es.hasNext()) {
	    GraphEdge e = es.next();
	    e.assignTag(newTag);
	}
    }

    /* Clear all edge tags */
    public void clearTags() {
	Iterator<GraphEdge> es = edges.iterator();
	while (es.hasNext()) {
	    GraphEdge e = es.next();
	    e.clearTag();
	}
    }

    /* Dump graph to file */
    public void write(File outputFile)
	throws IOException
    {
	BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
	Iterator<GraphEdge> es = edges.iterator();
	while (es.hasNext()) {
	    GraphEdge e = es.next();
	    writer.write(e.toString());
	    writer.newLine();
	}
	writer.close();
    }
}