package com.matrix;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Formatter;

//import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
//import org.apache.hadoop.io.Writable;
//import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.IntWritable;
//import org.apache.hadoop.mapred.FileInputFormat;
//import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
//import org.apache.hadoop.util.GenericOptionsParser;

@SuppressWarnings("deprecation")

public class MatrixMultDriver {

    /* Inputs: in-path-A in-path-B out-dir [mid-root] */
    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
	if (args.length < 3) {
	    System.out.println("Usage: MatrixMult <input A path> <input B path> <output path> [<mid path root>]");
	    System.exit(0);
	}
	String inApath = args[0];
	String inBpath = args[1];
	String outpath = args[2];
	String midroot = "mid";
	if (args.length > 3) 
	    midroot = args[3];
	String pinpath = midroot + "in";
	String pmidpath = midroot + "mid";
	try {
	    File fileA = new File(inApath);
	    Graph A = new Graph(fileA);
	    A.assignTags("A");
	    File dumpA = new File(pinpath, "A");
	    A.write(dumpA);
	} catch (IOException e) {
	    System.out.println("Can't process matrix A:");
	    System.out.println(e.toString());
	    return;
	}
	try {
	    File fileB = new File(inBpath);
	    Graph B = new Graph(fileB);
	    B.assignTags("B");
	    File dumpB = new File(pinpath, "B");
	    B.write(dumpB);
	} catch (IOException e) {
	    System.out.println("Can't process matrix B:");
	    System.out.println(e.toString());
	    return;
	}
	// new try
	Configuration conf= new Configuration();
	GenericOptionsParser parser= new GenericOptionsParser(conf,args);
	args = parser.getRemainingArgs();
	
	Job job1 =new Job(conf,"MatrixMult");
	job1.setJarByClass(P1Mapper.class);
	job1.setJarByClass(P1Reducer.class);
	job1.setOutputKeyClass(Text.class);
	job1.setOutputValueClass(IntWritable.class);
	
	job1.setInputFormatClass(TextInputFormat.class);
    job1.setOutputFormatClass(TextOutputFormat.class);

    
    FileInputFormat.setInputPaths(job1,new Path(pinpath));
    FileOutputFormat.setOutputPath(job1,new Path(pmidpath));
    
    job1.setMapperClass(P1Mapper.class);
	job1.setReducerClass(P1Reducer.class);
	
	//conf.set("mapred.job.tracker", "local");
	
	//job1.waitForCompletion(true);

	/* Phase 1 
	Job conf1 = new Job(new Configuration());
	conf1.setJobName("MatrixMult");
	conf1.setMapOutputKeyClass(IntWritable.class);
	conf1.setMapOutputValueClass(Text.class);
	conf1.setOutputKeyClass(Text.class);
	conf1.setOutputValueClass(Text.class);
	conf1.setInputPath(new Path(pinpath));
	conf1.setOutputPath(new Path(pmidpath));
	
	job1.set("mapred.job.tracker", "local");
	try {
	    JobClient.runJob(job1);
	} catch (Exception e) {
	    e.printStackTrace();
	} */    
	Configuration conf2= new Configuration();

	Job job2 =new Job(conf2,"MatrixMult2");

	job2.setJarByClass(P2Mapper.class);
	job2.setJarByClass(P2Reducer.class);
	
	job2.setOutputKeyClass(Text.class);
	job2.setOutputValueClass(IntWritable.class);
	//job2.waitForCompletion(true);
	System.out.println(job2.waitForCompletion(true));

	job2.setInputFormatClass(TextInputFormat.class);
    job2.setOutputFormatClass(TextOutputFormat.class);
    Formatter formatter = new Formatter();
    String outout = "Out" + formatter.format("%1$tm%1$td%1$tH%1$tM%1$tS", new Date());
    
    FileInputFormat.setInputPaths(job2,new Path(pmidpath));
    FileOutputFormat.setOutputPath(job2,new Path(outout));
    
    job2.setMapperClass(P2Mapper.class);
	job2.setReducerClass(P2Reducer.class);
    }
}