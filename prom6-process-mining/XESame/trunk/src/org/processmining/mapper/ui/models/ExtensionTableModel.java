/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.processmining.mapper.mapping.Extension;

/**
 * Class that shows the extensions in a nice table without 
 * edit possibilities
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class ExtensionTableModel extends AbstractTableModel {
	private String[] columns = new String[] { "URI", "Prefix", "Name" };
	private Vector<Extension> extensions;

	public ExtensionTableModel(Vector<Extension> extensions) {
		updateExtensions(extensions);
	}

	@Override
	public int getColumnCount() {
		return columns.length;
	}

	@Override
	public int getRowCount() {
		return extensions.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Extension extension = extensions.get(rowIndex);

		switch (columnIndex) {
		case 0:
			return extension.getURI();
		case 1:
			return extension.getPrefix();
		case 2:
			return extension.getName();
		default:
			return "";
		}
	}

	public String getColumnName(int col) {
		return columns[col];
	}

	public Class<?> getColumnClass(int col) {
		return getValueAt(0, col).getClass();
	}

	/**
	 * We don't allow editing, edit means remove & add for complexity reasons
	 */
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public void updateExtensions(Vector<Extension> newExtensionList) {
		if (newExtensionList == null)
			extensions = new Vector<Extension>();
		else
			extensions = newExtensionList;
	}

	public void fireTableRowsInserted(int from, int to) {
		// Extra check, if there are 0 extensions to begin with, one of the two
		// is invalid and the table will not be updated so fix that
		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;

		super.fireTableRowsInserted(from, to); // now delegate the call
	}
}
