/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.ui.browsers.ExecutionConsoleBrowser;
import org.processmining.mapper.ui.browsers.ExecutionSettingsBrowser;

import com.fluxicon.slickerbox.components.IconVerticalTabbedPane;

/**
 * The execution browser shows the two tabs which actually 
 * contain the GUI elements
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ExecutionBrowser extends JPanel {

	private static final long serialVersionUID = -1467890361679445697L;
	private static final Color BG = new Color(180, 180, 180);
	private static final Color FG = new Color(60, 60, 60);

	private IconVerticalTabbedPane tabs;
	private final UIController controller;

	private ExecutionSettingsBrowser executionSettingsBrowser;
	private ExecutionConsoleBrowser executionConsoleBrowser;

	public ExecutionConsoleBrowser getExecutionConsoleBrowser() {
		return executionConsoleBrowser;
	}

	private final static String TAB1 = "Settings";
	private final static String TAB2 = "Console";

	public ExecutionBrowser(UIController controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		setOpaque(false);
		setupUI();
	}

	private void setupUI() {
		// setup the tabs
		executionSettingsBrowser = new ExecutionSettingsBrowser(controller);
		executionConsoleBrowser = new ExecutionConsoleBrowser(controller);

		tabs = new IconVerticalTabbedPane(FG, BG, 100);
		tabs.setPassiveBackground(new Color(140, 140, 140));
		tabs.setMouseOverFadeColor(new Color(90, 90, 90));

		tabs.addTab(TAB1, ImageLoader.load("tab_settings_60x60_black.png"),
				executionSettingsBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						executionSettingsBrowser.updateData();
					}
				});

		tabs.addTab(TAB2, ImageLoader.load("tab_console_60x60_black.png"),
				executionConsoleBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						executionConsoleBrowser.updateData();
					}
				});

		this.add(tabs, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		executionSettingsBrowser.updateData();
		executionConsoleBrowser.updateData();
	}
	
	public IconVerticalTabbedPane getTabs()
	{
		return this.tabs;
	}
	
	/**/
	public String getConsoleTabName()
	{
		return ExecutionBrowser.TAB2;
	}/**/
	
	public void switchToConsoleTab()
	{
		this.tabs.selectTab(ExecutionBrowser.TAB2);
	}
}
