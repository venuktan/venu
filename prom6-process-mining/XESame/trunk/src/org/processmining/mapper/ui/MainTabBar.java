/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.BorderFactory;
import javax.swing.JComponent;

import org.deckfour.uitopia.ui.util.ImageLoader;

/**
 * This tabbar shows the 3 main tabs
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MainTabBar extends JComponent {

	private static final long serialVersionUID = 2468367772821953968L;
	private static final int TAB_HEIGHT = 38;
	private static final int TAB_WIDTH = 110;

	private static Color COLOR_TAB_ACTIVE_TOP = new Color(60, 60, 60);
	private static Color COLOR_TAB_ACTIVE_BOTTOM = new Color(20, 20, 20);
	private static Color COLOR_TAB_PASSIVE_TOP = new Color(160, 160, 160);
	private static Color COLOR_TAB_PASSIVE_BOTTOM = new Color(100, 100, 100);
	private static Color COLOR_TAB_MOUSEOVER_TOP = new Color(100, 100, 100);
	private static Color COLOR_TAB_MOUSEOVER_BOTTOM = new Color(60, 60, 60);
	private static Color COLOR_TAB_BORDER = new Color(0, 0, 0, 40);

	private MainView mainView;

	private Image ICONS_ACTIVE[];
	private Image ICONS_PASSIVE[];

	private int indexActive = 0;
	private int indexMouseOver = -1;

	private boolean isEnabled = true;

	public MainTabBar(MainView mainView) {
		this.mainView = mainView;
		// set fixed size
		Dimension dim = new Dimension((TAB_WIDTH * 3) + 1, TAB_HEIGHT);
		this.setMinimumSize(dim);
		this.setMaximumSize(dim);
		this.setPreferredSize(dim);
		this.setSize(dim);
		// set up component
		this.setOpaque(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		// preload icon images
		this.ICONS_ACTIVE = new Image[] {
				ImageLoader.load("toolbar_settings_40x30_white.png"),
				ImageLoader.load("toolbar_mapping_40x30_white.png"),
				ImageLoader.load("toolbar_execute_40x30_white.png") };
		this.ICONS_PASSIVE = new Image[] {
				ImageLoader.load("toolbar_settings_40x30_black.png"),
				ImageLoader.load("toolbar_mapping_40x30_black.png"),
				ImageLoader.load("toolbar_execute_40x30_black.png") };
		// register mouse listeners
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				requestFocus();
				if (e.getButton() == MouseEvent.BUTTON1
						&& MainTabBar.this.isEnabled()) {
					int x = e.getX();
					int active = 2;
					if (x < TAB_WIDTH) {
						active = 0;
					} else if (x < (TAB_WIDTH * 2)) {
						active = 1;
					}
					setActiveTab(active, true);
				}

			}

			public void mouseExited(MouseEvent e) {
				indexMouseOver = -1;
				repaint();
			}
		});
		this.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent e) {
				int x = e.getX();
				int over = 2;
				if (x < TAB_WIDTH) {
					over = 0;
				} else if (x < (TAB_WIDTH * 2)) {
					over = 1;
				}
				if (over != indexMouseOver) {
					indexMouseOver = over;
					repaint();
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.isEnabled = enabled;
		repaint();
	}

	public synchronized void setActiveTab(int index, boolean updateView) {
		if (index != indexActive) {
			indexActive = index;
			if (updateView) {
				if (indexActive == 0) {
					mainView.showSettingsView();
				} else if (indexActive == 1) {
					mainView.showMappingView();
				} else if (indexActive == 2) {
					mainView.showExecutionView();
				}
			}
		}
		repaint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		if (indexActive == 0) {
			paintTab(g2d, 2);
			paintTab(g2d, 1);
			paintTab(g2d, 0);
		} else if (indexActive == 1) {
			paintTab(g2d, 0);
			paintTab(g2d, 2);
			paintTab(g2d, 1);
		} else {
			paintTab(g2d, 0);
			paintTab(g2d, 1);
			paintTab(g2d, 2);
		}
	}

	protected void paintTab(Graphics2D g2d, int index) {
		g2d = (Graphics2D) g2d.create();
		int x = index * TAB_WIDTH - (5 * index);
		int width = TAB_WIDTH + 10;
		Color top, bottom;
		if (index == indexActive && isEnabled) {
			top = COLOR_TAB_ACTIVE_TOP;
			bottom = COLOR_TAB_ACTIVE_BOTTOM;
		} else if (index == indexMouseOver && isEnabled) {
			top = COLOR_TAB_MOUSEOVER_TOP;
			bottom = COLOR_TAB_MOUSEOVER_BOTTOM;
		} else {
			top = COLOR_TAB_PASSIVE_TOP;
			bottom = COLOR_TAB_PASSIVE_BOTTOM;
		}
		GradientPaint gradient = new GradientPaint(0, 0, top, 0, TAB_HEIGHT
				- (TAB_HEIGHT / 3), bottom, false);
		g2d.setPaint(gradient);
		g2d.fillRoundRect(x, 0, width, TAB_HEIGHT + 40, 20, 20);
		g2d.setColor(COLOR_TAB_BORDER);
		g2d.setStroke(new BasicStroke(2f));
		g2d.drawRoundRect(x, 0, width, TAB_HEIGHT + 40, 20, 20);
		g2d.setStroke(new BasicStroke(1f));
		g2d.setColor(new Color(0, 0, 0, 60));
		g2d.drawRoundRect(x, 0, width, TAB_HEIGHT + 40, 20, 20);
		Image icon;
		if (index == indexActive && isEnabled) {
			icon = this.ICONS_ACTIVE[index];
		} else {
			icon = this.ICONS_PASSIVE[index];
			if (index == indexMouseOver && isEnabled) {
				icon = this.ICONS_ACTIVE[index];
				g2d.setComposite(AlphaComposite.getInstance(
						AlphaComposite.SRC_OVER, 0.7f));
			}
		}
		int iconX = x + ((width - icon.getWidth(null)) / 2);
		int iconY = ((TAB_HEIGHT - icon.getHeight(null)) / 2);
		g2d.drawImage(icon, iconX, iconY, null);
		g2d.dispose();
	}

}
