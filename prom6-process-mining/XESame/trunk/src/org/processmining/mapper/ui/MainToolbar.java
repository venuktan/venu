/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */


package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.deckfour.uitopia.ui.util.ArrangementHelper;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.deckfour.uitopia.ui.util.LinkLabel;
import org.processmining.mapper.ui.MainView.View;

/**
 * This class provides the three workspace tabs and 
 * includes the XESame and Fluxicon logos
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MainToolbar extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final int HEIGHT = 45;
	private static final Color COLOR_TOP = new Color(200, 200, 200);
	private static final Color COLOR_BOTTOM = new Color(160, 160, 160);
	private Image appIcon;
	private Image attributionIcon;

	private org.processmining.mapper.ui.MainView mainView;
	private MainTabBar tabBar;

	public MainToolbar(MainView mainView) {
		this.mainView = mainView;
		this.setOpaque(true);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setMinimumSize(new Dimension(HEIGHT, HEIGHT));
		this.setMaximumSize(new Dimension(8000, HEIGHT));
		this.setPreferredSize(new Dimension(4000, HEIGHT));
		this.setup();
	}

	public void setEnabled(boolean enabled) {
		this.tabBar.setEnabled(enabled);
	}

	private void setup() {
		this.tabBar = new MainTabBar(this.mainView);
		this.appIcon = ImageLoader.load("xesame_logo_101x40.png");
		this.attributionIcon = ImageLoader.load("fluxicon_logo_130x40.png");
		this.setLayout(new BorderLayout());
		JLabel logoLabel = new JLabel(new ImageIcon(this.appIcon));
		logoLabel.setOpaque(false);
		logoLabel.setBorder(BorderFactory.createEmptyBorder());
		LinkLabel attributionLabel = new LinkLabel(this.attributionIcon,
				"http://www.fluxicon.com/");

		attributionLabel.setOpaque(false);
		attributionLabel.setBorder(BorderFactory.createEmptyBorder());

		this.add(logoLabel, BorderLayout.WEST);
		this.add(ArrangementHelper.centerHorizontally(ArrangementHelper
				.pushDown(this.tabBar)), BorderLayout.CENTER);
		this.add(attributionLabel, BorderLayout.EAST);
	}

	public void activateTab(View view) {
		if (view.equals(View.SETTINGS)) {
			tabBar.setActiveTab(0, false);
		} else if (view.equals(View.MAPPING)) {
			tabBar.setActiveTab(1, false);
		} else if (view.equals(View.EXECUTION)) {
			tabBar.setActiveTab(2, false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		int width = getWidth();
		int height = getHeight();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		// draw background gradient
		GradientPaint gradient = new GradientPaint(20, height / 3, COLOR_TOP,
				20, height, COLOR_BOTTOM, false);
		g2d.setPaint(gradient);
		g2d.fillRect(0, 0, width, height);
		gradient = new GradientPaint(20, height - 10, new Color(0, 0, 0, 0),
				20, height, new Color(0, 0, 0, 40), false);
		g2d.setPaint(gradient);
		g2d.fillRect(0, height - 10, width, 10);
	}

}
