/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.processmining.mapper.controller.XESameConstants;
import org.processmining.mapper.ui.UIController;
import org.processmining.mapper.ui.models.JTextFieldLimit;

import com.jhlabs.awt.ParagraphLayout;

/**
 * This browser provides the general settings tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class GeneralSettingsBrowser extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2668672328472241725L;

	private final UIController uiController;

	private JPanel settingsPanel = null;
	private JLabel mappingNameLabel = null;
	private JTextField mappingNameTextField = null;
	private JLabel mappingDescriptionLabel = null;
	private JTextArea mappingDescriptionTextArea = null;

	public GeneralSettingsBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(
				uiController
						.setupTopPanel("Provide a general mapping project name and description"),
				BorderLayout.NORTH);
		browser.add(getSettingsPanel(), BorderLayout.CENTER);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		mappingNameTextField.setText(uiController.getMapping().getName());
		mappingDescriptionTextArea.setText(uiController.getMapping()
				.getDescription());
	}

	/**
	 * This method initializes the project settingsPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getSettingsPanel() {
		if (settingsPanel == null) {
			settingsPanel = new JPanel();
			UIController.makeup(settingsPanel);

			settingsPanel.setLayout(new ParagraphLayout());

			mappingDescriptionLabel = new JLabel("Mapping Description");
			mappingDescriptionLabel.setLabelFor(mappingDescriptionTextArea);
			UIController.makeup(mappingDescriptionLabel);

			mappingNameLabel = new JLabel("Mapping Name");
			mappingNameLabel.setLabelFor(mappingNameTextField);
			UIController.makeup(mappingNameLabel);

			settingsPanel.add(mappingNameLabel, ParagraphLayout.NEW_PARAGRAPH);
			settingsPanel.add(getMappingNameTextField());
			settingsPanel.add(mappingDescriptionLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			settingsPanel.add(getMappingDescriptionTextArea());
		}

		return settingsPanel;
	}

	/**
	 * This method initializes mappingNameTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getMappingNameTextField() {
		if (mappingNameTextField == null) {
			mappingNameTextField = new JTextField();
			UIController.makeup(mappingNameTextField);
			mappingNameTextField.setMinimumSize(new Dimension(100, 28));
			mappingNameTextField.setPreferredSize(new Dimension(150, 28));
			mappingNameTextField.setMaximumSize(new Dimension(200, 28));
			mappingNameTextField.setHorizontalAlignment(JTextField.LEFT);
			mappingNameTextField
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			mappingNameTextField.setColumns(50);
			mappingNameTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextAreaMaxLength));

			mappingNameTextField
					.addFocusListener(new java.awt.event.FocusAdapter() {
						// Update the name as soon as the user goes somewhere
						// else
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController.getMapping().setName(
									mappingNameTextField.getText());
						}
					});
		}
		return mappingNameTextField;
	}

	/**
	 * This method initializes mappingDescriptionTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getMappingDescriptionTextArea() {
		if (mappingDescriptionTextArea == null) {
			mappingDescriptionTextArea = new JTextArea();
			UIController.makeup(mappingDescriptionTextArea);
			mappingDescriptionTextArea.setRows(3);
			mappingDescriptionTextArea.setWrapStyleWord(false);
			mappingDescriptionTextArea.setMinimumSize(new Dimension(150, 48));
			// e mappingDescriptionTextArea
			// e .setPreferredSize(new Dimension(200, 100));
			// e mappingDescriptionTextArea.setMaximumSize(new Dimension(300,
			// 300));
			mappingDescriptionTextArea
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			mappingDescriptionTextArea.setLineWrap(true);
			mappingDescriptionTextArea.setColumns(100);
			// mappingDescriptionTextArea.setDocument(new
			// JTextFieldLimit(9999));
			mappingDescriptionTextArea.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextAreaMaxLength));

			mappingDescriptionTextArea
					.addFocusListener(new java.awt.event.FocusAdapter() {
						// Update the description as soon as the user moves away
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController.getMapping().setDescription(
									mappingDescriptionTextArea.getText());
						}
					});
		}
		return mappingDescriptionTextArea;
	}

}
