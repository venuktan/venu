/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.sql.SQLException;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.controller.XESameConstants;
import org.processmining.mapper.ui.UIController;
import org.processmining.mapper.ui.models.ConnectionPropertiesTableModel;
import org.processmining.mapper.ui.models.JTextFieldLimit;

import com.jhlabs.awt.ParagraphLayout;

/**
 * This browser provides the connection settings tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ConnectionSettingsBrowser extends JPanel {

	//TODO add text to indicate that JAR location is optional
	//TODO add button to clear JAR location
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8111058383691281752L;

	private final UIController uiController;

	private JPanel connectionPanel;

	private JLabel connectionDescriptionLabel;
	private JLabel connectionDriverLabel;
	private JLabel connectionDriverLocationLabel;
	private JLabel connectionURLLabel;
	private JLabel connectionColumnSeperatorLabel;

	private JTextField connectionURLTextField;
	private JTextField connectionDriverTextField;
	private JTextField connectionDriverLocationTextField;
	private JTextField connectionColumnSeperatorStartTextField;
	private JTextField connectionColumnSeperatorEndTextField;

	private JTextArea connectionDescriptionTextArea;

	private AbstractButton testConnectionButton;
	private AbstractButton connectionDriverLocationButton;

	private JFileChooser connectionDriverDirChooser;

	private JLabel connectionPropertiesLabel;

	private JTable connectionPropertiesTable;

	public ConnectionSettingsBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(uiController
				.setupTopPanel("Configure the connection to the database"),
				BorderLayout.NORTH);
		browser.add(getConnectionPanel(), BorderLayout.CENTER);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		// Update the connection URL
		getConnectionURLTextField().setText(
				uiController.getMapping().getConnection().getURL());
		// The driver
		getConnectionDriverTextField().setText(
				uiController.getMapping().getConnection().getDriver());
		// The driver location (if set)
		getConnectionDriverLocationTextField().setText(
				uiController.getMapping().getConnection().getDriverLocation());
		// The properties table
		ConnectionPropertiesTableModel model = (ConnectionPropertiesTableModel) getConnectionPropertiesTable()
				.getModel();
		model.updateProperties(uiController.getMapping().getConnection()
				.getProperties());
		// the description
		getConnectionDescriptionTextArea().setText(
				uiController.getMapping().getConnection().getDescription());

		getConnectionColumnSeperatorStartTextField().setText(
				uiController.getMapping().getConnection()
						.getColumnSeperatorStart());
		getConnectionColumnSeperatorEndTextField().setText(
				uiController.getMapping().getConnection()
						.getColumnSeperatorEnd());
	}

	/**
	 * This method initializes connectionPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getConnectionPanel() {
		if (connectionPanel == null) {
			connectionDescriptionLabel = new JLabel();
			connectionDescriptionLabel
					.setText("Connection description (optional)");
			connectionDescriptionLabel
					.setLabelFor(connectionDescriptionTextArea);
			UIController.makeup(connectionDescriptionLabel);

			connectionPropertiesLabel = new JLabel();
			connectionPropertiesLabel.setText("Connection properties");
			connectionPropertiesLabel.setLabelFor(connectionPropertiesTable);
			UIController.makeup(connectionPropertiesLabel);

			connectionDriverLabel = new JLabel();
			connectionDriverLabel.setText("JDBC Driver");
			connectionDriverLabel.setLabelFor(connectionDriverTextField);
			UIController.makeup(connectionDriverLabel);

			connectionDriverLocationLabel = new JLabel();
			connectionDriverLocationLabel.setText("JDBC Driver Location");
			connectionDriverLocationLabel
					.setLabelFor(connectionDriverLocationTextField);
			UIController.makeup(connectionDriverLocationLabel);

			connectionURLLabel = new JLabel();
			connectionURLLabel.setText("URL to database");
			connectionURLLabel.setLabelFor(connectionURLTextField);
			UIController.makeup(connectionURLLabel);

			connectionColumnSeperatorLabel = new JLabel();
			connectionColumnSeperatorLabel
					.setText("SQL Query Column Seperator");
			connectionColumnSeperatorLabel
					.setLabelFor(connectionColumnSeperatorStartTextField);
			UIController.makeup(connectionColumnSeperatorLabel);

			connectionPanel = new JPanel();
			connectionPanel.setLayout(new ParagraphLayout());
			UIController.makeup(connectionPanel);

			// use an empty label to move the button to the 2nd column
			// connectionPanel.add(new JLabel(), ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getTestConnectionButton(),
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(connectionURLLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getConnectionURLTextField());
			connectionPanel.add(connectionDriverLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getConnectionDriverTextField());
			connectionPanel.add(connectionDriverLocationLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getConnectionDriverLocationTextField());
			connectionPanel.add(getConnectionDriverLocationButton(),
					ParagraphLayout.NEW_LINE);

			connectionPanel.add(connectionPropertiesLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			JPanel tablePane = new JPanel();
			tablePane.setLayout(new BorderLayout()); // unless already there
			tablePane.add(getConnectionPropertiesTable(), BorderLayout.CENTER);
			tablePane.add(getConnectionPropertiesTable().getTableHeader(),
					BorderLayout.NORTH);
			connectionPanel.add(tablePane);

			connectionPanel.add(connectionColumnSeperatorLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getConnectionColumnSeperatorStartTextField());
			connectionPanel.add(new JLabel("column|name"));
			connectionPanel.add(getConnectionColumnSeperatorEndTextField());

			connectionPanel.add(connectionDescriptionLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			connectionPanel.add(getConnectionDescriptionTextArea());
		}
		return connectionPanel;
	}

	/**
	 * This method initializes connectionURLTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getConnectionURLTextField() {
		if (connectionURLTextField == null) {
			connectionURLTextField = new JTextField();
			UIController.makeup(connectionURLTextField);
			connectionURLTextField.setMinimumSize(new Dimension(200,
					UIController.sizeTextfieldHight));
			connectionURLTextField.setPreferredSize(new Dimension(200,
					UIController.sizeTextfieldHight));
			connectionURLTextField.setMaximumSize(new Dimension(300,
					UIController.sizeTextfieldHight));

			connectionURLTextField.setHorizontalAlignment(JTextField.LEFT);
			connectionURLTextField
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			connectionURLTextField.setColumns(50);
			connectionURLTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));

			connectionURLTextField
					.addFocusListener(new java.awt.event.FocusAdapter() {
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController.getMapping().getConnection()
									.setURL(connectionURLTextField.getText());
						}
					});
		}
		return connectionURLTextField;
	}

	/**
	 * This method initializes connectionDriverTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getConnectionDriverTextField() {
		if (connectionDriverTextField == null) {
			connectionDriverTextField = new JTextField();
			UIController.makeup(connectionDriverTextField);
			connectionDriverTextField.setMinimumSize(new Dimension(200,
					UIController.sizeTextfieldHight));
			connectionDriverTextField.setPreferredSize(new Dimension(200,
					UIController.sizeTextfieldHight));
			connectionDriverTextField.setMaximumSize(new Dimension(400,
					UIController.sizeTextfieldHight));

			connectionDriverTextField.setHorizontalAlignment(JTextField.LEFT);
			connectionDriverTextField
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			connectionDriverTextField.setColumns(50);
			connectionDriverTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));

			connectionDriverTextField
					.addFocusListener(new java.awt.event.FocusAdapter() {
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController
									.getMapping()
									.getConnection()
									.setDriver(
											connectionDriverTextField.getText());
						}
					});
		}
		return connectionDriverTextField;
	}

	/**
	 * This method initializes the file chooser for selecting the driver jar
	 * file
	 * 
	 * @return
	 */
	private JFileChooser getConnectionDriverDirChooser() {
		if (connectionDriverDirChooser == null) {
			connectionDriverDirChooser = new JFileChooser();
			UIController.makeup(connectionDriverDirChooser);
			// load the driverDir from last time (or current dir if no such
			// thing exists)
			String lastDriverLocation = uiController.getConfiguration().get(
					UIController.SAVE_LOCATION_DRIVER, "./");
			connectionDriverDirChooser.setCurrentDirectory(new File(
					lastDriverLocation));
			// and also update the mapping with this value
			uiController.getMapping().getConnection()
					.setDriverLocation(lastDriverLocation);

			// Only allow selection of directories
			// A file filter so only files ending with 'mapping' can be
			// saved/loaded (although its not automagically added when saving)
			FileFilter filter = new FileNameExtensionFilter("JAR files", "jar");
			connectionDriverDirChooser.addChoosableFileFilter(filter);

			connectionDriverDirChooser
					.setFileSelectionMode(JFileChooser.FILES_ONLY);
		}

		return connectionDriverDirChooser;
	}

	/**
	 * This method initializes driverLocationTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getConnectionDriverLocationTextField() {
		if (connectionDriverLocationTextField == null) {
			connectionDriverLocationTextField = new JTextField();
			UIController.makeup(connectionDriverLocationTextField);
			connectionDriverLocationTextField.setPreferredSize(new Dimension(
					500, 28));
			connectionDriverLocationTextField.setEditable(false);
			
			connectionDriverLocationTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));

		}
		return connectionDriverLocationTextField;
	}

	/**
	 * This method initializes driverLocationButton
	 * 
	 * @return javax.swing.JButton
	 */
	private AbstractButton getConnectionDriverLocationButton() {
		if (connectionDriverLocationButton == null) {
			connectionDriverLocationButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Select .jar driver file");
			connectionDriverLocationButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// Always ask for a file location
							int returnVal = getConnectionDriverDirChooser()
									.showOpenDialog(getConnectionPanel());
							if (returnVal == JFileChooser.APPROVE_OPTION) {
								// Get the location of the driver
								String location = getConnectionDriverDirChooser()
										.getSelectedFile().toString();

								// Update the driver location in the connection
								uiController.getHandler().getMapping()
										.getConnection()
										.setDriverLocation(location);

								// Update the textfield in the GUI
								getConnectionDriverLocationTextField().setText(
										location);

								// And store for persistence
								uiController.getConfiguration().set(
										UIController.SAVE_LOCATION_DRIVER,
										location);
							}
						}
					});
		}

		return connectionDriverLocationButton;
	}

	/**
	 * This method initializes connectionDescriptionTextArea
	 * 
	 * @return javax.swing.JTextArea
	 */
	private JTextArea getConnectionDescriptionTextArea() {
		if (connectionDescriptionTextArea == null) {
			connectionDescriptionTextArea = new JTextArea();
			UIController.makeup(connectionDescriptionTextArea);

			connectionDescriptionTextArea
					.setMinimumSize(new Dimension(150, 48));
			connectionDescriptionTextArea.setPreferredSize(new Dimension(200,
					100));
			connectionDescriptionTextArea
					.setMaximumSize(new Dimension(300, 300));

			connectionDescriptionTextArea.setRows(3);
			connectionDescriptionTextArea.setWrapStyleWord(false);
			connectionDescriptionTextArea.setLineWrap(true);
			connectionDescriptionTextArea.setColumns(100);
			connectionDescriptionTextArea.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextAreaMaxLength));

		}

		connectionDescriptionTextArea
				.addFocusListener(new java.awt.event.FocusAdapter() {
					public void focusLost(java.awt.event.FocusEvent e) {
						uiController
								.getMapping()
								.getConnection()
								.setDescription(
										connectionDescriptionTextArea.getText());
					}
				});

		return connectionDescriptionTextArea;
	}

	/**
	 * This method initializes testConnectionButton
	 * 
	 * @return javax.swing.JButton
	 */
	private AbstractButton getTestConnectionButton() {
		if (testConnectionButton == null) {
			testConnectionButton = new ImageLozengeButton(
					ImageLoader.load("button_testConnection_30x30_black.png"),
					"Test Connection");
			// testConnectionButton.setText("Test Connection");
			// testConnectionButton.setActionCommand("Test Connection");

			testConnectionButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						uiController.getHandler().testConnection(
								uiController.getMapping().getConnection());
						// Notify the user of our success

						JOptionPane
								.showMessageDialog(
										getConnectionPanel(),
										"We have passed the connection test for the selected connection. Congratulations! ",
										"Connection test passed",
										JOptionPane.INFORMATION_MESSAGE);
					}
					// Or we failed and notify the user as detailed as possible
					// (they will understand)
					catch (ClassNotFoundException eClassNotFound) {
						JOptionPane.showMessageDialog(getConnectionPanel(),
								"Sorry, but the test failed because the driver could not be found: \n "
										+ eClassNotFound.getLocalizedMessage(),
								"Connection test failed",
								JOptionPane.ERROR_MESSAGE);
					} catch (SQLException eSQL) {
						JOptionPane.showMessageDialog(getConnectionPanel(),
								"Sorry, but the test failed because the URL could not be found: \n "
										+ eSQL.getLocalizedMessage(),
								"Connection test failed",
								JOptionPane.ERROR_MESSAGE);
					}// catch
					catch (MalformedURLException eMalformedURL) {
						JOptionPane.showMessageDialog(getConnectionPanel(),
								"Sorry, but the test failed because the URL was not correctly formatted: \n "
										+ eMalformedURL.getLocalizedMessage(),
								"Connection test failed",
								JOptionPane.ERROR_MESSAGE);
					} catch (InstantiationException eInstantiation) {
						JOptionPane.showMessageDialog(getConnectionPanel(),
								"Sorry, but the test failed because we could not instantiate the driver: \n "
										+ eInstantiation.getLocalizedMessage(),
								"Connection test failed",
								JOptionPane.ERROR_MESSAGE);
					} catch (IllegalAccessException eIllegalAccess) {
						JOptionPane.showMessageDialog(getConnectionPanel(),
								"Sorry, but the test failed because we could not access the driver: \n "
										+ eIllegalAccess.getLocalizedMessage(),
								"Connection test failed",
								JOptionPane.ERROR_MESSAGE);
					}

				}// actionPerformed

			});// addActionListener
		}
		return testConnectionButton;
	}

	private JTable getConnectionPropertiesTable() {
		if (connectionPropertiesTable == null) {
			connectionPropertiesTable = new JTable(
					new ConnectionPropertiesTableModel(uiController
							.getMapping().getConnection().getProperties()));
			UIController.makeup(connectionPropertiesTable);
		}
		return connectionPropertiesTable;
	}

	private JTextField getConnectionColumnSeperatorStartTextField() {
		if (connectionColumnSeperatorStartTextField == null) {
			connectionColumnSeperatorStartTextField = new JTextField();
			connectionColumnSeperatorStartTextField
					.setMinimumSize(new Dimension(52,
							UIController.sizeTextfieldHight));
			connectionColumnSeperatorStartTextField
					.setPreferredSize(new Dimension(52,
							UIController.sizeTextfieldHight));
			connectionColumnSeperatorStartTextField
					.setMaximumSize(new Dimension(52,
							UIController.sizeTextfieldHight));

			connectionColumnSeperatorStartTextField
					.setHorizontalAlignment(JTextField.LEFT);
			connectionColumnSeperatorStartTextField
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			connectionColumnSeperatorStartTextField.setColumns(2);
			connectionColumnSeperatorStartTextField
					.setDocument(new JTextFieldLimit(2));

			connectionColumnSeperatorStartTextField
					.addFocusListener(new java.awt.event.FocusAdapter() {
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController
									.getMapping()
									.getConnection()
									.setColumnSeperatorStart(
											connectionColumnSeperatorStartTextField
													.getText());
						}
					});

		}
		return connectionColumnSeperatorStartTextField;
	}

	private JTextField getConnectionColumnSeperatorEndTextField() {
		if (connectionColumnSeperatorEndTextField == null) {
			connectionColumnSeperatorEndTextField = new JTextField();
			connectionColumnSeperatorEndTextField.setMinimumSize(new Dimension(
					52, UIController.sizeTextfieldHight));
			connectionColumnSeperatorEndTextField
					.setPreferredSize(new Dimension(52,
							UIController.sizeTextfieldHight));
			connectionColumnSeperatorEndTextField.setMaximumSize(new Dimension(
					52, UIController.sizeTextfieldHight));

			connectionColumnSeperatorEndTextField
					.setHorizontalAlignment(JTextField.LEFT);
			connectionColumnSeperatorEndTextField
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			connectionColumnSeperatorEndTextField.setColumns(2);
			connectionColumnSeperatorEndTextField
					.setDocument(new JTextFieldLimit(2));

			connectionColumnSeperatorEndTextField
					.addFocusListener(new java.awt.event.FocusAdapter() {
						public void focusLost(java.awt.event.FocusEvent e) {
							uiController
									.getMapping()
									.getConnection()
									.setColumnSeperatorEnd(
											connectionColumnSeperatorEndTextField
													.getText());
						}
					});

		}
		return connectionColumnSeperatorEndTextField;
	}
}
