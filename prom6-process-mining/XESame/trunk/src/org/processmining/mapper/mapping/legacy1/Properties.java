/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

/**
 * Generic properties class for general mapping items
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class Properties extends GeneralMappingItem {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String where;

	/**
	 * @return the where
	 */
	public String getWhere() {
		if (where == null)
			where = "";
		return where;
	}

	/**
	 * @param where
	 *            the where to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setWhere(String where) {
		this.where = where;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String from;

	/**
	 * @return the entity
	 */
	public String getFrom() {
		if (from == null)
			from = "";
		return from;
	}

	/**
	 * Returns the base table of the from query (the first word of the from
	 * part)
	 * 
	 * @return table name (not the alias!)
	 */
	public String getFromBaseTable() {
		int i = getFrom().indexOf(" ");
		if (i >= 0) {
			return getFrom().substring(0, i);
		} else {
			return getFrom();
		}
	}

	/**
	 * @param entity
	 *            the entity to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	private String traceID;

	public String getTraceID() {
		if (traceID == null)
			traceID = "";
		return traceID;
	}

	public void setTraceID(String traceID) {
		this.traceID = traceID;
	}

	private String eventOrder;

	/**
	 * @return the SQL part on how to order events
	 */
	public String getEventOrder() {
		if (eventOrder == null)
			eventOrder = "";
		return eventOrder;
	}

	/**
	 * @param eventOrder
	 *            the SQL part on how to order events
	 */
	public void setEventOrder(String eventOrder) {
		this.eventOrder = eventOrder;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Link> links;

	/**
	 * @return the links
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Vector<Link> getLinks() {
		if (links == null)
			links = new Vector<Link>();
		return links;
	}

	/**
	 * @param links
	 *            the links to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setLinks(Vector<Link> links) {
		this.links = links;
	}

	/**
	 * Adds the given links to add to our collection This method is preferred
	 * over the setLink method
	 * 
	 * @param newLink
	 *            the new links to add to our collection
	 */
	public void addLink(Link newLink) {
		if (!getLinks().contains(newLink)) {
			// Add backreference
			newLink.setProperties(this);
			getLinks().add(newLink);
		}
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private GeneralMappingItem generalmappingitem;

	/**
	 * Returns the 'general mapping item' we belong to (NOTE: can return null
	 * but shouldn't)
	 * 
	 * @return the generalmappingitem
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public GeneralMappingItem getGeneralmappingitem() {
		return generalmappingitem;
	}

	/**
	 * @param generalmappingitem
	 *            the generalmappingitem to set as parent
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setGeneralmappingitem(GeneralMappingItem generalmappingitem) {
		this.generalmappingitem = generalmappingitem;
	}

	/**
	 * Returns the string 'Properties of ' + parent.toString()
	 */
	public String toString() {
		return "Properties of " + getGeneralmappingitem().toString();
	}
}