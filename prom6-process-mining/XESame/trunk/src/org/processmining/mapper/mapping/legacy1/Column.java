/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

import org.jgraph.graph.DefaultPort;

/**
 * Contains information about the columns used from the data source
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class Column extends DefaultPort {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Table table;

	public Column(String name) {
		super();
		setName(name);
	}

	/**
	 * @return the table
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Table getTable() {
		// begin-user-code
		return table;
		// end-user-code
	}

	/**
	 * @param table
	 *            the table to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setTable(Table table) {
		// begin-user-code
		this.table = table;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Relation> referredTo;

	/**
	 * @return the referredTo
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Vector<Relation> getReferredTo() {
		// begin-user-code
		return referredTo;
		// end-user-code
	}

	/**
	 * @param referredTo
	 *            the referredTo to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setReferredTo(Vector<Relation> referredTo) {
		// begin-user-code
		this.referredTo = referredTo;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Relation refersTo;

	/**
	 * @return the refersTo
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Relation getRefersTo() {
		// begin-user-code
		return refersTo;
		// end-user-code
	}

	/**
	 * @param refersTo
	 *            the refersTo to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setRefersTo(Relation refersTo) {
		// begin-user-code
		this.refersTo = refersTo;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Boolean isPrimaryKey;

	/**
	 * @return the isPrimaryKey
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Boolean getIsPrimaryKey() {
		if (isPrimaryKey == null)
			isPrimaryKey = false;

		return isPrimaryKey;
	}

	/**
	 * @param isPrimaryKey
	 *            the isPrimaryKey to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setIsPrimaryKey(Boolean isPrimaryKey) {
		// begin-user-code
		this.isPrimaryKey = isPrimaryKey;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null)
			name = "";

		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		if (name == null)
			name = "";

		this.name = name;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Attribute> attributes;

	/**
	 * @return the attributes
	 */
	public Vector<Attribute> getAttributeItems() {
		if (attributes == null)
			attributes = new Vector<Attribute>();

		return attributes;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void setAttributeItems(Vector<Attribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Adds the given attribute to the list of attributes, except when we
	 * already have this attribute in our list. Also makes sure the attribute
	 * has a reference to this column
	 * 
	 * @param attribute
	 */
	public void addAttributeItem(Attribute attribute) {
		if (!getAttributeItems().contains(attribute)) {
			getAttributeItems().add(attribute);
			// Set a back reference
			attribute.addColumn(this);
		}
	}

	/**
	 * Removes the given attribute from our list of attributes we are used in,
	 * also updates the attribute's column reference
	 * 
	 * @param attribute
	 */
	public void removeAttributeItem(Attribute attribute) {
		if (getAttributeItems().contains(attribute)) {
			getAttributeItems().remove(attribute);
			attribute.removeColumn(this);
		}
	}

	public String toString() {
		return name;
	}

}