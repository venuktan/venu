/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.tree.MutableTreeNode;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

/**
 * Data source table instance class
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class Table extends DefaultGraphCell
// extends PortLabelCell
{
	public Table(String name) {
		super(name);
		setName(name);

		GraphConstants.setBorder(this.getAttributes(), BorderFactory
				.createLineBorder(Color.black));
		GraphConstants.setAutoSize(getAttributes(), true);
		GraphConstants.setResize(getAttributes(), true);
		GraphConstants.setOpaque(getAttributes(), true);
		GraphConstants
				.setVerticalAlignment(getAttributes(), SwingConstants.TOP);
		GraphConstants.setEditable(getAttributes(), false);
	}

	/**
	 * Adds the given child to the table If the child is a column the offset is
	 * also calculated so it will be displayed correctly
	 */
	public void add(MutableTreeNode newChild) {
		super.add(newChild);
		if (newChild instanceof Column) {
			Column column = (Column) newChild;
			addColumn(column);

			// Calculate display offset
			GraphConstants.setOffset(column.getAttributes(),
					new Point2D.Double(0, (GraphConstants.PERMILLE / (columns
							.size() + 1))
							* (columns.indexOf(column) + 1)));
		}
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Column> columns;

	/**
	 * @return the columns
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Vector<Column> getColumns() {
		if (columns == null)
			columns = new Vector<Column>();

		return columns;
	}

	/**
	 * Returns (and possibly creates) the field with the given name
	 * 
	 * @param name
	 *            The name of the field to search/create
	 * @return Column
	 */
	public Column getColumn(String name) {
		for (Column column : getColumns()) {
			if (column.getName().equalsIgnoreCase(name))
				return column;
		}

		Column column = new Column(name);
		addColumn(column);

		return column;
	}

	public void addColumn(Column column) {
		Vector<Column> columns = getColumns();
		if (!columns.contains(column)) {
			columns.add(column);
			column.setTable(this);
			super.add(column);
			// this.addLeftPort(1, column, nestedMap, parentMap);
			column.setParent(this);
		}
	}

	/**
	 * @param columns
	 *            the columns to set
	 */
	@SuppressWarnings("unchecked")
	public void setColumns(Vector<Column> columns) {
		this.columns = columns;
		this.getChildren().addAll(columns);
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Connection connection;

	/**
	 * @return the connection
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Connection getConnection() {
		// begin-user-code
		return connection;
		// end-user-code
	}

	/**
	 * @param connection
	 *            the connection to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setConnection(Connection connection) {
		// begin-user-code
		this.connection = connection;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null)
			name = "";

		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setName(String name) {
		// begin-user-code
		this.name = name;
		// end-user-code
	}

	public String toString() {
		// return "Table: " + name;
		return name;
	}
}