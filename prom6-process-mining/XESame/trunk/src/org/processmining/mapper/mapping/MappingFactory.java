/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.Vector;
import java.util.AbstractMap.SimpleEntry;

/**
 * Mapping Factory for easily and correctly creating new instances of mapping
 * objects
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MappingFactory {
	/************************************************************************
	 * The newXXX methods create new instances of mapping objects with some *
	 * general properties set for convenience/correctness *
	 ************************************************************************/

	/**
	 * Initializes a new, empty mapping
	 * 
	 * @return Mapping the new mapping
	 */
	public static Mapping newMapping() {
		return new Mapping();
	}

	/**
	 * Creates a new connection with some values for a user to start from
	 * 
	 * @return Connection
	 */
	public static Connection newConnection() {
		Connection connection = new Connection();

		// Add some values as a base to start from
		connection.setDriver("sun.jdbc.odbc.JdbcOdbcDriver");
		connection.setURL("jdbc:odbc:NAME");
		connection.getProperties().add(
				new SimpleEntry<String, String>("username", ""));
		connection.getProperties().add(
				new SimpleEntry<String, String>("password", ""));
		// and an empty one to let the user fill it in
		connection.getProperties().add(new SimpleEntry<String, String>("", ""));
		connection
				.setDescription("DEFAULT connection to an ODBC source, please change to your situation");
		return connection;
	}

	/**
	 * Creates a new extension, nothing special
	 * 
	 * @return Extension
	 */
	public static Extension newExtension() {
		return new Extension();
	}

	/**
	 * Creates a log with all the attributes defined by the currently loaded
	 * extensions
	 * 
	 * @return Log
	 */
	public static Log newLog() {
		return new Log();
	}

	/**
	 * Creates a new trace with all the attributes defined by the currently
	 * loaded extensions
	 * 
	 * @return Trace
	 */
	public static Trace newTrace() {
		return new Trace();
	}

	/**
	 * Creates a new event with a generated display name
	 * 
	 * @return Event
	 */
	public static Event newEvent() {
		return newEvent("Generated Event");
	}

	/**
	 * Creates a new event with the given display name and with all the
	 * attributes defined by the currently loaded extensions
	 * 
	 * @param String
	 *            displayName the name to display in the GUI
	 * @return Event
	 */
	public static Event newEvent(String displayName) {
		Event event = new Event();
		event.setDisplayName(displayName);

		return event;
	}

	/**
	 * Creates a new attribute with a child/sibling/sub-attribute
	 * 
	 * @return
	 */
	public static Attribute newMetaAttribute() {
		Attribute metaAttribute = new Attribute();
		metaAttribute.setKey("NEW META");
		metaAttribute.setType("String"); // default type is string
		Attribute childAttribute = new Attribute();
		childAttribute.setKey("META child");
		metaAttribute.addItemAttribute(childAttribute);
		return metaAttribute;
	}

	/**
	 * Creates your basic attribute with a temporary key value
	 * 
	 * @return
	 */
	public static Attribute newLeafAttribute() {
		Attribute attribute = new Attribute();
		attribute.setKey("New_Attribute");
		attribute.setType("String"); // default type is string
		return attribute;
	}

	/**
	 * Produces a new Link, nothing special
	 * 
	 * @return Link
	 */
	public static Link newLink() {
		return new Link();
	}

	/**
	 * Produces a new classifier with a dummy name and keys
	 * 
	 * @return Classifier
	 */
	public static Classifier newClassifier() {
		Classifier classifier = new Classifier();
		classifier.setName("Classifier");
		classifier.setKeys("key1 key2");
		return classifier;
	}

	/**
	 * Connects the attributes that are already converted and added to the
	 * mapping items to this extension. (New attributes will be connected in the
	 * parseAttribute function)
	 * 
	 * @param extension
	 * @return
	 */
	public static Vector<Attribute> getAttributesForExtension(
			Extension extension, GeneralMappingItem mappingItem) {
		Vector<Attribute> attributes = new Vector<Attribute>();

		//We might have been given a null item...
		if (mappingItem == null) {
			return attributes;
		}

		for (Attribute attribute : mappingItem.getItemAttributes()) {
			Extension attributeExtension = attribute.getExtension();

			if (attributeExtension != null
					&& attributeExtension.getPrefix() == extension.getPrefix())
				attributes.add(attribute);

			// Now call ourselves on children...
			for (GeneralMappingItem item : getChildItems(mappingItem))
				attributes.addAll(getAttributesForExtension(extension, item));
		}

		return attributes;
	}

	/**
	 * Returns a list of all the attributes and/or other general mapping items
	 * that are children of the given item
	 * 
	 * @param item
	 * @return
	 */
	public static Vector<GeneralMappingItem> getChildItems(
			GeneralMappingItem item) {
		Vector<GeneralMappingItem> items = new Vector<GeneralMappingItem>();

		// Add trace for log
		if (item instanceof Log) {
			Log log = (Log) item;
			items.add(log.getTrace());
		}
		// Or events for trace
		if (item instanceof Trace) {
			Trace trace = (Trace) item;
			items.addAll(trace.getEvents());
		}

		// And of course all the attributes!
		items.addAll(item.getItemAttributes());

		return items;
	}

}