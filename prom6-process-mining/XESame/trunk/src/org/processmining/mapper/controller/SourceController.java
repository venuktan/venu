/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.mapping.Connection;

/**
 * Source connection controller class
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class SourceController {
	// We store our connection to the data source
	private java.sql.Connection connection;
	private MappingController controller;

	public SourceController(MappingController controller) {
		this.controller = controller;
	}

	/**
	 * Tries to connect to the given connection, throws exception on any
	 * failure. Please note that the connection is not stored!
	 * 
	 * @param connection
	 *            the connection to test
	 * @throws ClassNotFoundException
	 *             if the driver can not be found
	 * @throws SQLException
	 *             if no connection can be made
	 * @throws MalformedURLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void testConnection(Connection connection)
			throws ClassNotFoundException, SQLException, MalformedURLException,
			InstantiationException, IllegalAccessException {

		// Initialize the connection driver
		Driver driver = null;

		// Load the driver from our classpath or from an external jar file
		try {
			driver = getDriver(connection);
		} catch (ClassNotFoundException e) {
			throw e;
		} catch (MalformedURLException e) {
			throw e;
		} catch (InstantiationException e) {
			throw e;
		} catch (IllegalAccessException e) {
			throw e;
		}

		// prepare some properties
		Properties properties = buildProperties(connection);

		java.sql.Connection con = driver.connect(connection.getURL(),
				properties);
		con.close();// and close the connection
	}

	/**
	 * Returns a set of strings with all the table names of the data source
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Set<String> tablesInSource() throws SQLException {
		// A set of all the tables that are present in the database
		HashSet<String> databaseTables = new HashSet<String>();

		// Extract all the tables in our data source
		DatabaseMetaData databaseMeta;
		try {
			databaseMeta = connection.getMetaData();
			ResultSet tablesRes = databaseMeta.getTables(null, null, null,
					new String[] { "TABLE" });
			while (tablesRes.next()) {
				databaseTables.add(tablesRes.getString("TABLE_NAME"));
			}
		} catch (SQLException e) {
			controller.message(MESSAGELEVEL.ERROR,
					"We could not get a list of tables in the data source. ");
			throw e;
		}

		return databaseTables;
	}

	/**
	 * Returns a set of column names present in the given table
	 * 
	 * @param table
	 *            to search
	 * @return set of column names
	 * @throws SQLException
	 */
	public HashSet<String> getAllColumnsForTable(String table)
			throws SQLException {
		HashSet<String> columns = new HashSet<String>();

		// Get all the 'normal' tables
		ResultSet columnRes;
		try {
			DatabaseMetaData tableMetaData = connection.getMetaData();
			columnRes = tableMetaData.getColumns(null, null, table, null);

			// Now loop through all the tables in this schema
			while (columnRes.next()) {
				columns.add(columnRes.getString("COLUMN_NAME"));
			}

		} catch (SQLException e) {
			controller.message(MESSAGELEVEL.ERROR,
					"There was an error while we tried to retrieve the columns for the table "
							+ table + ". We encountered the following error: "
							+ e.getLocalizedMessage());
			throw e;

		}

		return columns;
	}

	/**
	 * Parses connection present in the mapping, tries to connect to it and
	 * stores the connection instance
	 * 
	 * @param connection
	 *            The connection to try to parse and connect to
	 * @param suppresErrorMessages
	 *            whether error MESSAGES should be suppressed (errors are always
	 *            thrown)
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws MalformedURLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public void parseConnection(Connection connection,
			boolean suppresErrorMessages) throws ClassNotFoundException,
			SQLException, MalformedURLException, InstantiationException,
			IllegalAccessException {
		/*
		 * We will (re-)connect every time this is asked because you never know
		 * if the connection still exists etc.
		 */

		// Initialize the connection driver
		Driver driver = null;

		// Load the driver from our classpath or from an external jar file
		try {
			driver = getDriver(connection);
		} catch (ClassNotFoundException e) {
			if (!suppresErrorMessages)
				throw e;
		} catch (MalformedURLException e) {
			if (!suppresErrorMessages)
				throw e;
		} catch (InstantiationException e) {
			if (!suppresErrorMessages)
				throw e;
		} catch (IllegalAccessException e) {
			if (!suppresErrorMessages)
				throw e;
		}

		// And then to connect to the DB using the driver
		try {
			// prepare some properties
			Properties prop = buildProperties(connection);

			// and connect (this is the big thing of this function...)
			this.connection = driver.connect(connection.getURL(), prop);
		} catch (SQLException e) {
			if (!suppresErrorMessages)
				controller.message(MESSAGELEVEL.ERROR,
						"We could not connect to the data source, we encountered the following error: "
								+ e.getLocalizedMessage());
			throw e;
		}
	}

	/**
	 * Parses the driver string. There are 2 possibilities: 1) the driver is of
	 * the form 'com.bla.bla.Driver' then it will just load the class. Option 2)
	 * is that it points to a jar file (e.g. 'path/to/jarfile.jar') and then it
	 * will load the org.jdbc.Driver class from that jar file
	 * 
	 * @param driverString
	 *            driver name or .jar file location
	 * @return Driver driver instance
	 * @throws ClassNotFoundException
	 * @throws MalformedURLException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private java.sql.Driver getDriver(Connection connection)
			throws ClassNotFoundException, MalformedURLException,
			InstantiationException, IllegalAccessException {
		Class<?> driverClass = null;

		/*
		 * If the driver location field has not been set then we just assume the
		 * driver is in our classpath.
		 */
		if (Utils.isEmptyString(connection.getDriverLocation())) {

			// Try to load the specified driver
			try {
				driverClass = Class.forName(connection.getDriver());
			} catch (ClassNotFoundException e) {
				controller
						.message(
								MESSAGELEVEL.ERROR,
								"We could not find the JDBC driver class you specified. Are you sure that "
										+ "the driver is located in the classpath? The exact error we got is: "
										+ e.getLocalizedMessage());
				throw (e);
			}
		}
		/*
		 * The other option is that it is a file location, ending with .jar
		 */
		else {
			File jarFile = new File(connection.getDriverLocation());
			URL url = null;
			try {
				url = jarFile.toURI().toURL();
			} catch (MalformedURLException e) {
				controller
						.message(
								MESSAGELEVEL.ERROR,
								"The specified URL does not have a valid format. Please check for typing errors. The exact error message is: "
										+ e.getLocalizedMessage());
				throw (e);
			}
			DriversClassLoader loader = new DriversClassLoader(
					new URL[] { url });

			// Load the class for the JDBC driver
			driverClass = loader.findClass(connection.getDriver());
		}

		// Instantiate it
		Object driverObject = null;
		try {
			driverObject = driverClass.newInstance();
		} catch (InstantiationException e) {
			controller.message(MESSAGELEVEL.ERROR,
					"We could not instantiate the driver class. The exact error message is: "
							+ e.getLocalizedMessage());
			throw (e);
		} catch (IllegalAccessException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We could not access the driver class (probably some rights are wrong???). The exact error message is: "
									+ e.getLocalizedMessage());
			throw (e);
		}

		// Then create a Driver object
		Driver driver = (Driver) driverObject;

		return driver;
	}

	public ResultSet runQuery(String query, int maxRows) throws SQLException {
		/*
		 * Anything that can help improve the execution of a query is welcome...
		 */
		Statement stmt = connection.createStatement(
		/*
		 * We must set it to forward only otherwise some queries will result in
		 * empty resultsets
		 */
		// ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
				ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);

		try {
			stmt.setMaxRows(maxRows);
		} catch (Exception e) {
		}

		return stmt.executeQuery(query);
	}

	@SuppressWarnings("unchecked")
	private Properties buildProperties(Connection connection) {
		Properties prop = new Properties();
		for (Iterator<?> iterator = connection.getProperties().iterator(); iterator
				.hasNext();) {
			SimpleEntry<String, String> entry = (SimpleEntry<String, String>) iterator
					.next();
			prop.put(entry.getKey(), entry.getValue());
		}
		return prop;
	}

	private class DriversClassLoader extends URLClassLoader {
		public DriversClassLoader(URL[] urlList) {
			super(urlList);
		}

		public Class<?> findClass(String className)
				throws ClassNotFoundException {
			return super.findClass(className);
		}

	}

	/**
	 * Close the connection to the cache DB
	 * 
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if (connection != null) {
			connection.close();
		}
	}

}
