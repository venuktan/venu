/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.zip.GZIPOutputStream;

import javax.xml.parsers.ParserConfigurationException;

import org.deckfour.xes.classification.XEventAttributeClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.extension.XExtensionParser;
import org.deckfour.xes.extension.std.XTimeExtension;
import org.deckfour.xes.factory.XFactory;
import org.deckfour.xes.factory.XFactoryRegistry;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.buffered.XAttributeMapBufferedImpl;
import org.deckfour.xes.model.buffered.XTraceBufferedImpl;
import org.deckfour.xes.model.impl.XAttributeBooleanImpl;
import org.deckfour.xes.model.impl.XAttributeContinuousImpl;
import org.deckfour.xes.model.impl.XAttributeDiscreteImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;
import org.deckfour.xes.out.XMxmlGZIPSerializer;
import org.deckfour.xes.out.XMxmlSerializer;
import org.deckfour.xes.out.XSerializer;
import org.deckfour.xes.out.XesXmlGZIPSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.deckfour.xes.util.XTimer;
import org.pojava.datetime.DateTime;
import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.mapping.Attribute;
import org.processmining.mapper.mapping.Classifier;
import org.processmining.mapper.mapping.Event;
import org.processmining.mapper.mapping.Extension;
import org.processmining.mapper.mapping.GeneralMappingItem;
import org.processmining.mapper.mapping.Link;
import org.processmining.mapper.mapping.Log;
import org.processmining.mapper.mapping.Mapping;
import org.processmining.mapper.mapping.Trace;
import org.xml.sax.SAXException;

/**
 * This class can be run in a separate thread and executes the mapping
 * definition and produces an event log
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 */
public class ExecutionController implements Runnable {
	// Volatile attributes to record internal states
	private volatile boolean bCancel = false;
	private volatile boolean bError = false;

	// A list of the trace IDs we want to include in our event log
	private Set<String> traceIDs;
	private MappingController mappingController;
	private Mapping mapping;
	private CacheDBController cacheController;
	private SourceController sourceController;

	// attributes to remember
	private String XESfilename;
	private int nrTraces;
	private boolean mxml;
	private boolean zip;

	/**
	 * Creates a new instance of the execution controller
	 * 
	 * @param XESfilename
	 *            location and name of the output file
	 * @throws Exception
	 */
	public ExecutionController(MappingController mappingController,
			String XESfilename) throws Exception {
		this(mappingController, XESfilename, -1);
	}

	/**
	 * Actually executes the mapping: connects to the data source(s), fills the
	 * cache database and generates the XES event log (zipped)
	 * 
	 * @param nrTraces
	 *            the number of traces to process. -1 means all
	 * @param XESfilename
	 *            location and name of the output file
	 * @throws Exception
	 */
	public ExecutionController(MappingController mappingController,
			String XESfilename, int nrTraces) throws Exception {
		this(mappingController, XESfilename, nrTraces, false);
	}

	/**
	 * Actually executes the mapping: connects to the data source(s), fills the
	 * cache database and generates the XES event log (zipped)
	 * 
	 * @param XESfilename
	 *            location and name of the output file
	 * @param nrTraces
	 *            the number of traces to process. -1 means all
	 * @param mxml
	 *            if true, an MXML file will be generated
	 * @throws Exception
	 */
	public ExecutionController(MappingController mappingController,
			String XESfilename, int nrTraces, boolean mxml) throws Exception {
		this(mappingController, XESfilename, nrTraces, mxml, false);
	}

	/**
	 * Actually executes the mapping: connects to the data source(s), fills the
	 * cache database and generates the XES event log (zipped)
	 * 
	 * @param XESfilename
	 *            location and name of the output file
	 * @param nrTraces
	 *            the number of traces to process. -1 means all
	 * @param mxml
	 *            if true, an MXML file will be generated
	 * @param zip
	 *            if true, the event log will be compressed in a zip file
	 * @throws Exception
	 */
	public ExecutionController(MappingController mappingController,
			String XESfilename, int nrTraces, boolean mxml, boolean zip)
			throws Exception {
		this.mappingController = mappingController;
		this.mapping = mappingController.getMapping();

		sourceController = new SourceController(mappingController);
		cacheController = new CacheDBController(mappingController);

		this.XESfilename = XESfilename;
		this.nrTraces = nrTraces;
		this.mxml = mxml;
		this.zip = zip;
	}

	public void run() {
		/*
		 * This function calls certain sub-functions and processes a large part
		 * itself in order to execute the mapping entirely
		 */

		// Lets time our duration
		XTimer mappingTimer = new XTimer();
		mappingTimer.start();

		int currentExecutionStep = 1;

		// Check if we are canceled already...
		if (bCancel) {
			shutdownSafely("Just after startup.");
			return;
		}

		/*
		 * 0. Prepare the stage:
		 */
		mappingController
				.updateProgress(currentExecutionStep, "initialization");
		// a) Connect to the data sources and store the connection
		// NOTE: only 1 connection is supported!
		try {
			sourceController.parseConnection(mapping.getConnection(), false);
		} catch (MalformedURLException e1) {
			reportError("The given connection URL is not correct.", e1);
		} catch (ClassNotFoundException e1) {
			reportError("The JDBC driver class can not be found.", e1);
		} catch (SQLException e1) {
			reportError(
					"There is an error in our SQL syntax to connect to the data source. Please contact the XESame developer.",
					e1);
		} catch (InstantiationException e1) {
			reportError(
					"There was an error trying to instantiate the connection to the data source.",
					e1);
		} catch (IllegalAccessException e1) {
			reportError(
					"There is an illegal access exception when trying to connect to the data source",
					e1);
		}
		// b) Create a new cache DB
		try {
			cacheController.initializeCacheDB();
		} catch (ClassNotFoundException e1) {
			reportError(
					"The cache class can not be found. Please contact the XESame developer.",
					e1);
		} catch (SQLException e1) {
			reportError(
					"There was an error in our SQL when trying to initialize the cache database.",
					e1);
		}
		// c) Oh yeah, reset the global trace IDs list of traces to include
		traceIDs = new HashSet<String>();

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("Just after initialization.");
			return;
		}

		// Lets put the log, trace and events in one list
		// (they require the same parsing sequence with minor details)
		Vector<GeneralMappingItem> items = new Vector<GeneralMappingItem>();
		items.add(mapping.getLog());
		items.add(mapping.getLog().getTrace());
		items.addAll(mapping.getLog().getTrace().getEvents());
		// Keep track of our last known inserted XAttribute record IDs to reduce
		// the #queries required
		Vector<String> lastXAttributeIDs = new Vector<String>();

		currentExecutionStep++;

		// Now for each log, trace and event instance:
		for (GeneralMappingItem item : items) {
			// Update the progressbar
			if (item instanceof Log) {
				// just starting
				mappingController.updateProgress(currentExecutionStep,
						"Extracting log info");
			} else if (item instanceof Trace) {
				// done: processing Log
				// to do: processing trace and event, convert cacheDB to event
				// log
				mappingController.updateProgress(currentExecutionStep,
						"Extracting traces");
			} else if (item instanceof Event) {
				// done: processing Log and trace
				// to do: processing event, convert cacheDB to event
				// log
				Event event = (Event) item;
				mappingController.updateProgress(currentExecutionStep,
						"Extracting " + event.toString());
			}

			// Check if we are canceled...
			if (bCancel) {
				shutdownSafely("Just before processing " + item.toString());
				return;
			}

			/*
			 * 1. Build the query we are going to run for this item
			 */
			String query = buildQuery(item);
			mappingController.message(MESSAGELEVEL.DEBUG,
					"We are about to run the following query to extract the items for "
							+ item + ": \n" + query);

			// Check if we are canceled...
			if (bCancel) {
				shutdownSafely("Just after building the query on the source data.");
				break;
			}

			if (query != "") {
				/*
				 * 2. Run the query for this item
				 */
				int maxRows = 0;
				// For the log, we only need 1 row for the data
				if (item instanceof Log)
					maxRows = 1;

				// Stop here if we did parse the traces but did not add a
				// single one to our list
				// The query will fail for the 'IN ()' part anyway
				if (item instanceof Event && traceIDs.size() == 0) {
					mappingController
							.message(
									MESSAGELEVEL.WARNING,
									"We did not find any maching traces so we won't try to "
											+ "extract any events. Your event log will be rather empty...");
					break;
				}

				ResultSet resultSet = null;
				try {
					resultSet = sourceController.runQuery(query, maxRows);
				} catch (SQLException e) {
					reportError(
							"There is an error in the query (turn on 'debug' mode to see it) "
									+ "to fetch the information for "
									+ item
									+ ". Please make sure that the query syntax is valid for the source database.",
							e);
				}

				// Check if we are canceled...
				if (bCancel) {
					shutdownSafely("Just after running the query.");
					return;
				}

				/*
				 * 3. Parse the query result and store it in our cache DB
				 */

				// This vector contains all the real attributes in the same
				// order as the column names
				Vector<Attribute> allAttributes = flattenAttributes(item);

				// now loop through the results and put it in the cache DB
				try {
					while (resultSet.next()) {
						// Keep track of the id of the item we just added
						String itemID = "";

						/*
						 * Determine the current type of item we're handling to
						 * add the item to the correct table
						 */
						if (item instanceof Log) {
							try {
								itemID = cacheController.insertLog();
								// halfway first quarter
								// updateProgress(13,
								// "Log inserted in cache DB");
							} catch (SQLException e) {
								mappingController
										.message(
												MESSAGELEVEL.ERROR,
												"There is an error in the query to insert a new log parent "
														+ "into our cache DB for the item: "
														+ item
														+ ", this is the error we got: "
														+ e.getLocalizedMessage());
								throw e;
							}
						} else if (item instanceof Trace) {
							/*
							 * Traces are a bit more difficult since we need to
							 * store the traceID (string!) to select events for
							 */
							try {
								/*
								 * But we should skip this trace if we already
								 * reached our limit for traces and the nrTraces
								 * was set to a positive value (=<0 means all
								 * traces)
								 */
								if (nrTraces >= 0
										&& traceIDs.size() >= nrTraces) {
									// Let's notify the user that we reached our
									// limit
									mappingController
											.message(
													MESSAGELEVEL.NOTICE,
													"We have reached our limit of "
															+ nrTraces
															+ " traces, we will now fetch their events.");

									// And actually stop detecting more
									// traces...
									break;
								}

								String traceID = resultSet.getString(1);

								// We do an extra parsing to allow the use of
								// custom
								// functions in the traceID

								traceIDs.add(traceID);
								// And copy the current traceID into the itemID
								itemID = traceID;

								// And store the query result in the XTrace
								// table in the cache DB
								cacheController.insertTrace(traceID);
							} catch (SQLException eSQL) {
								// We will catch and ignore a 'invalid cursor
								// state'exception which can occur for DISTINCT
								// SELECT queries (very strrrrange)
								if (eSQL.getSQLState().equals("24000")) {
									// we don't want to process this (duplicate)
									// trace any further
									break;
								} else {
									mappingController
											.message(
													MESSAGELEVEL.ERROR,
													"There is an error in the query to create a new trace "
															+ "parent in our cache DB for the item "
															+ item
															+ ", this is the error we got: "
															+ eSQL.getLocalizedMessage());
									cacheController.closeConnection();
									throw eSQL;
								}
							}
						} else if (item instanceof Event) {
							/*
							 * Insert the event related to the given trace with
							 * the given order value
							 */
							try {
								String traceID = resultSet.getString(1);

								// We do an extra parsing to allow the use of
								// custom
								// functions in the traceID
								Event event = (Event) item;
								// And for the order value
								String orderValue = resultSet.getString(2);
								orderValue =
										parseCustomFunctions(orderValue, event
												.getProperties()
												.getEventOrder());

								itemID =
										cacheController.insertEvent(traceID,
												orderValue);

							} catch (SQLException e) {
								/*-
								 * Ignore SQL state 23503:
								 * If we get an SQL state of 23503 then we tried to insert an event
								 * referencing to a non existent trace. (see
								 * http://apache-dox.net/IBM.Press-Apache.Derby-Off.to/0131855255/ch09lev1sec10.html for an
								 * explanation of this SQL state) 
								 * These errors we can (and should!)ignore since we don't want to 
								 * limit our selection but limit our insertion of events 
								 * (way easier, general and potentially faster)
								 * Fixes ticket #270
								 */
								if (!e.getSQLState().equalsIgnoreCase("23503")) {
									mappingController
											.message(
													MESSAGELEVEL.ERROR,
													"There is an error in the query to insert a new event parent in our cache DB for the item "
															+ item
															+ ", this is the error we got: "
															+ e.getLocalizedMessage());
									shutdownSafely("Error while inserting event, aborting.");
									throw e;
								} else {
									/*
									 * If we encounter a SQLstate 23503 we
									 * should not try to process this event any
									 * further and go to the next element
									 */
									continue;
								}
							}
						}

						// Check if we are canceled...
						if (bCancel) {
							shutdownSafely("Just after inserting the data for "
									+ item.toString() + " into the cache.");
							return;
						}

						/*
						 * GENERAL PROCESSING PART
						 */

						// Add the id of the item to the array at level 0 and
						// make sure no 'higher' levels exist any more
						lastXAttributeIDs =
								updateLastXAttributeIDsVector(
										lastXAttributeIDs, 0, itemID);

						// And now process all the attributes attached (if
						// there are any)
						if (allAttributes.size() > 0) {
							ResultSetMetaData metadata;
							try {
								metadata = resultSet.getMetaData();
							} catch (SQLException e) {
								mappingController
										.message(
												MESSAGELEVEL.ERROR,
												"There is an error when getting the resultset metadata for the item "
														+ item
														+ ", this is the error we got: "
														+ e.getLocalizedMessage());
								throw e;
							}

							// Detect the item type and maybe update the column
							// offset
							// Resultset columns start at 1...
							int firstAttributeColumn = 1;
							String itemType = "";
							if (item instanceof Log) {
								itemType = "Log";
							} else if (item instanceof Trace) {
								itemType = "Trace";
								firstAttributeColumn = 2; // first column is
															// traceID
							} else if (item instanceof Event) {
								itemType = "Event";
								// first column is traceID and 2nd column is
								// orderAtt
								firstAttributeColumn = 3;
							}

							// Now for each column we encounter, add the
							// attribute with the correct parent reference
							// NOTE: j is the resultset column id which starts
							// at 1!
							for (int j = firstAttributeColumn; j <= metadata
									.getColumnCount(); j++) {

								// Check if we are canceled...
								if (bCancel) {
									shutdownSafely("We were busy inserting attributes for "
											+ item.toString());
									return;
								}

								// Get the value
								String cellValue;
								try {
									cellValue = resultSet.getString(j);
								} catch (SQLException e) {
									mappingController
											.message(
													MESSAGELEVEL.ERROR,
													"There is an error when getting the value of column "
															+ j
															+ " from the resultset for the item "
															+ item
															+ ", this is the error we got: "
															+ e.getLocalizedMessage());
									throw e;
								}

								Attribute attribute =
										allAttributes.get(j
												- firstAttributeColumn);

								/*- * /
								int level;
								try {
									level = metadata.getColumnName(j)
											.replaceAll("[^|]", "").length();
								} catch (SQLException e) {
									mappingController
											.message(
													MESSAGELEVEL.ERROR,
													"There is an error when getting the columnname of column "
															+ j
															+ " for the item "
															+ item
															+ ", this is the error we got: "
															+ e.getLocalizedMessage());
									throw e;
								}

								String parentItemType = "";
								// If the level is 0, we are at the
								// Log/Trace/Event level
								if (level == 0) {
									parentItemType = itemType;
								} else
								// we have entered the meta-attribute level
								{
									parentItemType = "Attribute";
								}
								/**/

								String parentItemType = "";
								// If the level is 0, we are at the
								// Log/Trace/Event level
								if (!attribute.isMetaChild()) {
									parentItemType = itemType;
								} else
								// we have entered the meta-attribute level
								{
									parentItemType = "Attribute";
								}

								// Now extract some information into attributes
								String ID =
										lastXAttributeIDs.get(attribute
												.getLevel());
								// String ID = lastXAttributeIDs.get(level);
								String key = attribute.getKey();
								String type = attribute.getType();
								String value =
										convertToType(cellValue, attribute);

								if ((value == null || value.equals(""))
										&& !attribute.isMeta()) {
									/*
									 * If there is no value,then continue with
									 * the next iteration! But not if it's a
									 * meta attribute, we need those!
									 */
									continue;
								}

								// And insert it into the cache DB
								String lastAttributeID = "";
								try {
									lastAttributeID =
											cacheController.insertAttribute(
													parentItemType, ID, key,
													value, type);
								} catch (SQLException e) {
									mappingController
											.message(
													MESSAGELEVEL.ERROR,
													"There is an error in the query to add an attribute "
															+ "in our cache DB for the item "
															+ item
															+ ", this is the error we got: "
															+ e.getLocalizedMessage());
									throw e;
								}

								lastXAttributeIDs =
										updateLastXAttributeIDsVector(
												lastXAttributeIDs,
												attribute.getLevel() + 1,
												lastAttributeID);
							}// end for attributes
						}

						/*
						 * POST PROCESSING
						 */
						if (item instanceof Log) {
							break; // break out of the while, we will only
							// process the first log result
						}
					}
				} catch (SQLException e) {
					reportError(
							"There was an internal error when trying to cycle through the data results of the data source.",
							e);
				}// end while resultSet.next()
			}
			// We have finished a step
			currentExecutionStep++;
		}

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We just finished filling the cache.");
			return;
		}

		// 4. Convert the cache DB to the specified format (XES or MXML)
		mappingController.updateProgress(currentExecutionStep,
				"Converting Cache DB to XES");
		try {
			convertDBtoXES(XESfilename, mxml, zip);
		} catch (Exception e) {
			try {
				cacheController.closeConnection();
			} catch (SQLException e1) {
				reportError(
						"There was an error when we tried to close the cache database connection because of another error.",
						e1);
			}
			reportError(
					"There was an error while trying to convert the cache database to an XES file.",
					e);
		}

		// Show cache DB contents if debug level is 'on'
		if (mappingController.maxMessageLevel.compareTo(MESSAGELEVEL.VERBOSE) == 0) {
			cacheController.showContents();
		}

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were busy creating an XES file from the cache database.");
			return;
		}

		// Calculate how long we took to execute
		mappingTimer.stop();

		currentExecutionStep++;
		mappingController.updateProgress(currentExecutionStep, "Completed");
		mappingController.message(
				MESSAGELEVEL.PROGRESS,
				"Mapping execution completed after "
						+ mappingTimer.getDurationString());

		shutdownNormally();
	}

	/**
	 * This method builds and returns a query from the mapping defined for the
	 * given item
	 * 
	 * @param generalMappingItem
	 *            the item to build the query for
	 * @return the query to run
	 * @throws SQLException
	 */
	private String buildQuery(GeneralMappingItem generalMappingItem) {
		// Start with the SELECT part
		// We cycle through all the attributes and parse the 'value' to a query
		// part which we will call the same as the 'key' in the result
		StringBuffer queryBuffer = new StringBuffer();
		queryBuffer.append("SELECT ");

		// For traces and logs, we only want distinct results
		if (generalMappingItem instanceof Trace)
			queryBuffer.append("DISTINCT ");

		// For traces and event, we add a special field 'traceID' in our
		// result
		if (generalMappingItem instanceof Trace
				|| generalMappingItem instanceof Event) {
			String traceIDSpec =
					removeCustomSpecifications(generalMappingItem
							.getProperties().getTraceID());
			queryBuffer.append(traceIDSpec + " AS "
					+ mapping.getConnection().getColumnSeperatorStart()
					+ "traceID"
					+ mapping.getConnection().getColumnSeperatorEnd() + ", ");
		}

		// For events we have an extra column with a 'order by' value
		if (generalMappingItem instanceof Event) {
			Event event = (Event) generalMappingItem;
			String eventOrder = event.getProperties().getEventOrder();
			// Remove 'DESC' and 'ASC' parts, they don't belong in the result
			// itself
			eventOrder = eventOrder.replaceAll(" DESC", "");
			eventOrder = eventOrder.replaceAll(" ASC", "");
			// And trim spaces
			eventOrder = eventOrder.trim();
			// Prevent an empty order
			if (eventOrder.equalsIgnoreCase(""))
				eventOrder = "'0'";
			// Remove custom specifications
			eventOrder = removeCustomSpecifications(eventOrder);

			queryBuffer.append(eventOrder + " AS "
					+ mapping.getConnection().getColumnSeperatorStart()
					+ "orderAttribute"
					+ mapping.getConnection().getColumnSeperatorEnd() + ", ");
		}

		// And now the attributes
		queryBuffer.append(parseAttributesForQuery(generalMappingItem
				.getItemAttributes()));

		// Test if there is anything in the SELECT part
		if (queryBuffer.length() < 10) {
			// If there is nothing in the select part but we are handling a log
			// item we will add a dummy value
			if (generalMappingItem instanceof Log) {
				queryBuffer.append(" 'No attributes' AS DUMMY ");
			}
			// otherwise we will just return an empty query
			else {
				return "";
			}
		}

		// If there is a ',' then remove the last one because we always add one
		// too many...
		if (queryBuffer.lastIndexOf(",") > queryBuffer.length() - 3) {
			queryBuffer.delete(queryBuffer.lastIndexOf(","),
					queryBuffer.length());
		}

		// Now build the FROM part

		// loop through the tables used and generate the JOIN part for the FROM
		String joins = "";

		// The alternative way: just add all links to the FROM part
		joins = convertLinksToFromPart(generalMappingItem);

		// add the joins query part to the whole query
		queryBuffer.append(joins);

		// And the WHERE part is easy
		StringBuffer where =
				new StringBuffer(generalMappingItem.getProperties().getWhere());

		/*-
		 * Selecting our events for the included traces is tricky. If we have
		 * many traces and we say 'traceID' IN ... the query will be very large.
		 * On the other hand, if we only have a small number of traces but a
		 * large number of events for a lot of different traces, then this would
		 * be a good idea! 
		 * Therefore, we do the following:
		 * If we have found <256 traces, we specifically ask for the events of
		 * those traces. This number is chosen because its the lower limit of IN
		 * elements allowed (see about 100 lines below where we actually build
		 * the IN part of the query).
		 * If we have more than 256 traces, we will just get all the events we 
		 * can get and 'drop' them if we don't have the corresponding trace
		 * (this happens in the 'executemapping' function, where we test for 
		 * SQL state 23503).
		 * 
		 * So, we first determine if we should add this 'IN' part to the query
		 */
		// we assume that we won't select events and just drop unwanted events
		boolean selectEvents = false;

		// boolean itemIsEvent = false; // whether we are processing an event
		// boolean traceWhereNonEmpty = false; // and whether the trace has
		// selection criteria

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were building a query for "
					+ generalMappingItem.toString());
			return null;
		}

		// POINT event modulo part
		// First test: are we processing an event?
		if (generalMappingItem instanceof Event) {
			// If so, how many traces did we add to the cache DB?
			try {
				if (cacheController.countTraces() < XESameConstants.executionNrTracesBeforeEventSelection)
					selectEvents = true;
			} catch (SQLException e) {
				reportError(
						"There was an err counting the number of traces in the cache.",
						e);
			}

			/*-* /
			Event event = (Event) generalMappingItem;
			if (event.getTrace().getProperties().getWhere().length() > 0) {
				traceWhereNonEmpty = true;
			}
			/**/

		}
		// selectEvents = false;

		// And now react to this determination
		if (selectEvents) {
			/*
			 * First, we need to know the data type of the traceID result so we
			 * can construct the correct 'IN' query part
			 */
			boolean traceIDisString = false;
			try {
				/*
				 * We build a special, temporary query to detect the type of the
				 * traceID. This is important for the ' IN (traceID, traceID2,
				 * ...)' versus 'IN ('traceID', 'traceID2', ...)' part of the
				 * event query.
				 */
				Event event = (Event) generalMappingItem;
				String traceIDSpec = event.getProperties().getTraceID();
				String traceIDTypeQuery =
						"SELECT " + traceIDSpec + joins + " WHERE "
								+ traceIDSpec + " LIKE '123456ABCDEFG'"
								+ XESameConstants.sqlQueryEnd;

				mappingController.message(MESSAGELEVEL.DEBUG,
						"We are about to run the following query to detect the traceID type: "
								+ traceIDTypeQuery);

				ResultSet traceIdTypeRS =
						sourceController.runQuery(traceIDTypeQuery, 1);
				ResultSetMetaData rsmd = traceIdTypeRS.getMetaData();
				int traceIDType = rsmd.getColumnType(1);
				// Now detect string types
				if (traceIDType == Types.CHAR || traceIDType == Types.CLOB
						|| traceIDType == Types.LONGNVARCHAR
						|| traceIDType == Types.LONGVARBINARY
						|| traceIDType == Types.LONGVARCHAR
						|| traceIDType == Types.NCHAR
						|| traceIDType == Types.NVARCHAR
						|| traceIDType == Types.VARBINARY
						|| traceIDType == Types.VARCHAR) {
					traceIDisString = true;
				}
			} catch (Exception e) {
				// Hide the error, we will handle it later and assume the
				// traceID is not a string
			}

			// If we add something to the where clause, add an AND
			if (where.length() > 0) {
				where.append(" AND ");
			}

			// Create the '(traceID IN ('
			where.append("("
					+ removeCustomSpecifications(generalMappingItem
							.getProperties().getTraceID()) + " IN (");
			if (traceIDisString)
				where.append("'");

			// Transform our list of trace IDs to a SQL list
			if (traceIDs == null)
				traceIDs = new HashSet<String>();

			// We keep track of the number of traces added to our IN list
			int traceNumber = 0;

			for (String traceID : traceIDs) {
				// Check if we are canceled...
				if (bCancel) {
					shutdownSafely("We were parsing the traceIDs into our data query.");
					return null;
				}

				if (traceIDisString) {
					where.append(traceID + "', '");
				} else {
					where.append(traceID + ", ");
				}

				// We just added one...
				traceNumber++;

				/*-
				 * The IN list can not be too long for certain database systems.
				 * But there is a trick, do an OR IN(...) as many times as
				 * required. From
				 * http://forums.whirlpool.net.au/forum-replies.cfm
				 * ?t=747853&r=11760923#r11760923
				 * According to the Open SQL 'standard' by SUN the minimal limit
				 * of IN items is 256 so be safe...
				 */
				if (((traceNumber % 256) == 0) && traceNumber < traceIDs.size()) {
					// Remove last ', ' or ,
					if (traceIDisString) {
						if (where.lastIndexOf(", '") > 0) {
							where.delete(where.lastIndexOf(", '"),
									where.length());
						}
					} else {
						if (where.lastIndexOf(", ") > 0) {
							where.delete(where.lastIndexOf(", "),
									where.length());
						}
					}

					// Add the necessary code in between
					where.append(") OR ");
					where.append(removeCustomSpecifications(generalMappingItem
							.getProperties().getTraceID()) + " IN (");
					if (traceIDisString)
						where.append("'");
				}
			} // for

			// Check if we are canceled...
			if (bCancel) {
				shutdownSafely("We were building a query to get the data for "
						+ generalMappingItem.toString());
				return null;
			}

			// Remove last ', ' or ,
			if (traceIDisString) {
				if (where.lastIndexOf(", '") > 0) {
					where.delete(where.lastIndexOf(", '"), where.length());
				}
			} else {
				if (where.lastIndexOf(", ") > 0) {
					where.delete(where.lastIndexOf(", "), where.length());
				}
			}

			// And add two closing bracket, one for the IN statement and one for
			// the OR concatenation
			where.append("))");
		}

		// Only add the WHERE keyword and clause if we have something
		if (where.length() != 0
				&& (!(generalMappingItem instanceof Trace) || traceIDs.size() == 0))
			queryBuffer.append(" WHERE " + where);

		queryBuffer.append(XESameConstants.sqlQueryEnd);

		return queryBuffer.toString();
	}

	/**
	 * Adds the ID at the level+1 index in the array and removes all id's after
	 * that
	 * 
	 * @param lastXAttributeIDs
	 *            Vector to update
	 * @param level
	 *            (index-1) to update
	 * @param lastParentID
	 *            new ID to set
	 * @return
	 */
	private Vector<String> updateLastXAttributeIDsVector(
			Vector<String> lastXAttributeIDs, int level, String lastParentID) {
		// check if the level is too far according to the vector, we can only
		// add 1 new level at once
		if (level > lastXAttributeIDs.size() + 1) {
			mappingController
					.message(
							MESSAGELEVEL.ERROR,
							"During the execution of the mapping we (internally) provided an incorrect "
									+ "level in our attribute tree hierarchy. We chose to move on but you should "
									+ "carefully look at the attribute tree structure in your log!!!");
			return lastXAttributeIDs;
		}

		/*
		 * Okay, the idea is that we add the given id at index level and make
		 * sure that after that 'level'/index no other ids remain e.g. if we add
		 * a new attribute 42 at level 3 we don't want to have any ids remaining
		 * at level 4,5, etc. So, add the id at the given location
		 */
		lastXAttributeIDs.add(level, lastParentID);
		// and now remove everything after it
		for (int i = level + 1; i < lastXAttributeIDs.size(); i++) {
			lastXAttributeIDs.remove(i);
		}

		return lastXAttributeIDs;
	}

	/**
	 * Converts the links specified for this item AND its parents to a correct
	 * part for the FROM section of the SQL query
	 * 
	 * @param generalMappingItem
	 *            item to handle
	 * @return String string to add to the FROM part of the query
	 */
	private String convertLinksToFromPart(GeneralMappingItem generalMappingItem) {
		String joins = " FROM ";

		Vector<Link> links = generalMappingItem.getProperties().getLinks();

		// First the FROM part
		// Add ('s as much as the links-1
		int nrOfBrackets = links.size() - 1;
		for (int i = 0; i < nrOfBrackets; i++) {
			joins += "(";
		}

		// Add the from part
		joins += generalMappingItem.getProperties().getFrom() + " ";

		// Each link specification
		for (Link link : links) {
			joins += " INNER JOIN " + link.getSpecification() + ")";
		}

		// remove the last )
		if (joins.lastIndexOf(")") > joins.length() - 3) {
			joins = joins.substring(0, joins.lastIndexOf(")"));
		}

		// return and add an extra space to be sure
		return joins + " ";
	}

	/**
	 * Recursive method that walks through all the attributes and sub attributes
	 * of them and builds the SELECT part of the query
	 * 
	 * @param attributes
	 *            Vector of attributes to parse
	 * @return select part for query
	 */
	private String parseAttributesForQuery(Vector<Attribute> attributes) {
		String query = "";

		for (Attribute attribute : attributes) {
			// Parse this single attribute and add it to our query
			query += parseAttributeForQuery(attribute);

			if (attribute.isMeta()) {
				// If the current attribute has children, call ourselves on the
				// given sub-attributes
				query +=
						parseAttributesForQuery(attribute.getItemAttributes())
								+ ", ";
			}
		}

		// The query might contain a trailing ', ' which we need to remove to
		// get a correct SELECT query part
		if (query.lastIndexOf(',') > 0) {
			query = query.substring(0, query.lastIndexOf(','));
		}

		return query;
	}

	/**
	 * This method parses the key and value of a single attribute to a SELECT
	 * query part in the form of (value) AS key
	 * 
	 * @param attribute
	 * @return
	 */
	private String parseAttributeForQuery(Attribute attribute) {
		String queryPart = "";
		// Only return a string if the value is filled in (but always include
		// meta attributes)
		String value = attribute.getValue().trim();
		if ((value.equals("") || value.length() <= 0) && !attribute.isMeta())
			return "";

		// Else continue...
		// Extract the value but remove any format indication [{format}]
		queryPart += removeCustomSpecifications(attribute.getValue());

		// the value might be empty by now, breaking the query, so fix it
		if (queryPart.length() == 0) {
			queryPart += "''";
		}

		// Don't try renaming something to the same name...
		if (!getAttributeFieldName(attribute).equalsIgnoreCase(queryPart)) {
			queryPart +=
					" AS " + mapping.getConnection().getColumnSeperatorStart();
			// the (combined) key
			queryPart +=
					getAttributeFieldName(attribute)
							+ mapping.getConnection().getColumnSeperatorEnd();
		}
		// Add a comma for the remaining columns in the query
		queryPart += ", ";

		return queryPart;
	}

	/**
	 * Returns the field name used for the given key. The structure is like
	 * meta1|meta2|meta3|attKey
	 * 
	 * @param attribute
	 * @return flat key
	 */
	private String getAttributeFieldName(Attribute attribute) {
		// FIXME | is reserved in Oracle, find other char or make conf!
		GeneralMappingItem parent = attribute.getParentItem();
		// If the parent of this attribute is of the type attribute
		if (parent instanceof Attribute) {
			// Then the parent is a meta attribute
			Attribute metaAttribute = (Attribute) parent;
			// And we need to return a string in the form meta|key
			return getAttributeFieldName(metaAttribute) + "_"
					+ attribute.getKey().replaceAll(":", "_");
		} else
			// If not, then we reached the highest level of attributes
			return attribute.getKey().replaceAll(":", "_");
	}

	/**
	 * Returns a value that is a correct representation of the provided type
	 * from the input string
	 * 
	 * @param string
	 *            The result from the data source
	 * @param Attribute
	 *            the attribute the value relates to
	 * @return string representation of the given value in the given type
	 */
	private String convertToType(String input, Attribute attribute) {
		String type = attribute.getType();
		String value = attribute.getValue();

		// We can not work on null strings...
		if (input == null)
			return null;

		input = parseCustomFunctions(input, value);

		if (type.equalsIgnoreCase("Date")) {
			/*-
			 * Dates are very annoying to parse since there are so many 
			 * different formats. Therefore we allow the user to specify
			 * the format used for each date he uses.
			 * The format is as follows (in the value definition):
			 * timestamp [{format}] 
			 * where the format is according to
			 * http://java.sun.com/javase/6/docs/api/java/text/SimpleDateFormat.html 
			 */

			String formatString = extractCustomCode(value);

			if (formatString != "" && formatString.length() > 0) {
				SimpleDateFormat inFormat = new SimpleDateFormat(formatString);

				SimpleDateFormat outFormat =
						new SimpleDateFormat(XESameConstants.dateFormat);

				/*
				 * Now convert the input to a date according to the given format
				 */

				// Initialize the date to 1 Jan. 1970 00:00:00
				Date d = new Date(0);

				// Try to parse the input string
				try {
					d = inFormat.parse(input);
				} catch (ParseException e) {
					// If we fail notify the user but don't break...
					mappingController
							.message(
									MESSAGELEVEL.WARNING,
									"There was an error parsing "
											+ "a date value using the provided format in the event "
											+ attribute.toString()
											+ ". The value was "
											+ input
											+ " which we tried to parse using the format "
											+ formatString
											+ ". We tried to \'fix\' this by inserting a date "
											+ "of January 1 1970, which is most likely incorrect. "
											+ "We will try to continue as normal but the event log "
											+ "does contain invalid values!!!! We warned you!");
				}
				return outFormat.format(d);
			}
			/*
			 * We make it here iff there was a format delimiter in the attribute
			 * value definition but there where more than one start and/or end
			 * tags so we can not parse it correctly. Unlikely situation but you
			 * never know, do you?
			 */

			/*-
			 * If no (correct) format is provided then we need to make 
			 *    chocolate out of it by ourselves.... (sorry, I'm Dutch)
			 * 
			 * Note: we don't use the normal DateTime but a more 'smart'
			 * DateTime class provided by pojava.org that can interpret an
			 * unknown format
			 * Its the best we can do..
			 * See http://sourceforge.net/projects/pojava/
			 * We use version 2.2.0
			 */
			try {
				DateTime dt = new DateTime(input);
				return dt.toString(XESameConstants.dateFormat);
			} catch (IllegalArgumentException iae) {
				return null;
			} catch (NullPointerException npe) {
				// If we fail notify the user but don't break...
				mappingController
						.message(
								MESSAGELEVEL.WARNING,
								"There was an error parsing an empty date value. We tried to \'fix\' this by inserting a date "
										+ "of January 1 1970, which is most likely incorrect. "
										+ "We will try to continue as normal but the event log "
										+ "does contain invalid values!!!! We warned you!");

				return "1-1-1970";
			}

		} else if (type.equalsIgnoreCase("Integer")) {
			Integer tempInt = new Integer(input);
			return tempInt.toString();
		} else if (type.equalsIgnoreCase("Float")) {
			Float tempFloat = Float.parseFloat(input);
			return tempFloat.toString();
		} else if (type.equalsIgnoreCase("Boolean")) {
			Boolean tempBool = Boolean.parseBoolean(input);
			return tempBool.toString();
		}

		// Otherwise the type is unknown or just a string
		return input;
	}

	/**
	 * This method parses our custom functions as defined in the attribute value
	 * definition between [{spec}]. The date format is processed somewhere else!
	 * (inside the convertToType()-function)
	 * 
	 * @param input
	 *            the result to execute our functions on
	 * @param valueSpec
	 *            The value specification of the attribute
	 * @return
	 */
	private String parseCustomFunctions(String input, String valueSpec) {
		/*
		 * We have re-implemented certain scalar SQL methods because sometimes
		 * methods will break the query
		 */
		// TODO ENH allow multiple functions to be present!?!?
		String code = extractCustomCode(valueSpec);

		// The substring method as substr(start[,length])
		String substrString = "substr("; // the method name
		if (code.toLowerCase().contains(substrString)) {
			// Try to parse the function
			try {
				int methodNameIndex =
						code.indexOf(substrString) + substrString.length();
				int commaIndex = code.indexOf(",", methodNameIndex);
				int closingBrackedIndex = code.indexOf(")", commaIndex);

				/*
				 * We extract the start and end indexed of the substring, we
				 * also follow the 'start at 1' principle of the MID() function
				 * of SQL, to prevent confusion for non-ITers (and introduce it
				 * for Java experts)
				 */
				int firstNumberEnd = commaIndex;
				if (commaIndex < 0) {
					firstNumberEnd = closingBrackedIndex;
				}
				String firstNumber =
						code.substring(methodNameIndex, firstNumberEnd);
				int start = Integer.parseInt(firstNumber) - 1;

				// Perform some checks and corrections on the start value
				if (start < 0) {
					start = 0;
				}
				if (start > input.length()) {
					start = input.length();
				}

				/*
				 * We might not have a second parameter so we go until the end
				 * of the string
				 */
				int end = input.length();
				if (commaIndex >= 0) {
					// If we have a second parameter, process it
					// But convert length to end position
					end =
							start
									+ (Integer.parseInt(code
											.substring(commaIndex + 1,
													closingBrackedIndex)));
				}
				// Perform some checks and corrections on the end value
				if (end < 0) {
					end = 0;
				}
				if (end > input.length()) {
					end = input.length();
				}
				if (start > end) {
					start = end;
				}

				/*
				 * We did a pre-check and fixed stuff, so now lets extract the
				 * substring!
				 */
				input = input.substring(start, end);
			}
			// If we fail, display a warning notice
			catch (Exception e) {
				mappingController
						.message(
								MESSAGELEVEL.WARNING,
								"There was an error while processing the substring function, "
										+ "we did not change the resulting value and continued. "
										+ "The error we got was: "
										+ e.getLocalizedMessage());
			}
		}

		// The now() method as now()
		String nowString = "now()"; // the method name
		if (code.toLowerCase().contains(nowString)) {
			// append the current date and time to the input
			SimpleDateFormat sdf =
					new SimpleDateFormat(XESameConstants.dateFormat);
			input += sdf.format(new Date());
		}

		return input;
	}

	/**
	 * Removes the [{spec}] annotations in the attribute's value definition so
	 * that it can be used in the query.
	 * 
	 * @param valueSpec
	 *            the value specification
	 * @return the cleaned value specification
	 */
	private String removeCustomSpecifications(String valueSpec) {
		return valueSpec.replaceAll("\\[\\{.*?\\}\\]", "");
	}

	/**
	 * Extracts code which is present in the string as [{code}]
	 * 
	 * @param value
	 *            string to extract the custom code from
	 * @return the code part
	 */
	private String extractCustomCode(String value) {
		String code = "";

		// First try to extract the format from the input
		String matchregex = "(.*)\\[\\{(.*?)\\}\\](.*)";

		if (value.matches(matchregex)) {
			// We will only return the contents of the first [{ }] pair!
			int start = value.indexOf("[{") + 2; // exclude [{ ...
			int end = value.indexOf("}]");
			return value.substring(start, end);
		}

		return code;
	}

	/**
	 * Returns all the (sub)* attributes of the given item in the same order as
	 * the resultset columns
	 * 
	 * @param item
	 * @return vector of all attributes that have the given item as a (grand)*
	 *         parent
	 */
	private Vector<Attribute> flattenAttributes(GeneralMappingItem item) {
		Vector<Attribute> itemAttributes = item.getItemAttributes();
		Vector<Attribute> returnAttributes = new Vector<Attribute>();

		for (Attribute attribute : itemAttributes) {
			String value = attribute.getValue().trim();
			/*
			 * We add the attribute to the vector if it has a non-empty value OR
			 * if it has children
			 */
			if ((!value.equals("") && value.length() > 0) || attribute.isMeta()) {
				// Add the current attribute to the list
				returnAttributes.add(attribute);
			}
			// And all children
			returnAttributes.addAll(flattenAttributes(attribute));
		}
		return returnAttributes;
	}

	/**
	 * Creates an XES XML document from the cache DB
	 * 
	 * @param string
	 *            filename to write to
	 * @param mxml
	 *            if true, MXML will be created
	 * @param zip
	 *            if true, the event log will be compressed in a zip file
	 * @throws IOException
	 * @throws SQLException
	 * @throws SAXException
	 * @throws URISyntaxException
	 * @throws ParserConfigurationException
	 */
	private void convertDBtoXES(String eventFile, boolean mxml, boolean zip)
			throws IOException, SQLException, SAXException, URISyntaxException,
			ParserConfigurationException {
		// Create a Xfactory to produce the eventlog
		XFactory factory = XFactoryRegistry.instance().currentDefault();
		XLog xLog = factory.createLog();

		// Add the extensions
		Vector<Extension> extensions = mapping.getExtensions();
		// Try to parse the given URI into an extension instance
		XExtensionManager extManager = XExtensionManager.instance();

		for (Extension extension : extensions) {
			XExtensionParser extParser = new XExtensionParser();
			XExtension xExtension;
			try {
				// Retrieve from cache or from source and add to cache
				xExtension = extManager.getByUri(new URI(extension.getURI()));
				// TODO code below failed at Mike, bug report for OpenXES?
				// xExtension = extParser.parse(new URI(extension.getURI()));
				xLog.getExtensions().add(xExtension);
			} catch (URISyntaxException e) {
				mappingController.message(
						MESSAGELEVEL.ERROR,
						"We could not write the event log file because we encountered an error "
								+ "when parsing a stored extension ("
								+ extension.getName() + "):"
								+ e.getLocalizedMessage());
				throw e;
			}
		}

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were busy creating an XES file and just finished parsing the extensions.");
			return;
		}

		/*
		 * The globals
		 */
		// Trace globals:
		HashSet<String> traceGlobals = initGlobals(mapping.getLog().getTrace());
		for (String key : traceGlobals) {
			// Now get the attribute type
			String type =
					mapping.getLog().getTrace().getItemAttribute(key).getType();

			// Add it to the log definition
			xLog.getGlobalTraceAttributes().add(
					createGlobalXAttribute(type, key));
		}

		// Event globals:
		HashSet<String> eventGlobals =
				initGlobals(mapping.getLog().getTrace().getEvents());
		for (String key : eventGlobals) {
			// Now get the attribute type
			// We can do this for any event definition since it is a global...
			String type =
					mapping.getLog().getTrace().getEvents().get(0)
							.getItemAttribute(key).getType();

			// Add it to the log definition
			xLog.getGlobalEventAttributes().add(
					createGlobalXAttribute(type, key));
		}

		// The classifiers
		Vector<Classifier> classifiers = mapping.getLog().getClassifiers();
		for (Classifier classifier : classifiers) {
			XEventAttributeClassifier attributeClassifier =
					new XEventAttributeClassifier(classifier.getName(),
							classifier.getKeys());
			xLog.getClassifiers().add(attributeClassifier);

			// If the classifier keys are not in the event globals list
			if (!checkClassifiers(xLog, classifier.getKeys().split(" "))) {
				// TODO try to provide >which< classifier keys are not global
				// Notify the user!
				// TODO move this message 'lower' in the chain, its lost in debug mode
				mappingController
						.message(
								MESSAGELEVEL.WARNING,
								"Not every specified key in the classifier "
										+ classifier.getName()
										+ " is also in the list of global event attributes. "
										+ "This means that for at least 1 event the classifier is invalid!!!");
			}
		}

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were busy creating the XES file and just finished adding the globals and classifiers.");
			return;
		}

		// Log attributes
		xLog.getAttributes().putAll(convertAttributesToXESPart("Log", "0"));

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were busy creating the XES file and just finished adding the log attributes.");
			return;
		}

		// The traces and their attributes
		ResultSet traces;
		try {
			traces = cacheController.getTraces();
		} catch (SQLException e) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"There was an error while extracting the traces from the cache DB: "
							+ e.getLocalizedMessage());
			throw e;
		}

		// Try to detect how to order the events (ascending or descending)
		boolean ascending = true; // by default descending

		if (mapping.getLog().getTrace().getProperties().getEventOrder()
				.contains("DESC")) {
			// but if we find 'DESC' in the specification, its descending
			ascending = false;
		}

		try {
			while (traces.next() && !bCancel) {
				String traceID = traces.getString("traceID");
				XTraceBufferedImpl xTrace =
						(XTraceBufferedImpl) factory
								.createTrace(convertAttributesToXESPart(
										"Trace", traceID));

				/*
				 * The events with attributes for the current trace
				 */
				ResultSet events;
				try {
					events = cacheController.getEvents(traceID, ascending);
				} catch (SQLException e) {
					mappingController.message(
							MESSAGELEVEL.ERROR,
							"There was an error while extracting the events for trace '"
									+ traceID + "' from the cache DB: "
									+ e.getLocalizedMessage());
					throw e;
				}

				try {
					while (events.next() && !bCancel) {
						String eventID = events.getString("ID");
						XEvent xEvent =
								factory.createEvent(convertAttributesToXESPart(
										"Event", eventID));
						//FIXME use this always or make option???
						 xTrace.insertOrdered(xEvent);
//						xTrace.add(xEvent);
					}

					// Check if we are canceled...
					if (bCancel) {
						shutdownSafely("We were busy creating the XES file and were busy inserting events.");
						return;
					}
				} catch (SQLException e) {
					mappingController.message(
							MESSAGELEVEL.ERROR,
							"There was an error while processing the events for trace '"
									+ traceID + "'from the cache DB: "
									+ e.getLocalizedMessage());
					throw e;
				}	
				
				if (xTrace.size() > 0)
					xLog.add(xTrace);
			}

			// Check if we are canceled...
			if (bCancel) {
				shutdownSafely("We were busy creating the XES file and are in the process of adding traces.");
				return;
			}

		} catch (SQLException e) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"There was an error while extracting the traceID from the cache DB: "
							+ e.getLocalizedMessage());
			throw e;
		}

		// Check if we are canceled...
		if (bCancel) {
			shutdownSafely("We were busy creating the XES file "
					+ "and just finished adding all data to the XES internal object "
					+ "which we were about to serialize.");
			return;
		}

		// Write the file
		XSerializer serializer;
		BufferedOutputStream out;

		try {
			String fileExtension = "";
			// There are 4 cases: MXML/XES and plain/GZip
			// Instantiate the correct serializer
			// and set the correct filename
			if (mxml) {
				if (zip) {
					serializer = new XMxmlGZIPSerializer();
					fileExtension = ".mxml.gz";
				} else {
					serializer = new XMxmlSerializer();
					fileExtension = ".mxml";
				}
			} else {
				if (zip) {
					serializer = new XesXmlGZIPSerializer();
					fileExtension = ".xes.gz";
				} else {
					serializer = new XesXmlSerializer();
					fileExtension = ".xes";
				}
			}

			// strip the eventFile of extensions
			if (eventFile.endsWith(".gz"))
				eventFile = eventFile.substring(0, eventFile.length() - 3);

			if (eventFile.endsWith(".xes"))
				eventFile = eventFile.substring(0, eventFile.length() - 4);

			if (eventFile.endsWith(".mxml"))
				eventFile = eventFile.substring(0, eventFile.length() - 5);

			// and add the correct one
			// FIXME broken???
			String filename = eventFile + fileExtension;

			/*-if (eventFile.endsWith(fileExtension)) {
				if (eventFile.endsWith(fileExtension.replace(".gz", ""))) {
					filename = eventFile;
				} else {
					filename = eventFile + ".gz";
				}
			} else {
				filename = eventFile + fileExtension;
			}/**/

			// Our output stream
			out =
					new BufferedOutputStream(new FileOutputStream(new File(
							filename)));

		} catch (FileNotFoundException e) {
			mappingController.message(
					MESSAGELEVEL.ERROR,
					"There was an error while creating the eventlog file: "
							+ e.getLocalizedMessage());
			throw e;
		}
		try {
			// Serialize
			serializer.serialize(xLog, out);
		} catch (IOException e) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"There was an error while writing the event log to the file: "
							+ e.getLocalizedMessage());
			throw e;
		}

		// And close it
		out.close();
	}

	/**
	 * Checks if the given String array of keys is a subset of the event globals
	 * of the XLog item
	 * 
	 * @param xLog
	 *            XES Log instance
	 * @param keys
	 *            String[] of keys to check
	 */
	private boolean checkClassifiers(XLog xLog, String[] keys) {
		List<XAttribute> globalXAttributes = xLog.getGlobalEventAttributes();
		Vector<String> globalKeys = new Vector<String>();

		/*
		 * Instead of looping through the global list every time we are
		 * searching, we convert it once to a list of strings we can more easily
		 * search
		 */
		for (XAttribute xAttribute : globalXAttributes) {
			globalKeys.add(xAttribute.getKey());
		}

		// For each item in the classifier key array
		for (String key : keys) {
			// check if it is in our globals list
			if (!globalKeys.contains(key)) {
				return false; // one key that is not on our list is sufficient!
			}
		}

		return true; // if we made it this far, everything is Okeydokey!!
	}

	/**
	 * Creates an XAttribute instance with a standard value for global
	 * attributes
	 * 
	 * @param type
	 * @param key
	 * @return
	 */
	private XAttribute createGlobalXAttribute(String type, String key) {
		String value = "UNKNOWN";

		if (type.equalsIgnoreCase("string")) {
			value = "UNKNOWN";
		} else if (type.equalsIgnoreCase("boolean")) {
			value = "true";
		} else if (type.equalsIgnoreCase("float")) {
			value = "-1";
		} else if (type.equalsIgnoreCase("integer")) {
			value = "-1";
		} else if (type.equalsIgnoreCase("date")) {
			value = "1-1-1970 00:00:000";
		} else {
			// type is an unknown value, resort to the most expressive type:
			type = "string";
		}

		return createXAttribute(type, key, value);
	}

	/**
	 * Takes the attributes for the given parent type with the given ID and
	 * converts them (and the child attributes) to a valid XES XML part
	 * 
	 * @param type
	 *            type of parent (Log, Trace, Event or Attribute)
	 * @param id
	 *            id of the parent
	 * @param lvl
	 *            level of indentation in the XML code
	 * @throws SQLException
	 */
	private XAttributeMap convertAttributesToXESPart(String type, String id)
			throws SQLException {
		ResultSet attributesResultSet =
				cacheController.getAttributesFor(type, id);

		XAttributeMap attributeMap = new XAttributeMapBufferedImpl();

		// now loop through all the attributes of the parent we where called for
		try {
			while (attributesResultSet.next()) {
				// Add the attributes to the attributeMap
				String attributeType =
						attributesResultSet.getString("XType").toLowerCase();
				String attributeKey = attributesResultSet.getString("XKey");
				String attributeValue = attributesResultSet.getString("XValue");
				Integer attributeID = attributesResultSet.getInt("ID");
				Boolean isMeta = attributesResultSet.getBoolean("isMeta");

				XAttribute xAttribute =
						createXAttribute(attributeType, attributeKey,
								attributeValue);

				// Check if we have child attributes
				if (isMeta) {
					// if so, call ourselves on our children
					xAttribute.getAttributes().putAll(
							convertAttributesToXESPart("attribute",
									attributeID.toString()));
				}

				// And add the xAttribute to the attribute map
				attributeMap.put(attributeKey, xAttribute);
			}
		} catch (SQLException e) {
			mappingController
					.message(
							MESSAGELEVEL.ERROR,
							"There was an error while looping through the attributes to create an XES part for them: "
									+ e.getLocalizedMessage());
			cacheController.closeConnection();
			throw e;
		}

		return attributeMap;
	}

	/**
	 * Returns an XAttribute of the provided type with the given key and value
	 * 
	 * @param attributeType
	 *            Type of the attribute to return
	 * @param attributeKey
	 *            Key of the attribute to return
	 * @param attributeValue
	 *            Value of the attribute to return
	 * @return XAttribute
	 */
	private XAttribute createXAttribute(String attributeType,
			String attributeKey, String attributeValue) {
		// First, test if we can find the given type
		if (attributeType.equalsIgnoreCase("boolean")) {
			return new XAttributeBooleanImpl(attributeKey,
					Boolean.parseBoolean(attributeValue));
		} else if (attributeType.equalsIgnoreCase("float")) {
			return new XAttributeContinuousImpl(attributeKey,
					Double.parseDouble(attributeValue));
		} else if (attributeType.equalsIgnoreCase("integer")) {
			return new XAttributeDiscreteImpl(attributeKey,
					Long.parseLong(attributeValue));
		} else if (attributeType.equalsIgnoreCase("date")) {
			return new XAttributeTimestampImpl(attributeKey, DateTime.parse(
					attributeValue).toDate());
		}

		// We must have a default, literals/strings are a good candidate
		// But we must prevent empty strings
		if (attributeValue.trim().length() == 0)
			attributeValue = "EMPTY";
		return new XAttributeLiteralImpl(attributeKey, attributeValue);
	}

	/**
	 * Changes the location of the cache database
	 * 
	 * @param location
	 *            String new location path
	 */
	public void setCacheLocation(String location) {
		cacheController.setCacheDir(location);

		mappingController.message(
				MESSAGELEVEL.DEBUG,
				"Derby system dir before reboot: "
						+ System.getProperty("derby.system.home"));

		rebootDB();

		mappingController.message(
				MESSAGELEVEL.DEBUG,
				"Derby system dir after reboot: "
						+ System.getProperty("derby.system.home"));
	}

	/**
	 * Reboots the CacheDB to update the database location
	 */
	public void rebootDB() {
		try {
			cacheController.closeConnection();

			cacheController.initializeCacheDB();
		} catch (SQLException e) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"We encountered the following error while rebooting the Cache datbase: "
							+ e.getLocalizedMessage());
		} catch (ClassNotFoundException e) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"We encountered the following error while rebooting the Cache datbase: "
							+ e.getLocalizedMessage());
		}
	}

	/**
	 * Returns a set of attribute keys for a single mappingItem
	 * 
	 * @param trace
	 * @param item
	 * @return
	 */
	private HashSet<String> initGlobals(GeneralMappingItem item) {
		// Start with an empty key set
		HashSet<String> keys = new HashSet<String>();

		// Get the attributes
		Vector<Attribute> attributes = item.getItemAttributes();
		// Add each attribute to the key set
		for (Attribute attribute : attributes) {
			// Only add it if it has a non-empty value
			if (attribute.getValue() != null
					&& attribute.getValue().length() > 0) {
				keys.add(attribute.getKey());
			}
		}

		return keys;
	}

	/**
	 * Returns a set of common attributes for events
	 * 
	 * @param events
	 * @return Set of common attribute keys
	 */
	private HashSet<String> initGlobals(Vector<Event> events) {
		// Create a hashset of keys
		HashSet<String> keys = new HashSet<String>();

		// Only attempt the following on non empty vectors...
		if (events.size() > 0) {
			// Start with the keys of the first event
			keys = initGlobals(events.get(0));

			// Get the keys for each event
			for (Event event : events) {
				// Get the keys for this event
				// HashSet<String> eventKeys = initGlobals(event);

				// And intersect it with the keys already known
				keys.retainAll(initGlobals(event));
			}
		}

		return keys;
	}

	/**
	 * Reports the error encountered in this class back to the mapping
	 * controller and therefore to the user
	 * 
	 * @param errorMessage
	 */
	private void reportError(String errorMessage, Exception e1) {
		// Record that we quit with an error
		bError = true;
		// stop execution
		cancel();

		// report back
		// TODO show alert box...
		mappingController.message(MESSAGELEVEL.ERROR, errorMessage);
		mappingController.message(MESSAGELEVEL.ERROR, e1.getLocalizedMessage());

		// shutdownNormally();

	}

	/**
	 * Call this method to safely cancel the execution. It may take some time
	 * before the cancel state is checked but it allows for a nice stop.
	 */
	public void cancel() {
		bCancel = true;
	}

	/**
	 * This method closes some things so the thread can safely shut down in case
	 * of error or cancellation by user. Use shutdownNormally if everything went
	 * OK!
	 */
	private void shutdownSafely(String message) {
		mappingController.message(MESSAGELEVEL.WARNING,
				"Cancelling execution! " + message);

		shutdownNormally();

		mappingController.message(MESSAGELEVEL.NOTICE,
				"Execution safely terminated");
	}

	/**
	 * This method shuts down the connections after successful termination (so
	 * no messages)
	 */
	private void shutdownNormally() {
		try {
			cacheController.closeConnection();
			sourceController.closeConnection();
		} catch (SQLException e) {
			if (!bError) {
				reportError(
						"There was an error while closing the source connection, it might still be open",
						e);
			}
		}

		// call back to the GUI that we have finished (via the mapping
		// controller...)
		mappingController.executorFinished();
	}
}
