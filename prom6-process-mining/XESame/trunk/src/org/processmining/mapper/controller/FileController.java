/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.xml.parsers.ParserConfigurationException;

import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.mapping.*;
import org.processmining.mapper.mapping.legacy1.Legacy1Convertor;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;

/**
 * This class provides functions for correctly loading and saving mapping
 * definitions
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 */
public class FileController {

	private MappingController mappingController;

	private XStream xstream = null; // our XStream instance

	public FileController(MappingController mappingController) {
		this.mappingController = mappingController;

		// The class used to serialize and load the mapping definition
		this.xstream = newXStream();

	}

	/**
	 * Initializes an xstream instance and sets some properties
	 * 
	 * @return XStream
	 */
	private XStream newXStream() {
		XStream xstream = new XStream();

		// xstream.registerConverter(new GeneralMappingItemConverter());

		// Create some aliases to produce neater XML
		xstream.alias("mapping", Mapping.class);
		xstream.alias("connection", Connection.class);
		xstream.alias("extension", Extension.class);
		xstream.alias("log", Log.class);
		xstream.alias("classifier", Classifier.class);
		xstream.alias("trace", Trace.class);
		xstream.alias("event", Event.class);
		xstream.alias("attribute", Attribute.class);
		xstream.alias("generalMappingItem", GeneralMappingItem.class);

		return xstream;
	}

	/**
	 * Initializes an xstream instance and sets some properties
	 * 
	 * @return XStream
	 */
	private XStream newXStreamLegacy1() {
		XStream xstream = new XStream();

		// xstream.registerConverter(new GeneralMappingItemConverter());

		// Create some aliases to produce neater XML
		xstream.alias("mapping",
				org.processmining.mapper.mapping.legacy1.Mapping.class);
		xstream.alias("connection",
				org.processmining.mapper.mapping.legacy1.Connection.class);
		xstream.alias("extension",
				org.processmining.mapper.mapping.legacy1.Extension.class);
		xstream.alias("log", org.processmining.mapper.mapping.legacy1.Log.class);
		xstream.alias("classifier",
				org.processmining.mapper.mapping.legacy1.Classifier.class);
		xstream.alias("trace",
				org.processmining.mapper.mapping.legacy1.Trace.class);
		xstream.alias("event",
				org.processmining.mapper.mapping.legacy1.Event.class);
		xstream.alias("attribute",
				org.processmining.mapper.mapping.legacy1.Attribute.class);
		xstream.alias("table",
				org.processmining.mapper.mapping.legacy1.Table.class);
		xstream.alias("column",
				org.processmining.mapper.mapping.legacy1.Column.class);
		xstream.alias("relation",
				org.processmining.mapper.mapping.legacy1.Relation.class);

		return xstream;
	}

	/**
	 * Saves the mapping to the given file in the latest XML format
	 * 
	 * @param mappingFile
	 *            file handle to save to
	 * @throws IOException
	 */
	public void save(Mapping mapping, File mappingFile) throws IOException {
		// XStream xstream = newXStream();

		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(mappingFile));
			out.write("<?xml version =\"1.0\" encoding=\"UTF-8\" ?>"
					+ System.getProperty("line.separator"));
			out.write(xstream.toXML(mapping));
			out.close();
		} catch (IOException IOe) {
			mappingController.message(
					MESSAGELEVEL.ERROR,
					"Saving of the mapping failed: "
							+ IOe.getLocalizedMessage());
			throw IOe;
		}
	}

	/**
	 * Loads the mapping from the given XML file and converts it to the latest
	 * version if necessary
	 * 
	 * @param mappingFile
	 *            file to load from
	 * @return Mapping the loaded mapping
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws URISyntaxException
	 * @throws SAXException
	 */
	public Mapping load(File mappingFile) throws IOException,
			ParserConfigurationException, SAXException, URISyntaxException {
		// First load the file contents
		StringBuilder contents = read(mappingFile);

		// Now check the version of the mapping stored in the file
		int version = versionCheck(contents);

		/*
		 * And call the correct function to instantiate the mapping instance and
		 * possibly convert it to the latest version
		 */

		switch (version) {
		case 1:
			return loadLegacy1(contents);
		default:
			return loadLatest(contents);
		}
	}

	/**
	 * Checks the version of the of the mapping definition stored in the XML
	 * contents provided
	 * 
	 * @param contents
	 *            XML string of the file contents
	 * @return int <0 if latest, 1 for Mapping definition until Dec 2010
	 *         including visualization
	 * 
	 */
	private int versionCheck(StringBuilder contents) {
		/*
		 * At the moment there are 2 versions. Legacy1 contains a
		 * 'mutabletreenode' in line 4, the latest is very clean
		 */

		if (contents.indexOf("<javax.swing.tree.DefaultMutableTreeNode>") > 0)
			// Legacy 1 including JGraph visualization
			return 1;
		else
			// latest
			return 0;
	}

	/**
	 * Instantiates the latest version of the Mapping definition from the given
	 * XML file contents
	 * 
	 * @param contents
	 *            XML file contents
	 * @return Mapping instance
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	private Mapping loadLatest(StringBuilder contents) throws IOException,
			ParserConfigurationException {
		// now parse the XML and store the resulting mapping instance
		try {
			// If we would like xstream to process annotations, per class:
			// xstream.processAnnotations(Mapping.class);

			return (Mapping) xstream.fromXML(contents.toString());
		} catch (ConversionException e) {
			mappingController
					.message(
							MESSAGELEVEL.ERROR,
							"We could not load the given mapping. Are you sure the file "
									+ "contents is correct and made with a current version of the "
									+ "application? This is the exact error we got: "
									+ e.getLocalizedMessage());
			/*-
			message(MESSAGELEVEL.NOTICE, "Special message (December 2010): \n" +
					"If you just upgraded to this version of XESame mappings created with the older XESame version are not compatible. \n" +
					"We are sorry for the inconvenience but we made major changes to the back-end of XESame making it more fast and robust. \n" +
					"Unfortunately, this meant that we also had to change the file format.");
			 */
			throw e;
		}
	}

	/**
	 * Loads a legacy 1 Mapping XML definition and returns the latest Mapping
	 * definition
	 * 
	 * @param contents
	 * @return Mapping instance
	 */
	private Mapping loadLegacy1(StringBuilder contents) {
		XStream xstream = newXStreamLegacy1();

		/*
		 * There are some hard references to
		 * org.processmining.mapper.mapping.GeneralMappingItem which should be
		 * replaced
		 */
		contents = Utils.replaceAll(contents,
				"org.processmining.mapper.mapping",
				"org.processmining.mapper.mapping.legacy1");

		// System.out.println(contents);

		org.processmining.mapper.mapping.legacy1.Mapping legacy1Mapping = (org.processmining.mapper.mapping.legacy1.Mapping) xstream
				.fromXML(contents.toString());

		// And then convert the legacy 1 version to the latest version
		return Legacy1Convertor.parseMapping(legacy1Mapping);
	}

	private StringBuilder read(File mappingFile) throws IOException {
		/*
		 * Main contents from
		 * http://www.javapractices.com/topic/TopicAction.do?Id=42
		 */

		/*
		 * We initially start with 4096 chars, the smallest size for an empty
		 * mapping file without any definitions
		 */
		StringBuilder contents = new StringBuilder(4096);

		try {
			// use buffering, reading one line at a time
			// FileReader always assumes default encoding is OK!
			BufferedReader input = new BufferedReader(new FileReader(
					mappingFile));
			try {
				String line = null; // not declared within while loop
				/*
				 * readLine is a bit quirky : it returns the content of a line
				 * MINUS the newline. it returns null only for the END of the
				 * stream. it returns an empty String if two newlines appear in
				 * a row.
				 */
				while ((line = input.readLine()) != null) {
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			mappingController.message(MESSAGELEVEL.ERROR,
					"We could not load the mapping due to the following error: "
							+ ex.getLocalizedMessage());
			throw (ex); // pass it on...
		}

		return contents;
	}

}
