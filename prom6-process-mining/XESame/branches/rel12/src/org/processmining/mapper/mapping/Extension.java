/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.Vector;

/**
 * An object that relates to an XES Extension 
 * and contains some information about the attributes 
 * defined by the extension 
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Extension {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Mapping mapping;

	/**
	 * Gives the mapping instance the extension belongs to (NOTE: can return
	 * null but shouldn't)
	 * 
	 * @return the mapping
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Mapping getMapping() {
		return mapping;
	}

	/**
	 * @param mapping
	 *            the mapping to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null)
			name = "";
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String prefix;

	/**
	 * @return the prefix
	 */
	public String getPrefix() {
		if (prefix == null)
			prefix = "";
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String URI;

	/**
	 * @return the URI
	 */
	public String getURI() {
		if (URI == null)
			URI = "";
		return URI;
	}

	/**
	 * @param URI
	 *            the URI to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setURI(String URI) {
		this.URI = URI;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Attribute> attributes;

	/**
	 * @return the attributes
	 */
	public Vector<Attribute> getAttributes() {
		if (attributes == null)
			attributes = new Vector<Attribute>();
		return attributes;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setAttributes(Vector<Attribute> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Adds the given attribute to the list of attributes and updates the
	 * reference in the attribute
	 * 
	 * @param newAttribute
	 *            the attributes to add to the list of attributes
	 * @author Joos Buijs
	 */
	public void addAttribute(Attribute newAttribute) {
		if (!getAttributes().contains(newAttribute)) {
			newAttribute.setExtension(this);
			getAttributes().add(newAttribute);
		}
	}

	/**
	 * Returns the string 'Extension: ' + name
	 */
	public String toString() {
		return "Extension: " + name;
	}
}