/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;

import org.deckfour.uitopia.ui.components.ViewHeaderBar;
import org.deckfour.uitopia.ui.components.ViewHeaderButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.controller.XESameConstants;
import org.xml.sax.SAXException;

/**
 * The main header includes the new/open/save/help buttons
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MainHeader extends ViewHeaderBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3879786265492106566L;

	private File mappingFile = null; // the file last loaded from or saved to
	private boolean hasBeenSaved = false;

	private JDialog aboutDialog;
	private JPanel aboutContentPane;
	private JTextPane aboutTextPane;

	private ViewHeaderButton saveButton;
	private UIController uiController;

	private JFileChooser mappingFileChooser;

	private ViewHeaderButton openButton;

	private ViewHeaderButton saveAsButton;

	private ViewHeaderButton newFileButton;

	private ViewHeaderButton helpButton;

	MainHeader(UIController uiController) {
		super("");

		// Load the file last saved to in the previous session
		mappingFile = new File(uiController.getConfiguration().get(
				UIController.SAVE_LOCATION_MAPPINGFILE, "./"));

		this.uiController = uiController;
		// this.header = new ViewHeaderBar("Workspace");

		this.addComponent(getNewFileButton());
		this.addComponent(getOpenButton());
		this.addComponent(getSaveButton());
		this.addComponent(getSaveAsButton());
		this.addComponent(getHelpButton());
	}

	private JComponent getHelpButton() {
		if (helpButton == null) {
			helpButton = new ViewHeaderButton(
					ImageLoader.load("button_help_30x30_white.png"), "Help ");

			helpButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					getAboutDialog();
					JDialog aboutDialog = getAboutDialog();
					aboutDialog.pack();
					Point loc = uiController.getMainView().getLocation();
					// Point loc = getApplicationFrame().getLocation();
					loc.translate(20, 20);
					aboutDialog.setLocation(loc);
					aboutDialog.setVisible(true);
				}
			});

		}

		return helpButton;
	}

	private JComponent getNewFileButton() {
		if (newFileButton == null) {
			newFileButton = new ViewHeaderButton(
					ImageLoader.load("button_newFile_30x30_white.png"), "New ");

			newFileButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					uiController.getHandler().initializeMapping();
					uiController.getMainView().updateData();
				}
			});
		}

		return newFileButton;
	}

	private JComponent getSaveAsButton() {
		if (saveAsButton == null) {
			saveAsButton = new ViewHeaderButton(
					ImageLoader.load("button_saveAs_30x30_white.png"),
					"Save As ");

			saveAsButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {

					// Always ask for a file location
					int returnVal = getMappingFileChooser().showSaveDialog(
							saveAsButton);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						mappingFile = mappingFileChooser.getSelectedFile();
						uiController.getConfiguration().set(
								UIController.SAVE_LOCATION_MAPPINGFILE,
								mappingFile.getAbsolutePath());

						// And now write it to there
						try {
							if (!mappingFile.getName().endsWith(".mapping")) {
								mappingFile = new File(mappingFile.getPath()
										+ ".mapping");
							}
							uiController.getHandler().save(mappingFile);
							hasBeenSaved = true;

							// Show a confirmation message
							JOptionPane.showMessageDialog(saveAsButton,
									"The mapping has been successfully saved to "
											+ mappingFile.getName(),
									"Successfull save",
									JOptionPane.INFORMATION_MESSAGE);
						} catch (IOException e1) {
							JOptionPane
									.showMessageDialog(
											saveAsButton,
											"Sorry, we could not save the mapping to a file. We encountered the following error: \n"
													+ e1,
											"Error while saving mapping",
											JOptionPane.ERROR_MESSAGE);
						}
					} else {
						System.out.println("Save command cancelled by user.");
					}
				}
			});
		}

		return saveAsButton;
	}

	private JComponent getSaveButton() {
		if (saveButton == null) {
			saveButton = new ViewHeaderButton(
					ImageLoader.load("button_save_30x30_white.png"), "Save ");

			/**/
			saveButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					/*
					 * Ask for a file location if not known yet or if no save
					 * action has been performed in this session
					 */
					if (mappingFile == null || !hasBeenSaved) {
						int returnVal = getMappingFileChooser().showSaveDialog(
								saveButton);
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							mappingFile = mappingFileChooser.getSelectedFile();
							uiController.getConfiguration().set(
									UIController.SAVE_LOCATION_MAPPINGFILE,
									mappingFile.getAbsolutePath());
						} else {
							System.out
									.println("Save command cancelled by user.");
						}
					}

						// And now write it to there
						try {
							if (!mappingFile.getName().endsWith(".mapping")) {
								mappingFile = new File(mappingFile.getPath()
										+ ".mapping");
							}
							uiController.getHandler().save(mappingFile);
							hasBeenSaved = true;

							// Show a confirmation message
							JOptionPane.showMessageDialog(saveAsButton,
									"The mapping has been successfully saved to "
											+ mappingFile.getName(),
									"Successfull save",
									JOptionPane.INFORMATION_MESSAGE);
						} catch (IOException e1) {
							JOptionPane
									.showMessageDialog(
											saveButton,
											"Sorry, we could not save the mapping to a file. We encountered the following error: \n"
													+ e1,
											"Error while saving mapping",
											JOptionPane.ERROR_MESSAGE);
						}

					}
			});/**/
		}
		return saveButton;

	}

	private JComponent getOpenButton() {
		if (openButton == null) {
			openButton = new ViewHeaderButton(
					ImageLoader.load("button_open_30x30_white.png"), "Open ");

			openButton.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent e) {
					// Always ask for a file location
					int returnVal = getMappingFileChooser().showOpenDialog(
							openButton);
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						mappingFile = getMappingFileChooser().getSelectedFile();
						// persist:
						uiController.getConfiguration().set(
								UIController.SAVE_LOCATION_MAPPINGFILE,
								mappingFile.toString());
						try {
							uiController.getHandler()
									.setMapping(
											uiController.getHandler().load(
													mappingFile));
						} catch (IOException eIO) {
							JOptionPane
									.showMessageDialog(
											openButton,
											"Sorry, we could not open the mapping from the given file. We encountered the following error: \n"
													+ eIO,
											"Error while opening mapping",
											JOptionPane.ERROR_MESSAGE);
						} catch (ParserConfigurationException eParser) {
							JOptionPane
									.showMessageDialog(
											openButton,
											"Sorry, we could not open the mapping from the given file. We encountered the following error while parsing one of the extension definitions: \n"
													+ eParser,
											"Error while opening mapping: extension parser error",
											JOptionPane.ERROR_MESSAGE);
						} catch (SAXException eSAX) {
							JOptionPane
									.showMessageDialog(
											openButton,
											"Sorry, we could not open the mapping from the given file. We encountered the following error while parsing the XML of one of the extensions: \n"
													+ eSAX,
											"Error while opening mapping: parsing XML",
											JOptionPane.ERROR_MESSAGE);
						} catch (URISyntaxException eURI) {
							JOptionPane
									.showMessageDialog(
											openButton,
											"Sorry, we could not open the mapping from the given file. We encountered the following error while parsing the URI of one of the extensions: \n"
													+ eURI,
											"Error while opening mapping: extension URI",
											JOptionPane.ERROR_MESSAGE);
						}

						// And now update the GUI!
						uiController.getMainView().updateData();
					} else {
						System.out
								.println("Opening of file cancelled by user.");
					}

				}

			});
		}

		return openButton;
	}

	/**
	 * This method initializes aboutDialog
	 * 
	 * @return javax.swing.JDialog
	 */
	private JDialog getAboutDialog() {
		if (aboutDialog == null) {
			// TODO move to overlay dialog with 1 button
			aboutDialog = new JDialog();
			aboutDialog.setTitle("About");
			aboutDialog.setContentPane(getAboutContentPane());
		}
		return aboutDialog;
	}

	/**
	 * This method initializes aboutContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAboutContentPane() {
		if (aboutContentPane == null) {
			aboutContentPane = new JPanel();
			aboutContentPane.setLayout(new BorderLayout());
			aboutContentPane.add(getAboutTextPane(), BorderLayout.EAST);
		}
		return aboutContentPane;
	}

	/**
	 * This method initializes aboutTextPane
	 * 
	 * @return javax.swing.JTextPane
	 */
	private JTextPane getAboutTextPane() {
		if (aboutTextPane == null) {
			aboutTextPane = new JTextPane();
			// let Java know it will be HTML
			aboutTextPane.setContentType("text/html");

			aboutTextPane
					.setText("<b>XES Mapper application</b> (XESame) <br>"
							+ "<b>Version:</b> "+XESameConstants.APP_VERSION+"<br>"
							+ "<b>Build:</b> Revision "
							+ XESameConstants.SVN_REVISION
							+ " ("
							+ XESameConstants.SVN_DATE_STRING
							+ ")<br>"
							+ "<b>Author:</b> Joos Buijs (j.c.a.m.buijs@tue.nl)<br>"
							// mailto: doesn't work...
							+ "<b>License:</b> EPL (<a href=+\"http://www.eclipse.org/legal/epl-v10.html\" target=\"_blank\">"
							+ "http://www.eclipse.org/legal/epl-v10.html</a>)<br><br>"
							+ "More help can be found at <a href=+\"http://prom.win.tue.nl/research/wiki/xesame/start\" target=\"_blank\">"
							+ "http://prom.win.tue.nl/research/wiki/xesame/start</a> <br><br>"
					// +
					// "XESame is developed in the context of a Master project at Eindhoven University of Technology<br>"
					// +
					// "Master project title: <i>Mapping Data Sources to XES in a Generic Way</i>"
					);
			aboutTextPane.setEditable(false);
		}
		return aboutTextPane;
	}

	/**
	 * This method initializes the file chooser for loading and saving files
	 * 
	 * @return
	 */
	private JFileChooser getMappingFileChooser() {
		if (mappingFileChooser == null) {
			mappingFileChooser = new JFileChooser();
			// set the root to this project's folder submap
			mappingFileChooser.setCurrentDirectory(new File(uiController
					.getConfiguration().get(
							UIController.SAVE_LOCATION_MAPPINGFILE, "")));

			// A file filter so only files ending with 'mapping' can be
			// saved/loaded (although its not automagically added when saving)
			FileFilter filter = new FileNameExtensionFilter("Mapping",
					"mapping");
			mappingFileChooser.addChoosableFileFilter(filter);
		}

		return mappingFileChooser;
	}

}
