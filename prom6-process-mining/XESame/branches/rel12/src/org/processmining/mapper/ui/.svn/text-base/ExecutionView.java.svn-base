/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.deckfour.uitopia.ui.components.TiledPanel;
import org.deckfour.uitopia.ui.main.Viewable;
import org.deckfour.uitopia.ui.util.ImageLoader;

/**
 * The execution view calls the execution browser to 
 * build up the GUI
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ExecutionView extends JPanel implements Viewable {

	private static final long serialVersionUID = 7445448707084672645L;

	private JPanel contents;

	// The tab browser
	private volatile ExecutionBrowser executionBrowser;

	public ExecutionView(final UIController controller) {
		this.setLayout(new BorderLayout());
		this.setOpaque(true);
		this.setBorder(BorderFactory.createEmptyBorder());

		contents = new TiledPanel(ImageLoader.load("tile_corkboard.jpg"));
		contents.setBorder(BorderFactory.createEmptyBorder(20, 40, 20, 40));
		contents.setLayout(new BorderLayout());
		this.executionBrowser = new ExecutionBrowser(controller);
		contents.add(executionBrowser, BorderLayout.CENTER);
		this.add(contents, BorderLayout.CENTER);
	}

	public void viewFocusGained() {
	}

	public void viewFocusLost() {
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		executionBrowser.updateData();
	}

	public ExecutionBrowser getExecutionBrowser() {
		return executionBrowser;
	}
}
