/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;

//import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;

/**
 * Cache database controller class
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class CacheDBController {

	private MappingController controller;
	private String connectionString;
	// We also have a connection to our cache DB
	private java.sql.Connection cacheDBConnection;

	/**
	 * Initialize a new instance with a reference to the controller
	 * 
	 * @param controller
	 */
	public CacheDBController(MappingController controller) {
		this.controller = controller;

		// Initialize the connection string
		if (connectionString == null) {
			connectionString = "jdbc:derby:cacheDB";
		}
	}

	/**
	 * Initializes the cache database by creating a new one if it does not
	 * exist, drop all the tables we want to add and then create the tables we
	 * need
	 * 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void initializeCacheDB() throws ClassNotFoundException, SQLException {
		String query = "";

		// If we crashed before and forgot to close the connection, we're in
		// trouble
		// So, try to close the connection
		try {
			cacheDBConnection.close();
		} catch (Exception e) {
			// nothing! if we can not close it, we must be able to open it,
			// right?
		}

		// First things first: connect (and possibly create)
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
			cacheDBConnection =
					DriverManager.getConnection(connectionString
							+ ";create=true;autocommit=false");
			// TODO TEST difference
			// cacheDBConnection.setAutoCommit(false);

		} catch (ClassNotFoundException e1) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We can not locate the Apache Derby Database library. "
									+ "Please make sure it is present in the /lib dir!");
			throw e1;
		} catch (SQLException e1) {
			controller.message(
					MESSAGELEVEL.ERROR,
					"We could not instantiate a connection to our cache DB. "
							+ "We encountered the following error: "
							+ e1.getLocalizedMessage());
			throw e1;
		} catch (InstantiationException e) {
			controller.message(
					MESSAGELEVEL.ERROR,
					"We could not instantiate a new instance of our cache DB. "
							+ "We encountered the following error: "
							+ e.getLocalizedMessage());
		} catch (IllegalAccessException e) {
			controller.message(
					MESSAGELEVEL.ERROR,
					"We could not access our cache DB. "
							+ "We encountered the following error: "
							+ e.getLocalizedMessage());
		}

		Statement cacheDBstmt;
		try {
			cacheDBstmt = cacheDBConnection.createStatement();
		} catch (SQLException e1) {
			controller.message(
					MESSAGELEVEL.ERROR,
					"We could not create a statement to run queries on our cache DB. "
							+ "We encountered the following error: "
							+ e1.getLocalizedMessage());
			throw e1;
		}

		/*-
		 * We can only create tables that do not exist but we can only drop
		 * tables that do exist...
		 * Solution: try to drop a table, if it does not exist, hide the error.
		 * Either way, the table will not exist afterwards
		 */
		// Tables to drop:
		String[] tablenames = { "XLog", "XTrace", "XEvent", "XAttribute" };

		// Loop through them in reverse order otherwise the foreign keys don't
		// have anything to point to
		for (int i = tablenames.length - 1; i >= 0; i--) {
			try {
				cacheDBstmt.executeUpdate("DROP TABLE " + tablenames[i]);
			} catch (SQLException e) {
				// hide a 'table does not exist' error, but throw the others
				if (!e.getSQLState().equals("42Y55")) {
					controller.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while deleting tables "
									+ "from our cache DB to start afresh: "
									+ e.getLocalizedMessage());
					throw e;
				}
			}
		}

		/*
		 * Now we create the tables in the cache DB We store log (only 1
		 * record), trace, event and attribute attributes as key,value, type and
		 * relate them to each other
		 */

		// Create Log table
		query = "CREATE TABLE XLog (ID INTEGER NOT NULL PRIMARY KEY)";
		try {
			cacheDBstmt.executeUpdate(query);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while creating the log table in our cache DB: "
									+ e.getLocalizedMessage());
			throw e;
		}

		// Trace table
		query =
				"CREATE TABLE XTrace (traceID VARCHAR("
						+ XESameConstants.cacheTraceIDLength
						+ ") NOT NULL PRIMARY KEY, "
						+ "LogID INTEGER NOT NULL, "
						+ "FOREIGN KEY (LogID) REFERENCES XLog(ID))";
		try {
			cacheDBstmt.executeUpdate(query);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while creating the trace table in our cache DB: "
									+ e.getLocalizedMessage());
			throw e;
		}

		// Event table
		query =
				"CREATE TABLE XEvent (ID INTEGER NOT NULL PRIMARY KEY "
						+ "GENERATED ALWAYS AS IDENTITY (START WITH 0, INCREMENT BY 1), "
						+ "TraceID VARCHAR("
						+ XESameConstants.cacheTraceIDLength + ") NOT NULL, "
						+ "OrderValue VARCHAR("
						+ XESameConstants.cacheTraceIDLength + ") NOT NULL, "
						+ "FOREIGN KEY (TraceID) REFERENCES XTrace(TraceID))";
		try {
			cacheDBstmt.executeUpdate(query);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while creating the event table in our cache DB: "
									+ e.getLocalizedMessage());
			throw e;
		}

		// Index on the events table
		query = "CREATE INDEX orderValue ON XEvent(OrderValue)";
		try {
			cacheDBstmt.executeUpdate(query);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while creating an index on the event table in our cache DB: "
									+ e.getLocalizedMessage());
			throw e;
		}

		// Attribute table, a bigger query!
		// indexes are automatically created on foreign key fields (see JavaDB
		// tuning PDF)
		query =
				"CREATE TABLE XAttribute ("
						+ "ID INTEGER NOT NULL PRIMARY KEY "
						+ "GENERATED ALWAYS AS IDENTITY (START WITH 0, INCREMENT BY 1), "
						// reference cols
						+ "LogID INTEGER, "
						+ "TraceID VARCHAR("
						+ XESameConstants.cacheTraceIDLength
						+ "), "
						+ "EventID INTEGER, "
						+ "AttributeID INTEGER, "
						// value cols
						+ "XKey VARCHAR(255), "
						+ "XValue VARCHAR("
						+ XESameConstants.cacheAttributeValueLength
						+ "), "
						+ "XType VARCHAR(255), "
						// to prevent costly joins we keep track of meta
						+ "isMeta BOOLEAN DEFAULT FALSE, "
						// Connect keys
						+ "FOREIGN KEY (LogID) REFERENCES XLog(ID), "
						+ "FOREIGN KEY (TraceID) REFERENCES XTrace(traceID), "
						+ "FOREIGN KEY (EventID) REFERENCES XEvent(ID), "
						+ "FOREIGN KEY (AttributeID) REFERENCES XAttribute(ID))";
		try {
			cacheDBstmt.executeUpdate(query);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"We encountered the following error while creating the attribute table in our cache DB: "
									+ e.getLocalizedMessage());
			throw e;
		}

		cacheDBstmt.close();
	}

	/**
	 * Returns all the traces from the cache DB
	 * 
	 * @return ResultSet all trace columns ordered by traceID
	 * @throws SQLException
	 */
	public ResultSet getTraces() throws SQLException {
		// No prepared statement here, no parameters...
		Statement traceStatement = cacheDBConnection.createStatement();
		return traceStatement
				.executeQuery("SELECT * FROM XTrace ORDER BY traceID");
	}

	/**
	 * Returns all the events for the given traceID in the specified order
	 * 
	 * @param traceID
	 *            for which trace to get the events
	 * @param ascending
	 *            if true sort ascending on the order value
	 * @return ResultSet all event columns ordered by orderValue
	 * @throws SQLException
	 */
	public ResultSet getEvents(String traceID, boolean ascending)
			throws SQLException {
		String orderDirection = "ASC";
		if (!ascending) {
			orderDirection = "DESC";
		}
		PreparedStatement eventStatement =
				cacheDBConnection
						.prepareStatement("SELECT * FROM XEvent WHERE TraceID = ? "
								+ "ORDER BY orderValue "
								+ orderDirection
								+ ", ID ASC");

		eventStatement.setString(1, traceID);

		return eventStatement.executeQuery();
	}

	/**
	 * Returns all the attribute information present for the given type and id
	 * 
	 * @param type
	 *            type of parent element to get the attributes for
	 * @param id
	 *            id of the parent
	 * @return ResultSet all the attribute columns
	 * @throws SQLException
	 */
	public ResultSet getAttributesFor(String type, String id)
			throws SQLException {
		/*
		 * First, get all the attributes for the given element
		 */

		/*
		 * For traces the id is a string, for all the others it must be an
		 * integer in the query
		 */

		String idPart = "";
		if (type == "Trace")
			idPart = "" + id + "";
		// FIXME test for char traceID, used to be like:
		// idPart = "\'" + id + "\'";
		else
			idPart = id;

		/*
		 * Get the attributes
		 */

		String query = "SELECT * FROM XAttribute WHERE " + type + "ID = ?";

		PreparedStatement attributeStmt;
		try {
			attributeStmt =
					cacheDBConnection.prepareStatement(query,
							ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);

			attributeStmt.setString(1, idPart);
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"There was an error while creating a statement (not a query yet) on the cacheDB: "
									+ e.getLocalizedMessage());
			cacheDBConnection.close();
			throw e;
		}

		ResultSet attributesResultSet;
		try {
			attributesResultSet = attributeStmt.executeQuery();
		} catch (SQLException e) {
			controller.message(MESSAGELEVEL.ERROR,
					"There was an error while executing this query: \n" + query
							+ "\n on the cacheDB: " + e.getLocalizedMessage());
			cacheDBConnection.close();
			throw e;
		}

		return attributesResultSet;
	}

	/**
	 * Close the connection to the cache DB
	 * 
	 * @throws SQLException
	 */
	public void closeConnection() throws SQLException {
		if (cacheDBConnection != null) {
			cacheDBConnection.close();

			// And shutdown Derby

			try {
				DriverManager
						.getConnection(connectionString + ";shutdown=true");
			} catch (Exception e) {
				// Shutdowns always raise exceptions but that is ok
			}
		}
	}

	/**
	 * Insert a new log record in the cache DB
	 * 
	 * @return ID of the log inserted
	 * @throws SQLException
	 */
	public String insertLog() throws SQLException {
		String itemID = "";

		Statement insertStatement = cacheDBConnection.createStatement();
		insertStatement.executeUpdate("INSERT INTO XLog (ID) VALUES (0)",
				Statement.RETURN_GENERATED_KEYS);

		ResultSet itemKey = insertStatement.getGeneratedKeys();
		if (itemKey.next()) {
			itemID = itemKey.getString(1);
		}

		insertStatement.close();

		if (itemID == null)
			return "0";
		else
			return itemID;
	}

	/**
	 * Insert a new trace record in the cache DB with the given id
	 * 
	 * @param traceID
	 *            traceID of the trace
	 * @throws SQLException
	 */
	public void insertTrace(String traceID) throws SQLException {
		PreparedStatement insertStatement =
				cacheDBConnection
						.prepareStatement("INSERT INTO XTrace (TraceID,LogID) "
								+ "VALUES (?, 0)");

		insertStatement.setString(1, traceID);

		controller.message(MESSAGELEVEL.VERBOSE,
				"We will insert a new trace with traceID " + traceID + ".");

		try {
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			if (e.getSQLState().equals("23505"))
				controller
						.message(
								MESSAGELEVEL.NOTICE,
								"We encountered a duplicate trace identifier ("+traceID+"). " +
								"This might mean that you chose an incorrect identifying field. " +
								"However, it might also be a side effect (caused by the DISTINCT " +
								"keyword in the SQL query). We just thought you should know.");
			else
				throw e;
		}

		insertStatement.close();
	}

	/**
	 * Insert a new event record in the cache DB for the given trace and with
	 * the given order value
	 * 
	 * @param traceID
	 *            trace to connect to
	 * @param orderValue
	 *            value to order by when creating the log
	 * @return ID of the inserted record
	 * @throws SQLException
	 */
	public String insertEvent(String traceID, String orderValue)
			throws SQLException {
		String itemID = "";

		PreparedStatement addEventStatement =
				cacheDBConnection
						.prepareStatement(
								"INSERT INTO XEvent (traceID, orderValue) VALUES (?, ?)",
								Statement.RETURN_GENERATED_KEYS);

		// Add the attribute values
		addEventStatement.setString(1, traceID);
		addEventStatement.setString(2, orderValue);

		// Statement insertStatement = cacheDBConnection.createStatement();
		try {
			addEventStatement.execute();
			/*
			 * * / insertStatement.executeUpdate(
			 * "INSERT INTO XEvent (traceID, orderValue) VALUES ('" + traceID +
			 * "', '" + orderValue + "')", Statement.RETURN_GENERATED_KEYS); /*
			 */
		} catch (SQLException e) {
			throw e;
		}

		controller.message(MESSAGELEVEL.VERBOSE,
				"We will insert a new event (with order value " + orderValue
						+ ") into the cache DB for trace " + traceID + "");

		ResultSet itemKey = addEventStatement.getGeneratedKeys();
		if (itemKey.next()) {
			itemID = itemKey.getString(1);
		}

		addEventStatement.close();

		return itemID;
	}

	/**
	 * Insert a new attribute record in the cache DB
	 * 
	 * @param itemType
	 *            type of the parent
	 * @param ID
	 *            ID of the parent
	 * @param key
	 *            key of the attribute
	 * @param value
	 *            value of the attribute
	 * @param type
	 *            type of the attribute
	 * @return ID of the inserted attribute
	 * @throws SQLException
	 */
	public String insertAttribute(String itemType, String ID, String key,
			String value, String type) throws SQLException {
		/*-
		 * TODO cache insert requests and insert them with X at the time
		 * - but be sure to have the last of them too!!!
		 * - and how about meta attributes?
		 * - and it will return an ID... (fake it?)
		 */
		// Insert the given attribute
		String parentColumn = itemType + "ID";
		PreparedStatement addAttributeStatement =
				cacheDBConnection.prepareStatement("INSERT INTO XAttribute ( "
						+ parentColumn
						+ " , XKey, XValue, XType) VALUES (?, ?, ?, ?)",
						Statement.RETURN_GENERATED_KEYS);

		// Add the attribute values
		addAttributeStatement.setString(1, ID);
		addAttributeStatement.setString(2, key);
		addAttributeStatement.setString(3, value);
		addAttributeStatement.setString(4, type);

		controller.message(MESSAGELEVEL.VERBOSE,
				"We will insert a new attribute (" + key + " with value "
						+ value + ") into the cache DB for the " + itemType
						+ " item with ID " + ID + "");

		try {
			addAttributeStatement.execute();
		} catch (SQLException e) {
			controller.message(
					MESSAGELEVEL.ERROR,
					"There is an error in the query to add an attribute in our cache DB, "
							+ "this is the error we got: "
							+ e.getLocalizedMessage());
			throw e;
		}

		// Get the id of the last inserted attribute
		String lastAttributeID = "";
		ResultSet lastKeys;
		try {
			lastKeys = addAttributeStatement.getGeneratedKeys();
			if (lastKeys.next()) {
				lastAttributeID = lastKeys.getString(1);
			}
		} catch (SQLException e) {
			controller
					.message(
							MESSAGELEVEL.ERROR,
							"There is an error when getting the identifier for the attribute "
									+ "we just inserted in our cache DB, this is the error we got: "
									+ e.getLocalizedMessage());
			throw e;
		}

		// If this attribute belongs to another attribute, update that attribute
		// to be a meta one
		if (itemType.equalsIgnoreCase("Attribute")) {
			try {
				PreparedStatement updateMetaAtt =
						cacheDBConnection
								.prepareStatement("UPDATE XAttribute SET isMeta = TRUE WHERE ID=?");

				updateMetaAtt.setString(1, ID);

				controller.message(MESSAGELEVEL.VERBOSE,
						"We will update attribute " + ID
								+ " to be a meta attribute.");

				updateMetaAtt.executeUpdate();
				updateMetaAtt.close();
			} catch (SQLException e) {
				controller
						.message(
								MESSAGELEVEL.ERROR,
								"There is an error when updating the parent attribute "
										+ "of the given one to be a meta one in our cache DB, "
										+ "this is the error we got: "
										+ e.getLocalizedMessage());
				throw e;
			}
		}

		addAttributeStatement.close();

		return lastAttributeID;
	}

	/**
	 * Displays the current contents of the cache DB's tables on the system
	 * console
	 */
	public void showContents() {
		if (!controller.getMessageLevel().equals(MESSAGELEVEL.VERBOSE))
			return;

		String[] cacheTables = { "XLog", "XTrace", "XEvent", "XAttribute" };
		for (String table : cacheTables) {
			controller.message(MESSAGELEVEL.VERBOSE, "---- Contents of table "
					+ table);
			try {
				Statement cacheStmt = cacheDBConnection.createStatement();
				cacheStmt.execute("SELECT * FROM " + table + "");
				ResultSet result = cacheStmt.getResultSet();
				controller.displayResultSet(result);
			} catch (SQLException e) {
				controller.message(MESSAGELEVEL.ERROR,
						"Error while trying to display the contents of the table "
								+ table);
				e.printStackTrace();
			}
		}
	}

	/**
	 * Changes the system directory of the Derby database
	 * 
	 * @param location
	 */
	public void setCacheDir(String location) {
		this.connectionString = "jdbc:derby:" + location + "/cacheDB";
	}

	/**
	 * Returns the number of traces currently in the cache database
	 * 
	 * @return The number of traces of -1 in case of an error
	 * @throws SQLException
	 */
	public int countTraces() throws SQLException {
		Statement traceStatement = cacheDBConnection.createStatement();
		ResultSet traceCount =
				traceStatement.executeQuery("SELECT COUNT(*) FROM XTrace");
		// traceStatement.close();
		if (traceCount.next()) {
			return traceCount.getInt(1);
		} else
			return -1;
	}
}