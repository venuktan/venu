/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.processmining.mapper.mapping.Event;
import org.processmining.mapper.mapping.GeneralMappingItem;
import org.processmining.mapper.mapping.Attribute;
import org.processmining.mapper.mapping.Link;
import org.processmining.mapper.mapping.Log;
import org.processmining.mapper.mapping.Properties;
import org.processmining.mapper.mapping.Trace;

/**
 * Model that shows the properties of an item in a nice editable table
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class PropertiesTableModel extends AbstractTableModel {
	// We have 2 columns: "Property" and "Value"
	private String[] columnNames = new String[] { "Property", "Value" };

	// Stores whether there are properties (only false for Attributes)
	private boolean propertiesSet = false;
	// private Vector<Link> links = new Vector<Link>();
	private Vector<String> propertyKeys = new Vector<String>();
	// a reference to the 'parent' of the properties
	private GeneralMappingItem parent = new GeneralMappingItem();

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Class<?> getColumnClass(int col) {
		return String.class;
	}

	@Override
	public int getRowCount() {
		int count = 0;
		if (propertiesSet) {
			count += propertyKeys.size();
		}

		// Plus the number of links
		count += parent.getProperties().getLinks().size();
		return count;
	}

	@Override
	public Object getValueAt(int row, int col) {
		// First the left column with the keys
		if (col == 0) {
			if (propertiesSet) {
				if (row < propertyKeys.size())
					return propertyKeys.get(row);
				else
					return "Link";
			} else
				return null;
		} else {
			if (propertiesSet) {
				if (row < propertyKeys.size()) {
					if (propertyKeys.get(row).equalsIgnoreCase("from")) {
						return parent.getProperties().getFrom();
					} else if (propertyKeys.get(row).equalsIgnoreCase("where")) {
						return parent.getProperties().getWhere();
					} else if (propertyKeys.get(row)
							.equalsIgnoreCase("traceID")) {
						return parent.getProperties().getTraceID();
					} else if (propertyKeys.get(row).equalsIgnoreCase(
							"eventorder")) {
						return parent.getProperties().getEventOrder();
					}
				} else {
					/*
					 * Get the link specification at the given row minus the
					 * number of normal properties
					 */
					return parent.getProperties().getLinks().get(
							row - propertyKeys.size()).getSpecification();
				}
			} else
				return "";
		}
		return "";
	}

	public void reloadValues() {
		reloadValues(parent);
	}

	public void reloadValues(GeneralMappingItem node) {
		if (node == null) {
			parent = new GeneralMappingItem();
		} else
			parent = node;

		// Make sure there are properties
		if (parent.getProperties() == null) {
			Properties properties = new Properties();
			properties.setGeneralmappingitem(parent);
			properties.setLinks(new Vector<Link>());
			parent.setProperties(properties);
		}

		// Set the correct properties for each type of node
		if (parent instanceof Log) {
			propertiesSet = true;
			// Set the keys of the properties a Log has
			propertyKeys.clear();
			propertyKeys.add("From");
			propertyKeys.add("Where");
		} else if (parent instanceof Trace) {
			propertiesSet = true;
			// Set the keys of the properties a Trace has
			propertyKeys.clear();
			propertyKeys.add("From");
			propertyKeys.add("Where");
			propertyKeys.add("TraceID");
		} else if (parent instanceof Event) {
			propertiesSet = true;
			// Set the keys of the properties an Event has
			propertyKeys.clear();
			propertyKeys.add("From");
			propertyKeys.add("Where");
			propertyKeys.add("TraceID");
			propertyKeys.add("EventOrder");
		} else if (parent instanceof Attribute) {
			propertiesSet = false; // we have no properties
			propertyKeys.clear(); // set an empty key list
		}

		// and update the table
		fireTableDataChanged();
	}

	public boolean isCellEditable(int row, int col) {
		/*
		 * We don't want the user to edit the 'key' column but the rest he can
		 * do, why not...
		 */
		if (col == 0) // first column is not editable
		{
			return false;
		} else {
			return true;
		}
	}

	public void setValueAt(Object value, int row, int col) {
		if (col == 0)
			return;

		if (propertiesSet) {
			if (row < propertyKeys.size()) {
				if (propertyKeys.get(row).equalsIgnoreCase("from")) {
					parent.getProperties().setFrom(value.toString());
				} else if (propertyKeys.get(row).equalsIgnoreCase("where")) {
					parent.getProperties().setWhere(value.toString());
				} else if (propertyKeys.get(row).equalsIgnoreCase("traceID")) {
					parent.getProperties().setTraceID(value.toString());
				} else if (propertyKeys.get(row).equalsIgnoreCase("eventorder")) {
					parent.getProperties().setEventOrder(value.toString());
				}
			} else {
				/*
				 * Get the link specification at the given row minus the number
				 * of normal properties
				 */
				parent.getProperties().getLinks()
						.get(row - propertyKeys.size()).setSpecification(
								value.toString());
			}
		} else {
			/*
			 * Get the link specification at the given row minus the number of
			 * normal properties
			 */
			parent.getProperties().getLinks().get(row - propertyKeys.size())
					.setSpecification(value.toString());
		}

		fireTableCellUpdated(row, col);
	}

	public void fireTableRowsInserted(int from, int to) {
		// Update our representation
		reloadValues();

		// Extra check, if there are 0 attributes to begin with, one of the two
		// is invalid and the table will not be updated so fix that
		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;

		super.fireTableRowsInserted(from, to); // now delegate the call
	}

	public Vector<Link> getLinks() {
		return parent.getProperties().getLinks();
	}

	/**
	 * Returns the selected Link instance or null if a non-link row is selected
	 * 
	 * @param selectedRow
	 * @return Link or null if a non-link row is selected
	 */
	public Link getLinkAt(int selectedRow) {
		if (selectedRow < propertyKeys.size()) {
			return null;
		} else {
			return parent.getProperties().getLinks().get(
					selectedRow - propertyKeys.size());
		}
	}
}
