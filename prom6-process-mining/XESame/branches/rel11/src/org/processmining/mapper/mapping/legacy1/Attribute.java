/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

/**
 * An instance of the attribute that belongs to a 
 * general mapping item
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class Attribute extends GeneralMappingItem // implements Cloneable
{
	private String key;

	/**
	 * @return the key
	 */
	public String getKey() {
		if (this.key == null)
			this.key = "";
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setKey(String key) {
		// We have certain requirements on the key:
		// No spaces! Replace them by '_'
		key = key.replace(' ', '_');
		// '|' also has a special meaning (tree level separator in our query
		// result column names, you'll see)
		key = key.replace('|', '\\');
		this.key = key;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String value;

	/**
	 * @return the value
	 */
	public String getValue() {
		if (value == null)
			value = "";
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setValue(String value) {
		// Replace " with ' otherwise ODBC will think it's an argument...
		value = value.replace("\"", "'");

		this.value = value;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Extension extension;

	/**
	 * Returns the extension this attribute is defined by. Please note: might
	 * return null if the attribute does not belong to an extension!
	 * 
	 * @return the extension
	 */
	public Extension getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setExtension(Extension extension) {
		// begin-user-code
		this.extension = extension;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String type;

	/**
	 * @return the type
	 */
	public String getType() {
		if (type == null)
			type = "";
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setType(String type) {
		// if we change the attribute type to float
		if (type.equalsIgnoreCase("float")) {
			// We need to replace the hard coded , to . in the value
			value = value.replace(',', '.');
		}

		this.type = type;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private GeneralMappingItem parent;

	/**
	 * @return the parent
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public GeneralMappingItem getParentItem() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setParent(GeneralMappingItem parent) {
		this.parent = parent;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Column> columns;

	/**
	 * @return the columns
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Vector<Column> getColumns() {
		// begin-user-code
		return columns;
		// end-user-code
	}

	/**
	 * @param columns
	 *            the columns to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setColumns(Vector<Column> columns) {
		// begin-user-code
		this.columns = columns;
		// end-user-code
	}

	/**
	 * Adds the given referred column to the list of columns used in the value
	 * specification. Als makes sure the given column has a backreference to
	 * this attribute instance
	 * 
	 * @param column
	 */
	public void addColumn(Column column) {
		if (!getColumns().contains(column)) {
			getColumns().add(column);
			// Set a back reference
			column.addAttributeItem(this);
		}
	}

	/**
	 * Removes the given column from our list of columns referred to, also
	 * updates the column's attribute reference
	 * 
	 * @param column
	 */
	public void removeColumn(Column column) {
		if (getColumns().contains(column)) {
			getColumns().remove(column);
			column.removeAttributeItem(this);
		}
	}

	/**
	 * @author Joos Buijs
	 * @return true if attribute has child attribute(s)
	 */
	public boolean isMeta() {
		if (this.getItemAttributes() == null) {
			return false; // we have no attributes
		} else {
			// If our attributes list exists but is empty, then we are not a
			// meta
			return !this.getItemAttributes().isEmpty();
		}
	}

	/**
	 * Returns the string 'Attribute ' + key
	 * 
	 * @return String
	 */
	public String toString() {
		// return "Attribute: " + this.key;
		return this.key;
	}

	/**
	 * Creates a clone of the current attribute NOTE: the extension and
	 * properties reference is the same!
	 */
	/*
	 * * / public Attribute clone() { return (Attribute) super.clone(); }/*
	 */
}