/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

/**
 * Link specification for linking two data source tables
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Link {
	/**
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String specification;

	/**
	 * @return the specification
	 */
	public String getSpecification() {
		if (specification == null)
			specification = "";
		return specification;
	}

	/**
	 * @param specification
	 *            the specification to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setSpecification(String specification) {
		this.specification = specification;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Properties properties;

	/**
	 * Returns the properties instance this link belongs to (NOTE: can return
	 * null but shouldn't)
	 * 
	 * @return the properties
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Properties getProperties() {
		return properties;
	}

	/**
	 * @param properties
	 *            the properties to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Returns the string 'Link for' + getParent().toString();
	 */
	public String toString() {
		String parentString = "UNSET";
		if (getProperties() != null) {
			parentString = getProperties().toString();
		}
		return "Link for " + parentString;
	}
}