/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.LinkedList;
import java.util.AbstractMap.SimpleEntry;

/**
 * The object which stores the connection to the data source
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Connection {
	// First, some old attributes for backwards compatibility
	@SuppressWarnings("unused")
	private transient String username;
	@SuppressWarnings("unused")
	private transient String password;

	private String columnSeperatorStart = "[";
	private String columnSeperatorEnd = "]";
	
	public void setColumnSeperatorStart(String columnSeperatorStart) {
		this.columnSeperatorStart = columnSeperatorStart;
	}

	public String getColumnSeperatorStart() {
		if(columnSeperatorStart == null)
		{
			columnSeperatorStart = "[";
		}
		return columnSeperatorStart;
	}

	public void setColumnSeperatorEnd(String columnSeperatorEnd) {
		this.columnSeperatorEnd = columnSeperatorEnd;
	}

	public String getColumnSeperatorEnd() {
		if(columnSeperatorEnd == null)
		{
			columnSeperatorEnd = "]";
		}
		return columnSeperatorEnd;
	}
	
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String URL;

	/**
	 * @return the URL
	 */
	public String getURL() {
		if (URL == null)
			URL = "";
		return URL;
	}

	/**
	 * @param URL
	 *            the URL to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setURL(String URL) {
		this.URL = URL;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String driver;

	/**
	 * @return the driver
	 */
	public String getDriver() {
		if (driver == null)
			driver = "";
		return driver;
	}

	/**
	 * @param driver
	 *            the driver to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * The location of the driver jar file
	 */
	private String driverLocation;

	/**
	 * Get the location of the driver jar file
	 * 
	 * @return String file location
	 */
	public String getDriverLocation() {
		return driverLocation;
	}

	/**
	 * Set the location of the driver jar file
	 * 
	 * @param driverLocation
	 */
	public void setDriverLocation(String driverLocation) {
		this.driverLocation = driverLocation;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Mapping mapping;

	/**
	 * Returns the mapping the connection belongs to (Note: could return null
	 * but shouldn't)
	 * 
	 * @return the mapping
	 * @!generated 
	 *             "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Mapping getMapping() {
		return mapping;
	}

	/**
	 * @param mapping
	 *            the mapping to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}

	/**
	 * Various table properties such as username and password
	 */
	private LinkedList<SimpleEntry<String, String>> properties;

	/**
	 * Set the connection properties
	 * 
	 * @param properties
	 */
	public void setProperties(LinkedList<SimpleEntry<String, String>> properties) {
		this.properties = properties;
	}

	/**
	 * Get the connection properties
	 * 
	 * @return
	 */
	public LinkedList<SimpleEntry<String, String>> getProperties() {
		if (properties == null)
			properties = new LinkedList<SimpleEntry<String, String>>();

		return properties;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	// private String username;

	/**
	 * @return the username
	 */
	/*-* /
	public String getUsername()
	{
		if (username == null)
			username = "";
		return username;
	}
	/**/

	/**
	 * @param username
	 *            the username to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	/*- * /
	public void setUsername(String username)
	{
		// begin-user-code
		this.username = username;
		// end-user-code
	}/**/

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	// private String password;

	/**
	 * @return the password
	 */
	/*- * /
	public String getPassword() {
		if (password == null)
			password = "";
		return password;
	}/**/

	/**
	 * @param password
	 *            the password to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	/*- * /
	public void setPassword(String password) {
		this.password = password;
	}/**/

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String description;

	/**
	 * @return the description
	 */
	public String getDescription() {
		if (description == null)
			description = "";
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the string 'Connection + prefix'
	 */
	public String toString() {
		return "Connection " + getURL();
	}
}