/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.controller.ExecutionController;
import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.ui.UIController;

/**
 * This browser provides the execution console tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ExecutionConsoleBrowser extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2668672328472241725L;

	private final UIController uiController;

	private JTextPane consoleTextPane;

	private JPanel consoleProgressPanel;

	private JLabel debugLevelSelectionLabel;
	private JLabel progressbarLabel;

	private JScrollPane consoleScrollPane;

	private volatile JProgressBar consoleProgressBar;

	private JButton clearConsoleButton;

	private JComboBox debugLevelComboBox;

	private ExecutionController executor;

	private JButton cancelExecutionButton;

	public ExecutionConsoleBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(
				uiController
						.setupTopPanel("Monitor execution progress and possible error messages"),
				BorderLayout.NORTH);
		// browser.add(getSettingsPanel(), BorderLayout.CENTER);
		browser.add(getConsoleScrollPane(), BorderLayout.CENTER);
		browser.add(getConsoleProgressPanel(), BorderLayout.SOUTH);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		this.repaint();
	}

	/**
	 * This method initializes consoleTextPane
	 * 
	 * @return javax.swing.JTextPane
	 */
	private JTextPane getConsoleTextPane() {
		if (consoleTextPane == null) {
			consoleTextPane = new JTextPane();
			UIController.makeup(consoleTextPane);
			consoleTextPane.setMinimumSize(new Dimension(200, 200));
			consoleTextPane.setPreferredSize(new Dimension(300, 300));
			// consoleTextPane.setMaximumSize(new Dimension(400,400));
			consoleTextPane.setEditable(false);

			//tell the handler that we have a console
			uiController.getHandler().setConsole(consoleTextPane);
		}

		return consoleTextPane;
	}

	/**
	 * This method initializes consoleProgressPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getConsoleProgressPanel() {
		if (consoleProgressPanel == null) {
			consoleProgressPanel = new JPanel();
			UIController.makeup(consoleProgressPanel);

			debugLevelSelectionLabel = new JLabel("Minimum message level:");
			debugLevelSelectionLabel.setLabelFor(getDebugLevelComboBox());
			UIController.makeup(debugLevelSelectionLabel);

			progressbarLabel = new JLabel("Execution Progress: ");
			progressbarLabel.setLabelFor(getConsoleProgressBar());
			UIController.makeup(progressbarLabel);

			consoleProgressPanel.setLayout(new FlowLayout());
			consoleProgressPanel.add(getCancelExecutionButton());
			consoleProgressPanel.add(getClearConsoleButton());
			consoleProgressPanel.add(debugLevelSelectionLabel);
			consoleProgressPanel.add(getDebugLevelComboBox());
			consoleProgressPanel.add(progressbarLabel);
			consoleProgressPanel.add(getConsoleProgressBar());

		}
		return consoleProgressPanel;
	}

	/**
	 * This method initializes consoleScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getConsoleScrollPane() {
		if (consoleScrollPane == null) {
			consoleScrollPane = new JScrollPane();
			UIController.makeup(consoleScrollPane);
			consoleScrollPane.setViewportView(getConsoleTextPane());
		}
		return consoleScrollPane;
	}

	/**
	 * This method initializes consoleProgressBar
	 * 
	 * @return javax.swing.JProgressBar
	 */
	private JProgressBar getConsoleProgressBar() {
		if (consoleProgressBar == null) {
			consoleProgressBar = new JProgressBar();
			UIController.makeup(consoleProgressBar);
			consoleProgressBar.setPreferredSize(new Dimension(200,
					UIController.sizeTextfieldHight));
			//consoleProgressBar.setStringPainted(true);
			
			//Let the mapping controller know that there is a progress bar
			uiController.getHandler().setProgressBar(consoleProgressBar);
		}
		return consoleProgressBar;
	}

	/**
	 * This method initializes clearConsoleButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getClearConsoleButton() {
		if (clearConsoleButton == null) {
			clearConsoleButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Clear Console");
			clearConsoleButton.setHorizontalAlignment(SwingConstants.LEFT);
			clearConsoleButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							getConsoleTextPane().setText("");
						}
					});
		}
		return clearConsoleButton;
	}

	/**
	 * This method initializes cancelExecutionButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getCancelExecutionButton() {
		if (cancelExecutionButton == null) {
			cancelExecutionButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Cancel Execution");
			cancelExecutionButton.setHorizontalAlignment(SwingConstants.LEFT);
			cancelExecutionButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							uiController
									.getHandler()
									.message(
											MESSAGELEVEL.NOTICE,
											"Sending cancellation message, "
													+ "please wait while we are safely shutting down.");
							executor.cancel();
						}
					});
		}
		return cancelExecutionButton;
	}

	/**
	 * This method initializes debugLevelComboBox
	 * 
	 * @return javax.swing.JComboBox
	 */
	private JComboBox getDebugLevelComboBox() {
		if (debugLevelComboBox == null) {
			debugLevelComboBox = new JComboBox();
			UIController.makeup(debugLevelComboBox);
			debugLevelComboBox.setPreferredSize(new Dimension(200,
					UIController.sizeTextfieldHight));

			// This combobox lists the message levels
			debugLevelComboBox.setModel(new DefaultComboBoxModel(MESSAGELEVEL
					.values()));

			// We initialize it by the handler's message level
			debugLevelComboBox.setSelectedItem(uiController.getHandler()
					.getMessageLevel());

			// And we update our handler when the selection changes
			debugLevelComboBox
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							uiController.getHandler().setMessageLevel(
									(MESSAGELEVEL) debugLevelComboBox
											.getSelectedItem());
							// MESSAGELEVEL lvl = (MESSAGELEVEL)
							// debugLevelComboBox.getSelectedItem();
							// System.out.println("Handler message level now is: "
							// + handler.getMessageLevel().toString());
						}
					});
		}
		return debugLevelComboBox;
	}

	public void setExecutor(ExecutionController executor) {
		this.executor = executor;
	}

}
