/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.Iterator;
import java.util.Vector;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.processmining.mapper.mapping.*;

/**
 * Class that wraps the mapping model and displays the log (and children
 * nodes) in a nice tree Modeled using
 * <a>http://java.sun.com/docs/books/tutorial/uiswing/examples/components/
 * GenealogyExampleProject/src/components/GenealogyModel.java</a> as an
 * example 
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MappingTreeModel implements TreeModel {
	/**
	 * 
	 */
	private Log log;
	// Store of the listeners
	private Vector<TreeModelListener> treeModelListeners = new Vector<TreeModelListener>();

	public MappingTreeModel(Log log) {
		setRoot(log);
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
		treeModelListeners.addElement(l);
	}

	/**
	 * Return the child at the given index
	 */
	@Override
	public Object getChild(Object parent, int index) {
		/*
		 * The parent can be one of 4 types:
		 */

		// When its a log, return the trace or one of its meta attributes
		if (parent instanceof Log) {
			Log log = (Log) parent;
			if (index == 0) {
				return log.getTrace();
			} else {
				// We've entered the 'meta attributes' so delegate the call
				return getChildMetaAttribute(
						log.getItemAttributes().iterator(), index - 1);
			}
		}

		// When its a trace, return an event or one of its meta-attributes
		if (parent instanceof Trace) {
			Trace trace = (Trace) parent;

			// If the index is < than #events then return one of those events
			Vector<Event> events = trace.getEvents();
			if (index < events.size()) {
				return events.toArray()[index];
			}

			// Otherwise search for the meta attribute at the index - #events
			return getChildMetaAttribute(trace.getItemAttributes().iterator(),
					index - events.size());
		}

		// When its an event, return one of its meta-attributes
		if (parent instanceof Event) {
			Event event = (Event) parent;
			return getChildMetaAttribute(event.getItemAttributes().iterator(),
					index);
		}

		// And the final correct answer is: if its an attribute, return its meta
		// attributes!
		if (parent instanceof Attribute) {
			Attribute attribute = (Attribute) parent;
			return getChildMetaAttribute(attribute.getItemAttributes()
					.iterator(), index);
		}

		return null; // this is an ugly child but hey, what else, we must return
		// something...
	}

	/**
	 * Returns the attribute at the given index
	 * 
	 * @param attributes
	 * @param index
	 * @return
	 */
	public Attribute getChildMetaAttribute(Iterator<Attribute> attributes,
			int index) {
		// We need to keep track of where we are in the 'meta attribute' list
		int counter = -1; // we start at index -1 and not 0 because that is
		// already our first element!
		// (we update this counter 'soon')

		// Loop through the attributes and count the number of meta ones
		// But loop backwards because our LinkedHashSet likes to keep the most
		// recent addition in the front while we want it last!
		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			// Only meta attributes are considered children
			if (attribute.isMeta()) {
				counter++;
			}
			// If we have found the child we searched for return it to the one
			// who requested it
			if (counter == index) {
				return attribute;
			}
		}

		return null; // ugly child but hey, what else, we must return a child...
	}

	/**
	 * Returns how many children we are supposed to have
	 */
	@Override
	public int getChildCount(Object parent) {
		// Unfortunately, this is not easy and depends on the type of parent we
		// get (similar to the 'getChild' function)
		int children = 0; // we start our life without children of our own

		if (parent instanceof Log) {
			// We have as many children as trace + meta-att.
			Log log = (Log) parent;
			if (log.getTrace() != null) {
				children++; // we have a new child: a trace
			}

			if (log.getItemAttributes() != null) {
				children += getChildCountMetaAttribute(log.getItemAttributes()
						.iterator()); // plus the number of meta-att. we have
			}
		}

		if (parent instanceof Trace) {
			// We have as many children as we have events + meta-attributes
			Trace trace = (Trace) parent;
			Vector<Event> events = trace.getEvents();
			if (events != null) {
				children += events.size(); // events
			}

			if (trace.getItemAttributes() != null) {
				children += getChildCountMetaAttribute(trace
						.getItemAttributes().iterator()); // and meta's
			}
		}

		if (parent instanceof Event) {
			// We have as many children as we have meta-attributes
			Event event = (Event) parent;
			if (event.getItemAttributes() != null) {
				children += getChildCountMetaAttribute(event
						.getItemAttributes().iterator());
			}
		}

		if (parent instanceof Attribute) {
			// We have as many children as we have meta-attributes
			Attribute attribute = (Attribute) parent;
			if (attribute.getItemAttributes() != null) {
				children += getChildCountMetaAttribute(attribute
						.getItemAttributes().iterator());
			}
		}

		return children; // If all else fails, return zero
	}

	/**
	 * Returns the number of meta attributes
	 * 
	 * @param attributes
	 * @param index
	 * @return
	 */
	public int getChildCountMetaAttribute(Iterator<Attribute> attributes) {
		// We need to keep track of where we are in the 'meta attribute' list
		int counter = 0;

		// Loop through the attributes and count the number of meta ones
		while (attributes.hasNext()) {
			Attribute attribute = attributes.next();
			// Only meta attributes are considered children
			if (attribute.isMeta()) {
				counter++;
			}
		}

		return counter;
	}

	/**
	 * Returns the index of the child among the other children of the parent
	 */
	@Override
	public int getIndexOfChild(Object parent, Object child) {
		int index = 0;

		if (parent instanceof Log) {
			Log log = (Log) parent;
			// A log can contain a trace and attributes
			if (child instanceof Trace) {
				// A log can only have 1 trace so return 0!
				return index;
			} else {
				// if we are not searching for a trace, check if we have a trace
				// and count it
				if (log.getTrace() != null) {
					index += 1;
				}

				// And find the attribute
				if (child instanceof Attribute) {
					Attribute attribute = (Attribute) child;
					index += getIndexOfChildMetaAttribute(log
							.getItemAttributes().iterator(), attribute);
				}
			}
		}

		if (parent instanceof Trace) {
			Trace trace = (Trace) parent;

			if (child instanceof Event) {
				// Find the index of the event child in the event set of the
				// trace
				Event eventChild = (Event) child;
				if (trace.getEvents().contains(eventChild)) // pre-check if
				// it is in there in the first place
				{
					Iterator<Event> eventIt = trace.getEvents().iterator();
					while (eventIt.hasNext()) {
						Event event = eventIt.next();
						if (event.equals(eventChild)) {
							return index;
						} else {
							index++;
						}
					}
				} // if contains
				// else: throw some fancy error that we don't know this event
				// but I'm lazy and it won't ever really happen, right?
			} else
			// child instance of Attribute!
			{
				index += trace.getEvents().size(); // Add the number of
				// events
			}

			// The parent is a trace and we're looking for a meta-attribute
			if (child instanceof Attribute) {
				Attribute attribute = (Attribute) child;
				index += getIndexOfChildMetaAttribute(log.getItemAttributes()
						.iterator(), attribute);
			}
		}

		if (parent instanceof Event) {
			// Ah, this one is easy, an event only has (meta) attributes!
			Event event = (Event) parent;
			Attribute attribute = (Attribute) child;
			index = getIndexOfChildMetaAttribute(event.getItemAttributes()
					.iterator(), attribute);
		}

		if (parent instanceof Attribute) {
			// Ah, this one is easy again, an attribute only has (meta)
			// attributes!
			Attribute attributeParent = (Attribute) parent;
			Attribute attribute = (Attribute) child;
			index = getIndexOfChildMetaAttribute(attributeParent
					.getItemAttributes().iterator(), attribute);
		}

		return index;
	}

	/**
	 * Returns the index of the given meta-attribute in the list of all
	 * attributes (including non-meta but these are excluded of the counting)
	 */
	public int getIndexOfChildMetaAttribute(Iterator<Attribute> attributes,
			Attribute attribute) {
		int index = 0;

		while (attributes.hasNext()) {
			Attribute attFound = attributes.next();

			if (attFound.equals(attribute)) {
				return index; // We found it!
			}

			if (attFound.isMeta()) {
				index++; // We did not found our target yet but we found one of
				// its siblings which we should count
			}
		}

		return index;
	}

	/**
	 * Returns our root object (a Log instance)
	 * 
	 * @return Object Log
	 */
	@Override
	public Object getRoot() {
		return this.log;
	}

	/**
	 * Updates the root element and the tree
	 * 
	 * @param log
	 */
	public void setRoot(Log log) {
		this.log = log;
		fireNewRoot();
	}

	/**
	 * Determines whether the given node has children
	 */
	@Override
	public boolean isLeaf(Object node) {
		// surprise surprise, we have 4 different cases:

		if (node instanceof Log) {
			Log log = (Log) node;

			// Its a leafy if it does not have a trace or meta-attributes
			if (log.getTrace() != null) {
				// we have an trace so we're not a leaf
				return false;
			} else {
				if (log.getItemAttributes() == null
						|| log.getItemAttributes().isEmpty()) {
					return true;// we don't have any attributes at all so for
					// sure not meta's
				} else {
					// Now we need to check if we have any meta-attributes
					return getChildCountMetaAttribute(log.getItemAttributes()
							.iterator()) == 0;
				}
			}
		}

		if (node instanceof Trace) {
			Trace trace = (Trace) node;

			// Its a leafy if it does not have events or meta-attributes
			if (trace.getItemAttributes() != null
					|| !trace.getEvents().isEmpty()) {
				// we have at least one event so we're not a leaf
				return false;
			} else {
				if (trace.getItemAttributes() == null
						|| trace.getItemAttributes().isEmpty()) {
					return true;// we don't have any attributes
				} else {
					// Now we need to check if we have any meta-attributes
					return getChildCountMetaAttribute(trace.getItemAttributes()
							.iterator()) == 0;
				}
			}
		}

		if (node instanceof Event) {
			Event event = (Event) node;

			// Its a leafy if it does not have any attribute or no
			// meta-attributes
			if (event.getItemAttributes() == null
					|| event.getItemAttributes().isEmpty()) {
				return true;// we don't have any attributes
			} else {
				// Now we need to check if we have any meta-attributes
				return getChildCountMetaAttribute(event.getItemAttributes()
						.iterator()) == 0;
			}
		}

		if (node instanceof Attribute) {
			Attribute attribute = (Attribute) node;

			// Its a leafy if it does not have any attribute or no
			// meta-attributes
			if (attribute.getItemAttributes() == null
					|| attribute.getItemAttributes().isEmpty()) {
				return true;// we don't have any attributes
			} else {
				// Now we need to check if we have any meta-attributes
				return getChildCountMetaAttribute(attribute.getItemAttributes()
						.iterator()) == 0;
			}
		}

		return true; // If all else fails I guess its a leaf
	}

	/**
	 * Removes a listener previously added with addTreeModelListener().
	 */
	public void removeTreeModelListener(TreeModelListener l) {
		treeModelListeners.removeElement(l);
	}

	/** Call when there is a new root, which may be null, i.e. not existent. */
	protected void fireNewRoot() {
		Object root = getRoot();

		/*
		 * From the Java "How to use ..." tutorial(s) Undocumented. I think it
		 * is the only reasonable/possible solution to use use null as path if
		 * there is no root. TreeModels without root aren't important anyway,
		 * since JTree doesn't support them (yet).
		 */
		TreePath path = (root != null) ? new TreePath(root) : null;

		TreeModelEvent e = null;

		for (int i = 0; i < treeModelListeners.size(); i++) {
			if (treeModelListeners.get(i) instanceof TreeModelListener) {
				if (e == null)
					e = new TreeModelEvent(this, path, null, null);

				((TreeModelListener) treeModelListeners.get(i))
						.treeStructureChanged(e);
			}
		}
	}

	/**
	 * Call when everything but the root has changed. Only may be called when
	 * the root is not null. Otherwise there isn't a structure to have changed.
	 */
	protected void fireStructureChanged() {
		fireTreeStructureChanged(new TreePath(getRoot()));
	}

	/**
	 * Call when a node has changed its leaf state.
	 */
	protected void firePathLeafStateChanged(TreePath path) {
		fireTreeStructureChanged(path);
	}

	/**
	 * Call when the tree structure below the path has completely changed.
	 */
	protected void fireTreeStructureChanged(TreePath parentPath) {
		TreeModelEvent e = null;

		for (int i = 0; i < treeModelListeners.size(); i++) {
			if (treeModelListeners.get(i) instanceof TreeModelListener) {
				if (e == null)
					e = new TreeModelEvent(this, parentPath, null, null);

				((TreeModelListener) treeModelListeners.get(i))
						.treeStructureChanged(e);
			}
		}
	}

	/**
	 * Call when the path itself has changed, but no structure changes have
	 * occurred.
	 */
	protected void firePathChanged(TreePath path) {
		Object node = path.getLastPathComponent();
		TreePath parentPath = path.getParentPath();

		if (parentPath == null)
			fireChildrenChanged(path, null, null);
		else {
			Object parent = parentPath.getLastPathComponent();

			fireChildChanged(parentPath, getIndexOfChild(parent, node), node);
		}
	}

	public void fireChildAdded(TreePath parentPath, int index, Object child) {
		fireChildrenAdded(parentPath, new int[] { index },
				new Object[] { child });
	}

	protected void fireChildChanged(TreePath parentPath, int index, Object child) {
		fireChildrenChanged(parentPath, new int[] { index },
				new Object[] { child });
	}

	public void fireChildRemoved(TreePath parentPath, int index, Object child) {
		fireChildrenRemoved(parentPath, new int[] { index },
				new Object[] { child });
	}

	protected void fireChildrenAdded(TreePath parentPath, int[] indices,
			Object[] children) {
		TreeModelEvent e = null;

		for (int i = 0; i < treeModelListeners.size(); i++) {
			if (treeModelListeners.get(i) instanceof TreeModelListener) {
				if (e == null)
					e = new TreeModelEvent(this, parentPath, indices, children);

				((TreeModelListener) treeModelListeners.get(i))
						.treeNodesInserted(e);
			}
		}
	}

	protected void fireChildrenChanged(TreePath parentPath, int[] indices,
			Object[] children) {
		TreeModelEvent e = null;

		for (int i = 0; i < treeModelListeners.size(); i++) {
			if (treeModelListeners.get(i) instanceof TreeModelListener) {
				if (e == null)
					e = new TreeModelEvent(this, parentPath, indices, children);

				((TreeModelListener) treeModelListeners.get(i))
						.treeNodesChanged(e);
			}
		}
	}

	protected void fireChildrenRemoved(TreePath parentPath, int[] indices,
			Object[] children) {
		TreeModelEvent e = null;

		for (int i = 0; i < treeModelListeners.size(); i++) {
			if (treeModelListeners.get(i) instanceof TreeModelListener) {
				if (e == null)
					e = new TreeModelEvent(this, parentPath, indices, children);
				((TreeModelListener) treeModelListeners.get(i))
						.treeNodesRemoved(e);
			}
		}
	}

	public void valueForPathChanged(TreePath arg0, Object arg1) {
		// do nothing...
	}
}