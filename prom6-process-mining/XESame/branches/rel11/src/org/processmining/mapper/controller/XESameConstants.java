/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

/**
 * Class for storing and providing standard XESame values
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 * 
 */
public final class XESameConstants {

	/*
	 * UI stuff
	 */
	// Maximum lengths for text fields to prevent performance problems
	// Maximum length for text areas used for descriptions
	public final static int uiTextAreaMaxLength = 8192;
	// Maximum length for text fields used for file locations etc.
	public final static int uiTextFieldMaxLength = 512;

	/*
	 * Cache DB settings
	 */
	public final static int cacheTraceIDLength = 512;
	public final static int cacheAttributeValueLength = 16384;
	/**
	 * Size of the internal cache before it is committed to the database
	 */
	public final static int cacheInternalSize = 1;
	//TODO implement caching in the entire cacheDBcontroller
	//public final static int cacheInternalSize = 128;

	/*
	 * Source connection settings
	 */
	// NOTE should also be moved to the sourceConnector or queryBuilder class
	// TODO maybe ask the sourceController about how to end a query? Or let it
	// check by himself...
	public final static String sqlQueryEnd = "";

	/*
	 * More XESAme specific constants
	 */
	// The message levels
	public static enum MESSAGELEVEL {
		ERROR, WARNING, PROGRESS, NOTICE, DEBUG, VERBOSE
	};

	// And the message level we set the console on by default
	public final static MESSAGELEVEL messageLevelDefault = MESSAGELEVEL.NOTICE;
	/**
	 * This variable indicates the 'switchpoint' between selecting those events
	 * that belong to the traces that we extracted earlier (if #traces is below
	 * this value) or if we select all events and drop the ones for which we do
	 * not have a trace
	 */
	public static final int executionNrTracesBeforeEventSelection = Integer.MAX_VALUE;
	// public static final int executionNrTracesBeforeEventSelection = 10 * 256;

	/*
	 * XES constants
	 * 
	 * DO NOT CHANGE
	 */
	public static String dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";

	// The five default XES extensions
	public static String[] defaultExtensions = {
			"http://www.xes-standard.org/concept.xesext",
			"http://www.xes-standard.org/lifecycle.xesext",
			"http://www.xes-standard.org/org.xesext",
			"http://www.xes-standard.org/time.xesext",
			"http://www.xes-standard.org/semantic.xesext" };

	/*
	 * Version info updated by SVN
	 */
	private static final String SVN_REVISION_RAW = "$Revision: UNKNOWN $";
	private static final String SVN_DATE_RAW = "$Date: 2011-02-09 12:28:31 -0800 (Wed, 09 Feb 2011) $";
	public static final String SVN_REVISION = SVN_REVISION_RAW.replaceAll(
			"\\$Revision:\\s*", "").replaceAll("\\s*\\$", "");
	public static final String SVN_DATE_STRING = SVN_DATE_RAW.replaceAll(
			"\\$Date:\\s*", "").replaceAll("\\s*\\$", "");
	// TODO update application version here (AND in ant build)
	public static final String APP_VERSION = "XESame 1.1 BETA";

	/**
	 * Don't allow instantiations...
	 */
	private XESameConstants() {

	}

}