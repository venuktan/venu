/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.Vector;

/**
 * An instance of the attribute that belongs to a general mapping item
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Attribute extends GeneralMappingItem // implements Cloneable
{
	private String key;

	/**
	 * @return the key
	 */
	public String getKey() {
		if (this.key == null)
			this.key = "";
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setKey(String key) {
		// We have certain requirements on the key:
		// No spaces! Replace them by '_'
		key = key.replace(' ', '_');
		// '|' also has a special meaning (tree level separator in our query
		// result column names, you'll see)
		key = key.replace('|', '\\');
		this.key = key;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String value;

	/**
	 * @return the value
	 */
	public String getValue() {
		if (value == null)
			value = "";
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setValue(String value) {
		// Replace " with ' otherwise ODBC will think it's an argument...
		value = value.replace("\"", "'");

		this.value = value;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Extension extension;

	/**
	 * Returns the extension this attribute is defined by. Please note: might
	 * return null if the attribute does not belong to an extension!
	 * 
	 * @return the extension
	 */
	public Extension getExtension() {
		return extension;
	}

	/**
	 * @param extension
	 *            the extension to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setExtension(Extension extension) {
		// begin-user-code
		this.extension = extension;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String type;

	/**
	 * @return the type
	 */
	public String getType() {
		if (type == null)
			type = "";
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setType(String type) {
		// if we change the attribute type to float
		if (type.equalsIgnoreCase("float")) {
			// We need to replace the hard coded , to . in the value
			value = value.replace(',', '.');
		}

		this.type = type;
		// end-user-code
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private GeneralMappingItem parent;

	/**
	 * @return the parent
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public GeneralMappingItem getParentItem() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setParent(GeneralMappingItem parent) {
		this.parent = parent;
	}

	/**
	 * @author Joos Buijs
	 * @return true if attribute has child attribute(s)
	 */
	public boolean isMeta() {
		if (this.getItemAttributes() == null) {
			return false; // we have no attributes
		} else {
			// If our attributes list exists but is empty, then we are not a
			// meta
			return !this.getItemAttributes().isEmpty();
		}
	}

	/**
	 * 
	 * @return true if this attribute's parent is an attribute (e.g. if the
	 *         parent of this attribute is a meta attribute)
	 */
	public boolean isMetaChild() {
		if (getParentItem() instanceof Attribute)
			return true;
		else
			return false;
	}

	/**
	 * Returns the string 'Attribute ' + key
	 * 
	 * @return String
	 */
	public String toString() {
		// return "Attribute: " + this.key;
		return this.key;
	}

	/**
	 * Creates a clone of the current attribute NOTE: the extension and
	 * properties reference is the same!
	 */
	public Attribute clone() {
		Attribute clone = new Attribute();

		// clone.setColumns(this.columns);
		clone.setExtension(this.extension);

		clone.setItemAttributes(new Vector<Attribute>());
		for (Attribute subAtt : this.getItemAttributes()) {
			clone.getItemAttributes().add(subAtt.clone());
		}

		/*
		 * clone.setItemAttributes((Vector<Attribute>) this.getItemAttributes()
		 * .clone());
		 */
		clone.setKey(this.getKey());
		clone.setParent(this.getParentItem());
		clone.setType(this.getType());
		clone.setValue(this.getValue());

		return clone;
	}

	/**
	 * 
	 * Returns the level of the attribute. Level 0 = direct descendant of a
	 * Log/Trace/Event element.
	 * 
	 * 1 means meta attribute where its parent is of level 0 etc.
	 * 
	 * @return
	 */
	public int getLevel() {
		if (!isMetaChild())
			// If we are not a meta attribute we are of level 0
			return 0;
		else {
			/*
			 * If we are a meta attribute than our level is that of our parent
			 * +1
			 */
			Attribute parent = (Attribute) getParentItem();

			return parent.getLevel() + 1;
		}

	}
}