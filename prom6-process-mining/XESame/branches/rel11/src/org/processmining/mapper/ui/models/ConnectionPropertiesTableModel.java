/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.LinkedList;
import java.util.AbstractMap.SimpleEntry;

import javax.swing.table.AbstractTableModel;

import org.processmining.mapper.controller.Utils;

/**
 * Model that shows the properties of an item in a nice editable table
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ConnectionPropertiesTableModel extends AbstractTableModel {
	private static final long serialVersionUID = -575783655895846030L;

	// We have 2 columns: "Property" and "Value"
	private String[] columnNames = new String[] { "Property", "Value" };

	private LinkedList<SimpleEntry<String, String>> properties = new LinkedList<SimpleEntry<String, String>>();

	// private Connection connection = new Connection();

	public ConnectionPropertiesTableModel(
			LinkedList<SimpleEntry<String, String>> properties) {
		updateProperties(properties);
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Class<?> getColumnClass(int col) {
		return String.class;
	}

	@Override
	public int getRowCount() {
		return properties.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		// First the left column with the keys
		if (col == 0) {
			return properties.get(row).getKey();

		} else {
			return properties.get(row).getValue();
		}
	}

	public boolean isCellEditable(int row, int col) {
		return true;
	}

	/**
	 * This method updates the cell with the given value. It also makes sure
	 * that empty rows are removed and that if the last row is non-empty an
	 * empty row is added
	 */
	public void setValueAt(Object value, int row, int col) {
		// make sure there is at least one item there
		if (properties.size() == 0)
			properties.add(new SimpleEntry<String, String>("", ""));

		String stringValue = (String) value;
		// Get the entry
		SimpleEntry<String, String> entry = properties.get(row);

		// Update the value
		if (col == 0) {
			// Create a new entry with the given key and the original value
			SimpleEntry<String, String> newEntry = new SimpleEntry<String, String>(
					stringValue, entry.getValue());
			properties.set(row, newEntry);
		} else if (col == 1) {
			properties.get(row).setValue(stringValue);
		}

		/*-
		 * If a new empty value has been entered AND
		 * (
		 *   1. we just entered an empty key and the value is also empty
		 *   OR
		 *   2. we just entered an empty value and the key is also empty
		 * ) AND
		 * there is at least one other record present
		 */
		if ((stringValue == null || stringValue.length() == 0))
			if ((col == 0 && (entry.getValue() == null || entry.getValue()
					.length() == 0))
					|| (col == 1 && (entry.getKey() == null || entry.getKey()
							.length() == 0)))
				if (properties.size() > 0) {
					// Remove the entry
					properties.remove(row);
					// And notify the table we removed it
					fireTableRowsDeleted(row, row);
				}

		/*
		 * Check for a non-empty value at the last row which means we need to
		 * add a row
		 */
		SimpleEntry<String, String> lastEntry = properties.getLast();
		if (!Utils.isEmptyString(lastEntry.getKey())
				|| !Utils.isEmptyString(lastEntry.getValue())) {
			properties.add(new SimpleEntry<String, String>("", ""));
			fireTableRowsInserted(properties.size(), properties.size());
		}

		fireTableCellUpdated(row, col);
	}

	public void updateProperties(
			LinkedList<SimpleEntry<String, String>> properties) {
		if (this.properties == null) {
			this.properties = new LinkedList<SimpleEntry<String, String>>();
		} else {
			this.properties = properties;
		}

		fireTableDataChanged();
	}

}
