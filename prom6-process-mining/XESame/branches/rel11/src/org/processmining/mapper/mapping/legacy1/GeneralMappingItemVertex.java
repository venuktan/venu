/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.awt.Color;

import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;

/**
 * This class shows the general mapping items in nice vertexes
 * 
 * @version 1.1BETA
 * The visualization is still in a PROTOTYPE phase!!!
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class GeneralMappingItemVertex extends DefaultGraphCell {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1660250396116383850L;

	/**
	 * 
	 */
	public GeneralMappingItemVertex() {
		super();

		// Set the default lay-out of our log, trace, event and attribute items
		GraphConstants.setAutoSize(this.getAttributes(), true);
		GraphConstants.setBorderColor(this.getAttributes(), Color.black);
		GraphConstants.setBackground(this.getAttributes(), Color.blue);
		// TO DO implement group collapse/expand
	}
}