/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.classification;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;

/**
 * Composite event classifier, which can hold any number of
 * lower-level classifiers, concatenated with boolean AND
 * logic. 
 * 
 * This classifier will consider two events as equal, if all 
 * of its lower-level classifiers consider them as equal.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XEventAndClassifier implements XEventClassifier {
	
	/**
	 * Sorted array of lower-level classifiers.
	 */
	protected XEventClassifier[] comparators;
	
	/**
	 * Name of the classifier
	 */
	protected String name;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param comparators Any number of lower-level classifiers,
	 * which are evaluated with boolean AND logic.
	 */
	public XEventAndClassifier(XEventClassifier... comparators) {
		this.comparators = comparators;
		Arrays.sort(this.comparators);
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(comparators[0].name());
		for(int i=1; i<comparators.length; i++) {
			sb.append(" AND ");
			sb.append(comparators[i].name());
		}
		sb.append(")");
		this.name = sb.toString();
	}
	
	/**
	 * Assigns a custom name to this classifier
	 * 
	 * @param name Name to be assigned to this classifier.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.comp.XEventComparator#equals(org.deckfour.xes.model.XEvent, org.deckfour.xes.model.XEvent)
	 */
	public boolean sameEventClass(XEvent eventA, XEvent eventB) {
		for(XEventClassifier comp : comparators) {
			if(comp.sameEventClass(eventA, eventB) == false) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.comp.XEventComparator#name()
	 */
	public String name() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.comp.XEventComparator#getClassIdentity(org.deckfour.xes.model.XEvent)
	 */
	public String getClassIdentity(XEvent event) {
		StringBuilder sb = new StringBuilder();
		sb.append(comparators[0].getClassIdentity(event));
		for(int i=1; i<comparators.length; i++) {
			sb.append("+");
			sb.append(comparators[i].getClassIdentity(event));
		}
		return sb.toString();
	}
	
	public String toString() {
		return name();
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.classification.XEventClassifier#getDefiningAttributes()
	 */
	public Set<XAttribute> getDefiningAttributes() {
		HashSet<XAttribute> result = new HashSet<XAttribute>();
		for(XEventClassifier component : comparators) {
			result.addAll(component.getDefiningAttributes());
		}
		return result;
	}
	
	/**
	 * Returns the collection of lower-level classifiers combined
	 * in this classifier with AND logic.
	 * 
	 * @return The collection of lower-level classifiers combined
	 * 	in this classifier with AND logic.
	 */
	public Collection<XEventClassifier> getComponents() {
		HashSet<XEventClassifier> collection = new HashSet<XEventClassifier>();
		for(XEventClassifier component : comparators) {
			collection.add(component);
		}
		return collection;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(XEventClassifier o) {
		if(o instanceof XEventAndClassifier) {
			XEventAndClassifier other = (XEventAndClassifier)o;
			if(this.comparators.length != other.comparators.length) {
				return this.comparators.length - other.comparators.length;
			} else {
				for(int i=0; i<this.comparators.length; i++) {
					int comp = this.comparators[i].compareTo(other.comparators[i]);
					if(comp != 0) {
						return comp;
					}
				}
				return 0;
			}
		} else {
			return 1;
		}
	}

}
