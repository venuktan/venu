/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2009 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.in;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.model.XAttributable;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.factory.XModelFactory;
import org.deckfour.xes.model.factory.XModelFactoryRegistry;
import org.deckfour.xes.storage.XTraceBufferedImpl;

/**
 * Binary, compressed format parser for the XES format.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XesBinaryParser {
	
public static final int XESBIN_MAGIC_NUMBER = 80823;

	private XModelFactory factory;
	private int stringIndex = 1;
	private int extensionIndex = 1;
	private HashMap<Integer,String> stringMap = new HashMap<Integer,String>();
	private HashMap<Integer,XExtension> extensionMap = new HashMap<Integer,XExtension>();
	
	public XesBinaryParser(XModelFactory factory) {
		this.factory = factory;
	}
	
	public XesBinaryParser() {
		this(XModelFactoryRegistry.instance().getCurrentFactory());
	}
	
	public XLog parse(InputStream is) throws IOException {
		DataInputStream dis = new DataInputStream(new BufferedInputStream(new GZIPInputStream(is)));
		XLog log = null;
		// read header
		int magic = dis.readInt();
		if(magic == XESBIN_MAGIC_NUMBER) {
			log = factory.createLog();
			int numExtensions = dis.readInt();
			for(int i=0; i<numExtensions; i++) {
				log.getExtensions().add(parseExtension(dis));
			}
			XID id = XID.read(dis);
			log.setId(id);
			parseAttributeMap(dis, log);
			int size = dis.readInt();
			for(int i=0; i<size; i++) {
				log.add(parseTrace(dis));
			}
			dis.close();
		}
		return log;
	}

	public XLog parse(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		if(file.getName().toLowerCase().endsWith("gz")) {
			is = new GZIPInputStream(is);
		}
		return parse(is);
	}
	
	public XTrace parseTrace(DataInputStream dis) throws IOException {
		XID id = XID.read(dis);
		XTrace trace = factory.createTrace(id);
		parseAttributeMap(dis, trace);
		int size = dis.readInt();
		for(int i=0; i<size; i++) {
			trace.add(parseEvent(dis));
		}
		if(trace instanceof XTraceBufferedImpl) {
			((XTraceBufferedImpl) trace).consolidate();
		}
		return trace;
	}
	
	public XEvent parseEvent(DataInputStream dis) throws IOException {
		XID id = XID.read(dis);
		XEvent event = factory.createEvent(id);
		parseAttributeMap(dis, event);
		return event;
	}
	
	public void parseAttributeMap(DataInputStream dis, XAttributable attributable) throws IOException {
		int size = dis.readInt();
		for(int i=0; i<size; i++) {
			// 1) read key
			String key = parseString(dis);
			// 2) read extension
			XExtension extension = parseExtension(dis);
			// 3) read type and value, and assemble
			XAttribute attribute;
			byte type = dis.readByte();
			if(type == 0) {
				boolean value = dis.readBoolean();
				attribute = factory.createAttributeBoolean(key, value, extension);
			} else if(type == 1) {
				double value = dis.readDouble();
				attribute = factory.createAttributeContinuous(key, value, extension);
			} else if(type == 2) {
				long value = dis.readLong();
				attribute = factory.createAttributeDiscrete(key, value, extension);
			} else if(type == 3) {
				long value = dis.readLong();
				attribute = factory.createAttributeDuration(key, value, extension);
			} else if(type == 4) {
				String value = dis.readUTF();
				attribute = factory.createAttributeLiteral(key, value, extension);
			} else if(type == 5) {
				long value = dis.readLong();
				attribute = factory.createAttributeTimestamp(key, value, extension);
			} else {
				throw new AssertionError("Unknown attribute type, cannot deserialize!");
			}
			// 4) recurse
			parseAttributeMap(dis, attribute);
			// add to return set
			attributable.getAttributes().put(key, attribute);
		}
	}
	
	public String parseString(DataInputStream dis) throws IOException {
		int index = readEfficientInt(dis, stringIndex);
		if(index == 0) {
			String result = dis.readUTF();
			stringMap.put(stringIndex, result);
			stringIndex++;
			return result;
		} else {
			return stringMap.get(index);
		}
	}
	
	public XExtension parseExtension(DataInputStream dis) throws IOException {
		byte index = dis.readByte();
		if(index == 0) {
			return null;
		} else if(index > 0) {
			return extensionMap.get(index);
		} else {
			URI uri = URI.create(dis.readUTF());
			XExtension extension = XExtensionManager.instance().getByUri(uri);
			extensionMap.put(extensionIndex, extension);
			extensionIndex++;
			return extension;
		}
	}
	
	public int readEfficientInt(DataInputStream dis, int maxValue) throws IOException {
		if(maxValue <= Byte.MAX_VALUE) {
			return dis.readByte();
		} else if(maxValue <= Short.MAX_VALUE) {
			return dis.readShort();
		} else {
			return dis.readInt();
		}
	}

}
