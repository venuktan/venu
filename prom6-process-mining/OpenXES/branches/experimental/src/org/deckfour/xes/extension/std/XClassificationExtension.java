/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.extension.std;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.deckfour.xes.classification.XEventAndClassifier;
import org.deckfour.xes.classification.XEventAttributeClassifier;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.info.XGlobalAttributeNameMap;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.factory.XModelFactoryRegistry;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;

/**
 * This extension allows for storing event classifiers attached
 * to a log. 
 * 
 * The classifiers are stored in a <code>class:declaration</code>
 * attribute, which can be attached to the log element of a log.
 * 
 * Classifiers are stored in enclosures, which are attributes of
 * the <code>class:declaration</code> log attribute. There can be
 * one standard classifier, which is enclosed in a specially defined
 * <code>class:standard</code> attribute. Other classifiers are
 * stored in generic attribute enclosures, where the key of the
 * attribute can be arbitrary, but must be unique; this implementation
 * uses the classifier name as attribute key. The attribute value
 * of the enclosure must be the classifier name, with type 
 * <code>LITERAL</code>.
 * 
 * The enclosure attributes contain the actual classifier declaration.
 * If a single attribute defines the classifier, this attribute
 * is featured on its own, as a prototype attribute with no meaningful
 * value.
 * 
 * Composite classifiers have a generic attribute, which needs to
 * be of type <code>LITERAL</code>, and have the logic of the
 * composite (e.g., <code>AND</code>) as value. This attribute then
 * contains the composed classifiers (recursively defined).
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XClassificationExtension extends XExtension {
	
	/**
	 * Unique URI of this extension.
	 */
	public static final URI EXTENSION_URI = URI.create("http://code.deckfour.org/xes/classification.xesext");
	/**
	 * Key for the declaration attribute.
	 */
	public static final String KEY_DECLARATION= "class:declaration";
	/**
	 * Key for the standard classifier declaration attribute.
	 */
	public static final String KEY_STANDARD = "class:standard";
	/**
	 * Definition declaration prototype
	 */
	public static XAttributeLiteral ATTR_DECLARATION;
	/**
	 * Standard classifier declaration prototype
	 */
	public static XAttributeLiteral ATTR_STANDARD;
	
	/**
	 * Singleton instance of this extension.
	 */
	private static XClassificationExtension singleton = new XClassificationExtension();
	
	/**
	 * Provides access to the singleton instance.
	 * @return Singleton extension.
	 */
	public static XClassificationExtension instance() {
		return singleton;
	}

	/**
	 * Private constructor
	 */
	private XClassificationExtension() {
		super("Classification", "class", EXTENSION_URI);
		ATTR_DECLARATION = new XAttributeLiteralImpl(KEY_DECLARATION, "", this);
		ATTR_STANDARD = new XAttributeLiteralImpl(KEY_STANDARD, "", this);
		
		this.logAttributes.add((XAttribute)ATTR_DECLARATION.clone());
		this.metaAttributes.add((XAttribute)ATTR_STANDARD.clone());
		// register aliases
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_ENGLISH, KEY_DECLARATION, "Event Classification Declaration");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_ENGLISH, KEY_STANDARD, "Standard Event Classifier");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_GERMAN, KEY_DECLARATION, "Eventklassifizierungssdeklaration");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_GERMAN, KEY_STANDARD, "Standardeventklassifizierung");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_FRENCH, KEY_DECLARATION, "Déclaration de Classification des Événements");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_FRENCH, KEY_STANDARD, "Classification Standard des Événements");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_SPANISH, KEY_DECLARATION, "Declaración de Clasificación de Eventos");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_SPANISH, KEY_STANDARD, "Clasificador de Eventos Estándar");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_PORTUGUESE, KEY_DECLARATION, "Declaração da Classificação de Evento");
		XGlobalAttributeNameMap.instance().registerMapping(
				XGlobalAttributeNameMap.MAPPING_PORTUGUESE, KEY_STANDARD, "Classificador de Evento Padrão");
	}
	
	/**
	 * Assigns the given list of classifiers to a log, in order to attach them.
	 * The first item in the given list is assumed to be the standard classifier
	 * for that log, i.e.~this method will replace the previous standard classifier
	 * with the first element of the given list. Note that this method thus replaces
	 * all previously attached classifiers of the given log.
	 * 
	 * @param log Log to attach classifiers to.
	 * @param classifiers Classifiers to attach.
	 */
	public void assignClassifiers(XLog log, List<XEventClassifier> classifiers) {
		XAttributeLiteral declaration = (XAttributeLiteral)ATTR_DECLARATION.clone();
		declaration.setValue("Event Classifier Declaration");
		declaration.getAttributes().clear();
		int numWritten = 0;
		for(XEventClassifier classifier : classifiers) {
			XAttribute classifierEncoding = encodeClassifier(classifier, classifier.name());
			XAttributeLiteral classifierEnclosure;
			if(numWritten == 0) {
				classifierEnclosure = (XAttributeLiteral)ATTR_STANDARD.clone();
			} else {
				classifierEnclosure = 
					XModelFactoryRegistry.instance().getCurrentFactory().createAttributeLiteral("alternativeClassifier" + numWritten, classifier.name(), null);
			}
			classifierEnclosure.setValue(classifier.name());
			classifierEnclosure.getAttributes().clear();
			classifierEnclosure.getAttributes().put(classifierEncoding.getKey(), classifierEncoding);
			declaration.getAttributes().put(classifierEnclosure.getKey(), classifierEnclosure);
			numWritten++;
		}
		log.getAttributes().put(declaration.getKey(), declaration);
	}
	
	/**
	 * This method extracts the classifiers attached to the given log, where
	 * the first element in the returned list is the standard classifier of
	 * that log. If the log has no standard classifier set, the first element
	 * of the returned list is arbitrary. In case no classifiers at all have
	 * been attached to the log, the returned list will be empty.
	 * 
	 * @param log Log to extract attached classifiers from.
	 * @return Classifiers attached to the given log.
	 */
	public List<XEventClassifier> extractClassifiers(XLog log) {
		ArrayList<XEventClassifier> classifiers = new ArrayList<XEventClassifier>();
		XAttribute declaration = log.getAttributes().get(KEY_DECLARATION);
		XEventClassifier standardClassifier = null;
		for(XAttribute classifierAttribute : declaration.getAttributes().values()) {
			String name = ((XAttributeLiteral)classifierAttribute).getValue();
			if(classifierAttribute.getAttributes().size() == 0) {
				System.err.println("XClassifierExtension: Found illegal classifier definition!");
				continue;
			}
			XAttribute definingAttribute = classifierAttribute.getAttributes().values().iterator().next();
			XEventClassifier classifier = decode(definingAttribute);
			classifier.setName(name);
			if(classifierAttribute.getKey().equals(KEY_STANDARD)) {
				standardClassifier = classifier;
			} else {
				classifiers.add(classifier);
			}
		}
		if(standardClassifier != null) {
			classifiers.add(0, standardClassifier);
		}
		return classifiers;
	}
	
	/**
	 * This method will assign to the given log the given standard classifier.
	 * Previously set standard classifiers will be retained, but will lose their
	 * standard status. If the given standard classifier has already been attached
	 * to the given log, it will only change its status. If the standard 
	 * classifier has changed its name, it will be contained twice.
	 * 
	 * @param log Log to assign standard classifier to.
	 * @param standardClassifier Standard classifier to be attached to the given log.
	 */
	public void assignStandardClassifier(XLog log, XEventClassifier standardClassifier) {
		List<XEventClassifier> classifiers = extractClassifiers(log);
		// remove standard classifier if already contained
		classifiers.remove(standardClassifier);
		classifiers.add(0, standardClassifier);
		assignClassifiers(log, classifiers);
	}
	
	/**
	 * This method extracts the standard classifier as attached to the
	 * given log by this extension. If no standard classifier has been
	 * attached, it will return an arbitrary classifier attached to the
	 * log. If no classifier at all has been attached to the log, this
	 * method will return <code>null</code>.
	 * 
	 * @param log Log to extract standard classifier from.
	 * @return The standard classifier of the given log.
	 */
	public XEventClassifier extractStandardClassifier(XLog log) {
		List<XEventClassifier> classifiers = extractClassifiers(log);
		return classifiers.get(0);
	}
	
	/**
	 * Encodes the given classifier.
	 * 
	 * @param classifier Classifier to be encoded.
	 * @param key Attribute key to be used for classifier.
	 * @return Attribute encoding the given classifier.
	 */
	private XAttribute encodeClassifier(XEventClassifier classifier, String key) {
		XAttribute result;
		if(classifier instanceof XEventAndClassifier) {
			// encode components
			XEventAndClassifier andClassifier = (XEventAndClassifier)classifier;
			Collection<XEventClassifier> components = andClassifier.getComponents();
			result = encodeCompositeClassifier(key, "AND", components);
		} else if(classifier instanceof XEventAttributeClassifier) {
			// attribute classifier
			XEventAttributeClassifier attributeClassifier = (XEventAttributeClassifier)classifier;
			XAttribute definingAttribute = attributeClassifier.getDefiningAttributes().iterator().next();
			result = (XAttribute)definingAttribute.clone();
		} else {
			throw new IllegalArgumentException("Can only handle composite AND and OR, and attribute classifiers!");
		}
		return result;
	}
	
	/**
	 * Encodes a composite classifier.
	 * 
	 * @param key Key to be used for attribute.
	 * @param logic Composite logic of classifier.
	 * @param components Components of composite classifier.
	 * @return Attribute encoding the given composite classifier.
	 */
	private XAttribute encodeCompositeClassifier(String key, String logic, Collection<XEventClassifier> components) {
		XAttribute encoding = XModelFactoryRegistry.instance().getCurrentFactory().createAttributeLiteral(key, logic, null);
		// encode components
		int counter = 1;
		for(XEventClassifier component : components) {
			XAttribute componentEncoding = encodeClassifier(component, "classifierComponent" + counter);
			encoding.getAttributes().put(componentEncoding.getKey(), componentEncoding);
		}
		return encoding;
	}
	
	/**
	 * Decodes a classifier from the given attribute (within the enclosure).
	 * 
	 * @param classifierAttribute In-enclosure attribute encoding the classifier.
	 * @return Decoded classifier.
	 */
	private XEventClassifier decode(XAttribute classifierAttribute) {
		if(classifierAttribute instanceof XAttributeLiteral) {
			String value = ((XAttributeLiteral)classifierAttribute).getValue();
			if(value.equals("AND")) {
				List<XEventClassifier> components = new ArrayList<XEventClassifier>();
				for(XAttribute componentAttribute : classifierAttribute.getAttributes().values()) {
					components.add(decode(componentAttribute));
				}
				XEventAndClassifier andClassifier = new XEventAndClassifier(
						components.toArray(new XEventClassifier[components.size()]));
				return andClassifier;
			}
		}
		return new XEventAttributeClassifier(classifierAttribute);
	}
	
}
