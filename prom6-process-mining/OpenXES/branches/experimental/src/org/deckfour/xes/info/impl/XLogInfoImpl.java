/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.info.impl;

import java.util.HashMap;

import org.deckfour.xes.classification.XEventAndClassifier;
import org.deckfour.xes.classification.XEventClasses;
import org.deckfour.xes.classification.XEventClassifier;
import org.deckfour.xes.classification.XEventLifeTransClassifier;
import org.deckfour.xes.classification.XEventNameClassifier;
import org.deckfour.xes.classification.XEventResourceClassifier;
import org.deckfour.xes.info.XAttributeInfo;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.XTimeBounds;
import org.deckfour.xes.model.XAttributable;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;

/**
 * This class implements a bare-bones log info summary which can
 * be created on demand by using applications.
 * 
 * The log info summary is based on an event classifier, which is
 * used to identify event class abstractions. 
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XLogInfoImpl implements XLogInfo {
	
	/**
	 * Standard event classifier. This classifier considers two 
	 * events as belonging to the same class, if they have both
	 * the same event name and the same lifecycle transition
	 * (if available).
	 */
	public static final XEventClassifier STANDARD_CLASSIFIER = new XEventAndClassifier(
			new XEventNameClassifier(), new XEventLifeTransClassifier());
	
	/**
	 * Creates a new log info summary with the standard event classifier.
	 * 
	 * @param log The event log to create an info summary for.
	 * @return The log info for this log.
	 */
	public static XLogInfo create(XLog log) {
		return create(log, STANDARD_CLASSIFIER);
	}
	
	/**
	 * Creates a new log info summary with a custom event classifier.
	 * 
	 * @param log The event log to create an info summary for.
	 * @param classifier The event classifier to be used.
	 * @return The log info summary for this log.
	 */
	public static XLogInfo create(XLog log, XEventClassifier classifier) {
		return new XLogInfoImpl(log, classifier);
	}
	
	/**
	 * The event log which is summarized.
	 */
	protected XLog log;
	/**
	 * The total number of events in this log.
	 */
	protected int numberOfEvents;
	/**
	 * The number of traces in this log.
	 */
	protected int numberOfTraces;
	/**
	 * The event classes in this log, as defined
	 * by the event classifier of this summary.
	 */
	protected XEventClasses eventClasses;
	/**
	 * The event classes in this log, as classified
	 * by their resource.
	 */
	protected XEventClasses resourceClasses;
	/**
	 * The event classes in this log, as classified
	 * by their event name.
	 */
	protected XEventClasses nameClasses;
	/**
	 * The event classes in this log, as classified
	 * by their lifecycle transition.
	 */
	protected XEventClasses transitionClasses;
	/**
	 * Timestamp boundaries for the complete log.
	 */
	protected XTimeBoundsImpl logBoundaries;
	/**
	 * Map of timestamp boundaries for each trace, indexed
	 * by reference to the respective trace.
	 */
	protected HashMap<XTrace,XTimeBoundsImpl> traceBoundaries;
	/**
	 * Attribute information registry on the log level.
	 */
	protected XAttributeInfoImpl logAttributeInfo;
	/**
	 * Attribute information registry on the trace level.
	 */
	protected XAttributeInfoImpl traceAttributeInfo;
	/**
	 * Attribute information registry on the event level.
	 */
	protected XAttributeInfoImpl eventAttributeInfo;
	/**
	 * Attribute information registry on the meta level.
	 */
	protected XAttributeInfoImpl metaAttributeInfo;
	
	/**
	 * Creates a new log summary.
	 * 
	 * @param log The log to create a summary of.
	 * @param classifier The event classifier to be used.
	 */
	public XLogInfoImpl(XLog log, XEventClassifier classifier) {
		this.log = log;
		this.numberOfEvents = 0;
		this.numberOfTraces = 0;
		this.eventClasses = new XEventClasses(classifier);
		this.resourceClasses = new XEventClasses(new XEventResourceClassifier());
		this.nameClasses = new XEventClasses(new XEventNameClassifier());
		this.transitionClasses = new XEventClasses(new XEventLifeTransClassifier());
		this.logBoundaries = new XTimeBoundsImpl();
		this.traceBoundaries = new HashMap<XTrace,XTimeBoundsImpl>();
		this.logAttributeInfo = new XAttributeInfoImpl();
		this.traceAttributeInfo = new XAttributeInfoImpl();
		this.eventAttributeInfo = new XAttributeInfoImpl();
		this.metaAttributeInfo = new XAttributeInfoImpl();
		setup();
	}

	/**
	 * Creates the internal data structures of this summary on setup
	 * from the log.
	 */
	protected synchronized void setup() {
		registerAttributes(logAttributeInfo, log);
		for(XTrace trace : log) {
			numberOfTraces++;
			registerAttributes(traceAttributeInfo, trace);
			XTimeBoundsImpl traceBounds = new XTimeBoundsImpl();
			for(XEvent event : trace) {
				registerAttributes(eventAttributeInfo, event);
				eventClasses.register(event);
				resourceClasses.register(event);
				nameClasses.register(event);
				transitionClasses.register(event);
				traceBounds.register(event);
				numberOfEvents++;
			}
			this.traceBoundaries.put(trace, traceBounds);
			this.logBoundaries.register(traceBounds);
		}
		// harmonize event class indices
		eventClasses.harmonizeIndices();
		resourceClasses.harmonizeIndices();
		nameClasses.harmonizeIndices();
		transitionClasses.harmonizeIndices();
	}
	
	/**
	 * Registers all attributes of a given attributable, i.e.
	 * model type hierarchy element, in the given attribute info registry.
	 * 
	 * @param attributeInfo Attribute info registry to use for registration.
	 * @param attributable Attributable whose attributes to register.
	 */
	protected void registerAttributes(XAttributeInfoImpl attributeInfo, XAttributable attributable) {
		for(XAttribute attribute : attributable.getAttributes().values()) {
			// register attribute in appropriate map
			attributeInfo.register(attribute);
			// register meta-attributes globally
			registerAttributes(metaAttributeInfo, attribute);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getLog()
	 */
	public XLog getLog() {
		return log;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getNumberOfEvents()
	 */
	public int getNumberOfEvents() {
		return numberOfEvents;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getNumberOfTraces()
	 */
	public int getNumberOfTraces() {
		return numberOfTraces;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getEventClasses()
	 */
	public XEventClasses getEventClasses() {
		return eventClasses;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getResourceClasses()
	 */
	public XEventClasses getResourceClasses() {
		return resourceClasses;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getNameClasses()
	 */
	public XEventClasses getNameClasses() {
		return nameClasses;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getTransitionClasses()
	 */
	public XEventClasses getTransitionClasses() {
		return transitionClasses;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getLogTimeBoundaries()
	 */
	public XTimeBounds getLogTimeBoundaries() {
		return logBoundaries;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#getTraceTimeBoundaries(org.deckfour.xes.model.XTrace)
	 */
	public XTimeBounds getTraceTimeBoundaries(XTrace trace) {
		return traceBoundaries.get(trace);
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.info.XLogInfo#getLogAttributeInfo()
	 */
	public XAttributeInfo getLogAttributeInfo() {
		return logAttributeInfo;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.info.XLogInfo#getTraceAttributeInfo()
	 */
	public XAttributeInfo getTraceAttributeInfo() {
		return traceAttributeInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.info.XLogInfo#getEventAttributeInfo()
	 */
	public XAttributeInfo getEventAttributeInfo() {
		return eventAttributeInfo;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.info.XLogInfo#getMetaAttributeInfo()
	 */
	public XAttributeInfo getMetaAttributeInfo() {
		return metaAttributeInfo;
	}
	
	/* (non-Javadoc)
	 * @see org.deckfour.xes.summary.XLogSummary#toString()
	 */
	public String toString() {
		return "Summary for " + log.getId();
	}
	
	
}
