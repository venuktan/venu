/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.in;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashSet;
import java.util.Stack;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.model.XAttributable;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.model.factory.XModelFactory;
import org.deckfour.xes.model.factory.XModelFactoryRegistry;
import org.deckfour.xes.storage.XTraceBufferedImpl;
import org.deckfour.xes.util.XAttributeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Parser for the XES format, in its standard XML representation.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XesXmlParser {

	/**
	 * Unique URI for the format definition.
	 */
	protected static final URI XES_URI = URI.create("http://code.deckfour.org/xes");

	/**
	 * XES model factory used to build model.
	 */
	protected XModelFactory factory;
	
	
	/**
	 * Creates a new parser instance.
	 * 
	 * @param factory The XES model factory
	 * instance used to build the model from
	 * the serialization.
	 */
	public XesXmlParser(XModelFactory factory) {
		this.factory = factory;
	}
	
	/**
	 * Creates a new parser instance, using the
	 * currently-set standard factory for building
	 * the model.
	 */
	public XesXmlParser() {
		this(XModelFactoryRegistry.instance().getCurrentFactory());
	}
	
	/**
	 * Parses a log from the given input stream, which is supposed to deliver
	 * an XES log in XML representation.
	 * 
	 * @param is Input stream, which is supposed to deliver
	 * an XES log in XML representation.
	 * @return The parsed log.
	 */
	public XLog parse(InputStream is) throws ParserConfigurationException, SAXException, IOException {
		BufferedInputStream bis = new BufferedInputStream(is);
		// set up a specialized SAX2 handler to fill the container
		XesXmlHandler handler = new XesXmlHandler();
		// set up SAX parser and parse provided log file into the container
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		parserFactory.setNamespaceAware(false);
		SAXParser parser = parserFactory.newSAXParser();
		parser.parse(bis, handler);
		bis.close();
		return handler.getLog();
	}
	
	/**
	 * Parses a log from the given file, which is supposed to contain
	 * an XES log in XML representation.
	 * 
	 * @param file File, which is supposed to contain
	 * an XES log in XML representation.
	 * @return The parsed log.
	 */
	public XLog parse(File file) throws IOException, ParserConfigurationException, SAXException {
		InputStream is = new FileInputStream(file);
		if(file.getName().toLowerCase().endsWith("gz")) {
			is = new GZIPInputStream(is);
		}
		return parse(is);
	}
	
	
	/**
	 * SAX handler class for XES in XML representation.
	 * 
	 * @author Christian W. Guenther (christian@deckfour.org)
	 *
	 */
	protected class XesXmlHandler extends DefaultHandler {
		
		/**
		 * Buffer log.
		 */
		protected XLog log;
		/**
		 * Buffer trace.
		 */
		protected XTrace trace;
		/**
		 * Buffer event.
		 */
		protected XEvent event;
		/**
		 * Buffer for attributes.
		 */
		protected Stack<XAttribute> attributeStack;
		/**
		 * Buffer for attributables.
		 */
		protected Stack<XAttributable> attributableStack;
		/**
		 * Buffer for extensions.
		 */
		protected HashSet<XExtension> extensions;
		
		/**
		 * Creates a new handler instance.
		 */
		public XesXmlHandler() {
			log = null;
			trace = null;
			event = null;
			attributeStack = new Stack<XAttribute>();
			attributableStack = new Stack<XAttributable>();
			extensions = new HashSet<XExtension>();
		}

		/**
		 * Retrieves the parsed log.
		 * @return The parsed log.
		 */
		public XLog getLog() {
			return log;
		}

		/* (non-Javadoc)
		 * @see org.xml.sax.helpers.DefaultHandler#startPrefixMapping(java.lang.String, java.lang.String)
		 */
		@Override
		public void startPrefixMapping(String prefix, String uriStr)
				throws SAXException {
			URI uri = URI.create(uriStr);
			XExtension extension = XExtensionManager.instance().getByUri(uri);
			if(extension != null && prefix != null) {
				extensions.add(extension);
			}
		}

		/* (non-Javadoc)
		 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
		 */
		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			// resolve extension
			XExtension extension = null;
			if(uri != null && uri.length() > 0) {
				URI extUri = URI.create(uri);
				if(extUri.equals(XES_URI) == false) {
					extension = XExtensionManager.instance().getByUri(XES_URI);
				}
			} else {
				int colonIndex = qName.indexOf(':');
				if(colonIndex > 0 && colonIndex < (qName.length() - 1)) {
					String prefix = qName.substring(0, colonIndex);
					extension = XExtensionManager.instance().getByPrefix(prefix);
				}
			}
			// resolve tag name
			String tagName = localName.trim();
			if (tagName.length() == 0) {
				tagName = qName;
			}
			// parse content
			if(extension == null) {
				// standard XES elements (non-extension)
				if(tagName.equalsIgnoreCase("attribute")) {
					String key = attributes.getValue("key");
					String value = attributes.getValue("value");
					String type = attributes.getValue("type");
					XAttribute attribute = XAttributeUtils.composeAttribute(factory, key, value, type, null);
					attributeStack.push(attribute);
					attributableStack.push(attribute);
				}  else if(tagName.equalsIgnoreCase("event")) {
					String id = attributes.getValue("id");
					event = factory.createEvent(XID.parse(id));
					attributableStack.push(event);
				} else if(tagName.equalsIgnoreCase("trace")) {
					String id = attributes.getValue("id");
					trace = factory.createTrace(XID.parse(id));
					attributableStack.push(trace);
				} else if(tagName.equalsIgnoreCase("log")) {
					String id = attributes.getValue("id");
					log = factory.createLog(XID.parse(id));
					attributableStack.push(log);
					for(int i=0; i<attributes.getLength(); i++) {
						String key = attributes.getQName(i);
						if(key.startsWith("xmlns:")) {
							// namespace declaration found in attribute tag
							URI extUri = URI.create(attributes.getValue(i));
							XExtension ext = XExtensionManager.instance().getByUri(extUri);
							extensions.add(ext);
						}
					}
				} 
			} else {
				// extension attribute
				String value = attributes.getValue("value");
				String type = attributes.getValue("type");
				XAttribute attribute = XAttributeUtils.composeAttribute(factory, qName.trim(), value, type, extension);
				attributeStack.push(attribute);
				attributableStack.push(attribute);
			}
		}
		
		/* (non-Javadoc)
		 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
		 */
		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			// resolve extension
			XExtension extension = null;
			if(uri != null && uri.length() > 0) {
				URI extUri = URI.create(uri);
				if(extUri.equals(XES_URI) == false) {
					extension = XExtensionManager.instance().getByUri(XES_URI);
				} 
			} else {
				int colonIndex = qName.indexOf(':');
				if(colonIndex > 0 && colonIndex < (qName.length() - 1)) {
					String prefix = qName.substring(0, colonIndex);
					extension = XExtensionManager.instance().getByPrefix(prefix);
				}
			}
			// resolve tag name
			String tagName = localName.trim();
			if (tagName.length() == 0) {
				tagName = qName;
			}
			// parse content
			if(extension == null) {
				// standard XES elements (non-extension)
				if(tagName.equalsIgnoreCase("attribute")) {
					XAttribute attribute = attributeStack.pop();
					attributableStack.pop();	// remove self from top
					attributableStack.peek().getAttributes().put(attribute.getKey(), attribute);
				} else if(tagName.equalsIgnoreCase("event")) {
					trace.add(event);
					event = null;
				} else if(tagName.equalsIgnoreCase("trace")) {
					if(trace instanceof XTraceBufferedImpl) {
						((XTraceBufferedImpl)trace).consolidate();
					}
					log.add(trace);
					trace = null;
				} else if(tagName.equalsIgnoreCase("log")) {
					// add all extensions
					for(XExtension ext : extensions) {
						log.getExtensions().add(ext);
					}
				} 
			} else {
				// extension attribute
				XAttribute attribute = attributeStack.pop();
				attributableStack.pop();	// remove self from top
				attributableStack.peek().getAttributes().put(attribute.getKey(), attribute);
			}
		}	
		
	}

}
