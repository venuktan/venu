/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2009 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.storage.softstrings;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collection;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeBoolean;
import org.deckfour.xes.model.XAttributeContinuous;
import org.deckfour.xes.model.XAttributeDiscrete;
import org.deckfour.xes.model.XAttributeDuration;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.impl.XAttributeBooleanImpl;
import org.deckfour.xes.model.impl.XAttributeContinuousImpl;
import org.deckfour.xes.model.impl.XAttributeDiscreteImpl;
import org.deckfour.xes.model.impl.XAttributeDurationImpl;
import org.deckfour.xes.model.impl.XAttributeLiteralImpl;
import org.deckfour.xes.model.impl.XAttributeMapImpl;
import org.deckfour.xes.model.impl.XAttributeMapLazyImpl;
import org.deckfour.xes.model.impl.XAttributeTimestampImpl;

/**
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XSoftStringAttributeSerializer {
	
	/**
	 * Encoding for non-existent extension (generic attributes)
	 */
	private static final int EXTENSION_GENERIC = -1;
	private static XSoftStringMap map = XSoftStringMap.instance();
	
	
	public static void encodeAttributes(Collection<XAttribute> attributes,
			DataOutput output) throws IOException {
		// record size of set
		output.writeInt(attributes.size());
		for(XAttribute attribute : attributes) {
			// write attribute key
			output.writeInt(map.encode(attribute.getKey()));
			// encode attribut extension
			// extension may be non-existent (generic attributes)
			XExtension extension = attribute.getExtension();
			if(extension != null) {
				output.writeByte(XExtensionManager.instance().getIndex(extension));
			} else {
				output.writeByte(EXTENSION_GENERIC);
			}
			// encode attribute type and value
			if(attribute instanceof XAttributeLiteral) {
				output.writeByte(0);
				output.writeUTF(((XAttributeLiteral)attribute).getValue());
			} else if(attribute instanceof XAttributeBoolean) {
				output.writeByte(1);
				output.writeBoolean(((XAttributeBoolean)attribute).getValue());
			} else if(attribute instanceof XAttributeContinuous) {
				output.writeByte(2);
				output.writeDouble(((XAttributeContinuous)attribute).getValue());
			} else if(attribute instanceof XAttributeDiscrete) {
				output.writeByte(3);
				output.writeLong(((XAttributeDiscrete)attribute).getValue());
			} else if(attribute instanceof XAttributeDuration) {
				output.writeByte(4);
				output.writeLong(((XAttributeDuration)attribute).getValue());
			} else if(attribute instanceof XAttributeTimestamp) {
				output.writeByte(5);
				output.writeLong(((XAttributeTimestamp)attribute).getValueMillis());
			} else {
				throw new AssertionError("Unexpected attribute type!");
			}
			// recurse to meta-attributes
			encodeAttributes(attribute.getAttributes().values(), output);
		}
	}

	public static XAttributeMap readAttributes(DataInput input)
			throws IOException {
		// read size of attribute map and iterate attributes
		int size = input.readInt();
		// initialize new attribute map
		if(size == 0) {
			// attribute map is empty: use lazy implementation!
			return new XAttributeMapLazyImpl<XAttributeMapImpl>(XAttributeMapImpl.class);
		}
		XAttributeMap attributes = new XAttributeMapImpl(size * 2);
		// iterate attributes
		for(int i=0; i<size; i++) {
			// read attribute parameters
			String key = map.decode(input.readInt());
			// extract extension reference if present, use null otherwise
			int extensionIndex = input.readByte();
			XExtension extension = null;
			if(extensionIndex != EXTENSION_GENERIC) {
				extension = XExtensionManager.instance().getByIndex(extensionIndex);
			}
			// extract attribute type and value,
			// and assemble attribute according to type
			XAttribute attribute;
			byte type = input.readByte();
			if(type == 0) {
				String value = input.readUTF();
				attribute = new XAttributeLiteralImpl(key, value, extension);
			} else if(type == 1) {
				boolean value = input.readBoolean();
				attribute = new XAttributeBooleanImpl(key, value, extension);
			} else if(type == 2) {
				double value = input.readDouble();
				attribute = new XAttributeContinuousImpl(key, value, extension);
			} else if(type == 3) {
				long value = input.readLong();
				attribute = new XAttributeDiscreteImpl(key, value, extension);
			} else if(type == 4) {
				long value = input.readLong();
				attribute = new XAttributeDurationImpl(key, value, extension);
			} else if(type == 5) {
				long value = input.readLong();
				attribute = new XAttributeTimestampImpl(key, value, extension);
			} else {
				throw new AssertionError("Unexpected attribute type!");
			}
			// recurse to parse meta-attributes, and add to attribute
			XAttributeMap metaAttributes = readAttributes(input);
			attribute.setAttributes(metaAttributes);
			// add attribute to map
			attributes.put(attribute.getKey(), attribute);
		}
		return attributes;
	}

}
