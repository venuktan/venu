package nl.tue.astar.impl.fast;
import gnu.trove.map.TObjectIntMap;
import java.util.List;
import nl.tue.astar.AStarException;
import nl.tue.astar.Delegate;
import nl.tue.astar.Head;
import nl.tue.astar.Record;
import nl.tue.astar.Tail;
import nl.tue.astar.Trace;
import nl.tue.astar.impl.AbstractAStarThread;
import nl.tue.astar.impl.State;

public class FastAStarThread<H extends Head, T extends Tail> extends
		AbstractAStarThread<H, T> {

	private final TObjectIntMap<H> head2int;
	private final List<State<H, T>> stateList;
	public FastAStarThread(Delegate<H, T> delegate, TObjectIntMap<H> head2int,
			List<State<H, T>> stateList, H initialHead, Trace trace,
			int maxStates) throws AStarException {
		super(delegate, trace, maxStates);
		synchronized (head2int) {
			synchronized (stateList) {
				this.head2int = head2int;
				this.stateList = stateList;
				// get the index where initialHead is stored
				initializeQueue(initialHead);
			}
		}
	}

	protected T getStoredTail(T tail, long index, int modelMove,
			int movedEvent, int logMove) {
		return stateList.get((int) index).getTail();
	}

	protected void storeStateForRecord(State<H, T> state, Record newRec) {
		synchronized (head2int) {
			synchronized (stateList) {
				int newIndex = stateList.size() + 1;
				stateList.add(state);
				head2int.put(state.getHead(), newIndex);
				newRec.setState(newIndex - 1);
			}
		}
	}

	protected long getIndexOf(H head) {
		synchronized (head2int) {
			synchronized (stateList) {
				return head2int.get(head) - 1;
			}
		}
	}

	protected State<H, T> getStoredState(Record rec) {
		return stateList.get((int) rec.getState());
	}

	@Override
	protected int getEstimate(H head, long index) {
		return stateList.get((int) index).getTail()
				.getEstimatedCosts(delegate, head);
	}

}
