package nl.tue.astar.impl.memefficient;

import nl.tue.astar.Head;
import nl.tue.astar.Tail;
import nl.tue.astar.impl.State;
import nl.tue.astar.impl.StateCompressor;
import nl.tue.storage.CompressedHashSet;
import nl.tue.storage.CompressedStore;
import nl.tue.storage.impl.CompressedStoreHashSetImpl;

public class MemoryEfficientAStarAlgorithm<H extends Head, T extends Tail> {

	private final CompressedHashSet<State<H, T>> statespace;
	private final CompressedStore<State<H, T>> store;
	private final StorageAwareDelegate<H, T> delegate;
	private final StateCompressor<H, T> compressor;

	public MemoryEfficientAStarAlgorithm(StorageAwareDelegate<H, T> delegate) {
		this.compressor = new StateCompressor<H, T>(delegate);
		this.delegate = delegate;
		this.statespace = new CompressedStoreHashSetImpl.Int32G<State<H, T>>(
				compressor, compressor, 2 * 1024 * 1024, compressor,
				compressor, 2 * 1024 * 1024);
		this.store = statespace.getBackingStore();
		delegate.setStateSpace(statespace);
	}

	public CompressedHashSet<State<H, T>> getStatespace() {
		return statespace;
	}

	public CompressedStore<State<H, T>> getStore() {
		return store;
	}

	public StorageAwareDelegate<H, T> getDelegate() {
		return delegate;
	}

}
