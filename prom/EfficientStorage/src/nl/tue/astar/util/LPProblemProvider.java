package nl.tue.astar.util;

import lpsolve.LpSolve;
import lpsolve.LpSolveException;

public class LPProblemProvider {

	private final LpSolve[] problems;
	private final boolean[] problemsInUse;
	private int available;

	public LPProblemProvider(LpSolve problem, int num) throws LpSolveException {
		problems = new LpSolve[num];
		problemsInUse = new boolean[num];
		available = num;
		problems[0] = problem;
		for (int i = 1; i < num; i++) {
			problems[i] = problem.copyLp();
		}
	}

	/**
	 * Returns the first available solver. If no solver is available, the
	 * current thread is blocked until one becomes available.
	 * 
	 * The returned solver can be used without the need for synchronizing on it.
	 * Furthermore, once finished with a solver, it should be returned in the
	 * finished method.
	 * 
	 * It is good practice to call the finished() method from a finally block
	 * after catching any exception coming from the solver, to make sure no
	 * solvers ever get lost.
	 * 
	 * @return
	 */
	public LpSolve firstAvailable() {
		synchronized (this) {
			while (available == 0) {
				try {
					this.wait();
				} catch (InterruptedException e) {
				}
			}
			for (int i = problems.length; i-- > 0;) {
				if (!problemsInUse[i]) {
					available--;
					problemsInUse[i] = true;
					return problems[i];
				}
			}
			assert false;
			return null;
		}
	}

	/**
	 * 
	 * Signals that this solver is done and can be used by another thread.
	 * 
	 * It is good practice to call the finished() method from a finally block
	 * after catching any exception coming from the solver, to make sure no
	 * solvers ever get lost.
	 * 
	 * @param solver
	 */
	public void finished(LpSolve solver) {
		synchronized (this) {
			for (int i = problems.length; i-- > 0;) {
				if (problems[i] == solver) {
					problemsInUse[i] = false;
					available++;
					this.notify();
					return;
				}
			}
			// finished called on a solver not belonging to this provider
			assert false;
		}
	}

	/**
	 * Cleanup all solvers, regardless whether they are currently in use. If
	 * this class is used correctly, then problemsInUse should be empty when
	 * finalize is called by the garbage collector.
	 */
	protected void finalize() throws Throwable {
		try {
			for (int i = problems.length; i-- > 0;) {
				try {
					problems[i].deleteLp();
				} catch (Exception e) {
					// Ignore
				}
			}
		} finally {
			super.finalize();
		}
	}
}
