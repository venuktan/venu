package nl.tue.astar.util;

import nl.tue.astar.Record;

public interface FastLookupPriorityQueue {

	public boolean isEmpty();

	public int contains(Record newRec);

	public Record peek();

	public int size();

	public Record poll();

	public boolean add(Record newE);

}