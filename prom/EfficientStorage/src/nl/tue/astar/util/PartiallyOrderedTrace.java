package nl.tue.astar.util;

import gnu.trove.TIntCollection;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

import java.util.Arrays;

import nl.tue.storage.compressor.BitMask;

public class PartiallyOrderedTrace extends LinearTrace {

	protected final int[][] predecessors;

	public PartiallyOrderedTrace(String label, int numEvents) {
		super(label, numEvents);
		predecessors = new int[numEvents][];
	}

	public PartiallyOrderedTrace(String label, int[] activitySequence) {
		super(label, activitySequence);
		predecessors = new int[activitySequence.length][];
	}

	public PartiallyOrderedTrace(String label, int[] activitySequence,
			int[][] predecessors) {
		super(label, activitySequence);
		this.predecessors = predecessors;
	}

	/**
	 * sets the predecessors for the event at index event. It is advisable to
	 * provide the predecessors in ascending order.
	 * 
	 * @param event
	 * @param predecessors
	 */
	public void setPredecessors(int event, int... predecessors) {
		this.predecessors[event] = Arrays.copyOf(predecessors,
				predecessors.length);
	}

	/**
	 * Returns true if and only if the event at the given index is enabled,
	 * given that those events for which executed is true have been executed.
	 * 
	 * More formally, returns true if and only if for all i in
	 * predecessors[index] holds that executed[i] == true
	 * 
	 * This method checks the predecessors in descending order, if they are
	 * specified in ascending order.
	 * 
	 * @param index
	 * @param executed
	 * @return
	 */

	public boolean isEnabled(int index, boolean[] executed) {
		assert executed.length == activities.length;
		if (predecessors[index] == null) {
			return true;
		}
		boolean ok = true;
		for (int i = predecessors[index].length; ok && i-- > 0;) {
			ok &= executed[predecessors[index][i]];
		}
		return ok;
	}

	@Override
	public TIntCollection getNextEvents(boolean[] executed) {
		assert executed.length == activities.length;
		TIntList enabled = new TIntArrayList(activities.length);
		for (int i = 0; i < activities.length; i++) {
			if (isEnabled(i, executed)) {
				enabled.add(i);
			}
		}
		return enabled;
	}

	@Override
	public TIntCollection getNextEvents(BitMask bitMask) {
		return getNextEvents(bitMask.toBooleanArray());
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof PartiallyOrderedTrace
				&& Arrays.equals(((PartiallyOrderedTrace) o).activities,
						activities)
				&& Arrays.deepEquals(((PartiallyOrderedTrace) o).predecessors,
						predecessors);
	}

	public int hashCode() {
		return Arrays.hashCode(activities) + 37
				* Arrays.deepHashCode(predecessors);
	}

}
