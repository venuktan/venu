package nl.tue.astar.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import nl.tue.astar.AStarObserver;
import nl.tue.astar.AStarThread;
import nl.tue.astar.ObservableAStarThread;
import nl.tue.astar.Record;

public class DotGraphAStarObserver implements AStarObserver {

	protected Writer writer;
	protected int visitCounter;
	protected int finalNodeCounter;
	protected final File outputFile;

	public DotGraphAStarObserver(File outputFile) {
		this.outputFile = outputFile;
	}

	@Override
	public void nodeVisited(Record node) {
		try {
			writer.write("n"
					+ node.getState()
					+ " [label=<n"
					+ node.getState()
					+ (node.getPredecessor() != null ? "<BR/>h:"
							+ node.getEstimatedRemainingCost() : "") + "<BR/>("
					+ (++visitCounter) + ")>,style=filled,color=lightgray];");
		} catch (Exception e) {

		}
	}

	@Override
	public void edgeTraversed(Record from, Record to) {
		try {
			String parameters;
			if (to.getModelMove() == AStarThread.NOMOVE) {
				parameters = "color=gold2";
			} else if (to.getMovedEvent() == AStarThread.NOMOVE) {
				parameters = "color=magenta2";
			} else {
				parameters = "color=limegreen";
			}

			writer.write("n"
					+ from.getState()
					+ " -> n"
					+ to.getState()
					+ " [penwidth=2.0,label=\""
					+ (to.getCostSoFar() - from.getCostSoFar())
					+ to.toString()
					+ "\","
					+ (to.getEstimatedRemainingCost() == ObservableAStarThread.ESTIMATEIRRELEVANT ? "arrowhead=teenormal,"
							: "") + parameters + "];");
			if (to.getEstimatedRemainingCost() != ObservableAStarThread.ESTIMATEIRRELEVANT) {
				writer.write("n" + to.getState() + " [label=<n" + to.getState()
						+ "<BR/>h:" + to.getEstimatedRemainingCost() + ">];");
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void initialNodeCreated(Record node) {
		try {

			visitCounter = 0;
			finalNodeCounter = 0;
			outputFile.createNewFile();
			OutputStream stream = new BufferedOutputStream(
					new FileOutputStream(outputFile));
			this.writer = new OutputStreamWriter(stream);
			writer.append("digraph G { ");
			writer.write("n" + node.getState() + " [peripheries=2];");
		} catch (IOException e) {
			this.writer = null;
		}
	}

	@Override
	public void finalNodeFound(Record node) {
		finalNodeCounter++;
		try {
			writer.write("n" + node.getState() + " [peripheries=2];");
			addPathToRoot(node);
		} catch (Exception e) {
		}
	}

	public void close() {
		try {
			if (writer != null) {
				writer.write("}");
				writer.close();
			}
		} catch (Exception e) {
		}
	}

	protected void addPathToRoot(Record node) throws IOException {
		while (node.getPredecessor() != null) {
			writer.write("n"
					+ node.getPredecessor().getState()
					+ " -> n"
					+ node.getState()
					+ " [weight=1,penwidth=5.0,style=dashed,color=red,arrowhead=none,arrowtail=none];");
			node = node.getPredecessor();
		}
	}

	@Override
	public void stoppedUnreliablyAt(Record node) {
		if (finalNodeCounter > 0) {
			return;
		}
		finalNodeCounter++;
		try {
			if (node != null) {
				writer.write("n" + node.getState()
						+ " [peripheries=2,shape=diamond];");
				addPathToRoot(node);
			}
			writer.write("}");
			writer.close();
		} catch (IOException e) {
		}
	}

}
