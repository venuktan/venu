/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.Vector;

/**
 * The Log mapping object
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Log extends GeneralMappingItem {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Mapping mapping;

	/**
	 * Returns the mapping the log belongs to (NOTE: can return null but
	 * shouldn't)
	 * 
	 * @return the mapping
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Mapping getMapping() {
		return mapping;
	}

	/**
	 * @param mapping
	 *            the mapping to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Trace trace;

	/**
	 * @return the trace
	 */
	public Trace getTrace() {
		if (trace == null)
			trace = new Trace();
		return trace;
	}

	/**
	 * @param newTrace
	 *            the trace to set
	 */
	public void setTrace(Trace newTrace) {
		newTrace.setLog(this);
		trace = newTrace;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Classifier> classifiers;

	/**
	 * @return the classifiers
	 */
	public Vector<Classifier> getClassifiers() {
		if (classifiers == null)
			classifiers = new Vector<Classifier>();
		return classifiers;
	}

	/**
	 * @param classifiers
	 *            the classifiers to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setClassifiers(Vector<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	/**
	 * @param newClassifier
	 *            the new classifier to add to the list
	 */
	public void addClassifier(Classifier newClassifier) {
		if (!getClassifiers().contains(newClassifier)) {
			newClassifier.setLog(this);
			getClassifiers().add(newClassifier);
		}
	}

	/**
	 * Returns the string 'Log' (since there can be only one)
	 */
	public String toString() {
		return "Log";
	}

}