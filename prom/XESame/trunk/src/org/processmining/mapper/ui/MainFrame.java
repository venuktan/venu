/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.deckfour.uitopia.ui.conf.ConfigurationSet;
import org.deckfour.uitopia.ui.conf.UIConfiguration;
import org.deckfour.uitopia.ui.util.ImageLoader;

/**
 * This is the Main GUI class which should be started!
 * This class calls on all the other classes to do their
 * thing.
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MainFrame extends JFrame {

	/*-
	 * General ToDo's:
	 * TODO Use 'overlay' windows instead of popup boxes
	 * 
	 * 
	 */

	/*
	 * The GUI uses the Paragraph layout class provided by JHlabs
	 * (http://www.jhlabs.com/java/layout/index.html). This library is provided
	 * under the Apache License, Version 2.0
	 */

	private static final long serialVersionUID = 3000058846006966241L;

	private static final String CONF_X = "window_x";
	private static final String CONF_Y = "window_y";
	private static final String CONF_WIDTH = "window_width";
	private static final String CONF_HEIGHT = "window_height";

	private final ConfigurationSet conf;

	//private final UIController controller;

	public MainFrame(final UIController controller) {
		 //this.controller = controller;

		// register closing action..
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				exitApplication(true);
			}

			public void windowClosed(WindowEvent e) {
				windowClosing(e);
			}
		});
		addComponentListener(new ComponentAdapter() {
			public void componentMoved(ComponentEvent e) {
				saveWindowState();
			}

			public void componentResized(ComponentEvent e) {
				saveWindowState();
			}
		});
		// restore window position and size
		conf = UIConfiguration.master().getChild(
				this.getClass().getCanonicalName());
		restoreWindowState();
		// set up window contents
		setLayout(new BorderLayout());
		this.add(controller.getMainView(), BorderLayout.CENTER);
		setTitle("XESame");

		// Application icon!
		setIconImage(ImageLoader.load("xesame_icon_32x32.png"));

	}

	protected void saveWindowState() {
		Point p = getLocation();
		conf.setInteger(CONF_X, p.x);
		conf.setInteger(CONF_Y, p.y);
		conf.setInteger(CONF_WIDTH, getWidth());
		conf.setInteger(CONF_HEIGHT, getHeight());
	}

	protected void restoreWindowState() {
		int x = conf.getInteger(CONF_X, 10);
		int y = conf.getInteger(CONF_Y, 10);
		int width = conf.getInteger(CONF_WIDTH, 1024);
		int height = conf.getInteger(CONF_HEIGHT, 750);
		x = Math.max(0, x);
		y = Math.max(0, y);
		width = Math.min(width,
				Toolkit.getDefaultToolkit().getScreenSize().width);
		height = Math.min(height,
				Toolkit.getDefaultToolkit().getScreenSize().height);
		this.setLocation(x, y);
		this.setSize(width, height);
	}

	protected void exitApplication(boolean askUser) {
		//Not needed anymore, the executor stops to exist by itself
		//controller.getHandler().shutdown();
		
		try {
			UIConfiguration.save();
		} catch (IOException e) {
			System.err.println("ERROR: Could not save UITopia configuration!");
			e.printStackTrace();
		}
		System.exit(0);
	}

	public static void main(String[] args) {
		new MainFrame(new UIController()).setVisible(true);
	}

}
