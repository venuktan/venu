/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.deckfour.uitopia.ui.conf.ConfigurationSet;
import org.deckfour.uitopia.ui.conf.UIConfiguration;
import org.deckfour.uitopia.ui.main.MainViewport;
import org.deckfour.uitopia.ui.main.Overlayable;
import org.deckfour.uitopia.ui.main.Viewable;
import org.deckfour.uitopia.ui.overlay.OverlayEnclosure;
import org.deckfour.uitopia.ui.overlay.TwoButtonOverlayDialog;

/**
 * This class builds the toolbar and headerbar and calls 
 * the three main browsers
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MainView extends JPanel implements Overlayable {

	private static final long serialVersionUID = 1L;

	public enum View {

		SETTINGS(), MAPPING(), EXECUTION();

		private Viewable viewable;

		public Viewable getViewable() {
			return viewable;
		}

		public void setViewable(Viewable viewable) {
			this.viewable = viewable;
		}

	}

	private UIController controller;

	private ConfigurationSet conf;
	private MainToolbar toolbar;
	private MainViewport viewport;

	private View activeView;
	private SettingsView settingsView;
	private MappingView mappingView;
	private volatile ExecutionView executionView;

	private OverlayEnclosure overlay;

	private JPanel header;

	public MainView(UIController uiController) {
		this.controller = uiController;
		this.setOpaque(false);
		this.setBorder(BorderFactory.createEmptyBorder());
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		viewport = new MainViewport();
		this.header = new MainHeader(uiController);
		toolbar = new MainToolbar(this);
		this.add(toolbar);
		this.add(header);
		this.add(viewport);
		JPanel blankView = new JPanel();
		blankView.setBackground(new Color(50, 50, 50));
		show(blankView);
		this.settingsView = new SettingsView(uiController);
		View.SETTINGS.setViewable(settingsView);
		this.mappingView = new MappingView(uiController);
		View.MAPPING.setViewable(mappingView);
		this.executionView = new ExecutionView(uiController);
		View.EXECUTION.setViewable(executionView);
		this.conf = UIConfiguration.master().getChild(
				this.getClass().getCanonicalName());
		this.activeView = View.valueOf(conf.get("activeView", "SETTINGS"));
		showView(activeView);
	}

	public UIController controller() {
		return this.controller;
	}

	public SettingsView getSettingsView() {
		return this.settingsView;
	}

	public MappingView getMappingView() {
		return this.mappingView;
	}

	public ExecutionView getExecutionView() {
		return this.executionView;
	}

	public void showSettingsView() {
		if (!this.activeView.equals(View.SETTINGS)) {
			showView(View.SETTINGS);
		}
	}

	public void showMappingView() {
		if (!this.activeView.equals(View.MAPPING)) {
			showView(View.MAPPING);
		}
	}

	public void showExecutionView() {
		if (!this.activeView.equals(View.EXECUTION)) {
			showView(View.EXECUTION);
		}
	}

	protected void showView(View view) {
		if (activeView.getViewable() != view.getViewable()) {
			activeView.getViewable().viewFocusLost();
		}
		if (view.equals(View.SETTINGS)) {
			show(settingsView);
		} else if (view.equals(View.MAPPING)) {
			show(mappingView);
		} else if (view.equals(View.EXECUTION)) {
			show(executionView);
		}
		conf.set("activeView", view.name());
		if (activeView.getViewable() != view.getViewable()) {
			view.getViewable().viewFocusGained();
		}
		this.activeView = view;
		toolbar.activateTab(view);
	}

	public void show(JComponent view) {
		viewport.setView(view);
	}

	public void showOverlay(JComponent overlay) {
		final OverlayEnclosure enclosure = new OverlayEnclosure(overlay, 1024,
				768);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				toolbar.setEnabled(false);
				viewport.showOverlay(enclosure);
			}
		});
		this.overlay = enclosure;
	}

	public boolean showOverlayDialog(TwoButtonOverlayDialog dialog) {
		// dialog.setMainView(this);
		// showOverlay(dialog);
		return dialog.getResultBlocking();
		// return true;
	}

	public void hideOverlay() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				toolbar.setEnabled(true);
				viewport.hideOverlay();
			}
		});
		overlay = null;
	}

	public boolean hideOverlay(JComponent overlay) {
		if (this.overlay != null && this.overlay.getEnclosed() == overlay) {
			hideOverlay();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		settingsView.updateData();
		mappingView.updateData();
		executionView.updateData();
	}

	/*-
	public Resource showResourcePickerDialog(String title,
			ResourceFilter filter, Comparator<Resource> comparator) {
		List<? extends Resource> resources = controller.getFrameworkHub()
				.getResourceManager().getAllResources(filter);
		Collections.sort(resources, comparator);
		ResourcePickerDialog dialog = new ResourcePickerDialog(this, title,
				resources);
		showOverlay(dialog);
		return dialog.getResourceBlocking();
	}*/

	/*-
	public ResourceType showResourceTypePickerDialog(String title) {
		List<? extends ResourceType> types = controller.getFrameworkHub()
				.getResourceManager().getAllSupportedResourceTypes();
		Collections.sort(types, new Comparator<ResourceType>() {
			public int compare(ResourceType o1, ResourceType o2) {
				return o1.getTypeName().compareTo(o2.getTypeName());
			}
		});
		ResourceTypePickerDialog dialog = new ResourceTypePickerDialog(this,
				title, types);
		showOverlay(dialog);
		return dialog.getResourceTypeBlocking();
	}
	 */

	/*-
	public InteractionResult showActionFeedbackDialog(String title,
			JComponent payload, boolean wizard, boolean first, boolean last) {
		if (wizard) {
			ActionWizardDialog dialog = new ActionWizardDialog(this, title,
					first, last, payload);
			showOverlay(dialog);
			Result result = dialog.getResultBlocking();
			if (result.equals(Result.PREVIOUS)) {
				return InteractionResult.PREV;
			} else if (result.equals(Result.CONTINUE)) {
				return (last ? InteractionResult.FINISHED
						: InteractionResult.NEXT);
			} else {
				return InteractionResult.CANCEL;
			}
		} else {
			ActionFeedbackDialog dialog = new ActionFeedbackDialog(this, title,
					payload);
			showOverlay(dialog);
			if (dialog.getResultBlocking()) {
				return InteractionResult.CONTINUE;
			} else {
				return InteractionResult.CANCEL;
			}
		}
	}*/

}
