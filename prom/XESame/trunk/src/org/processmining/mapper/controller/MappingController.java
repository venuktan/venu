/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.xml.parsers.ParserConfigurationException;

import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.mapping.*;
import org.processmining.mapper.ui.ExecutionBrowser;
import org.processmining.mapper.ui.UIController;
import org.xml.sax.SAXException;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.extension.XExtensionManager;
import org.deckfour.xes.model.*;

import org.pojava.datetime.*;

import com.fluxicon.slickerbox.components.IconVerticalTabbedPane;

/**
 * This class provides main functions for handling the mapping definition
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 */
public class MappingController {
	/*-
	 * TODO split up this large loggy controller into an:
	 *  - executor class
	 *  - 'handler' class (to which the UI talks to etc.)
	 *  - utils class (STATIC)
	 *  - mapping save/load class ('file handling')
	 *  - and maybe even a query builder class
	 */

	private volatile SourceController sourceConn;
	private volatile ExecutionController executor;
	private volatile UIController uiController;

	private Mapping mapping;
	private JTextPane console = null; // if set, output here
	// if set, update with progress
	private JProgressBar progressBar = null;

	// Store the attributes we add to new elements
	private Vector<Attribute> logExtensionAttributes;
	private Vector<Attribute> traceExtensionAttributes;
	private Vector<Attribute> eventExtensionAttributes;

	// Set the max message level to the default
	protected MESSAGELEVEL maxMessageLevel = XESameConstants.messageLevelDefault;
	private FileController fileController;

	// Constructor which initializes a mapping
	public MappingController(UIController uiController) {
		this.uiController = uiController;

		// Initialize stuff we need
		initialize();

		// And create a basic mapping to start from
		initializeMapping();
	}

	/**
	 * Constructor which adds the console first
	 * 
	 * @param consoleTextPane
	 *            Message console
	 */
	public MappingController(JTextPane consoleTextPane) {
		setConsole(consoleTextPane);

		// Initialize stuff we need
		initialize();

		// And create a basic mapping to start from
		initializeMapping();
	}

	/**
	 * Initializes the private variables needed further on
	 */
	private void initialize() {
		sourceConn = new SourceController(this);
		fileController = new FileController(this);

		// Private vectors storing the attributes defined by extensions
		logExtensionAttributes = new Vector<Attribute>();
		traceExtensionAttributes = new Vector<Attribute>();
		eventExtensionAttributes = new Vector<Attribute>();
	}

	/**
	 * When a new mapping is created Log and Trace objects can automatically be
	 * added so a good basic mapping exists to start from
	 */
	public void initializeMapping() {
		/*
		 * Build the initial mapping 'tree' from bottom up
		 */
		logExtensionAttributes = new Vector<Attribute>();
		traceExtensionAttributes = new Vector<Attribute>();
		eventExtensionAttributes = new Vector<Attribute>();

		// The trace
		Trace trace = MappingFactory.newTrace();
		addExtensionAttributes(trace, traceExtensionAttributes);

		// The log
		Log log = MappingFactory.newLog();
		log.setTrace(trace);
		addExtensionAttributes(log, logExtensionAttributes);

		// An event classifier that will fit most purposes
		Classifier classifier = MappingFactory.newClassifier();
		classifier.setName("Activity classifier");
		classifier.setKeys("concept:name lifecycle:transition");
		log.getClassifiers().add(classifier);
		// And a resource classifier comes in handy...
		Classifier resourceClassifier = MappingFactory.newClassifier();
		resourceClassifier.setName("Resource classifier");
		resourceClassifier.setKeys("org:resource");
		log.getClassifiers().add(resourceClassifier);

		this.mapping = MappingFactory.newMapping();
		this.mapping.setLog(log);

		mapping.setConnection(MappingFactory.newConnection());

		/*
		 * And now, add the 5 'default extensions'. This can fail and then we
		 * just continue but we show it on the console for the user
		 */
		for (String extension : XESameConstants.defaultExtensions) {
			try {
				parseExtension(extension, true);
			} catch (Exception e) {
				// Hide the exception to the user but display it on a console
				message(MESSAGELEVEL.ERROR, "Adding default extension '"
						+ extension + "' failed: " + e.getLocalizedMessage());
			}
		}
	}

	/**
	 * Adds clones of the attributes in the attribute list to the given item.
	 * Especially useful for initializing items with attributes as defined by
	 * extensions
	 * 
	 * @param item
	 *            Item to add the attributes to
	 * @param attributes
	 *            Attributes to add to the item
	 */
	private void addExtensionAttributes(GeneralMappingItem item,
			Vector<Attribute> attributes) {
		// Add to it, all the attributes that a log should have according to
		// our extensions
		for (Attribute attribute : attributes) {
			// Clone it!
			Attribute newAttribute = (Attribute) attribute.clone();

			/*
			 * And let the extension know it has a new follower. Unfortunately,
			 * the only way to get to the extension is via the newly cloned
			 * attribute. From there we can add the new attribute which is
			 * itself. Strange huh...
			 */
			newAttribute.getExtension().addAttribute(newAttribute);

			// And add the new attribute to the event
			item.addItemAttribute(newAttribute);
		}
	}

	/**
	 * Returns the current mapping reference
	 * 
	 * @return mapping reference
	 */
	public Mapping getMapping() {
		if (mapping == null)
			initializeMapping();

		return mapping;
	}

	/**
	 * Sets the mapping reference to the given mapping
	 * 
	 * @param mapping
	 */
	public void setMapping(Mapping mapping) {
		this.mapping = mapping;
	}

	/**
	 * Tries to parse the XES Extension definition from the given URI to an
	 * extension we understand and then store all the attributes defined so we
	 * can add those to all new events added. We might also update the current
	 * mapping instance, depending on the boolean attribute value
	 * 
	 * @param uri
	 *            location of the XESEXT specification file
	 * @param updateMapping
	 *            whether or not to update the current mapping instance (set to
	 *            FALSE when loading a mapping, to TRUE when updating an
	 *            existing mapping)
	 * @throws Exception
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws URISyntaxException
	 */
	public void parseExtension(String uri, Boolean updateMapping)
			throws IOException, ParserConfigurationException, SAXException,
			URISyntaxException {
		// Try to parse the given URI into an extension instance
		XExtensionManager extManager = XExtensionManager.instance();

		// Retrieve from cache or from source and add to cache
		XExtension xExtension = extManager.getByUri(new URI(uri));

		/*-
		 * Now that we have a correct XES extension instance, we need to do 2
		 * things: 
		 * 1: update our internal list of what attributes new
		 * log/trace/event items should have 
		 * and maybe 
		 * 2: add the attributes to all the existing log/trace/event instances
		 */

		// But before we do that, we create a new extension instance for our
		// domain model
		Extension newExtension = new Extension();
		newExtension.setURI(uri);
		newExtension.setPrefix(xExtension.getPrefix());
		newExtension.setName(xExtension.getName());

		// Loop through the meta definitions in the extension and store them
		// internally while possibly also updating the existing items

		// We start with the log attributes
		HashSet<XAttribute> logAttributes = (HashSet<XAttribute>) xExtension
				.getLogAttributes();

		for (XAttribute xAttribute : logAttributes) {
			// And now create an instance of our attribute from the XAttribute
			// instance
			Attribute attribute = converXtoAttribute(xAttribute);
			attribute.setExtension(newExtension);

			// But only add it when we don't have it yet...
			if (!isAttributeInVector(logExtensionAttributes, attribute)) {

				// Add the current attribute to our list so we can clone them
				// later
				// into new log instances (yes, that might occur, I guess)
				logExtensionAttributes.add(attribute);

				// And add them to the log in our current mapping if we are told
				// to do so
				if (updateMapping) {
					// But first clone it so its not the same as our internal
					// list
					Attribute attributeInstance = (Attribute) attribute.clone();
					// Create backreferences (both are created by just calling
					// this function!)
					newExtension.addAttribute(attributeInstance);
					// And add it to our domain model instance
					mapping.getLog().addItemAttribute(attributeInstance);
				}
			}
		}// end log attribute iteration

		// Now we parse the Trace attributes
		HashSet<XAttribute> traceAttributes = (HashSet<XAttribute>) xExtension
				.getTraceAttributes();

		for (XAttribute xAttribute : traceAttributes) {
			// And now create an instance of our attribute from the XAttribute
			// instance
			Attribute attribute = converXtoAttribute(xAttribute);
			attribute.setExtension(newExtension);

			// But only add it when we don't have it yet...
			if (!isAttributeInVector(traceExtensionAttributes, attribute)) {
				// Add the current attribute to our list so we can clone them
				// later
				// into new logs
				traceExtensionAttributes.add(attribute);

				// And add them to the log in our current mapping if we are told
				// to
				// do so
				if (updateMapping) {
					// But first clone it so its not the same as our internal
					// list
					Attribute attributeInstance = (Attribute) attribute.clone();
					// Create backreferences (both are created!)
					newExtension.addAttribute(attributeInstance);
					// And add it to our domain model instance
					mapping.getLog().getTrace()
							.addItemAttribute(attributeInstance);
				}
			}
		}

		// And the Event attributes
		HashSet<XAttribute> eventAttributes = (HashSet<XAttribute>) xExtension
				.getEventAttributes();

		for (XAttribute xAttribute : eventAttributes) {
			// And now create an instance of our attribute from the XAttribute
			// instance
			Attribute attribute = converXtoAttribute(xAttribute);
			attribute.setExtension(newExtension);

			// But only add it when we don't have it yet...
			if (!isAttributeInVector(eventExtensionAttributes, attribute)) {
				// Add the current attribute to our list so we can clone them
				// later
				// into new logs
				eventExtensionAttributes.add(attribute);

				// And add it to all our events if we are told to do so
				if (updateMapping) {
					Vector<Event> events = mapping.getLog().getTrace()
							.getEvents();
					for (Event event : events) {
						// First clone it so its not the same as our internal
						// list
						Attribute attributeInstance = (Attribute) attribute
								.clone();
						// Create backreferences (both are created!)
						newExtension.addAttribute(attributeInstance);

						// Now add this new clone to the current event instance
						event.addItemAttribute(attributeInstance);
					}// end loop through all events
				}
			}// end if updateMapping
		}

		// We skip the meta-attributes, that would be too messy I guessy (and
		// infinite)

		// And now that we have done everything, add the extension to our
		// mapping, if we should so
		if (updateMapping) {
			mapping.addExtension(newExtension);
		}
	}

	/**
	 * Returns true if the key of the given attribute is present in the vector
	 * of attributes given
	 * 
	 * @param vector
	 *            <Attribute>
	 * @param attribute
	 *            Attribute's key to look for
	 * @return TRUE if an attribute with given attribute key exists in vector
	 */
	private boolean isAttributeInVector(Vector<Attribute> vector,
			Attribute attribute) {
		for (Attribute vectorAtt : vector) {
			if (vectorAtt.getKey() == attribute.getKey()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Converts an XES Attribute instance to 'our' attribute instance
	 * 
	 * @param xAttribute
	 *            the XES Attribute instance to parse
	 * @return Attribute the attribute as we know it
	 */
	private Attribute converXtoAttribute(XAttribute xAttribute) {
		Attribute attribute = new Attribute();
		attribute.setKey(xAttribute.getKey());
		attribute.setType(getTypeString(xAttribute));
		// We can not get the value without knowing the type but we don't need
		// the value anyway, we're not importing xes event logs, we only use
		// this to understand the extensions

		return attribute;
	}

	/**
	 * Extracts our string representation of the type of the given XES attribute
	 * 
	 * @param aXttribute
	 *            XES Attribute
	 * @return String value of the type of XES Attribute
	 */
	private String getTypeString(XAttribute aXttribute) {
		if (aXttribute instanceof XAttributeLiteral) {
			return "String";
		} else if (aXttribute instanceof XAttributeBoolean) {
			return "Boolean";
		} else if (aXttribute instanceof XAttributeContinuous) {
			return "Float";
		} else if (aXttribute instanceof XAttributeDiscrete) {
			return "Integer";
		} else if (aXttribute instanceof XAttributeTimestamp) {
			return "Date";
		} else {
			return null;
		}
	}

	/**
	 * Removes the given extension instance from the mapping, removes the
	 * attributes defined by this extension in the mapping and removes the
	 * attributes stored in our list of attributes that should be added to new
	 * items
	 * 
	 * @param extension
	 *            extension to delete from the mapping
	 */
	public void removeExtension(Extension extension) {
		// First, for each attribute attached to this extension, detach it from
		// their parent
		Vector<Attribute> attributes = extension.getAttributes();

		for (Attribute attribute : attributes) {
			attribute.getParentItem().getItemAttributes().remove(attribute);
			// And hint the garbage collector to really delete it
			attribute = null;
		}

		/*
		 * Now we would like to update our internal list of default attributes
		 * defined by this extension
		 */
		removeAttributesFromInternalVector(logExtensionAttributes, extension);
		removeAttributesFromInternalVector(traceExtensionAttributes, extension);
		removeAttributesFromInternalVector(eventExtensionAttributes, extension);

		// And remove the extension itself too
		mapping.getExtensions().remove(extension);
		// And hint the garbage collector to really delete it
		extension = null;
	}

	/**
	 * Deletes all attributes that are related to the given extension from the
	 * given vector
	 * 
	 * @param vector
	 *            vector of attributes
	 * @param extension
	 *            extension for which the attributes should be deleted
	 */
	private void removeAttributesFromInternalVector(Vector<Attribute> vector,
			Extension extension) {
		/*
		 * The extension itself does not list the attribute in our
		 * 'extensionAttributes' vector. The attribute in that vector does have
		 * a reference to the extension it belongs to. Therefore, we loop
		 * through our extension attributes vectors, check if the attribute
		 * refers to the extension we are deleting and if so, remove that
		 * attribute from the vector
		 */

		for (int i = 0; i < vector.size(); i++) {
			if (vector.get(i).getExtension() == extension) {
				vector.remove(i);
				i--; // lets not skip elements...
			}
		}
	}

	/**
	 * Function that displays a resultset
	 * 
	 * @param resultSet
	 * @throws SQLException
	 */
	protected void displayResultSet(ResultSet resultSet) throws SQLException {
		ResultSetMetaData rsmd = resultSet.getMetaData();

		int columns = rsmd.getColumnCount();
		StringBuilder tableOut = new StringBuilder();
		for (int i = 1; i <= columns; i++) {
			int jdbcType = rsmd.getColumnType(i);
			String name = rsmd.getColumnTypeName(i);
			tableOut.append("Column " + i + " is JDBC type " + jdbcType);
			tableOut.append(", which the DBMS calls " + name + "\n");
		}

		int numberOfColumns = rsmd.getColumnCount();

		for (int i = 1; i <= numberOfColumns; i++) {
			if (i > 1)
				tableOut.append(",  ");
			String columnName = rsmd.getColumnName(i);
			tableOut.append(columnName);
		}
		tableOut.append("\n");

		while (resultSet.next()) {
			for (int i = 1; i <= numberOfColumns; i++) {
				if (i > 1)
					tableOut.append(",  ");
				try {
					String columnValue = resultSet.getString(i);
					tableOut.append(columnValue);
				} catch (SQLException eSQL) {
					// We will catch and ignore a 'invalid cursor state'
					// exception which can occur for DISTINCT Select queries
					// (very strrrrange)
					if (eSQL.getSQLState().equals("24000")) {
						// Do nothing
					} else {
						// Throw up (you know what I mean...)
						throw eSQL;
					}
				}
			}
			tableOut.append("\n");
		}
		message(MESSAGELEVEL.VERBOSE, tableOut.toString());
	}

	/**
	 * Outputs the given message of the given level to the 'message sink'
	 * 
	 * @param level
	 * @param message
	 */
	public void message(MESSAGELEVEL level, String message) {
		// Only show messages that are at least as serious as our max level
		if (level.compareTo(maxMessageLevel) <= 0) {
			// Prepare the message to show
			DateTime now = new DateTime();
			String timestamp = now.toString("yyyy-MM-dd HH:mm:ss");

			String outString = timestamp + ": (" + level + ") " + message;

			// Output it on the System console for real-time updates
			System.out.println(outString);

			// and in the GUI with coloring
			if (console != null)
			// Output to the JTextPane
			{
				StyledDocument doc = console.getStyledDocument();
				try {
					MutableAttributeSet attrs = console.getInputAttributes();

					// Depending on the message level, set the font color
					switch (level) {
					case ERROR:
						// Red
						StyleConstants.setForeground(attrs, Color.red);
						break;
					case WARNING:
						// Orange err magenta (orange on gray: not a success)
						StyleConstants.setForeground(attrs, Color.magenta);
						break;
					case PROGRESS:
						// Blue
						StyleConstants.setForeground(attrs, Color.blue);
						break;
					case NOTICE:
						// Black
						StyleConstants.setForeground(attrs, Color.black);
						break;
					case DEBUG:
						// Dark Gray
						StyleConstants.setForeground(attrs, Color.darkGray);
						break;
					case VERBOSE:
						// Gray
						StyleConstants.setForeground(attrs, Color.gray);
						break;
					default:
						StyleConstants.setForeground(attrs, Color.black);
						break;
					}

					doc.insertString(doc.getLength(), outString + "\n", attrs);

					// scroll down
					console.setCaretPosition(console.getDocument().getLength());
				} catch (BadLocationException e) {
					System.out
							.println("--"
									+ MESSAGELEVEL.ERROR.toString()
											.toUpperCase()
									+ "-- There is an error while updating the console.");
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Creates a new event with the given name (using the factory) but also adds
	 * the attributes as defined by the extensions
	 * 
	 * @param eventName
	 * @return Event created
	 */
	public Event newEvent(String eventName) {
		Event event = MappingFactory.newEvent(eventName);
		addExtensionAttributes(event, eventExtensionAttributes);
		return event;
	}

	/**
	 * Function to transfer the call to the source connection class without
	 * exposing it
	 * 
	 * @param connection
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws MalformedURLException
	 */
	public void testConnection(Connection connection)
			throws ClassNotFoundException, SQLException, MalformedURLException,
			InstantiationException, IllegalAccessException {
		sourceConn.testConnection(connection);
	}

	/**
	 * Connects to the given data source
	 * 
	 * @param connection
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws MalformedURLException
	 */
	public void parseConnection(Connection connection,
			boolean supressErrorMessages) throws ClassNotFoundException,
			SQLException, MalformedURLException, InstantiationException,
			IllegalAccessException {
		sourceConn.parseConnection(connection, supressErrorMessages);
	}

	/**
	 * Sets the console reference to output messages to
	 * 
	 * @param console
	 *            console to output, null to remove
	 */
	public void setConsole(JTextPane console) {
		this.console = console;
	}

	/**
	 * Set the maximum message level to output
	 * 
	 * @param level
	 *            maximum message type level to output
	 */
	public void setMessageLevel(MESSAGELEVEL level) {
		maxMessageLevel = level;
	}

	/**
	 * Get the maximum message level shown
	 * 
	 * @return MESSAGELEVEL max level shown
	 */
	public MESSAGELEVEL getMessageLevel() {
		return maxMessageLevel;
	}

	/**
	 * Set the progress bar to update our progress to, null to remove
	 * 
	 * @param progressBar
	 */
	public void setProgressBar(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	/**
	 * Update the progress bar (if set) to the given step
	 * 
	 * @param step
	 */
	public void updateProgress(int step) {
		updateProgress(step, null);
	}

	/**
	 * Update the progress bar (if set) to the given step and status text
	 * 
	 * @param step
	 * @param status
	 */
	public void updateProgress(final int step, final String status) {
		/*-Runnable doSetProgressBarValue = new Runnable() {
			public void run() {/**/

		// Calculate the number of steps
		/*-
		 * init, log extraction, trace extraction, event extractions,
		 * cacheDB=>XES 
		 * So 4 + #events
		 */

		int totalExecutionSteps = 4 + mapping.getLog().getTrace().getEvents()
				.size();

		if (progressBar != null) {
			progressBar.setValue((step / (totalExecutionSteps + 1)) * 100);
			if (status != null) {
				// progressBar.setString(status);
				// progressBar.setString("" + step + '/' +
				// totalExecutionSteps);
			}
			// progressBar.updateUI();
		}

		// Only show a message if we have an active step
		if (step <= totalExecutionSteps) {
			String messageString = "Starting step " + step + " of "
					+ totalExecutionSteps;
			if (status != null)
				messageString += ": " + status + ".";

			message(MESSAGELEVEL.PROGRESS, messageString);
		}

		/*-}
		};

		SwingUtilities.invokeLater(doSetProgressBarValue);/**/
	}

	/**
	 * Changes the location of the cache database
	 * 
	 * @param location
	 *            String new location path
	 */
	public void setCacheLocation(String location) {
		executor.setCacheLocation(location);
	}

	/**
	 * This function is called by the executing class to tell the GUI that the
	 * overlay may be closed. The executor class has no access to the GUI, only
	 * to the mapping controller.
	 */
	public synchronized void executorFinished() {
		//System.out.println("MappingController.executorFinished called");
		
		// We then close the overlay so the user has control again
		uiController.getMainView().hideOverlay(
				uiController.getMainView().getExecutionView()
						.getExecutionBrowser().getExecutionConsoleBrowser());

		//System.out.println("MappingController.executorFinished overlay closed");

		// Switch to the console tab
		ExecutionBrowser browser = uiController.getMainView()
				.getExecutionView().getExecutionBrowser();
		
		/**/
		String consoleTabName = browser.getConsoleTabName();
		//System.out.println("MappingController.executorFinished Console tab name: " + consoleTabName);
		
		IconVerticalTabbedPane pane = browser.getTabs();
		
		//System.out.println("MappingController.executorFinished Got the pane");
		
		
		//DISABLED buggy
		//pane.selectTab(consoleTabName);
		/**/
		
		//browser.switchToConsoleTab();

		//System.out.println("MappingController.executorFinished Finished");
	}

	/**
	 * Saves the current Mapping definition to the given file
	 * 
	 * @param mappingFile
	 * @throws IOException
	 */
	public void save(File mappingFile) throws IOException {
		fileController.save(mapping, mappingFile);
	}

	/**
	 * Loads, returns and updates the internal Mapping definition from the given
	 * file
	 * 
	 * @param mappingFile
	 * @return Mapping instance
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws URISyntaxException
	 */
	public Mapping load(File mappingFile) throws IOException,
			ParserConfigurationException, SAXException, URISyntaxException {
		mapping = fileController.load(mappingFile);

		// Parse the stored extensions so our internal attributes lists are up
		// to date
		Vector<Extension> extensions = mapping.getExtensions();
		for (Extension extension : extensions) {
			try {
				parseExtension(extension.getURI(), false);
			} catch (IOException e) {
				// TODO ENH ask for an update if we can not find the location
				message(MESSAGELEVEL.ERROR,
						"We could not load the extensions as specified in the saved mapping "
								+ "due to the following error: "
								+ e.getLocalizedMessage());
				throw (e); // pass it on to notify the user...
			} catch (ParserConfigurationException e) {
				message(MESSAGELEVEL.ERROR,
						"We could not load the extensions as specified in the saved mapping "
								+ "due to the following error: "
								+ e.getLocalizedMessage());
				throw (e); // pass it on to notify the user...
			} catch (SAXException e) {
				message(MESSAGELEVEL.ERROR,
						"We could not load the extensions as specified in the saved mapping "
								+ "due to the following error: "
								+ e.getLocalizedMessage());
				throw (e); // pass it on to notify the user...
			} catch (URISyntaxException e) {
				message(MESSAGELEVEL.ERROR,
						"We could not load the extensions as specified in the saved mapping "
								+ "due to the following error: "
								+ e.getLocalizedMessage());
				throw (e); // pass it on to notify the user...
			}
		}

		return mapping;
	}
}
