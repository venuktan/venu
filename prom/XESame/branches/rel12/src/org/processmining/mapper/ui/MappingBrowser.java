/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.ui.browsers.MappingDefinitionBrowser;

import com.fluxicon.slickerbox.components.IconVerticalTabbedPane;

/**
 * The mapping browser calls two more specific browsers to build the GUI
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MappingBrowser extends JPanel {

	private static final long serialVersionUID = -1467890361679445697L;
	private static final Color BG = new Color(180, 180, 180);
	private static final Color FG = new Color(60, 60, 60);

	private IconVerticalTabbedPane tabs;
	private final UIController controller;

	private MappingDefinitionBrowser mappingDefinitionBrowser;
	//private MappingVisualizationBrowser mappingVisualizationBrowser;

	private final static String TAB1 = "Definition";

	public MappingBrowser(UIController controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		setOpaque(false);
		setupUI();
	}

	private void setupUI() {
		// setup the tabs
		mappingDefinitionBrowser = new MappingDefinitionBrowser(controller);

		tabs = new IconVerticalTabbedPane(FG, BG, 100);
		tabs.setPassiveBackground(new Color(140, 140, 140));
		tabs.setMouseOverFadeColor(new Color(90, 90, 90));

		tabs.addTab(TAB1, ImageLoader.load("tab_definition_60x60_black.png"),
				mappingDefinitionBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mappingDefinitionBrowser.updateData();
					}
				});

		/*-
		tabs.addTab(TAB2,
				ImageLoader.load("tab_visualization_60x60_black.png"),
				mappingVisualizationBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mappingVisualizationBrowser.updateData();
					}
				});
				*/

		this.add(tabs, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		mappingDefinitionBrowser.updateData();
//		mappingVisualizationBrowser.updateData();
	}
}
