/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.mapping.Attribute;
import org.processmining.mapper.mapping.GeneralMappingItem;
import org.processmining.mapper.mapping.Link;
import org.processmining.mapper.mapping.Log;
import org.processmining.mapper.mapping.MappingFactory;
import org.processmining.mapper.mapping.Properties;
import org.processmining.mapper.mapping.Trace;
import org.processmining.mapper.ui.UIController;
import org.processmining.mapper.ui.models.AttributesTableModel;
import org.processmining.mapper.ui.models.ClassifierTableModel;
import org.processmining.mapper.ui.models.MappingTreeModel;
import org.processmining.mapper.ui.models.PropertiesTableModel;

import com.fluxicon.slickerbox.components.RoundedPanel;
import com.fluxicon.slickerbox.components.SlickerTabbedPane;
import com.fluxicon.slickerbox.ui.SlickerCheckBoxUI;
import com.fluxicon.slickerbox.ui.SlickerComboBoxUI;
import com.jhlabs.awt.BasicGridLayout;

/**
 * This browser provides the mapping definition tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class MappingDefinitionBrowser extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2668672328472241725L;

	private final UIController uiController;

	private JPanel definitionPanel = null;
	private SlickerTabbedPane propertiesTabbedPane;

	private JPanel mappingTreePanel;
	private JPanel attributesPanel;
	private JPanel propertiesPanel;
	private JPanel classifiersPanel;

	private JScrollPane classifiersScrollPane;
	private JTable classifiersTable;
	private JPanel classifiersButtonPanel;

	private JTree mappingTree;

	private JButton addEventButton;
	private JButton removeEventButton;

	private JButton addAttributeButton;
	private JButton removeAttributeButton;
	private JButton addLeafAttributeButton;
	private JButton removeLeafAttributeButton;
	private JButton promoteAttributeLeafButton;

	private JButton addLinkButton;
	private JButton removeLinkButton;

	private JButton addClassifierButton;
	private JButton removeClassifierButton;

	private JPanel mappingTreeButtonPanel;
	private JScrollPane mappingTreeScrollPane;

	private JScrollPane attrbituesTableScrollPane;

	private JTable attributesTable;
	private JPanel attributesButtonPanel;

	private JPanel propertiesButtonPanel;
	private JScrollPane propertiesTableScrollPane;
	private JTable propertiesTable;

	public MappingDefinitionBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(uiController.setupTopPanel("Define the mapping "
				+ "between the data elements and the event log concepts"),
				BorderLayout.NORTH);
		browser.add(getDefinitionPanel(), BorderLayout.CENTER);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		MappingTreeModel model = (MappingTreeModel) getMappingTree().getModel();
		model.setRoot(uiController.getMapping().getLog());

		// getAttributesTable().updateUI();
	}

	/**
	 * This method initializes the project settingsPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getDefinitionPanel() {
		if (definitionPanel == null) {
			definitionPanel = new JPanel();

			UIController.makeup(definitionPanel);
			definitionPanel.setLayout(new BorderLayout());

			definitionPanel.add(getMappingTreePanel(), BorderLayout.WEST);
			definitionPanel.add(getPropertiesTabbedPane(), BorderLayout.CENTER);
		}

		return definitionPanel;
	}

	/**
	 * This method initializes mappingTreePanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getMappingTreePanel() {
		if (mappingTreePanel == null) {
			mappingTreePanel = new RoundedPanel(15, 0, 3);

			UIController.makeup(mappingTreePanel);

			mappingTreePanel.setLayout(new BorderLayout());
			mappingTreePanel.setMinimumSize(new Dimension(200, 112));
			mappingTreePanel.setPreferredSize(new Dimension(200, 112));
			mappingTreePanel.setMaximumSize(new Dimension(300, 300));
			mappingTreePanel.add(getMappingTreeButtonPanel(),
					BorderLayout.NORTH);
			mappingTreePanel.add(getMappingTreeScrollPane(),
					BorderLayout.CENTER);
		}
		return mappingTreePanel;
	}

	/**
	 * This method initializes mappingTreeScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getMappingTreeScrollPane() {
		if (mappingTreeScrollPane == null) {
			mappingTreeScrollPane = new JScrollPane();

			UIController.makeup(mappingTreeScrollPane);

			mappingTreeScrollPane
					.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
			mappingTreeScrollPane.setViewportView(getMappingTree());
			mappingTreeScrollPane.setBorder(null);
		}
		return mappingTreeScrollPane;
	}

	/**
	 * This method initializes mappingTree and implements 'selection' actions
	 * 
	 * @return javax.swing.JTree
	 */
	private JTree getMappingTree() {
		if (mappingTree == null) {
			mappingTree = new JTree(new MappingTreeModel(uiController
					.getMapping().getLog()));
			UIController.makeup(mappingTree);

			// Modify some of the tree's properties
			mappingTree.getSelectionModel().setSelectionMode(
					TreeSelectionModel.SINGLE_TREE_SELECTION);
			mappingTree.setShowsRootHandles(true);

			// Make stuff happen when we select another node
			mappingTree.addTreeSelectionListener(new TreeSelectionListener() {
				public void valueChanged(TreeSelectionEvent e) {
					// Reload the attributes- and properties tables
					GeneralMappingItem node = (GeneralMappingItem) mappingTree
							.getLastSelectedPathComponent();

					if (node == null)
						// Nothing is selected so nothing is updated
						return;

					// Update the attributes table
					AttributesTableModel attributesTableModel = (AttributesTableModel) getAttributesTable()
							.getModel();
					attributesTableModel.reloadAttributes(node
							.getItemAttributes());
					attributesTableModel.fireTableDataChanged();

					// And the properties table (requires a bit more work)
					if (node instanceof Log
							|| node instanceof Trace
							|| node instanceof org.processmining.mapper.mapping.Event) {
						// Add the properties Panel if not present already
						// so try to remove it first (the slickertabbedpane has
						// limited functionality)
						getPropertiesTabbedPane().removeTab(
								getPropertiesPanel());
						getPropertiesTabbedPane().addTab("Properties",
								getPropertiesPanel());
					} else {
						// Select the attributes tab
						getPropertiesTabbedPane().selectTab(
								getAttributesPanel());
						// And remove the properties tab
						getPropertiesTabbedPane().removeTab(
								getPropertiesPanel());
					}

					PropertiesTableModel propertiesTableModel = (PropertiesTableModel) getPropertiesTable()
							.getModel();
					propertiesTableModel.reloadValues(node);

					// And enable/disable the 'event classifiers' tab, depending
					// on the selection
					if (node instanceof Log) {
						getPropertiesTabbedPane().removeTab(
								getClassifiersPanel());
						// Add classifiers tab
						getPropertiesTabbedPane().addTab("Classifiers",
								getClassifiersPanel());
					} else {
						// If the classifiers tab is selected, go to the
						// attributes tab
						if (getPropertiesTabbedPane().getSelected() == getClassifiersPanel())
							getPropertiesTabbedPane().selectTab(
									getAttributesPanel());

						// Remove the classifiers tab
						getPropertiesTabbedPane().removeTab(
								getClassifiersPanel());
						getPropertiesTabbedPane().updateUI();
					}

					ClassifierTableModel classifierTableModel = (ClassifierTableModel) classifiersTable
							.getModel();
					classifierTableModel.reloadValues(uiController.getMapping()
							.getLog().getClassifiers());
				}
			});
		}

		return mappingTree;
	}

	/**
	 * This method initializes mappingTreeButtonPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getMappingTreeButtonPanel() {
		if (mappingTreeButtonPanel == null) {
			mappingTreeButtonPanel = new RoundedPanel(15, 0, 3);

			UIController.makeup(mappingTreeButtonPanel);
			mappingTreeButtonPanel
					.setBackground(UIController.colorButtonGroupBg);

			mappingTreeButtonPanel.setLayout(new BasicGridLayout(2, 2));
			mappingTreeButtonPanel.add(getAddEventButton(), null);
			mappingTreeButtonPanel.add(getRemoveEventButton(), null);
			mappingTreeButtonPanel.add(getAddAttributeButton(), null);
			mappingTreeButtonPanel.add(getRemoveAttributeButton(), null);
		}
		return mappingTreeButtonPanel;
	}

	/**
	 * This method initializes propertiesTabbedPane
	 * 
	 * @return javax.swing.JTabbedPane
	 */
	private SlickerTabbedPane getPropertiesTabbedPane() {
		if (propertiesTabbedPane == null) {
			propertiesTabbedPane = new SlickerTabbedPane("Definition",
					UIController.colorButtonGroupBg, UIController.colorTabFg,
					UIController.colorTabTitle);

			UIController.makeup(propertiesTabbedPane);

			propertiesTabbedPane.setVisible(true);
			// In the beginning there is only the attributes tab
			propertiesTabbedPane.addTab("Attributes", getAttributesPanel());
		}
		return propertiesTabbedPane;
	}

	/**
	 * This method initializes attributesPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAttributesPanel() {
		if (attributesPanel == null) {
			attributesPanel = new RoundedPanel(15, 0, 3);

			UIController.makeup(attributesPanel);

			attributesPanel.setLayout(new BorderLayout());
			attributesPanel.add(getAttributesButtonPanel(), BorderLayout.NORTH);
			attributesPanel.add(getAttrbituesTableScrollPane(),
					BorderLayout.CENTER);
		}
		return attributesPanel;
	}

	/**
	 * This method initializes attrbituesTableScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getAttrbituesTableScrollPane() {
		if (attrbituesTableScrollPane == null) {
			attrbituesTableScrollPane = new JScrollPane();
			UIController.makeup(attrbituesTableScrollPane);
			attrbituesTableScrollPane.setViewportView(getAttributesTable());
		}
		return attrbituesTableScrollPane;
	}

	/**
	 * This method initializes attributesTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTable getAttributesTable() {
		if (attributesTable == null) {
			attributesTable = new JTable(new AttributesTableModel());
			UIController.makeup(attributesTable);

			// Let's give the type column a drop down box
			TableColumn attributeTypeColumn = attributesTable.getColumnModel()
					.getColumn(3);
			JComboBox attributeTypeComboBox = new JComboBox();
			attributeTypeComboBox.setUI(new SlickerComboBoxUI());
			attributeTypeComboBox.addItem("String");
			attributeTypeComboBox.addItem("Date");
			attributeTypeComboBox.addItem("Integer");
			attributeTypeComboBox.addItem("Float");
			attributeTypeComboBox.addItem("Boolean");
			attributeTypeColumn.setCellEditor(new DefaultCellEditor(
					attributeTypeComboBox));

			TableColumn subAttColumn = attributesTable.getColumnModel()
					.getColumn(0);
			// TODO ENH make checkbox in table look Slick
			JCheckBox subAttCheckBox = new JCheckBox();
			subAttCheckBox.setUI(new SlickerCheckBoxUI());
			subAttColumn.setCellEditor(new DefaultCellEditor(subAttCheckBox));

		}
		return attributesTable;
	}

	/**
	 * This method initializes attributesButtonPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getAttributesButtonPanel() {
		if (attributesButtonPanel == null) {
			attributesButtonPanel = new JPanel();
			UIController.makeup(attributesButtonPanel);
			attributesButtonPanel.setLayout(new FlowLayout());
			attributesButtonPanel.add(getAddLeafAttributeButton(), null);
			attributesButtonPanel.add(getRemoveLeafAttributeButton(), null);
			attributesButtonPanel.add(getPromoteAttributeLeafButton(), null);
		}
		return attributesButtonPanel;
	}

	/**
	 * This method initializes propertiesPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getPropertiesPanel() {
		if (propertiesPanel == null) {
			propertiesPanel = new JPanel();
			UIController.makeup(propertiesPanel);
			propertiesPanel.setLayout(new BorderLayout());
			propertiesPanel.add(getPropertiesButtonPanel(), BorderLayout.NORTH);
			propertiesPanel.add(getPropertiesTableScrollPane(),
					BorderLayout.CENTER);
		}
		return propertiesPanel;
	}

	/**
	 * This method initializes propertiesTableScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getPropertiesTableScrollPane() {
		if (propertiesTableScrollPane == null) {
			propertiesTableScrollPane = new JScrollPane();
			UIController.makeup(propertiesTableScrollPane);
			propertiesTableScrollPane.setViewportView(getPropertiesTable());
		}
		return propertiesTableScrollPane;
	}

	/**
	 * This method initializes propertiesTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTable getPropertiesTable() {
		if (propertiesTable == null) {
			propertiesTable = new JTable(new PropertiesTableModel());
			UIController.makeup(propertiesTable);
		}
		return propertiesTable;
	}

	/**
	 * This method initializes propertiesButtonPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getPropertiesButtonPanel() {
		if (propertiesButtonPanel == null) {
			propertiesButtonPanel = new JPanel();
			UIController.makeup(propertiesButtonPanel);
			propertiesButtonPanel.setLayout(new FlowLayout());
			propertiesButtonPanel.add(getAddLinkButton(), null);
			propertiesButtonPanel.add(getRemoveLinkButton(), null);
		}
		return propertiesButtonPanel;
	}

	/**
	 * This method initializes classifiersPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getClassifiersPanel() {
		if (classifiersPanel == null) {
			classifiersPanel = new JPanel();
			UIController.makeup(classifiersPanel);
			classifiersPanel.setLayout(new BorderLayout());
			classifiersPanel.setName("classifiersPanel");
			classifiersPanel.add(getClassifiersButtonPanel(),
					BorderLayout.NORTH);
			classifiersPanel.add(getClassifiersScrollPane(),
					BorderLayout.CENTER);
		}
		return classifiersPanel;
	}

	/**
	 * This method initializes classifiersScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getClassifiersScrollPane() {
		if (classifiersScrollPane == null) {
			classifiersScrollPane = new JScrollPane();
			UIController.makeup(classifiersScrollPane);
			classifiersScrollPane.setViewportView(getClassifiersTable());
		}
		return classifiersScrollPane;
	}

	/**
	 * This method initializes classifiersTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTable getClassifiersTable() {
		if (classifiersTable == null) {
			classifiersTable = new JTable(new ClassifierTableModel(uiController
					.getMapping().getLog().getClassifiers()));
			UIController.makeup(classifiersTable);
		}
		return classifiersTable;
	}

	/**
	 * This method initializes classifiersButtonPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getClassifiersButtonPanel() {
		if (classifiersButtonPanel == null) {
			classifiersButtonPanel = new JPanel();
			UIController.makeup(classifiersButtonPanel);
			classifiersButtonPanel.setLayout(new FlowLayout());
			classifiersButtonPanel.add(getAddClassifierButton(), null);
			classifiersButtonPanel.add(getRemoveClassifierButton(), null);
		}
		return classifiersButtonPanel;
	}

	/**
	 * This method initializes addEventButton (using the mapping tree)
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddEventButton() {
		if (addEventButton == null) {
			addEventButton = new ImageLozengeButton(
					ImageLoader.load("button_eventAdd_30x30_black.png"), "");
			addEventButton.setToolTipText("Add new event definition");
			// addEventButton = new JButton();
			// addEventButton.setName("AddEvent");
			// addEventButton.setText("Add Event");

			addEventButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Request the display name, this is not editable (yet)
					String eventName = JOptionPane
							.showInputDialog("What should be the display name of the new event mapping?");

					// Create our new event with some initial attributes
					org.processmining.mapper.mapping.Event event = uiController
							.getHandler().newEvent(eventName);

					// and add it to the correct node
					Trace trace = uiController.getMapping().getLog().getTrace();
					trace.addEvent(event);

					// And let our tree know what we've done
					MappingTreeModel model = (MappingTreeModel) mappingTree
							.getModel();
					// construct a fixed path since we need no selection...
					TreePath parentPath = new TreePath(new Object[] {
							uiController.getMapping().getLog(), trace });
					// The location the event is added to is the total number of
					// children minus the number of meta-attributes which we
					// calculate in this cumbersome way...
					int childCount = model.getChildCount(trace)
							- 1
							- model.getChildCountMetaAttribute(trace
									.getItemAttributes().iterator());
					// and now fire the update method on the tree
					model.fireChildAdded(parentPath, childCount, event);
				}
			});
		}
		return addEventButton;
	}

	/**
	 * This method initializes removeEventButton (using the mapping tree)
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveEventButton() {
		if (removeEventButton == null) {
			removeEventButton = new ImageLozengeButton(
					ImageLoader.load("button_eventRemove_30x30_black.png"), "");
			removeEventButton.setToolTipText("Remove selected event");
			// removeEventButton = new JButton();
			// removeEventButton.setText("Remove Event");

			removeEventButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					// First we need to know the selected node
					TreePath selectionPath = mappingTree.getSelectionPath();

					// Check if the user selected something and if he selected
					// an attribute
					if (selectionPath == null
							|| !(selectionPath.getLastPathComponent() instanceof org.processmining.mapper.mapping.Event)) {
						// We can not delete the selected item, notify user
						JOptionPane
								.showMessageDialog(
										mappingTree,
										"Sorry, but you did not select an event to delete. Please select the event you want to delete and try again.",
										"No event selected",
										JOptionPane.ERROR_MESSAGE);
					} else {
						// Okay, we have an event and it should be deleted
						org.processmining.mapper.mapping.Event event = (org.processmining.mapper.mapping.Event) selectionPath
								.getLastPathComponent();
						Trace parentNode = event.getTrace();

						MappingTreeModel model = (MappingTreeModel) mappingTree
								.getModel();
						int index = model.getIndexOfChild(parentNode, event);

						// remove the event
						parentNode.getEvents().remove(event);

						// update the tree
						model.fireChildRemoved(selectionPath.getParentPath(),
								index, event);
					}
				}
			});
		}
		return removeEventButton;
	}

	/**
	 * This method initializes addAttributeButton (using the mapping tree)
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddAttributeButton() {
		if (addAttributeButton == null) {
			addAttributeButton = new ImageLozengeButton(
					ImageLoader.load("button_attributeAdd_30x30_black.png"), "");
			addAttributeButton.setToolTipText("Add new attribute");
			// addAttributeButton = new JButton();
			// addAttributeButton.setText("Add Attribute");

			addAttributeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// First we need to know the selected node
					MappingTreeModel model = (MappingTreeModel) mappingTree
							.getModel();
					GeneralMappingItem parentNode = null;
					TreePath parentPath = mappingTree.getSelectionPath();

					if (parentPath == null) {
						parentNode = (GeneralMappingItem) mappingTree
								.getModel().getRoot();
						parentPath = new TreePath(parentNode);
					} else {
						parentNode = (GeneralMappingItem) (parentPath
								.getLastPathComponent());
					}

					// Now we can get a new meta attribute
					Attribute metaAttribute = MappingFactory.newMetaAttribute();

					// and add it to the correct node
					parentNode.addItemAttribute(metaAttribute);

					// And let our tree know what we've done
					model.fireChildAdded(parentPath,
							model.getChildCount(parentNode) - 1, metaAttribute);
				}
			});
		}
		return addAttributeButton;
	}

	/**
	 * This method initializes removeAttributeButton (using the mapping tree)
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveAttributeButton() {
		if (removeAttributeButton == null) {
			removeAttributeButton = new ImageLozengeButton(
					ImageLoader.load("button_attributeRemove_30x30_black.png"),
					"");
			removeAttributeButton.setToolTipText("Remove selected attribute");
			// removeAttributeButton = new JButton();
			// removeAttributeButton.setText("Remove Attribute");

			removeAttributeButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					// First we need to know the selected node
					TreePath selectionPath = mappingTree.getSelectionPath();

					// Check if the user selected something and if so, if he
					// selected an attribute
					if (selectionPath == null
							|| !(selectionPath.getLastPathComponent() instanceof Attribute)) {
						// We can not delete the selected item, notify user
						JOptionPane
								.showMessageDialog(
										mappingTree,
										"Sorry, but you did not select an attribute to delete. Please select the attribute you want to delete and try again.",
										"No attribute selected",
										JOptionPane.ERROR_MESSAGE);
					} else {
						// Okay, we have an attribute and it should be deleted
						Attribute attribute = (Attribute) selectionPath
								.getLastPathComponent();
						GeneralMappingItem parentNode = attribute
								.getParentItem();

						MappingTreeModel model = (MappingTreeModel) mappingTree
								.getModel();
						int index = model
								.getIndexOfChild(parentNode, attribute);

						// delete the attribute
						parentNode.getItemAttributes().remove(attribute);

						// and notify the tree
						model.fireChildRemoved(selectionPath.getParentPath(),
								index, attribute);
					}
				}
			});
		}
		return removeAttributeButton;
	}

	/**
	 * This method initializes addLeafAttributeButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddLeafAttributeButton() {
		if (addLeafAttributeButton == null) {
			addLeafAttributeButton = new ImageLozengeButton(
					ImageLoader.load("button_leafAttributeAdd_30x30_black.png"),
					"");
			addLeafAttributeButton.setToolTipText("Add sub attribute");
			// addLeafAttributeButton = new JButton();
			// addLeafAttributeButton.setText("Add Attribute");

			addLeafAttributeButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// First we need to know the selected node in our
							// tree (to add our attribute to)
							GeneralMappingItem parentNode = null;
							TreePath parentPath = mappingTree
									.getSelectionPath();

							if (parentPath == null) {
								parentNode = (GeneralMappingItem) mappingTree
										.getModel().getRoot();
								parentPath = new TreePath(parentNode);
							} else {
								parentNode = (GeneralMappingItem) (parentPath
										.getLastPathComponent());
							}

							// Let the controller create a new leaf attribute
							// and add it to the correct node
							parentNode.addItemAttribute(MappingFactory
									.newLeafAttribute());

							// And let our table know what we've done
							AttributesTableModel tableModel = (AttributesTableModel) attributesTable
									.getModel();
							tableModel.fireTableRowsInserted(
									tableModel.getRowCount(),
									tableModel.getRowCount() + 1);
						}
					});
		}
		return addLeafAttributeButton;
	}

	/**
	 * This method initializes removeLeafAttributeButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveLeafAttributeButton() {
		if (removeLeafAttributeButton == null) {
			removeLeafAttributeButton = new ImageLozengeButton(
					ImageLoader
							.load("button_leafAttributeRemove_30x30_black.png"),
					"");
			removeLeafAttributeButton.setToolTipText("Remove attribute");
			// removeLeafAttributeButton = new JButton();
			// removeLeafAttributeButton.setText("Remove Attribute");

			removeLeafAttributeButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// Get the selected attribute
							AttributesTableModel tableModel = (AttributesTableModel) getAttributesTable()
									.getModel();
							int selectedRow = getAttributesTable()
									.getSelectedRow();

							if (selectedRow < 0)
								// If nothing is selected just do
								// well, nothing...
								return; 

							// otherwise, delete the bad attribute, we don't
							// want him here anymore
							Attribute badAttribute = tableModel
									.getAttributeAt(selectedRow);
							GeneralMappingItem parent = badAttribute
									.getParentItem();

							// IFF the attribute was a meta attribute then we
							// need to notify the tree of our removal
							if (badAttribute.isMeta()) {
								// First we need to know the selected node
								TreePath selectionPath = mappingTree
										.getSelectionPath();

								// Check if the user selected something
								if (selectionPath != null) {
									// Okay, we have a selected item in the tree
									// that we should update
									GeneralMappingItem parentNode = badAttribute
											.getParentItem();
									// GeneralMappingItem parentNode =
									// (GeneralMappingItem)
									// selectionPath.getLastPathComponent();

									MappingTreeModel model = (MappingTreeModel) mappingTree
											.getModel();
									int index = model.getIndexOfChild(
											parentNode, badAttribute);

									// FIRST delete the attribute
									parentNode.getItemAttributes().remove(
											badAttribute);

									// and THEN notify the tree
									model.fireChildRemoved(
											selectionPath,
											index, badAttribute);
								}
							} else {
								// if not, then we need to still remove it
								// (we could not split this since we can only
								// update the tree after removing the thing but
								// if we don't need to update the tree then we
								// didn't delete it yet...)
								parent.getItemAttributes().remove(badAttribute);
							}

							// And make it disappear from the table
							tableModel.fireTableDataChanged();
							tableModel.fireTableRowsDeleted(selectedRow,
									selectedRow + 1);

						}
					});
		}
		return removeLeafAttributeButton;
	}

	/**
	 * This method initializes promoteAttributeLeafButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getPromoteAttributeLeafButton() {
		if (promoteAttributeLeafButton == null) {
			promoteAttributeLeafButton = new ImageLozengeButton(
					ImageLoader
							.load("button_leafAttributePromote_30x30_black.png"),
					"");
			promoteAttributeLeafButton
					.setToolTipText("Add a child attribute");
			// promoteAttributeLeafButton = new JButton();
			// promoteAttributeLeafButton.setText("Add sub attribute");

			promoteAttributeLeafButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// Get the selected attribute
							AttributesTableModel tableModel = (AttributesTableModel) getAttributesTable()
									.getModel();
							int selectedRow = getAttributesTable()
									.getSelectedRow();

							if (selectedRow < 0)
								return; // If nothing is selected just do
							// nothing...

							Attribute pregnantAttribute = tableModel
									.getAttributeAt(selectedRow);
							pregnantAttribute.addItemAttribute(MappingFactory
									.newLeafAttribute());

							// Now update the table
							tableModel.fireTableDataChanged();
							tableModel.fireTableRowsDeleted(selectedRow,
									selectedRow + 1);

							// And the tree
							TreePath parentPath = mappingTree
									.getSelectionPath();
							MappingTreeModel treeModel = (MappingTreeModel) mappingTree
									.getModel();
							treeModel.fireChildAdded(parentPath, treeModel
									.getChildCount(parentPath
											.getLastPathComponent()) - 1,
									pregnantAttribute);
						}
					});
		}
		return promoteAttributeLeafButton;
	}

	/**
	 * This method initializes addLinkButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddLinkButton() {
		if (addLinkButton == null) {
			addLinkButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Add Link");
			addLinkButton.setToolTipText("Add a new link definition");
			// addLinkButton = new JButton();
			// addLinkButton.setText("Add Link");

			addLinkButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// First we need to know the selected node in our
					// tree (to add our attribute to)
					GeneralMappingItem parentNode = null;
					TreePath parentPath = mappingTree.getSelectionPath();

					if (parentPath == null) {
						parentNode = (GeneralMappingItem) mappingTree
								.getModel().getRoot();
						parentPath = new TreePath(parentNode);
					} else {
						parentNode = (GeneralMappingItem) (parentPath
								.getLastPathComponent());
					}

					// Add the link to the properties of the node
					GeneralMappingItem generalMappingItem = (GeneralMappingItem) parentNode;
					generalMappingItem.getProperties().addLink(
							MappingFactory.newLink());

					// And let our table know what we've done
					PropertiesTableModel tableModel = (PropertiesTableModel) getPropertiesTable()
							.getModel();
					tableModel.fireTableRowsInserted(tableModel.getRowCount(),
							tableModel.getRowCount() + 1);
				}
			});
		}
		return addLinkButton;
	}

	/**
	 * This method initializes removeLinkButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveLinkButton() {
		if (removeLinkButton == null) {
			removeLinkButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Remove Link");
			removeLinkButton
					.setToolTipText("Remove the selected link definition");

			removeLinkButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					PropertiesTableModel propertiesTableModel = (PropertiesTableModel) getPropertiesTable()
							.getModel();

					int selectedRow = getPropertiesTable().getSelectedRow();

					if (selectedRow < 0)
						return; // If nothing is selected just do
					// nothing...

					Vector<Link> links = propertiesTableModel.getLinks();
					Link selectedLink = propertiesTableModel
							.getLinkAt(selectedRow);

					Properties properties = null;
					if (selectedLink.getProperties() != null)
						properties = selectedLink.getProperties();
					else
						return; // run and hide!

					// Check if the user selected a link row
					if (selectedLink != null) {
						links.remove(selectedLink);
					}

					properties.setLinks(links);

					propertiesTableModel.fireTableRowsDeleted(selectedRow,
							selectedRow + 1);
				}
			});
		}
		return removeLinkButton;
	}

	/**
	 * This method initializes addClassifierButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddClassifierButton() {
		if (addClassifierButton == null) {
			addClassifierButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Add Classifier");
			addClassifierButton.setToolTipText("Add a new classifier");
			// addClassifierButton = new JButton();
			// addClassifierButton.setText("Add Classifier");

			addClassifierButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					uiController.getMapping().getLog().getClassifiers()
							.add(MappingFactory.newClassifier());

					ClassifierTableModel tableModel = (ClassifierTableModel) classifiersTable
							.getModel();

					tableModel.fireTableRowsInserted(tableModel.getRowCount(),
							tableModel.getRowCount() + 1);
				}
			});
		}
		return addClassifierButton;
	}

	/**
	 * This method initializes removeClassifierButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveClassifierButton() {
		if (removeClassifierButton == null) {
			removeClassifierButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Remove Classifier");
			removeClassifierButton
					.setToolTipText("Remove the selected classifier");
			// removeClassifierButton = new JButton();
			// removeClassifierButton.setText("Remove Classifier");

			removeClassifierButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Get the selected classifier
					ClassifierTableModel tableModel = (ClassifierTableModel) classifiersTable
							.getModel();
					int selectedRow = classifiersTable.getSelectedRow();

					if (selectedRow < 0)
						return; // If nothing is selected just do
					// nothing...

					// Delete from the list and update the table
					uiController.getMapping().getLog().getClassifiers()
							.remove(selectedRow);
					tableModel.fireTableRowsDeleted(selectedRow,
							selectedRow + 1);
				}
			});
		}
		return removeClassifierButton;
	}

}
