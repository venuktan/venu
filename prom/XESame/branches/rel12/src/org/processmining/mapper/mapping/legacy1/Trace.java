/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

/**
 * Trace mapping object
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class Trace extends GeneralMappingItem {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Log log;

	/**
	 * Returns the log we belong to (NOTE: can return null but shouldn't)
	 * 
	 * @return the log
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @param log
	 *            the log to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setLog(Log log) {
		this.log = log;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Event> events;

	/**
	 * @return the events
	 */
	public Vector<Event> getEvents() {
		if (events == null)
			events = new Vector<Event>();
		return events;
	}

	/**
	 * @param events
	 *            the events to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setEvents(Vector<Event> events) {
		this.events = events;
	}

	/**
	 * Adds the given event to the list of events and sets the reference in the
	 * event
	 * 
	 * @param newEvent
	 *            the events to add to the list of events
	 * @author Joos Buijs
	 */
	public void addEvent(Event newEvent) {
		if (!getEvents().contains(newEvent)) {
			newEvent.setTrace(this);
			getEvents().add(newEvent);
		}
	}

	/**
	 * Returns the string 'Trace' since we can have only one...
	 */
	public String toString() {
		return "Trace";
	}
}