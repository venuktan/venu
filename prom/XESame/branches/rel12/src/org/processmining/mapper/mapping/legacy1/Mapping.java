/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

/**
 * Main mapping class to which all other mapping objects relate
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Mapping {
	// Add attributes for the mapping execution settings (mxml, cacheDB,
	// eventlog)???

	/**
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Log log;

	/**
	 * Returns the log set for this mapping (NOTE: can return null but shouldn't
	 * generally)
	 * 
	 * @return the log
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Log getLog() {
		return log;
	}

	/**
	 * @param log
	 *            the log to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setLog(Log log) {
		this.log = log;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String name;

	/**
	 * @return the name
	 */
	public String getName() {
		if (name == null)
			name = "";
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String description;

	/**
	 * @return the description
	 */
	public String getDescription() {
		if (description == null)
			description = "";
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Extension> extensions;

	/**
	 * @return the extensions
	 */
	public Vector<Extension> getExtensions() {
		if (extensions == null)
			extensions = new Vector<Extension>();
		return extensions;
	}

	/**
	 * @param extensions
	 *            the extensions to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setExtensions(Vector<Extension> extensions) {
		this.extensions = extensions;
	}

	/**
	 * Add an extension to the list of extensions
	 * 
	 * @param newExtension
	 */
	public void addExtension(Extension newExtension) {
		// Only add it when its not already in the list
		if (!getExtensions().contains(newExtension)) {
			newExtension.setMapping(this);
			getExtensions().add(newExtension);
		}
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Connection connection;

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		if (connection == null)
			connection = new Connection();

		return connection;
	}

	/**
	 * @param connection
	 *            the connection to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * Returns the string 'Mapping: ' + name
	 */
	public String toString() {
		return "Mapping: " + name;
	}
}