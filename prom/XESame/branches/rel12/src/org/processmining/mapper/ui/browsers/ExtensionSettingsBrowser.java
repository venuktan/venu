/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ArrangementHelper;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.ui.UIController;
import org.processmining.mapper.ui.models.ExtensionTableModel;

/**
 * This browser provides the extension settings tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ExtensionSettingsBrowser extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8111058383691281752L;

	private final UIController uiController;

	private JPanel extensionPanel;
	private JPanel extensionButtonPanel;
	private JScrollPane extensionsScrollPane;
	private JTable extensionsTable;
	private JButton addExtensionButton;
	private JButton removeExtensionButton;

	public ExtensionSettingsBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(uiController
				.setupTopPanel("Manage which XES extensions to use"),
				BorderLayout.NORTH);
		browser.add(getExtensionPanel(), BorderLayout.CENTER);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		// The Extension table
		ExtensionTableModel extensionTableModel = (ExtensionTableModel) extensionsTable
				.getModel();
		extensionTableModel.updateExtensions(uiController.getMapping()
				.getExtensions());
		extensionTableModel.fireTableDataChanged();
	}

	/**
	 * This method initializes extensionPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getExtensionPanel() {
		if (extensionPanel == null) {
			extensionPanel = new JPanel();
			UIController.makeup(extensionPanel);
			extensionPanel.setLayout(new BorderLayout());
			extensionPanel.add(getExtensionButtonPanel(), BorderLayout.NORTH);
			extensionPanel.add(getExtensionsScrollPane(), BorderLayout.CENTER);
		}
		return extensionPanel;
	}

	/**
	 * This method initializes extensionButtonPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getExtensionButtonPanel() {
		if (extensionButtonPanel == null) {
			extensionButtonPanel = new JPanel();
			UIController.makeup(extensionButtonPanel);
			extensionButtonPanel.setLayout(new GridBagLayout());
			extensionButtonPanel.add(ArrangementHelper
					.pushLeft(getAddExtensionButton()));
			extensionButtonPanel.add(ArrangementHelper
					.pushLeft(getRemoveExtensionButton()));
		}
		return extensionButtonPanel;
	}

	/**
	 * This method initializes extensionsScrollPane
	 * 
	 * @return javax.swing.JScrollPane
	 */
	private JScrollPane getExtensionsScrollPane() {
		if (extensionsScrollPane == null) {
			extensionsScrollPane = new JScrollPane();
			UIController.makeup(extensionsScrollPane);

			extensionsScrollPane.setMinimumSize(new Dimension(200, 100));
			extensionsScrollPane.setPreferredSize(new Dimension(200, 200));
			extensionsScrollPane.setMaximumSize(new Dimension(300, 300));
			extensionsScrollPane.setViewportView(getExtensionsTable());
		}
		return extensionsScrollPane;
	}

	/**
	 * This method initializes extensionsTable
	 * 
	 * @return javax.swing.JTable
	 */
	private JTable getExtensionsTable() {
		if (extensionsTable == null) {
			extensionsTable = new JTable(new ExtensionTableModel(uiController
					.getMapping().getExtensions()));
			UIController.makeup(extensionsTable);

		}
		return extensionsTable;
	}

	/**
	 * This method initializes addExtensionButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getAddExtensionButton() {
		if (addExtensionButton == null) {
			addExtensionButton = new ImageLozengeButton(ImageLoader
					.load("button_extensionAdd_30x30_black.png"),
					"Add Extension");
			// addExtensionButton = new JButton();
			// addExtensionButton.setText("Add Extension");

			addExtensionButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Extensions are a bit different and require parsing by the
					// handler

					// First, we ask the user for an URI
					String URI = JOptionPane
							.showInputDialog("Please provide a location (local or an URL) to the extension definition");

					/*
					 * Then, ask the handler to add this extension to the
					 * mapping definition but only if the URI is non-empty
					 */
					if (URI != null && URI.length() > 0) {
						try {
							uiController.getHandler().parseExtension(URI, true);

							ExtensionTableModel tableModel = (ExtensionTableModel) extensionsTable
									.getModel();

							tableModel.fireTableRowsInserted(tableModel
									.getRowCount(),
									tableModel.getRowCount() + 1);

						} catch (Exception eExt) {
							// Display a nice but informative dialog to the user
							// with error info

							//TODO ENH ask for an update if we can not find the location
							JOptionPane.showMessageDialog(getExtensionPanel(),
									"Sorry, but the extension could not be parsed due to the following error: \n "
											+ eExt.getLocalizedMessage(),
									"Extension parsing failed",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			});
		}
		return addExtensionButton;
	}

	/**
	 * This method initializes removeExtensionButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getRemoveExtensionButton() {
		if (removeExtensionButton == null) {
			removeExtensionButton = new ImageLozengeButton(ImageLoader
					.load("button_extensionRemove_30x30_black.png"),
					"Remove Extension");
			// removeExtensionButton = new JButton();
			// removeExtensionButton.setText("Remove Extension");

			removeExtensionButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// Since this is a rather drastic operation, ask the user if
					// he is really sure
					int ask = JOptionPane
							.showConfirmDialog(
									getExtensionPanel(),
									"When you delete this extension, all related attributes "
											+ "with all its values will also be deleted. \n\n "
											+ "Are you really sure you want to remove this extension from you mapping?",
									"Are you sure you want to delete the extension?",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.WARNING_MESSAGE);

					// Well, lets not be a complete jerk and listen to the
					// user's choice (yes = 0, no = 1)
					if (ask == 0) {
						// Get the selected extension
						ExtensionTableModel tableModel = (ExtensionTableModel) extensionsTable
								.getModel();
						int selectedRow = extensionsTable.getSelectedRow();

						if (selectedRow < 0)
							return; // If nothing is selected just do
						// nothing...

						// Delete from the list and update the table
						uiController.getHandler().removeExtension(
								uiController.getMapping().getExtensions().get(
										selectedRow));
						tableModel.fireTableRowsDeleted(selectedRow,
								selectedRow + 1);

						// Also update the currently visible attribute table!
						/*-
						AttributesTableModel attributeTableModel = (AttributesTableModel) attributesTable
								.getModel();
						attributeTableModel.fireTableDataChanged();
						 */
					}
				}
			});
		}
		return removeExtensionButton;
	}

}
