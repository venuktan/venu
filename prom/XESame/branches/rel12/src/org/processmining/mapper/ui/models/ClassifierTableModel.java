/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.processmining.mapper.mapping.Classifier;

/**
 * Class that shows the classifiers in a nice editable table
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class ClassifierTableModel extends AbstractTableModel {
	// We have 2 columns: "Name" and "Keys"
	private String[] columnNames = new String[] { "Name", "Keys" };
	
	private Vector<Classifier> classifiers;

	public ClassifierTableModel(Vector<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	public ClassifierTableModel() {

	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Class<?> getColumnClass(int col) {
		return String.class;
	}

	@Override
	public int getRowCount() {
		return classifiers.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		Classifier classifier = classifiers.get(row);

		switch (col) {
		case 0:
			return classifier.getName();
		case 1:
			return classifier.getKeys();
		default:
			return null;
		}
	}

	/**
	 * Every cell is editable so we always return true
	 */
	public boolean isCellEditable(int row, int col) {
		return true;
	}

	public void setValueAt(Object value, int row, int col) {
		Classifier classifier = classifiers.get(row);

		switch (col) {
		case 0:
			classifier.setName(value.toString());
			break;
		case 1:
			classifier.setKeys(value.toString());
			break;
		default:
			break;
		}

		fireTableCellUpdated(row, col);
	}

	public void fireTableRowsInserted(int from, int to) {
		// Extra check, if there are 0 classifiers to begin with, one of the two
		// is invalid and the table will not be updated so fix that
		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;

		super.fireTableRowsInserted(from, to); // now delegate the call
	}

	/**
	 * Updates the internal classifiers list
	 * 
	 * @param newClassifierList
	 */
	public void reloadValues(Vector<Classifier> newClassifierList) {
		if (newClassifierList == null)
			classifiers = new Vector<Classifier>();
		else
			classifiers = newClassifierList;
	}
}
