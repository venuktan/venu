/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

import java.util.Vector;

/**
 * Generalization of a mapping item which has properties and attributes 
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class GeneralMappingItem {
//extends GeneralMappingItemVertex{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8599097927284819096L;
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Vector<Attribute> itemAttributes;

	/**
	 * @return the itemAttributes
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Vector<Attribute> getItemAttributes() {
		if (itemAttributes == null)
			itemAttributes = new Vector<Attribute>();
		return itemAttributes;
	}

	/**
	 * Retrieve the attribute with the given key
	 * 
	 * @param key
	 * @return
	 */
	public Attribute getItemAttribute(String key) {
		for (Attribute attribute : getItemAttributes()) {
			if (attribute.getKey().equalsIgnoreCase(key))
				return attribute;
		}

		return null;
	}

	/**
	 * @param itemAttributes
	 *            the itemAttributes to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setItemAttributes(Vector<Attribute> attributes) {
		this.itemAttributes = attributes;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @!generated 
	 *             "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Properties properties;

	/**
	 * @return the properties
	 * @!generated 
	 *             "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Properties getProperties() {
		if (properties == null) {
			// We always should have properties so make them and link them
			properties = new Properties();
			properties.setGeneralmappingitem(this);
		}
		return properties;
	}

	/**
	 * @param properties
	 *            the properties to set
	 * @!generated 
	 *             "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Adds the given attribute to our set of itemAttributes (if not already in
	 * there) and sets a backlink to us in the attribute
	 * 
	 * @param newItemAttribute
	 *            the itemAttributes to add to the list of itemAttributes
	 * @author Joos Buijs
	 */
	public void addItemAttribute(Attribute newItemAttribute) {
		if (!getItemAttributes().contains(newItemAttribute)) {
			// set reference
			newItemAttribute.setParent(this);
			// and add
			getItemAttributes().add(newItemAttribute);
		}
	}
}