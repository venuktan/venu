/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping.legacy1;

import java.util.Vector;

import org.processmining.mapper.controller.Utils;
import org.processmining.mapper.mapping.Event;
import org.processmining.mapper.mapping.Extension;
import org.processmining.mapper.mapping.Link;
import org.processmining.mapper.mapping.Log;
import org.processmining.mapper.mapping.Properties;
import org.processmining.mapper.mapping.Trace;

/**
 * Allows a Legacy1 mapping to be converted to the latest mapping definition
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 * 
 */
public final class Legacy1Convertor {

	/**
	 * Converts a given legacy1 mapping to the latest mapping definition
	 * 
	 * @param legacyMapping
	 * @return
	 */
	public static org.processmining.mapper.mapping.Mapping parseMapping(
			Mapping legacyMapping) {
		org.processmining.mapper.mapping.Mapping mapping = new org.processmining.mapper.mapping.Mapping();

		/*
		 * Convert all the elements
		 */
		// First the simple types
		mapping.setName(legacyMapping.getName());
		mapping.setDescription(legacyMapping.getDescription());

		// Then delegate to specific parse functions
		mapping.setConnection(parseConnection(legacyMapping.getConnection(),
				mapping));
		mapping.setExtensions(parseExtensions(legacyMapping.getExtensions(),
				mapping));

		mapping.setLog(parseLog(legacyMapping.getLog(), mapping));

		return mapping;
	}

	/**
	 * Converts a vector of legacy1 extensions to the latest version of
	 * extensions
	 * 
	 * @param legacyExtensions
	 * @param mapping
	 * @return
	 */
	private static Vector<Extension> parseExtensions(
			Vector<org.processmining.mapper.mapping.legacy1.Extension> legacyExtensions,
			org.processmining.mapper.mapping.Mapping mapping) {
		Vector<Extension> extensions = new Vector<Extension>();

		for (org.processmining.mapper.mapping.legacy1.Extension extension : legacyExtensions) {
			extensions.add(parseExtension(extension, mapping));
		}

		return extensions;
	}

	/**
	 * Converts a given legacy1 extension to the latest extension definition
	 * 
	 * @param legacyExtension
	 * @return
	 */
	private static Extension parseExtension(
			org.processmining.mapper.mapping.legacy1.Extension legacyExtension,
			org.processmining.mapper.mapping.Mapping mapping) {
		Extension extension = new Extension();

		extension.setName(legacyExtension.getName());
		extension.setPrefix(legacyExtension.getPrefix());
		extension.setURI(parseExtensionURI(legacyExtension.getURI()));

		extension.setMapping(mapping);
		extension.setAttributes(org.processmining.mapper.mapping.MappingFactory
				.getAttributesForExtension(extension, mapping.getLog()));

		return extension;
	}

	/**
	 * Converts an old URI to a new one. The 5 standard extensions used to be
	 * stored on code.fluxicon.com but are now moved to xes-standard.org
	 * 
	 * @param uri old extension uri
	 * @return updated extension uri
	 */
	private static String parseExtensionURI(String uri) {
		StringBuilder newURI = new StringBuilder(uri);
		
		//Replace the old URI's with the new ones (try them all)
		newURI = Utils.replaceAll(newURI, "http://code.fluxicon.com/xes/concept.xesext", "http://www.xes-standard.org/concept.xesext");
		newURI = Utils.replaceAll(newURI, "http://code.fluxicon.com/xes/lifecycle.xesext", "http://www.xes-standard.org/lifecycle.xesext");
		newURI = Utils.replaceAll(newURI, "http://code.fluxicon.com/xes/org.xesext", "http://www.xes-standard.org/org.xesext");
		newURI = Utils.replaceAll(newURI, "http://code.fluxicon.com/xes/time.xesext", "http://www.xes-standard.org/time.xesext");
		newURI = Utils.replaceAll(newURI, "http://code.fluxicon.com/xes/semantic.xesext", "http://www.xes-standard.org/semantic.xesext");
		
		return newURI.toString();
	}

	/**
	 * Converts a given legacy1 connection to the latest connection definition
	 * 
	 * @param legacyConnection
	 * @param mapping
	 * @return
	 */
	private static org.processmining.mapper.mapping.Connection parseConnection(
			Connection legacyConnection,
			org.processmining.mapper.mapping.Mapping mapping) {
		org.processmining.mapper.mapping.Connection connection = new org.processmining.mapper.mapping.Connection();

		/*
		 * Convert all the elements
		 */
		// First the simple types
		connection.setColumnSeperatorStart(legacyConnection
				.getColumnSeperatorStart());
		connection.setColumnSeperatorEnd(legacyConnection
				.getColumnSeperatorEnd());
		connection.setDescription(legacyConnection.getDescription());
		connection.setDriver(legacyConnection.getDriver());
		connection.setDriverLocation(legacyConnection.getDriverLocation());
		connection.setMapping(mapping);
		connection.setProperties(legacyConnection.getProperties());
		connection.setURL(legacyConnection.getURL());

		// Then delegate to specific parse functions
		// -

		return connection;
	}

	/**
	 * Converts a given legacy1 classifier to the latest classifier definition
	 * 
	 * @param legacyClassifier
	 * @param log
	 * @return
	 */
	private static org.processmining.mapper.mapping.Classifier parseClassifier(
			Classifier legacyClassifier, Log log) {
		org.processmining.mapper.mapping.Classifier classifier = new org.processmining.mapper.mapping.Classifier();
		classifier.setKeys(legacyClassifier.getKeys());
		classifier.setLog(log);
		classifier.setName(legacyClassifier.getName());
		return classifier;
	}

	/**
	 * Converts a given legacy1 log to the latest log definition
	 * 
	 * @param legacyLog
	 * @param mapping
	 * @return
	 */
	private static Log parseLog(
			org.processmining.mapper.mapping.legacy1.Log legacyLog,
			org.processmining.mapper.mapping.Mapping mapping) {
		org.processmining.mapper.mapping.Log log = new Log();

		/*
		 * Convert all the elements
		 */
		// First the simple types
		log.setMapping(mapping);

		// Then delegate to specific parse functions
		// classifiers (we do our own for loop)
		for (Classifier legacyClassifier : legacyLog.getClassifiers()) {
			log.getClassifiers().add(parseClassifier(legacyClassifier, log));
		}

		// attributes!
		log.setItemAttributes(parseAttributes(legacyLog.getItemAttributes(),
				log, mapping));

		// And properties
		log.setProperties(parseProperties(legacyLog, null));

		// And then continue to the trace
		log.setTrace(parseTrace(legacyLog.getTrace(), log));

		return log;
	}

	/**
	 * Converts a given legacy1 trace to the latest trace definition
	 * 
	 * @param legacyTrace
	 * @param log
	 * @return
	 */
	private static Trace parseTrace(
			org.processmining.mapper.mapping.legacy1.Trace legacyTrace,
			org.processmining.mapper.mapping.Log log) {
		Trace trace = new Trace();

		// First connect to its parent, the log
		trace.setLog(log);

		// Parse the events, attributes and properties
		trace.setEvents(parseEvents(legacyTrace, trace));
		trace.setItemAttributes(parseAttributes(
				legacyTrace.getItemAttributes(), log, log.getMapping()));
		trace.setProperties(parseProperties(legacyTrace, log));

		return trace;
	}

	/**
	 * Converts a given vector of legacy1 event to the latest event definition
	 * 
	 * @param legacyTrace
	 * @param trace
	 * @return
	 */
	private static Vector<Event> parseEvents(
			org.processmining.mapper.mapping.legacy1.Trace legacyTrace,
			Trace trace) {
		Vector<Event> events = new Vector<Event>();

		// loop and add
		for (org.processmining.mapper.mapping.legacy1.Event legacyEvent : legacyTrace
				.getEvents()) {
			events.add(parseEvent(legacyEvent, trace));
		}

		return events;
	}

	/**
	 * Converts a given legacy1 event to the latest event definition
	 * 
	 * @param legacyEvent
	 * @param trace
	 * @return
	 */
	private static Event parseEvent(
			org.processmining.mapper.mapping.legacy1.Event legacyEvent,
			Trace trace) {
		Event event = new Event();

		//Connect to parent
		event.setTrace(trace);
		
		// copy the simple attribute
		event.setDisplayName(legacyEvent.getDisplayName());

		// parse more complex items
		event.setItemAttributes(parseAttributes(
				legacyEvent.getItemAttributes(), trace, trace.getLog()
						.getMapping()));
		event.setProperties(parseProperties(legacyEvent, trace));
		
		return event;
	}

	/**
	 * Converts a given legacy1 attribute vector to the latest attribute
	 * definition
	 * 
	 * @param itemAttributes
	 * @param parentItem
	 * @param mapping
	 * @return
	 */
	private static Vector<org.processmining.mapper.mapping.Attribute> parseAttributes(
			Vector<Attribute> itemAttributes,
			org.processmining.mapper.mapping.GeneralMappingItem parentItem,
			org.processmining.mapper.mapping.Mapping mapping) {
		Vector<org.processmining.mapper.mapping.Attribute> attributes = new Vector<org.processmining.mapper.mapping.Attribute>();

		// loop and add
		for (Attribute legacyAttribute : itemAttributes) {
			attributes
					.add(parseAttribute(legacyAttribute, parentItem, mapping));
		}

		return attributes;
	}

	/**
	 * Converts a given legacy1 attribute to the latest attribute definition
	 * 
	 * @param legacyAttribute
	 * @param mapping
	 * @return
	 */
	private static org.processmining.mapper.mapping.Attribute parseAttribute(
			Attribute legacyAttribute,
			org.processmining.mapper.mapping.GeneralMappingItem parentItem,
			org.processmining.mapper.mapping.Mapping mapping) {

		org.processmining.mapper.mapping.Attribute attribute = new org.processmining.mapper.mapping.Attribute();

		attribute.setKey(legacyAttribute.getKey());
		attribute.setParent(parentItem);
		attribute.setType(legacyAttribute.getType());
		attribute.setValue(legacyAttribute.getValue());

		/*
		 * See if we can find the extension to which this attribute belongs. If
		 * not, then don't connect it and make the connection when adding
		 * extensions.
		 */
		org.processmining.mapper.mapping.legacy1.Extension legacyAttributeExtension = legacyAttribute
				.getExtension();
		if (legacyAttributeExtension != null) {
			org.processmining.mapper.mapping.Extension extension = mapping
					.getExtensionByPrefix(legacyAttributeExtension.getPrefix());
			if (extension != null) {
				attribute.setExtension(extension);
			}
		}

		attribute.setProperties(parseProperties(legacyAttribute, parentItem));
		attribute.setItemAttributes(parseAttributes(
				legacyAttribute.getItemAttributes(), attribute, mapping));

		return attribute;
	}

	/**
	 * Converts a given legacy1 properties to the latest properties definition
	 * 
	 * @param mappingItem
	 *            the legacy mapping item to get the properties from
	 * @param parentItem
	 *            the new parent item to connect the properties to
	 * @return
	 */
	private static Properties parseProperties(
			org.processmining.mapper.mapping.legacy1.GeneralMappingItem mappingItem,
			org.processmining.mapper.mapping.GeneralMappingItem parentItem) {
		Properties properties = new Properties();

		// Copy simple attributes
		properties.setEventOrder(mappingItem.getProperties().getEventOrder());
		properties.setFrom(mappingItem.getProperties().getFrom());

		// the log doesn't have a parent. Trace, events and attributes do
		if (!(mappingItem instanceof org.processmining.mapper.mapping.legacy1.Log)) {
			properties.setGeneralmappingitem(parentItem);
			parentItem.setProperties(properties);
		}

		// Parse more complex items
		// Convert the links
		Vector<Link> links = parseLinks(mappingItem.getProperties().getLinks());
		// Add them
		properties.setLinks(links);
		// And link them...
		for (Link link : links) {
			link.setProperties(properties);
		}

		properties.setTraceID(mappingItem.getProperties().getTraceID());
		properties.setWhere(mappingItem.getProperties().getWhere());

		return properties;
	}

	/**
	 * Converts a given vector of legacy1 links to the latest link definition
	 * 
	 * @param legacyLinks
	 * @return
	 */
	private static Vector<Link> parseLinks(
			Vector<org.processmining.mapper.mapping.legacy1.Link> legacyLinks) {
		Vector<org.processmining.mapper.mapping.Link> links = new Vector<Link>();

		// loop and add
		for (org.processmining.mapper.mapping.legacy1.Link legacyLink : legacyLinks) {
			links.add(parseLink(legacyLink));
		}

		return links;
	}

	/**
	 * Converts a given legacy1 link to the latest link definition
	 * 
	 * @param legacyLink
	 * @return
	 */
	private static Link parseLink(
			org.processmining.mapper.mapping.legacy1.Link legacyLink) {
		Link link = new Link();

		link.setSpecification(legacyLink.getSpecification());
		// properties are connected in the properties parsing method

		return link;
	}
}
