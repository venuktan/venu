/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.ui.browsers.ExtensionSettingsBrowser;
import org.processmining.mapper.ui.browsers.GeneralSettingsBrowser;
import org.processmining.mapper.ui.browsers.ConnectionSettingsBrowser;

import com.fluxicon.slickerbox.components.IconVerticalTabbedPane;

/**
 * The settings browser class initializes three tabs that
 * display further GUI elements for defining the main settings
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class SettingsBrowser extends JPanel {

	private static final long serialVersionUID = -1467890361679445697L;
	private static final Color BG = new Color(180, 180, 180);
	private static final Color FG = new Color(60, 60, 60);

	private IconVerticalTabbedPane tabs;
	private final UIController controller;

	private GeneralSettingsBrowser generalSettingsBrowser;
	private ConnectionSettingsBrowser connectionSettingsBrowser;
	private ExtensionSettingsBrowser extensionSettingsBrowser;

	private final static String TAB1 = "General";
	private final static String TAB2 = "Connection";
	private final static String TAB3 = "Extensions";

	public SettingsBrowser(UIController controller) {
		this.controller = controller;
		setLayout(new BorderLayout());
		setOpaque(false);
		setupUI();
	}

	private void setupUI() {
		// setup the tabs
		generalSettingsBrowser = new GeneralSettingsBrowser(controller);
		connectionSettingsBrowser = new ConnectionSettingsBrowser(controller);
		extensionSettingsBrowser = new ExtensionSettingsBrowser(controller);

		tabs = new IconVerticalTabbedPane(FG, BG, 100);
		tabs.setPassiveBackground(new Color(140, 140, 140));
		tabs.setMouseOverFadeColor(new Color(90, 90, 90));

		tabs.addTab(TAB1, ImageLoader.load("tab_general_60x60_black.png"),
				generalSettingsBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						generalSettingsBrowser.updateData();
					}
				});

		tabs.addTab(TAB2, ImageLoader.load("tab_connection_60x60_black.png"),
				connectionSettingsBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						connectionSettingsBrowser.updateData();
					}
				});

		tabs.addTab(TAB3, ImageLoader.load("tab_extensions_60x60_black.png"),
				extensionSettingsBrowser, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						extensionSettingsBrowser.updateData();
					}
				});

		this.add(tabs, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		generalSettingsBrowser.updateData();
		connectionSettingsBrowser.updateData();
		extensionSettingsBrowser.updateData();
	}
}
