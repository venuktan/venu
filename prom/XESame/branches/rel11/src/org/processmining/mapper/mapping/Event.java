/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.mapping;

/**
 * The instance of an event in the mapping definition
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class Event extends GeneralMappingItem {
	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private String displayName;

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		if (displayName == null)
			displayName = "";
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * <!-- begin-UML-doc --> <!-- end-UML-doc -->
	 * 
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Trace trace;

	/**
	 * Returns the trace the event belongs to (NOTE: can return null but
	 * shouldn't)
	 * 
	 * @return the trace
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Trace getTrace() {
		return trace;
	}

	/**
	 * @param trace
	 *            the trace to set
	 * @generated 
	 *            "UML to Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setTrace(Trace trace) {
		this.trace = trace;
	}

	/**
	 * Returns the string 'Event: ' + displayname
	 */
	public String toString() {
		return "Event: " + displayName;
		// return displayName;
	}
}