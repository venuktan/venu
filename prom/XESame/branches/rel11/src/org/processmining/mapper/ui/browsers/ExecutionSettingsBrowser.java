/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.browsers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.deckfour.uitopia.ui.components.ImageLozengeButton;
import org.deckfour.uitopia.ui.util.ImageLoader;
import org.processmining.mapper.controller.ExecutionController;
import org.processmining.mapper.controller.XESameConstants;
import org.processmining.mapper.controller.XESameConstants.MESSAGELEVEL;
import org.processmining.mapper.ui.UIController;
import org.processmining.mapper.ui.models.JTextFieldLimit;

import com.jhlabs.awt.ParagraphLayout;

/**
 * This browser provides the execution settings tab
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
public class ExecutionSettingsBrowser extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2668672328472241725L;

	private final UIController uiController;

	private JPanel executionSettingsPanel;

	private JLabel zipCheckboxLabel;
	private JLabel eventLogLocationLabel;
	private JLabel mxmlCheckboxLabel;
	private JLabel nrTracesLabel;
	private JLabel cacheDBLocationLabel;

	private JButton executeButton;
	private JButton cacheDBLocationButton;
	private JButton eventLogLocationButton;

	private JTextField cacheDBLocationTextField;

	private JFileChooser cacheDBDirChooser;

	private JFileChooser eventlogFileChooser;

	private JTextField eventLogLocationTextField;

	private JTextField nrTracesTextField;

	private JCheckBox mxmlCheckBox;

	private JCheckBox zipCheckBox;

	private String defaultEventlogLocation = "."
			+ System.getProperty("file.separator") + "eventlog.xes";

	private boolean firstExecutionThisSession = true;

	public ExecutionSettingsBrowser(UIController controller) {
		this.uiController = controller;
		setupUI();
		updateData();
	}

	private void setupUI() {
		setOpaque(true);
		setBackground(new Color(180, 180, 180));
		setBorder(BorderFactory.createEmptyBorder());
		JPanel browser = uiController.setupBrowser();
		browser.add(
				uiController
						.setupTopPanel("Configure execution specific settings"),
				BorderLayout.NORTH);
		browser.add(getExecutionSettingsPanel(), BorderLayout.CENTER);
		setLayout(new BorderLayout());
		this.add(browser, BorderLayout.CENTER);
	}

	/**
	 * Updates the GUI children to update to the new data
	 */
	public void updateData() {
		cacheDBLocationTextField.setText(getCacheDBDirChooser()
				.getCurrentDirectory().toString());

		eventLogLocationTextField.setText(uiController.getConfiguration().get(
				UIController.SAVE_LOCATION_EVENTLOG, defaultEventlogLocation));
	}

	/**
	 * This method initializes executionSettingsPanel
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getExecutionSettingsPanel() {
		if (executionSettingsPanel == null) {
			executionSettingsPanel = new JPanel();
			UIController.makeup(executionSettingsPanel);

			// Labels! My labels! (from Finding Nemo, not LOTR...)
			zipCheckboxLabel = new JLabel("Zip outputted event log?");
			zipCheckboxLabel.setLabelFor(getZipCheckBox());
			UIController.makeup(zipCheckboxLabel);
			eventLogLocationLabel = new JLabel("Output event log location");
			eventLogLocationLabel.setLabelFor(getEventLogLocationTextField());
			UIController.makeup(eventLogLocationLabel);
			mxmlCheckboxLabel = new JLabel("Output to MXML instead of XES?");
			mxmlCheckboxLabel.setLabelFor(getMxmlCheckBox());
			UIController.makeup(mxmlCheckboxLabel);
			nrTracesLabel = new JLabel(
					"Number of traces to process (0 or negative for ALL)");
			nrTracesLabel.setLabelFor(getNrTracesTextField());
			UIController.makeup(nrTracesLabel);
			cacheDBLocationLabel = new JLabel(
					"Location of the cache DB (warning: can grow large!)");
			cacheDBLocationLabel.setLabelFor(getCacheDBLocationTextField());
			UIController.makeup(cacheDBLocationLabel);

			executionSettingsPanel.setLayout(new ParagraphLayout());
			executionSettingsPanel.add(getExecuteButton(),
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(nrTracesLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(getNrTracesTextField());
			executionSettingsPanel.add(mxmlCheckboxLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(getMxmlCheckBox());
			executionSettingsPanel.add(zipCheckboxLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(getZipCheckBox());
			executionSettingsPanel.add(cacheDBLocationLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(getCacheDBLocationTextField());
			executionSettingsPanel.add(getCacheDBLocationButton(),
					ParagraphLayout.NEW_LINE);
			executionSettingsPanel.add(eventLogLocationLabel,
					ParagraphLayout.NEW_PARAGRAPH);
			executionSettingsPanel.add(getEventLogLocationTextField());
			executionSettingsPanel.add(getEventLogLocationButton(),
					ParagraphLayout.NEW_LINE);

		}
		return executionSettingsPanel;
	}

	/**
	 * This method initializes executeButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getExecuteButton() {
		if (executeButton == null) {
			executeButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Execute Conversion");

			executeButton.setMaximumSize(new Dimension(100, 28));

			executeButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						// Broken and temporarily disabled
						/*-
						//First notify the user if he didn't set the event log location itself
						if(firstExecutionThisSession && (getEventLogLocationTextField().getText() == controller
								.getConfiguration().get(
										UIController.SAVE_LOCATION_EVENTLOG,
										defaultEventlogLocation)))
						{
							//Ask
							int confirm = JOptionPane.showConfirmDialog(getExecutionSettingsPanel(), 
									"This is the first time you run this mapping in this session. " +
									"However, we didn't detect that you changed the output event log " +
									"location and file name. Are you sure you want to continue?", 
									"Check your output event location!", JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
							//Act
							if(confirm == JOptionPane.CANCEL_OPTION)
							{
								return;
							}
						}
						
						//remember that we got here at least once
						firstExecutionThisSession = false;
						/**/

						// And start the mapping execution
						// We listen to the number of traces to process
						int nrTraces;
						try {
							nrTraces = Integer.parseInt(getNrTracesTextField()
									.getText());

							// If its a number and its 0 or smaller, we do it
							// all
							if (nrTraces <= 0) {
								nrTraces = -1;
							}
						} catch (NumberFormatException e1) {
							// If its not a number we do it all
							nrTraces = -1;
						}

						// listen to the mxml and zip checkboxes
						boolean mxml = getMxmlCheckBox().isSelected();
						boolean zip = getZipCheckBox().isSelected();

						/*
						 * Now we can start to run the 'stuff'
						 */

						// First start the mapping executor in a separate thread
						ExecutionController executor = new ExecutionController(
								uiController.getHandler(),
								getEventLogLocationTextField().getText(),
								nrTraces, mxml, zip);

						// Then we overlay the GUI with the progress console
						// and a cancellation button etc.
						ExecutionConsoleBrowser console = uiController
								.getMainView().getExecutionView()
								.getExecutionBrowser()
								.getExecutionConsoleBrowser();
						// the console should however have access to the thread
						// to be able to cancel it
						console.setExecutor(executor);

						uiController.getMainView().showOverlay(console);

						// Now run it
						Thread execThread = new Thread(executor);
						execThread.start();

					} catch (Exception ex) {
						JOptionPane.showMessageDialog(
								getExecutionSettingsPanel(),
								"Sorry, but the mapping could not be executed due to the following error: \n "
										+ ex.getLocalizedMessage(),
								"Mapping execution failed",
								JOptionPane.ERROR_MESSAGE);
						uiController
								.getHandler()
								.message(MESSAGELEVEL.PROGRESS,
										"We stopped execution because of a critical error.");
						ex.printStackTrace();
					}
				}
			});
		}
		return executeButton;
	}

	/**
	 * This method initializes cacheDBLocationTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getCacheDBLocationTextField() {
		if (cacheDBLocationTextField == null) {
			cacheDBLocationTextField = new JTextField();
			UIController.makeup(cacheDBLocationTextField);
			cacheDBLocationTextField.setPreferredSize(new Dimension(500, 28));
			cacheDBLocationTextField.setEditable(false);
			cacheDBLocationTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));
		}
		return cacheDBLocationTextField;
	}

	/**
	 * This method initializes cacheDBLocationButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getCacheDBLocationButton() {
		if (cacheDBLocationButton == null) {
			cacheDBLocationButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"), "Change CacheDB Location");
			cacheDBLocationButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// Always ask for a file location
							int returnVal = getCacheDBDirChooser()
									.showOpenDialog(getExecutionSettingsPanel());
							if (returnVal == JFileChooser.APPROVE_OPTION) {
								String location = getCacheDBDirChooser()
										.getSelectedFile().toString();

								uiController.getHandler().setCacheLocation(
										location);

								getCacheDBLocationTextField().setText(location);

								// and persist:
								uiController.getConfiguration().set(
										UIController.SAVE_LOCATION_CACHEDB,
										location);
							}
						}
					});
		}

		return cacheDBLocationButton;
	}

	/**
	 * This method initializes the file chooser for selecting the cache DB
	 * directory
	 * 
	 * @return
	 */
	private JFileChooser getCacheDBDirChooser() {
		if (cacheDBDirChooser == null) {
			cacheDBDirChooser = new JFileChooser();
			UIController.makeup(cacheDBDirChooser);
			// start in a subfolder of this project by default
			cacheDBDirChooser.setCurrentDirectory(new File(uiController
					.getConfiguration().get(UIController.SAVE_LOCATION_CACHEDB,
							"./cacheDB/")));

			// Only allow selection of directories
			cacheDBDirChooser
					.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		}

		return cacheDBDirChooser;
	}

	/**
	 * This method initializes eventLogLocationButton
	 * 
	 * @return javax.swing.JButton
	 */
	private JButton getEventLogLocationButton() {
		if (eventLogLocationButton == null) {
			eventLogLocationButton = new ImageLozengeButton(
					ImageLoader.load("empty.png"),
					"Change event log file location");

			eventLogLocationButton
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(java.awt.event.ActionEvent e) {
							// Always ask for a file location
							int returnVal = getEventLogLocationChooser()
									.showSaveDialog(getExecutionSettingsPanel());
							if (returnVal == JFileChooser.APPROVE_OPTION) {
								// Get the whole file including extension (if
								// not set manually)
								File file = getEventLogLocationChooser()
										.getSelectedFile();

								// Get the selected extension
								FileNameExtensionFilter filter = (FileNameExtensionFilter) getEventLogLocationChooser()
										.getFileFilter();
								String ext = filter.getExtensions()[0];

								// and add if necessary
								String eventLog = file.getAbsolutePath();
								if (!file.getName().endsWith(ext))
									eventLog += "." + ext;

								// Set the whole file including location AND
								// EXTENSION
								getEventLogLocationTextField()
										.setText(eventLog);

								// and persist:
								uiController.getConfiguration().set(
										UIController.SAVE_LOCATION_EVENTLOG,
										eventLog);
							}
						}
					});
		}
		return eventLogLocationButton;
	}

	/**
	 * This method initializes the file chooser for saving the event log
	 * 
	 * @return
	 */
	private JFileChooser getEventLogLocationChooser() {
		if (eventlogFileChooser == null) {
			eventlogFileChooser = new JFileChooser();
			UIController.makeup(eventlogFileChooser);
			// restore the last location
			/*-
			eventlogFileChooser.setSelectedFile(new File(controller
					.getConfiguration().get(
							UIController.SAVE_LOCATION_EVENTLOG,
							defaultEventlogLocation)));
							/**/
			// No, just set a fixed location, otherwise you overwrite
			eventlogFileChooser.setSelectedFile(new File(uiController
					.getConfiguration().get(
							UIController.SAVE_LOCATION_EVENTLOG,
							defaultEventlogLocation)));

			FileFilter XESFilter = new FileNameExtensionFilter("XES", "xes");
			FileFilter XESGzipFilter = new FileNameExtensionFilter("XES GZipped", "xes.gz");
			FileFilter MXMLFilter = new FileNameExtensionFilter("MXML", "mxml");
			FileFilter MXMLGzipFilter = new FileNameExtensionFilter("MXML GZipped", "mxml.gz");
			eventlogFileChooser.addChoosableFileFilter(MXMLFilter);
			eventlogFileChooser.addChoosableFileFilter(MXMLGzipFilter);
			eventlogFileChooser.addChoosableFileFilter(XESFilter);
			eventlogFileChooser.addChoosableFileFilter(XESGzipFilter);
		}

		return eventlogFileChooser;
	}

	/**
	 * This method initializes eventLogLocationTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getEventLogLocationTextField() {
		if (eventLogLocationTextField == null) {
			eventLogLocationTextField = new JTextField();
			UIController.makeup(eventLogLocationTextField);
			eventLogLocationTextField.setPreferredSize(new Dimension(400, 28));
			eventLogLocationTextField.setEditable(false);
			eventLogLocationTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));
		}
		return eventLogLocationTextField;
	}

	/**
	 * This method initializes nrTracesTextField
	 * 
	 * @return javax.swing.JTextField
	 */
	private JTextField getNrTracesTextField() {
		if (nrTracesTextField == null) {
			nrTracesTextField = new JTextField();
			UIController.makeup(nrTracesTextField);
			nrTracesTextField.setPreferredSize(new Dimension(40, 28));
			nrTracesTextField.setText("-1");
			nrTracesTextField.setDocument(new JTextFieldLimit(
					XESameConstants.uiTextFieldMaxLength));
		}
		return nrTracesTextField;
	}

	/**
	 * This method initializes mxmlCheckBox
	 * 
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getMxmlCheckBox() {
		if (mxmlCheckBox == null) {
			mxmlCheckBox = new JCheckBox();
			UIController.makeup(mxmlCheckBox);
			mxmlCheckBox.setSelected(false);
		}
		return mxmlCheckBox;
	}

	/**
	 * This method initializes zipCheckBox
	 * 
	 * @return javax.swing.JCheckBox
	 */
	private JCheckBox getZipCheckBox() {
		if (zipCheckBox == null) {
			zipCheckBox = new JCheckBox();
			UIController.makeup(zipCheckBox);
			zipCheckBox.setSelected(true);
		}
		return zipCheckBox;
	}

}
