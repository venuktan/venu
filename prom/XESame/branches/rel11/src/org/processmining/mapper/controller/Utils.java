/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.controller;

/**
 * This class provides some general (static) methods for our convenience
 * 
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * @version 1.1BETA
 *
 */
public final class Utils {

	/**
	 * Tests whether the given string is empty (null, 0 char or only spaces)
	 * 
	 * @param string
	 * @return
	 */
	public static boolean isEmptyString(String string) {
		return string == null || string.trim().isEmpty();
	}

	/**
	 * Searches and replaces all occurences of 'search' to 'replace' in 'text'
	 * @param text String to search in
	 * @param search String to search for
	 * @param replace String to replace with
	 * @return Changed string
	 */
	public static StringBuilder replaceAll(StringBuilder text, String search, String replace)
	{
		int searchFromIndex = 0;
		int searchStringLength = search.length();
		int replaceStringLength = replace.length();
		
		//While we can find this string in the rest of the stringBuilder
		while(text.indexOf(search, searchFromIndex) >= 0)
		{
			int startIndex = text.indexOf(search, searchFromIndex);
			
			//replace it
			text.replace(startIndex, startIndex+searchStringLength, replace);
			
			//And update the index to start searching from
			searchFromIndex = startIndex+replaceStringLength;
		}
		
		return text;
	}

}
