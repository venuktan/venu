/*
 * XES Mapper Application (XESame)
 * Generic data conversion tool for extracting event logs
 * 
 * LICENSE: 
 * This software is licensed under the EPL v1.0 license.
 * The license should be provided with this application. 
 * If the license was not provided with the application 
 *   it can be retrieved from 
 *   http://www.eclipse.org/legal/epl-v10.html 
 */

package org.processmining.mapper.ui.models;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.processmining.mapper.mapping.*;

/**
 * This class wraps the attributes class and makes it displayable in a JTree
 * 
 * @version 1.1BETA
 * @author Joos Buijs (j.c.a.m.buijs@tue.nl)
 * 
 */
@SuppressWarnings("serial")
public class AttributesTableModel extends AbstractTableModel {
	private String[] columnNames = new String[] { "Has children?", "Key",
			"Value", "Type", "Extension" };
	// hard coded column names, nice huh :D

	// We store references to the original attributes and the complete vector of
	// attributes
	private Vector<Attribute> originalAttributes = new Vector<Attribute>();

	/**
	 * Initialize an empty table that is still able to display
	 */
	public AttributesTableModel() {
	}

	/**
	 * Extracts all the attributes we want to show and store them internally
	 * 
	 * @param attributes
	 */
	public AttributesTableModel(Vector<Attribute> attributes) {
		originalAttributes = attributes; // store for later >reference<
		fireTableDataChanged();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return originalAttributes.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		Attribute attribute = originalAttributes.get(row);

		switch (col) {
		case 0:
			return attribute.isMeta();
		case 1:
			if (attribute.getKey() != null)
				return attribute.getKey();
		case 2:
			if (attribute.getValue() != null)
				return attribute.getValue();
		case 3:
			if (attribute.getType() != null)
				return attribute.getType();
		case 4:
			if (attribute.getExtension() != null)
				return attribute.getExtension().getName();
		default:
			return "";
		}
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Class<?> getColumnClass(int col) {
		return getValueAt(0, col).getClass();
	}

	/**
	 * Returns the attribute at the given row
	 * 
	 * @param row
	 * @return
	 */
	public Attribute getAttributeAt(int row) {
		return originalAttributes.get(row);
	}

	// TODO move this detection to the attribute class and enforce it?
	public boolean isCellEditable(int row, int col) {
		Boolean hasExtension = (originalAttributes.get(row).getExtension() != null);

		switch (col) {
		case 0:
			return false; // isMeta should never be editable
		case 1:
			if (hasExtension) // key only when no extension is set
				return false;
			else
				return true;
		case 2:
			return true; // Value always
		case 3:
			if (hasExtension)// type only when no extension is set
				return false;
			else
				return true;
		case 4:
			return false;// extension never!

		default:
			return false; // when in doubt: restrict!
		}
	}

	/**
	 * We can edit the data in the table so we should update the domain model
	 */
	public void setValueAt(Object value, int row, int col) {
		Attribute attribute = originalAttributes.get(row);

		switch (col) {
		case 0:
			//TODO ENH make editable ((un)check == add/delete sub att)
			break; // not editable!
		case 1:
			attribute.setKey(value.toString());
			break;
		case 2:
			attribute.setValue(value.toString());
			break;
		case 3:
			attribute.setType(value.toString());

			// The attribute's value might have been updated so fire!
			fireTableCellUpdated(row, col - 1);
			break;
		default:
			break;
		}

		fireTableCellUpdated(row, col);
	}

	/**
	 * Resets the internal attributes reference to the given vector. Listeners
	 * are not notified!
	 * 
	 * @param attributes
	 */
	public void reloadAttributes(Vector<Attribute> attributes) {
		if (originalAttributes == null)
			originalAttributes = new Vector<Attribute>();
		else
			originalAttributes = attributes;
	}

	public void fireTableRowsInserted(int from, int to) {
		fireTableDataChanged(); // refresh our internal nonMeta vector

		// Extra check, if there are 0 attributes to begin with, one of the two
		// is invalid and the table will not be updated so fix that
		if (from < 0)
			from = 0;
		if (to < 0)
			to = 0;

		super.fireTableRowsInserted(from, to); // now delegate the call
	}
}
