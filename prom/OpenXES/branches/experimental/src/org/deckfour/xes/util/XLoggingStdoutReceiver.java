/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2009 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.util;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.deckfour.xes.util.XLogging.Importance;

/**
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XLoggingStdoutReceiver implements XLoggingReceiver {
	
	private class XLoggingMessage {
		private String message;
		private XLogging.Importance importance;
		private XLoggingMessage(String message, XLogging.Importance importance) {
			this.message = message;
			this.importance = importance;
		}
		public String toString() {
			return "[" + importance.name() + "] " + message;
		}
	}
	
	private class XLoggingMonitor implements Runnable {

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			StringBuilder sb = new StringBuilder();
			while(true) {
				if(messageQueue.isEmpty() == false) {
					while(messageQueue.isEmpty() == false) {
						sb.append(messageQueue.poll().toString());
						sb.append('\n');
					}
					System.out.print(sb.toString());
					sb = new StringBuilder();
				}
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// fuckup
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private XLogging.Importance threshold = XLogging.Importance.DEBUG;
	private ConcurrentLinkedQueue<XLoggingMessage> messageQueue;
	
	public XLoggingStdoutReceiver() {
		this(XLogging.Importance.DEBUG);
	}
	
	public XLoggingStdoutReceiver(XLogging.Importance threshold) {
		this.threshold = threshold;
		this.messageQueue = new ConcurrentLinkedQueue<XLoggingMessage>();
		Thread outThread = new Thread(new XLoggingMonitor());
		outThread.start();
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.util.XLoggingReceiver#receive(java.lang.String, org.deckfour.xes.util.XLogging.Importance)
	 */
	public void receive(String message, Importance importance) {
		if(importance.compareTo(threshold) >= 0) {
			this.messageQueue.offer(new XLoggingMessage(message, importance));
		}
	}

}
