/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.classification;

import java.util.HashSet;
import java.util.Set;

import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.util.XAttributeUtils;

/**
 * Event classifier which considers two events as equal,
 * if, for a given (configurable) attribute, they have
 * either the same value, both have no value set.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XEventAttributeClassifier implements XEventClassifier {
	
	/**
	 * Key of the attribute used for event comparison.
	 */
	protected XAttribute attribute;
	
	/**
	 * Name of the classifier
	 */
	protected String name;
	
	/**
	 * Creates a new instance, configured by the given attribute.
	 * 
	 * @param attribute Attribute to be used for event classification.
	 */
	public XEventAttributeClassifier(XAttribute attribute) {
		this.attribute = XAttributeUtils.derivePrototype(attribute);
		this.name = attribute.getKey();
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.classification.XEventClassifier#getClassIdentity(org.deckfour.xes.model.XEvent)
	 */
	public String getClassIdentity(XEvent event) {
		XAttribute anAttribute = event.getAttributes().get(attribute.getKey());
		if(anAttribute == null) {
			return "NIL";
		}
		String value = anAttribute.toString().trim();
		if(value == null || value.length() == 0) {
			value = "NIL";
		}
		return value;
	}
	
	/**
	 * Assigns a custom name to this classifier
	 * 
	 * @param name Name to be assigned to this classifier.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.classification.XEventClassifier#name()
	 */
	public String name() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.classification.XEventClassifier#sameEventClass(org.deckfour.xes.model.XEvent, org.deckfour.xes.model.XEvent)
	 */
	public boolean sameEventClass(XEvent eventA, XEvent eventB) {
		return getClassIdentity(eventA).equals(getClassIdentity(eventB));
	}

	public String toString() {
		return name();
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.classification.XEventClassifier#getDefiningAttributes()
	 */
	public Set<XAttribute> getDefiningAttributes() {
		HashSet<XAttribute> result = new HashSet<XAttribute>(1);
		result.add((XAttribute)attribute.clone());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(XEventClassifier o) {
		if(o instanceof XEventAttributeClassifier) {
			return attribute.compareTo(((XEventAttributeClassifier)o).attribute);
		} else {
			return -1;
		}
	}
	
}
