/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.out;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;

import org.deckfour.spex.SXDocument;
import org.deckfour.spex.SXTag;
import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.util.XAttributeUtils;
import org.deckfour.xes.util.XLogging;
import org.deckfour.xes.util.XRuntimeUtils;

/**
 * XES.XML serialization for the XES format.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XesXmlSerializer implements XesSerializer {

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getDescription()
	 */
	public String getDescription() {
		return "XES.XML Serialization";
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getName()
	 */
	public String getName() {
		return "XES.XML";
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getSuffices()
	 */
	public String[] getSuffices() {
		return new String[] {"xes", "xesxml"};
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#serialize(org.deckfour.xes.model.XLog, java.io.OutputStream)
	 */
	public void serialize(XLog log, OutputStream out) throws IOException {
		XLogging.log("start serializing log " + log.getId() + " to XES.XML", XLogging.Importance.DEBUG);
		long start = System.currentTimeMillis();
		SXDocument doc = new SXDocument(out);
		doc.addComment("This file has been generated with the OpenXES library. It conforms");
		doc.addComment("to the XML serialization of the XES standard for log storage and");
		doc.addComment("management.");
		doc.addComment("XES standard version: " + XRuntimeUtils.XES_VERSION);
		doc.addComment("OpenXES library version: " + XRuntimeUtils.OPENXES_VERSION);
		doc.addComment("OpenXES is available from http://code.deckfour.org/xes/");
		SXTag logTag = doc.addNode("log");
		logTag.addAttribute("xes.version", XRuntimeUtils.XES_VERSION);
		logTag.addAttribute("openxes.version", XRuntimeUtils.OPENXES_VERSION);
		logTag.addAttribute("xmlns", "http://code.deckfour.org/xes");
		for(XExtension extension : log.getExtensions()) {
			String key = "xmlns:" + extension.getPrefix();
			String value = extension.getUri().toString();
			logTag.addAttribute(key, value);
		}
		logTag.addAttribute("id", log.getId().toString());
		addAttributes(logTag, log.getAttributes().values());
		for(XTrace trace : log) {
			SXTag traceTag = logTag.addChildNode("trace");
			traceTag.addAttribute("id", trace.getId().toString());
			addAttributes(traceTag, trace.getAttributes().values());
			for(XEvent event : trace) {
				SXTag eventTag = traceTag.addChildNode("event");
				eventTag.addAttribute("id", event.getId().toString());
				addAttributes(eventTag, event.getAttributes().values());
			}
		}
		//
		doc.close();
		String duration = " (" + (System.currentTimeMillis() - start) + " msec.)";
		XLogging.log("finished serializing log " + log.getId() + duration, XLogging.Importance.DEBUG);
	}
	
	/**
	 * Helper method, adds the given collection of attributes to the
	 * given Tag.
	 * 
	 * @param tag Tag to add attributes to.
	 * @param attributes The attributes to add.
	 */
	protected void addAttributes(SXTag tag, Collection<XAttribute> attributes) throws IOException {
		for(XAttribute attribute : attributes) {
			SXTag attributeTag = null;
			XExtension ext = attribute.getExtension();
			if(ext != null) {
				String tagName = attribute.getKey();
				attributeTag = tag.addChildNode(tagName);
			} else {
				attributeTag = tag.addChildNode("attribute");
				attributeTag.addAttribute("key", attribute.getKey());
			}
			attributeTag.addAttribute("type", XAttributeUtils.getTypeString(attribute));
			attributeTag.addAttribute("value", attribute.toString());
			addAttributes(attributeTag, attribute.getAttributes().values());
		}
	}

}
