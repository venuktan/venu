/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2009 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.storage.softstrings;

import java.util.ArrayList;
import java.util.HashMap;

import org.deckfour.xes.util.XLogging;

/**
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XSoftStringMap {
	
	private static XSoftStringMap instance = new XSoftStringMap();
	
	public static XSoftStringMap instance() {
		return instance;
	}
	
	private HashMap<String,Integer> mapStringToInteger;
	private ArrayList<String> stringList;

	/**
	 * 
	 */
	public XSoftStringMap() {
		mapStringToInteger = new HashMap<String,Integer>(65536);
		stringList = new ArrayList<String>();
	}

	public synchronized int encode(String string) {
		Integer code = mapStringToInteger.get(string);
		if(code == null) {
			code = stringList.size();
			mapStringToInteger.put(string, code);
			stringList.add(string);
			if(code % 100 == 0) {
				XLogging.log("SoftString directory size is " + code, XLogging.Importance.DEBUG);
			}
		}
		return code;
	}

	public synchronized String decode(int code) {
		return stringList.get(code);
	}

	public int size() {
		return stringList.size();
	}

}