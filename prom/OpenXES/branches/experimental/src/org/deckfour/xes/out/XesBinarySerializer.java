/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2009 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.out;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.model.XAttribute;
import org.deckfour.xes.model.XAttributeBoolean;
import org.deckfour.xes.model.XAttributeContinuous;
import org.deckfour.xes.model.XAttributeDiscrete;
import org.deckfour.xes.model.XAttributeDuration;
import org.deckfour.xes.model.XAttributeLiteral;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.util.XLogging;
import org.deckfour.xes.util.XTimer;

/**
 * Binary, compressed serialization for the XES format.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XesBinarySerializer implements XesSerializer {
	
	public static final int XESBIN_MAGIC_NUMBER = 80823;
	
	private int stringIndex = 1;
	private int extensionIndex = 1;
	private HashMap<String,Integer> stringMap = new HashMap<String,Integer>();
	private HashMap<XExtension,Integer> extensionMap = new HashMap<XExtension,Integer>();

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getDescription()
	 */
	public String getDescription() {
		return "Binary XES Serializer";
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getName()
	 */
	public String getName() {
		return "XES.BIN";
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#getSuffices()
	 */
	public String[] getSuffices() {
		return new String[] {"xeb", "xesbin"};
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.out.XesSerializer#serialize(org.deckfour.xes.model.XLog, java.io.OutputStream)
	 */
	public void serialize(XLog log, OutputStream out) throws IOException {
		XLogging.log("start serializing log " + log.getId() + " to XES.BIN", XLogging.Importance.DEBUG);
		XTimer timer = new XTimer();
		timer.start();
		DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(out)));
		writeHeader(log, dos);
		XID.write(log.getId(), dos);
		writeAttributeMap(log.getAttributes(), dos);
		dos.writeInt(log.size());
		for(XTrace trace : log) {
			serialize(trace, dos);
		}
		timer.stop();
		XLogging.log("finished serializing log " + log.getId() + "(" + timer.getDurationString() 
				+ ", " + stringMap.size() + " symbols", XLogging.Importance.DEBUG);
		dos.flush();
		dos.close();
	}
	
	private void serialize(XTrace trace, DataOutputStream dos) throws IOException {
		XID.write(trace.getId(), dos);
		writeAttributeMap(trace.getAttributes(), dos);
		dos.writeInt(trace.size());
		for(XEvent event : trace) {
			serialize(event, dos);
		}
	}
	
	private void serialize(XEvent event, DataOutputStream dos) throws IOException {
		XID.write(event.getId(), dos);
		writeAttributeMap(event.getAttributes(), dos);
	}
	
	private void writeHeader(XLog log, DataOutputStream dos) throws IOException {
		dos.writeInt(XESBIN_MAGIC_NUMBER);
		Set<XExtension> extensions = log.getExtensions();
		dos.writeInt(extensions.size());
		for(XExtension extension : extensions) {
			writeExtension(extension, dos);
		}
	}
	
	private void writeExtension(XExtension extension, DataOutputStream dos) throws IOException {
		if(extension == null) {
			dos.writeByte(0);
		} else {
			Integer index = extensionMap.get(extension);
			if(index != null) {
				dos.writeByte(index);
			} else {
				dos.writeByte(-1);
				extensionMap.put(extension, extensionIndex);
				extensionIndex++;
				dos.writeUTF(extension.getUri().toString());
			}
		}
	}
	
	private void writeAttributeMap(XAttributeMap attributeMap, DataOutputStream dos) throws IOException {
		dos.writeInt(attributeMap.size());
		for(String key : attributeMap.keySet()) {
			XAttribute attribute = attributeMap.get(key);
			// 1) write attribute key
			writeString(attribute.getKey(), dos);
			// 2) write attribute extension
			writeExtension(attribute.getExtension(), dos);
			// 3) write attribute type, and 4) value
			if(attribute instanceof XAttributeBoolean) {
				dos.writeByte(0);
				dos.writeBoolean(((XAttributeBoolean)attribute).getValue());
			} else if(attribute instanceof XAttributeContinuous) {
				dos.writeByte(1);
				dos.writeDouble(((XAttributeContinuous)attribute).getValue());
			} else if(attribute instanceof XAttributeDiscrete) {
				dos.writeByte(2);
				dos.writeLong(((XAttributeDiscrete)attribute).getValue());
			} else if(attribute instanceof XAttributeDuration) {
				dos.writeByte(3);
				dos.writeLong(((XAttributeDuration)attribute).getValue());
			} else if(attribute instanceof XAttributeLiteral) {
				dos.writeByte(4);
				dos.writeUTF(((XAttributeLiteral)attribute).getValue());
			} else if(attribute instanceof XAttributeTimestamp) {
				dos.writeByte(5);
				dos.writeLong(((XAttributeTimestamp)attribute).getValueMillis());
			} else {
				throw new AssertionError("Unknown attribute type, cannot serialize!");
			}
			// 5) recurse
			writeAttributeMap(attribute.getAttributes(), dos);
		}
	}
	
	private void writeString(String string, DataOutputStream dos) throws IOException {
		Integer key = stringMap.get(string);
		if(key == null) {
			stringMap.put(string, stringIndex);
			writeEfficientInt(dos, 0, stringIndex);
			stringIndex++;
			dos.writeUTF(string);
		} else {
			writeEfficientInt(dos, key, stringIndex);
		}
	}
	
	private void writeEfficientInt(DataOutputStream dos, int value, int maxValue) throws IOException {
		if(maxValue <= Byte.MAX_VALUE) {
			dos.writeByte(value);
		} else if(maxValue <= Short.MAX_VALUE) {
			dos.writeShort(value);
		} else {
			dos.writeInt(value);
		}
	}
	
	

}
