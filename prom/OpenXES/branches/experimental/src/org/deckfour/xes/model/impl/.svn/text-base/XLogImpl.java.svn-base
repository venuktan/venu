/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
package org.deckfour.xes.model.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.deckfour.xes.extension.XExtension;
import org.deckfour.xes.id.XID;
import org.deckfour.xes.id.XIDFactory;
import org.deckfour.xes.model.XAttributeMap;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.deckfour.xes.storage.XAttributeMapBufferedImpl;

/**
 * Implementation for the XLog interface.
 * 
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class XLogImpl extends ArrayList<XTrace> implements XLog {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -9192919845877466525L;
	
	/**
	 * The unique ID of this log.
	 */
	private XID id;
	/**
	 * Map of attributes for this log.
	 */
	private XAttributeMap attributes;
	/**
	 * Extensions
	 */
	private Set<XExtension> extensions;
	
	/**
	 * Creates a new log.
	 */
	public XLogImpl() {
		this(XIDFactory.instance().createId());
	}
	
	/**
	 * Creates a new log.
	 * 
	 * @param id Unique ID of the log.
	 */
	public XLogImpl(XID id) {
		this.id = id;
		this.attributes = new XAttributeMapLazyImpl<XAttributeMapBufferedImpl>(XAttributeMapBufferedImpl.class);
		this.extensions = new HashSet<XExtension>();
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.model.XElement#getId()
	 */
	public XID getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.model.XElement#setId()
	 */
	public void setId(XID id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.model.XAttributable#getAttributes()
	 */
	public XAttributeMap getAttributes() {
		return attributes;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.model.XAttributable#setAttributes(java.util.Map)
	 */
	public void setAttributes(XAttributeMap attributes) {
		this.attributes = attributes;
	}

	/* (non-Javadoc)
	 * @see org.deckfour.xes.model.XAttributable#getExtensions()
	 */
	public Set<XExtension> getExtensions() {
		return extensions;
	}

	/* (non-Javadoc)
	 * @see java.util.ArrayList#clone()
	 */
	public Object clone() {
		XLogImpl clone = (XLogImpl) super.clone();
		clone.id = (XID)id.clone();
		clone.attributes = (XAttributeMap)attributes.clone();
		clone.clear();
		for(XTrace trace : this) {
			clone.add((XTrace)trace.clone());
		}
		return clone;
	}
	
	/**
	 * Tests for equality.
	 */
	public boolean equals(Object obj) {
		if(obj instanceof XLog) {
			return id.equals(((XLog)obj).getId());
		} else {
			return false;
		}
	}
	
	/**
	 * Returns a hash code.
	 */
	public int hashCode() {
		return id.hashCode();
	}
	
	/**
	 * Returns a string representation.
	 */
	public String toString() {
		return id.toString();
	}

}
