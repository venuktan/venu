/*
 * OpenXES
 * 
 * The reference implementation of the XES meta-model for event 
 * log data management.
 * 
 * Copyright (c) 2008 Christian W. Guenther (christian@deckfour.org)
 * 
 * 
 * LICENSE:
 * 
 * This code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 * 
 * EXEMPTION:
 * 
 * The use of this software can also be conditionally licensed for
 * other programs, which do not satisfy the specified conditions. This
 * requires an exemption from the general license, which may be
 * granted on a per-case basis.
 * 
 * If you want to license the use of this software with a program
 * incompatible with the LGPL, please contact the author for an
 * exemption at the following email address: 
 * christian@deckfour.org
 * 
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.xml.parsers.ParserConfigurationException;

import org.deckfour.xes.in.XMxmlParser;
import org.deckfour.xes.in.XesBinaryParser;
import org.deckfour.xes.in.XesXmlParser;
import org.deckfour.xes.info.XLogInfo;
import org.deckfour.xes.info.impl.XLogInfoImpl;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.out.XMxmlSerializer;
import org.deckfour.xes.out.XesBinarySerializer;
import org.deckfour.xes.out.XesXmlGzipSerializer;
import org.deckfour.xes.out.XesXmlSerializer;
import org.deckfour.xes.util.XTimer;
import org.xml.sax.SAXException;


/**
 * @author Christian W. Guenther (christian@deckfour.org)
 *
 */
public class ConversionTest {

	/**
	 * @param args
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		String formats[] = {"XES.XML", "MXML", "XES.BIN", "XES.XML GZ"};
		// ask user for source file
		String inFormat = (String)JOptionPane.showInputDialog(null, "Pick input format", 
				"Input format", JOptionPane.QUESTION_MESSAGE, null, 
				formats, formats[0]);
		JFileChooser openChooser = new JFileChooser();
		openChooser.setDialogType(JFileChooser.OPEN_DIALOG);
		openChooser.showOpenDialog(null);
		File source = openChooser.getSelectedFile();
		// load source file
		XLog log = null;
		XTimer timer = new XTimer();
		if(inFormat.equals(formats[0])) {
			log = (new XesXmlParser()).parse(source);
		} else if(inFormat.equals(formats[1])) {
			log = (new XMxmlParser()).parse(source).iterator().next();
		} else if(inFormat.equals(formats[2])) {
			log = (new XesBinaryParser()).parse(source);
		} else if(inFormat.equals(formats[3])) {
			log = (new XesXmlParser()).parse(source);
		}
		System.out.println("Parsing / building performance: " + timer.getDurationString());
		timer.start();
		XLogInfo info = XLogInfoImpl.create(log);
		System.out.println("Reading performance: " + timer.getDurationString());
		// ask user for target file
		String outFormat = (String)JOptionPane.showInputDialog(null, "Pick output format", 
				"Output format", JOptionPane.QUESTION_MESSAGE, null, 
				formats, formats[0]);
		JFileChooser saveChooser = new JFileChooser();
		saveChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		saveChooser.showSaveDialog(null);
		File target = saveChooser.getSelectedFile();
		//OutputStream os = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(target)));
		OutputStream os = new BufferedOutputStream(new FileOutputStream(target));
		timer.start();
		if(outFormat.equals(formats[0])) {
			(new XesXmlSerializer()).serialize(log, os);
		} else if(outFormat.equals(formats[1])){
			(new XMxmlSerializer()).serialize(log, os);
		} else if(outFormat.equals(formats[2])) {
			(new XesBinarySerializer()).serialize(log, os);
		} if(outFormat.equals(formats[3])) {
			(new XesXmlGzipSerializer()).serialize(log, os);
		}
		os.flush();
		os.close();
		System.out.println("Writing performance: " + timer.getDurationString());
	}

}
