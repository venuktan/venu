	A
       / \
      B   C
     /   / \
    D   E   F
           / \
          G   H

The post-order traversal above (Left, Right, Root): D B E G H F C A 
Algorithm: 

RE_postorder(node) //recursion
{
 if (node == NULL)
 {
  return node;
 }
RE_postorder(node.left)
RE_postorder(node.right)
visit(node);
}

////////////////////////Iterative////////////////////////
// using 2 stacks to reverser the order of left and right childs
It_postorder(Binary Tree * root)
{
  if (!root ) 
    return;
  Stack <Binary Tree *> temp, last;
  temp.push(root);
  while(!temp.empty())
  {
    Binary Tree *currrent = temp.top();
    last.push(current) ; 
    temp.pop();
    if (current -> left) 
      temp.push(current -> left);
      temp.push(current -> right);
  }//end of while 1
  while(! last.empty())
  {
    printf("%d", last.top());
    last.pop();
  }
}
