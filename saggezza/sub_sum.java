//Venu K Tangirala
// java code for finding the numbers in an array 
// that add up to the number X.
// to run this $ javac sub_sum.java
// $ java sub_sum 101 
// 101 can be any number
import java.util.HashMap;

public class sub_sum
{
    public static void hash(int []arr,int sum)
    {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < arr.length; ++i) 
            map.put(arr[i], sum-arr[i]); // key= arr[i] ; val= difference

        for (int i = 0; i < arr.length; ++i) 
            if(map.containsValue(arr[i]) && map.get(arr[i])!=null)
             {
                System.out.println("{"+arr[i]+","+map.get(arr[i])+"}");
                map.remove(arr[i]);
             }
    }

public static void main(String[] args)
{
    Integer sum = Integer.parseInt(args[0]);
    int []arr={1, 10, 6, 4, 2, 130, 100, 9, 3, 23, 45, 8, 7, 8, 3, 2};
    hash(arr,sum);
}
}
